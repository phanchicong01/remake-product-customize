import React from 'react'
import { MenuItem } from './MenuItem'
import { MenuInnerWithSub } from './MenuInnerWithSub'

export function MenuInner() {
  return (
    <>
      <MenuItem title={"Artworks"} to='/artworks' />

      <MenuInnerWithSub
        title='Assets'
        to='/assets'
        menuPlacement='bottom-start'
        menuTrigger='click'
      >
        {/* PAGES */}
        <MenuItem to='/assets/clipart-categories' title='Clipart Categories' hasBullet={true} />
        <MenuItem to='/assets/additional-option' title='Additional Options' hasBullet={true} />
        <MenuItem to='/assets/fonts' title='Fonts' hasBullet={true} />
      </MenuInnerWithSub>
      <MenuInnerWithSub
        title='Admin'
        to='/admin'
        menuPlacement='bottom-start'
        menuTrigger='click'
      >
        {/* PAGES */}
        <MenuItem to='/admin/users' title='User List' hasBullet={true} />
      </MenuInnerWithSub>
    </>
  )
}
