import clsx from 'clsx'
import React, { FC } from 'react'
import { toAbsoluteUrl } from 'helpers'
import { HeaderUserMenu } from 'partials'
import { useAuth } from 'modules/auth'

const toolbarButtonMarginClass = 'ms-1 ms-lg-3',
  toolbarUserAvatarHeightClass = 'symbol-30px symbol-md-40px'

const Topbar: FC = () => {
  const { currentUser } = useAuth()
  return (
    <div className='d-flex align-items-stretch flex-shrink-0'>
      {/* begin::User */}
      <div
        className={clsx('d-flex align-items-center', toolbarButtonMarginClass)}
        id='kt_header_user_menu_toggle'
      >
        {/* begin::Toggle */}
        <div
          className={clsx('cursor-pointer symbol', toolbarUserAvatarHeightClass)}
          data-kt-menu-trigger='click'
          data-kt-menu-attach='parent'
          data-kt-menu-placement='bottom-end'
          data-kt-menu-flip='bottom'
        >
          <img src={currentUser?.avatar ? currentUser.avatar : toAbsoluteUrl('/media/avatars/300-1.jpg')} alt={currentUser?.name} />
        </div>
        <HeaderUserMenu />
        {/* end::Toggle */}
      </div>
      {/* end::User */}



    </div>
  )
}

export { Topbar }
