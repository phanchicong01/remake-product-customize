import clsx from 'clsx'
import { checkIsActive } from 'helpers'
import React from 'react'
import { useLocation } from 'react-router'

type Props = {
  to: string
  title: string
  icon?: string
  hasBullet?: boolean
}

const AsideMenuItemWithSub: React.FC<Props> = ({
  children,
  to,
  title,
  icon,
  hasBullet,
}) => {
  const { pathname } = useLocation()
  const isActive = checkIsActive(pathname, to)


  return (
    <div
      className={clsx('menu-item', { 'here show': isActive }, 'menu-accordion')}
      data-kt-menu-trigger='click'
    >
      <span className='menu-link'>
        {hasBullet && !icon ? (
          <span className='menu-bullet'>
            <span className='bullet bullet-dot'></span>
          </span>
        ) : ''}
        {icon && !hasBullet ? <i className={icon}></i> : ''}
        <span className='menu-title'>{title}</span>
        <span className='menu-arrow'></span>
      </span>
      <div className={clsx('menu-sub menu-sub-accordion', { 'menu-active-bg': isActive })}>
        {children}
      </div>
    </div>
  )
}

export { AsideMenuItemWithSub }
