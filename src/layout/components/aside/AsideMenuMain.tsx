/* eslint-disable react/jsx-no-target-blank */
import React from 'react'
import { KTSVG } from 'helpers'
import { AsideMenuItemWithSub } from './AsideMenuItemWithSub'
import { AsideMenuItem } from './AsideMenuItem'
import { RouteItem } from 'apis/get-routes/interface'

type Props = {
  routes: Array<RouteItem>
}

const renderRoutes = (routes: Array<RouteItem>) => {
  if (routes && routes.length > 0) {

    return routes.map((route, index) => {
      if (route.children && route.children.length > 0) {
        return <AsideMenuItemWithSub
          to={route.route}
          title={route.label}
          icon={route.icon}
          key={route.id}
          hasBullet={!route.icon}
        >
          {renderRoutes(route.children)}

        </AsideMenuItemWithSub>
      } else {
        return <AsideMenuItem
          to={route.route}
          title={route.label}
          icon={route.icon}
          key={route.id}
          hasBullet={!route.icon}
        />
      }
    })
  }
}
export function AsideMenuMain({ routes }: Props) {

  return (
    <>
      {renderRoutes(routes)}
      <div className='menu-item'>
        <div className='menu-content'>
          <div className='separator mx-1 my-4'></div>
        </div>
      </div>
      <div className='menu-item'>
        <a
          target='_blank'
          className='menu-link'
          href={process.env.REACT_APP_PREVIEW_DOCS_URL + '/docs/changelog'}
        >
          <span className='menu-icon'>
            <KTSVG path='/media/icons/duotune/general/gen005.svg' className='svg-icon-2' />
          </span>
          <span className='menu-title'>Changelog {process.env.REACT_APP_VERSION}</span>
        </a>
      </div>
    </>
  )
}
