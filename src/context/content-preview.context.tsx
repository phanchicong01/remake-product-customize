import React, {
    createContext,
    memo,
    useCallback,
    useMemo,
    useState,
} from 'react'
export interface DataSelectedState {
    layerId: string,
    dataSelectOption: any
}
interface ContentPreviewContextState {
    optionSelected: DataSelectedState[],
    onUpdateState: (data: DataSelectedState, layerId: string) => void
    removeOptionSelected: (layerId: string) => void
}
interface ContentPreviewProps {
    children: React.ReactNode
}
export const ContentPreviewContext = createContext<ContentPreviewContextState>({
    optionSelected: [],
    onUpdateState: (data: DataSelectedState, layerId: string) => { },
    removeOptionSelected: (layerId: string) => { },
})

export const ContentPreviewProvider = memo(({ children }: ContentPreviewProps) => {
    const [optionSelected, setOptionSelected] = useState<DataSelectedState[]>([])


    const onUpdateState = useCallback((data: DataSelectedState, layerId: string) => {
        const newTemplateOption = [...optionSelected];
        const index = newTemplateOption.findIndex((item: DataSelectedState) => item.layerId === layerId);
        if (index === -1) {
            newTemplateOption.push(data)
            setOptionSelected([...newTemplateOption])
        } else {
            newTemplateOption[index] = { ...newTemplateOption[index], ...data }
            setOptionSelected([...newTemplateOption])
        }
    }, [optionSelected]);

    const removeOptionSelected = useCallback((layerId: string) => {
        const newTemplateOption = [...optionSelected];
        const index = newTemplateOption.findIndex((item: DataSelectedState) => item.layerId === layerId);
        if (index === -1) {
            newTemplateOption.splice(index, 1);
            setOptionSelected([...newTemplateOption])
        }
    }, [optionSelected]);

    const ContentPreviewContextValues = useMemo(() => {
        return {
            optionSelected,
            onUpdateState,
            removeOptionSelected
        }
    }, [onUpdateState, optionSelected, removeOptionSelected])

    return (
        <ContentPreviewContext.Provider value={ContentPreviewContextValues}>
            {children}
        </ContentPreviewContext.Provider>
    )
})
ContentPreviewContext.displayName = 'ContentPreviewProvider'