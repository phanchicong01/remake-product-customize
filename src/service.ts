import { callApiFuc } from "server/fetcher";

export const getAllArtworks = async (id): Promise<any> => {
    const response = await callApiFuc(`/api/artworks/${id}`)
        .then((rs) => {
            return rs.data;
        })
        .catch((err) => console.error(err));
    return response;
};
