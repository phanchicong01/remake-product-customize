export interface Artwork {
    nameArtwork:string,
    categoryArtwork:{
        created_at: Date
        id: number
        name: string
        parent_id: number
        sort: any
        updated_at: Date
    },
    size:{
        width:number,
        height:number
    }
    setting:{
        template:{
            displayOption:"Radio" | "Dropdown",
            label:string
        },
        scale:{x:number, y:number},
        background:"",
        showGrid:boolean
    },
    templates:template[]
}

export interface template{
    id:any,
    name:string,
    layers:layer[],
}
export interface layer extends IShape{
   id:any,
   name:string,
   type:"image" |  "text" | "addition",
   width:number,
   height:number,
   lock:boolean,
   show:boolean,
   isActiveConfig:boolean
   
} 
export interface IShape   {

} 

export interface Image    {
    src:string,
    indexOrder:number
    configuration:{
        reOrder:number
        personalization:{
            toggleShowLayer:boolean,
            option:"none" | "clipart" | "group-clipart" | "upload-photo" | "maps"
        },
        clipArtSetting:{
            title:string,
            clipArtCategory:ClipArtCategory,
            rePosition:{
                items: [];
                x: number;
                y: number;
                width: number;
                height: number;
            }
            defaultOption:ClipArtCategory
            require:boolean
            showCustomization:boolean,
        },
        groupClipArtSetting:{
            title:string,
            clipArtCategory:ClipArtCategory,
            rePosition:[
                {
                    items: [];
                    x: number;
                    y: number;
                    width: number;
                    height: number;
                }
            ]
            defaultOption:ClipArtCategory
            placeholder:string
            require:boolean
            showCustomization:boolean,
        },
        uploadPhotoSetting:{
            title:string,
            cropper:boolean
            liveReview:boolean,
            instruction:string,
            require:boolean
            showCustomization:boolean,
        }
        conditionalSetting:{
            otherOption:any
        },
        extra:{
            customClass:string
        }
    }
}
export interface Text    {
    configuration:{
        reOrder:number
        personalization:{
            toggleShowLayer:boolean,
            option:"none" | "Enable",
            title:string,
            placeholder:string,
            limitCharacter:number,
            require:boolean
            showCustomization:boolean,
        },
        conditionalSetting:{
            otherOption:any
        },
        extra:{
            customClass:string
        }
    }
}
export interface ClipArtCategory {
    id: number;
    name: string;
    thumbnail: string;
    parent_category_id: number;
    order: number;
    max_width: number;
    display: "image" | "text" | "radio"; //text => dropdown , image => thumbnail , radio => inline-button
    is_show_name_on_hover: true;
    items: any[];
    children: ClipArtCategory[];
}
