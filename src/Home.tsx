/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect, useMemo, useState } from "react";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import { createTheme, styled, ThemeProvider } from "@mui/material/styles";
import ArtworkComponent from "./components/artwork";
import LeftSideBar from "./components/left-sidebar";
// import TopBar from "./components/top-bar";
import BottomBar from "./components/bottom-bar";
import RightSidebarComponent from "components/right-sidebar";

import { useAppDispatch, useAppSelector } from "hooks";
import EditSidebar from "components/edit-sidebar";
import { getAllArtworks } from "./service";

import { onRemoveLayerByID, onUpdateInitDataTemplate, onUpdatePositionLayerSelected } from "store/template-reducer/templateSlice";

import { Helmet } from "react-helmet";

import { useParams } from "react-router-dom";
import { getCustomFonts } from "components/top-bar/service";
import { createCustomFonts } from "store/fonts-family/fontsFamilySlice";
import { updateArtwork } from "store/artwork-reducer/artworkSlice";
import WebFont from 'webfontloader';
import { Layer } from "store/template-reducer/interface";
import { PositionLayer, TypeLayer } from "constants/enum";

const Item = styled(Paper)(({ theme }) => ({
    ...theme.typography.body2,

    height: "100%",

    color: theme.palette.text.secondary,
    "& .MuiTabs-indicator": {
        backgroundColor: "transparent",
    },
    "&.MuiTabs-selected": {
        color: "#1890ff",
        // fontWeight: theme.typography.fontWeightMedium,
    },
}));
declare module "@mui/material/styles" {
    interface Palette {
        dimgray: Palette["primary"];
    }

    // allow configuration using `createTheme`
    interface PaletteOptions {
        dimgray?: PaletteOptions["primary"];
    }
}

// Update the Button's color prop options
declare module "@mui/material/Button" {
    interface ButtonPropsColorOverrides {
        dimgray: true;
    }
}
const theme = createTheme({
    palette: {
        dimgray: {
            main: "#595959",
            contrastText: "#fff",
        },
    },
});
const Home = () => {
    const [openPreview, setOpenPreview] = useState(false);

    const drawerMenu = useAppSelector((state) => state.drawerMenu);
    const { customFonts } = useAppSelector((state) => state.fontsFamily);
    const { layers, layerSelected } = useAppSelector((state) => state.template.templateActive);

    const params = useParams();
    const dispatch = useAppDispatch();
    const [loadingPage, setLoadingPage] = useState(true);

    const [listMarkupCustomFont, setListMarkupCustomFont] = useState([])
    const [isReadyFont, setIsReadyFont] = useState(false)
    const handleRemoveObject = (idLayer) => {
        dispatch(onRemoveLayerByID({ idLayer }));
    };
    const handleUpdatePosition = (position: PositionLayer, value: number) => {
        dispatch(onUpdatePositionLayerSelected({ position, value }));
    };
    const handleEventListener = () => {
        window.addEventListener("keydown", event => {
            const layerActive = window.layerActive;
            switch (event.code) {
                case "Delete":
                    return layerActive.map(item => handleRemoveObject(item.id))
                case "ArrowRight":
                    console.log(event.code)
                    return handleUpdatePosition(PositionLayer.X, +1)
                case "ArrowLeft":
                    return handleUpdatePosition(PositionLayer.X, -1)
                case "ArrowDown":
                    return handleUpdatePosition(PositionLayer.Y, +1)
                case "ArrowUp":
                    return handleUpdatePosition(PositionLayer.Y, -1)

                default:
                    return;
            }
        })

    }
    const handleOpenSideBar = (flag) => {
        return setOpenPreview(flag)
    }

    useEffect(() => {
        handleEventListener();
        getCustomFonts().then((res) => {
            const newRes = res.map((item) => {
                return {
                    ...item,
                    family: item.name,
                    kind: "customfonts#customfontList",
                };
            });
            dispatch(createCustomFonts(newRes));
        });

        if (params?.idArtwork) {
            setLoadingPage(true);
            getAllArtworks(params.idArtwork)
                .then((res) => {
                    try {
                        let parseDataContent = JSON.parse(res.content);
                        const initTemplates = parseDataContent?.templates

                        if (typeof parseDataContent === "string") {
                            parseDataContent = JSON.parse(parseDataContent)
                        }

                        dispatch(updateArtwork(parseDataContent))
                        dispatch(onUpdateInitDataTemplate(initTemplates))
                        setLoadingPage(false)
                    } catch (error) {
                        setLoadingPage(false);
                        console.error(error);
                    }
                })
                .catch((err) => {
                    console.log(err.responsive);
                    setLoadingPage(false);
                });
        } else {
            setLoadingPage(false);

        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    const importFontGoogle = useCallback((listGoogleFontSelected) => {
        WebFont.load({
            google: {
                families: listGoogleFontSelected
            },
            fontactive: () => {
                setIsReadyFont(true)
            }
        })
    }, [])

    const importFontCustom = useCallback((listCustomFontSelected: any[]) => {
        const listMarkup = listCustomFontSelected.map((fontItem: Layer) => {
            let markup = [
                '@font-face {\n',
                '\tfont-family: \'', fontItem.name, '\';\n',
                '\tfont-style: \'normal\';\n',
                '\tfont-weight: \'normal\';\n',
                '\tsrc: url(\'', fontItem.url, '\');\n',
                '}\n'
            ].join('');
            return markup
        })
        setListMarkupCustomFont(listMarkup);
        WebFont.load({
            custom: {
                families: listCustomFontSelected.map(font => font.name)
            },
            fontactive: function () {
                setIsReadyFont(true)
            }
        })
    }, [])
    const handleImportFont = async (listFontSelected) => {
        const listGoogleFontSelected = []
        const listCustomFontSelected = []
        await Promise.all(listFontSelected.map(async (layer: Layer) => {
            if (layer.fontKind === 'customfonts#customfontList') {
                const findItem = customFonts?.items?.find(item => layer.fontFamily === item.family)
                listCustomFontSelected.push(findItem)
            }
            if (layer.fontKind === 'webfonts#webfontList') {
                listGoogleFontSelected.push(layer.fontFamily)
            }
        }))
        if (listCustomFontSelected.length > 0 && listGoogleFontSelected.length === 0) {
            importFontCustom(listCustomFontSelected);
            return
        }
        if (listCustomFontSelected.length === 0 && listGoogleFontSelected.length > 0) {
            importFontGoogle(listGoogleFontSelected);
            return;

        }
        if (listCustomFontSelected.length > 0 && listGoogleFontSelected.length > 0) {
            // importFontCustom(listCustomFontSelected);
            const listMarkup = listCustomFontSelected.map((fontItem: Layer) => {
                let markup = [
                    '@font-face {\n',
                    '\tfont-family: \'', fontItem.name, '\';\n',
                    '\tfont-style: \'normal\';\n',
                    '\tfont-weight: \'normal\';\n',
                    '\tsrc: url(\'', fontItem.url, '\');\n',
                    '}\n'
                ].join('');
                return markup
            })
            setListMarkupCustomFont(listMarkup)
            WebFont.load({
                custom: {
                    families: listCustomFontSelected.map(font => font.name)
                },
                google: {
                    families: listGoogleFontSelected
                },
                fontactive: function () {
                    setIsReadyFont(true)
                }
            })
        }
    }
    useEffect(() => {
        if (Array.isArray(layers)) {
            const listFontSelected = layers.filter((layer: Layer) => layer.type === TypeLayer.Text);
            if (listFontSelected.length > 0) {
                handleImportFont(listFontSelected)

            }
        }
        if (Array.isArray(layers) && layers.length === 0) {
            setIsReadyFont(true)
        }

        window.layers = layers;
        window.layerActive = layerSelected
    }, [layers, layerSelected, customFonts]);

    if (loadingPage) {
        return (
            <div
                style={{
                    position: "fixed",
                    top: 0,
                    height: "100vh",
                    width: "100%",
                    left: 0,
                    background: "#f2f2f2",
                    justifyContent: "center",
                    display: "flex",
                    alignItems: "center",
                }}
            >
                Loading....
            </div>
        );
    }
    return (
        <ThemeProvider theme={theme}>
            <div className="App">
                {
                    listMarkupCustomFont.length > 0 && <Helmet>
                        {listMarkupCustomFont.map((font: any) => <style type="text/css">
                            {
                                font
                            }
                        </style>

                        )}
                    </Helmet>
                }
                {
                    (isReadyFont) &&
                    <Container style={{ padding: 0 }} maxWidth={false}>
                        <Grid sx={{ bgcolor: "#cfe8fc" }} container>
                            <Grid item xs={4} lg={3} columns={12} sx={{ position: "relative" }}>
                                <Item>{drawerMenu.open ? <EditSidebar /> : <LeftSideBar />}</Item>
                            </Grid>
                            <Grid item xs={8} lg={9}>
                                <Grid container style={{ background: "#232323" }}>
                                    <Grid item xs={openPreview ? 8 : 12} style={{ height: "100vh", position: "relative" }}>
                                        {/* <TopBar /> */}

                                        <ArtworkComponent />
                                        <BottomBar />
                                    </Grid>
                                    <Grid
                                        item
                                        xs={openPreview ? 4 : 0}
                                    // style={{
                                    //     position: "relative",
                                    //     transition: "all ease-in-out 0.3s",
                                    // }}
                                    >
                                        <RightSidebarComponent visble={openPreview} openSidebar={handleOpenSideBar} />
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Container>
                }
            </div>
        </ThemeProvider >
    );
};

export default Home;
