import TreeAntd from 'components/common/tree-antd';
import useArtworkCategories from 'data/hooks/use-artwork-categories';
import React, { useCallback, useMemo, useRef, useState } from 'react'
import { useNavigate, useSearchParams } from 'react-router-dom';
import { ArtworkHeader } from './components/header/artworkHeader';
import TableArtwork from './table-artworks';
import _isEmpty from "lodash-es/isEmpty"

import { CreateModal } from './components/create-modal';

import clsx from 'clsx';
import { PageLink, PageTitle } from 'layout/core';

type Props = {}

const breadcrumbs: Array<PageLink> = [
    {
        title: 'Home',
        path: '/',
        isSeparator: false,
        isActive: false,
        isRedirect: true
    },
    {
        title: '',
        path: '',
        isSeparator: true,
        isActive: false,
    },
]

const ListArtwork = (props: Props) => {
    const [searchParams] = useSearchParams();
    const navigate = useNavigate();
    const [dataModal, setShowModal] = useState({
        show: false,
        data: null
    })
    const [checkedKeys, setCheckedKeys] = useState([])

    const useArtworkCategoryQuery = useMemo(() => {
        const catgoryId = searchParams.get("id") === "All" ? 0 : parseInt(searchParams.get("id"))
        return {
            id: catgoryId,
            search: searchParams.get("search_categroy") ?? undefined
        }
    }, [searchParams])

    const { data, isFetched, isLoading, isRefetching, refetch } = useArtworkCategories(useArtworkCategoryQuery.search)

    const onCheck = useCallback((nodeData: string[]) => {
        setCheckedKeys(nodeData)
    }, [])

    const handleOnSelect = useCallback((selectedKeys, e: { selected: boolean, selectedNodes, node, event }) => {
        if (selectedKeys[0] === 0) {
            return navigate('/artworks?id=All')
        }
        if (!!selectedKeys[0]) {
            navigate('/artworks?id=' + selectedKeys[0])
        }
    }, [navigate])

    const handleClose = useCallback((isReload) => {
        setShowModal({
            show: false,
            data: null
        })
        if (!!isReload) {
            refetch()
        }
    }, [refetch])

    const handleRefetch = () => {
        refetch()
        setCheckedKeys([])
    }
    const renderData = useCallback(() => {
        const newData = [{ name: "All", id: 0, key: 0, title: "All", children: [], checkable: false }].concat(data?.map((item: any) => (
            { ...item, key: item.id, title: item.name }
        )))
        return newData
    }, [data])


    const findDataLoop = (
        data: any[],
        key: React.Key,
        callback:(node: any, i: number, data: any[])=>void
    ) => {
        for (let i = 0; i < data.length; i++) {
            if (data[i]['id'] === key) {
                return callback(data[i], i, data);
            }
            if (data[i]['children']) {
                findDataLoop(data[i]['children']!, key , callback);
            }
        }
    };

    const handleEdit = () => {
        let findData = null 
        findDataLoop(data, useArtworkCategoryQuery.id , (item, index, arr) => {
            findData = item;
        });
        setShowModal({
            data: findData,
            show: true
        })

    }
    const refAntd = useRef(null)
    return (
        <>
            <PageTitle breadcrumbs={breadcrumbs}>Artwork</PageTitle>
            <div className='card'>
                <div className='card-body'>
                    <div className='row'>
                        <div className='col-lg-12'>
                            <ArtworkHeader
                                onRefetch={handleRefetch}
                                onEdit={handleEdit}
                                onCreate={(data?: any) => setShowModal((prev) => ({ ...prev, data: data, show: true }))} />
                        </div>
                        <div className={clsx('col-lg-3')} style={{ borderRight: "2px solid #f2f2f2" }}>
                            {
                                !isRefetching && isFetched && !isLoading && !_isEmpty(data) &&
                                <TreeAntd treeData={renderData()}
                                    selectable={true}
                                    onSelect={handleOnSelect}
                                    onCheck={onCheck}
                                    checkedKeys={checkedKeys}
                                    selectedKeys={[useArtworkCategoryQuery.id]}
                                    ref={refAntd}
                                    blockNode={true}
                                    defaultExpandAll
                              
                                    fieldNames={{
                                        key: "id",
                                        title: "name",
                                        children: "children"
                                    }}
                                />
                            }
                        </div>

                        <div className='col-lg-8'>
                            <TableArtwork />
                        </div>
                    </div>
                    {
                        isFetched && !isLoading && !_isEmpty(data) &&
                        <CreateModal dataCategory={data?.map((item: any) => (
                            { ...item, key: item.id, title: item.name }
                        ))} show={dataModal.show} handleClose={handleClose} data={dataModal.data} />
                    }
                </div>
            </div>
        </>
    )
}

export default ListArtwork