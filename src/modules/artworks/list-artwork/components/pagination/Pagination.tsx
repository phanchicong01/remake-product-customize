/* eslint-disable jsx-a11y/anchor-is-valid */
import clsx from 'clsx'
import {  PaginationState } from 'helpers'

type Props = {
  id:string,
  pagination:PaginationState
  isLoading:boolean
  updateState: (data:{
    page:number,
    items_per_page:number
  }) => void;
}
const Pagination = ({id , isLoading ,pagination ,updateState}:Props) => {

  const updatePage = (page: number | null) => {
    if (!page || isLoading || pagination.page === page) {
      return
    }
    updateState({page, items_per_page: pagination.items_per_page || 10})
  }

  return (
    <div className='row'>
      <div className='col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'></div>
      <div className='col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'>
        <div id={id}>
          <ul className='pagination'>
            <li
                key={"&laquo; Previous"}
                className={clsx('page-item next', {
                  active: pagination.page === pagination.links[0].page,
                  disabled: isLoading,
                })}
              >
                <a
                  className='page-link'
                  onClick={() => updatePage(pagination.links[0].page)}
                  dangerouslySetInnerHTML={{__html: "&laquo; Previous"}}
                  style={{cursor: 'pointer'}}
                />
              </li>
            {pagination.links?.map((link) => (
              <li
                key={link.label}
                className={clsx('page-item', {
                  active: pagination.page === link.page,
                  disabled: isLoading,
                  previous: link.label === '&laquo; Previous',
                  next: link.label === 'Next &raquo;',
                })}
              >
                <a
                  className='page-link'
                  onClick={() => updatePage(link.page)}
                  dangerouslySetInnerHTML={{__html: link.label}}
                  style={{cursor: 'pointer'}}
                />
              </li>
            ))}
              <li
                key={"Next &raquo;"}
                className={clsx('page-item next', {
                  active: pagination.page === pagination.links[pagination.links.length - 1].page,
                  disabled: isLoading,
                })}
              >
                <a
                  className='page-link'
                  onClick={() => updatePage(pagination.links[pagination.links.length - 1].page)}
                  dangerouslySetInnerHTML={{__html: "Next &raquo;"}}
                  style={{cursor: 'pointer'}}
                />
              </li>
          </ul>
        </div>
      </div>
    </div>
  )
}

export default Pagination
