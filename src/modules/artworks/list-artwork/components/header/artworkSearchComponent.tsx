/* eslint-disable react-hooks/exhaustive-deps */
import { useState,  useCallback } from 'react'
import { createSearchParams, useNavigate, useSearchParams } from 'react-router-dom'
import {  KTSVG } from 'helpers'
import _debounce from "lodash-es/debounce"
import _pickBy from "lodash-es/pickBy"
import _identity from "lodash-es/identity"


const ArtworkSearchComponent = () => {
  const navigate = useNavigate()
  const [searchParams] = useSearchParams()
  const [searchTerm, setSearchTerm] = useState<string>(searchParams.get("search_categroy"));

  const handleChangeSearchValue = useCallback(
    _debounce((value) => {
      handlePushQuery(value)
    }, 500),
    [],
  )
  const handlePushQuery = useCallback((newSearch: string) => {
    const newArtworkQuery = _pickBy(
      {
        id:searchParams.get('id') ?? undefined,
        search_categroy: newSearch
      },
      _identity()
    )
    navigate({
      pathname: '/artworks',
      search: `?${createSearchParams(newArtworkQuery)}`,
    });
  }, [ navigate , searchParams])

  const handleChangeInputSearch = (event:React.ChangeEvent<HTMLInputElement>) => {
    const  {value} = event.target
    setSearchTerm(value);
    handleChangeSearchValue(value)
  }
  return (
    <div className='card-title'>
      {/* begin::Search */}
      <div className='d-flex align-items-center position-relative my-1'>
        <KTSVG
          path='/media/icons/duotune/general/gen021.svg'
          className='svg-icon-1 position-absolute ms-6'
        />
        <input
          type='text'
          data-kt-user-table-filter='search'
          className='form-control form-control-solid w-100 ps-14'
          placeholder='Search Category'
          value={searchTerm}
          onChange={handleChangeInputSearch}
        />
      </div>
      {/* end::Search */}
    </div>
  )
}

export { ArtworkSearchComponent }
