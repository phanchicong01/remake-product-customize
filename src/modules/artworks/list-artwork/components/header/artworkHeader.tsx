import { ClipartToolbar } from './artworkToolbar'

import { ArtworkSearchComponent } from './artworkSearchComponent'
import { ClipartGrouping } from './artworkGrouping'

type Props = { onCreate: () => void, onRefetch: () => void, onEdit: () => void }

const ArtworkHeader = ({ onCreate, onRefetch, onEdit }: Props) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  // const { selected } = useListView();
  return (
    <div className='card-header border-0 pt-6'>
      <div className='d-flex flex-column'>
        <ClipartGrouping onCreate={onCreate} onRefetch={onRefetch} onEdit={onEdit} />
        <ArtworkSearchComponent />

      </div>
      {/* begin::Card toolbar */}
      <div className='card-toolbar'>
        {/* begin::Group actions */}
        <div className='d-flex '>

          <ClipartToolbar />
        </div>
        {/* <ClipartToolbar /> */}
        {/* end::Group actions */}
      </div>
      {/* end::Card toolbar */}
    </div>
  )
}

export { ArtworkHeader }
