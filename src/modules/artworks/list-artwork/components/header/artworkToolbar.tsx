import { KTSVG } from 'helpers'
import { useNavigate } from 'react-router-dom'

const ClipartToolbar = () => {

  const navigate = useNavigate();
  const handleNewArtwork = () => {
    navigate('/admin/fe-artworks/create' )
  }

  return (
    <div className='d-flex justify-content-end' data-kt-user-table-toolbar='base'>
      {/* <UsersListFilter /> */}
   
      {/* begin::Add user */}
      {
        <button type='button' className='btn btn-primary' onClick={handleNewArtwork} >
        <KTSVG path='/media/icons/duotune/arrows/arr075.svg' className='svg-icon-2' />
        Create new Artwork
      </button>
      }
     
      {/* end::Add user */}
    </div>
  )
}

export { ClipartToolbar }
