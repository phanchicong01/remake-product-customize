import {  useMutation } from 'react-query'
import { ID } from 'helpers'
import { useNavigate, useSearchParams } from 'react-router-dom';
import removeArtworkCategory from 'apis/remove-artwork-category';
import { useCallback } from 'react';
import { toast } from 'react-toastify';
import useConfirmDialog from 'hooks/use-confirm-dialog';
import PageLoading from 'components/common/page-loading';
 type Props ={
  onCreate:()=>void , onEdit:()=>void,  onRefetch:()=>void 
 }
const ClipartGrouping = ({onCreate ,onEdit, onRefetch }: Props) => {
  const [searchParams] = useSearchParams();
  const navigate = useNavigate()
  const  {getDialogConfirmResult} = useConfirmDialog()

  const useMutationRemoveClipartItem = useMutation((artworkCategoryId: ID) =>
    removeArtworkCategory(artworkCategoryId),
)


  const handleRemoveItem = useCallback(async ()=>{
    const isConfirmOk = await  getDialogConfirmResult({
      title:"Delete Artwork Category",
      description:"Are you sure delete this categroy?"
    })
    if(isConfirmOk){
      try {
        if(!isNaN(parseInt(searchParams.get('id')))){
          const result = await  useMutationRemoveClipartItem.mutateAsync(parseInt(searchParams.get("id")));
          if(result.code === 200){
            toast.success(result.message);
            onRefetch()
            navigate('/artworks')
         
          }else{
            toast.error(result.message);
           
          }
        }
        
      } catch (error) {
        console.log(error)
        toast.error("Something went wrong. Please try again!")
      }
    }
   
  },[getDialogConfirmResult, navigate, onRefetch, searchParams, useMutationRemoveClipartItem])

  return (
    <div className='d-flex justify-content-end align-items-center me-2'>
      {
        useMutationRemoveClipartItem.isLoading && 
        <PageLoading />
      }
       <div className='me-2'>
        <button
          onClick={onCreate}
          type='button'
          className='btn btn-sm btn-primary '
        >
          <i className='fa fa-plus'></i>
          Create Category
        </button>
      </div>
      <div className='me-2'>
        <button
          disabled={isNaN(parseInt(searchParams.get('id')))}
          type='button'
          className='btn btn-primary btn-sm'
          onClick={onEdit}
        >
          Edit Category
        </button>
        </div>
      <div className=''>
        <button
          disabled={isNaN(parseInt(searchParams.get('id')))}
          type='button'
          className='btn btn-danger btn-sm'
          onClick={handleRemoveItem}
        >
          Delete category
        </button>
      </div>
    </div>
  )
}

export { ClipartGrouping }
