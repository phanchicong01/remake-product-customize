import { useEffect, useState } from "react";

const Thumb = ({ file }) => {
    const [thumb, setThumb] = useState<any>(null)
    const [loading, setLoading] = useState<boolean>(false)

    useEffect(() => {
        if (!!file) {
            setLoading(true);
            let reader = new FileReader();
            reader.onloadend = () => {
                setLoading(false)
                setThumb(reader.result)
            };
            reader.readAsDataURL(file);
        }

    }, [file])


    if (!file) { return null; }

    if (loading) { return <p>loading...</p>; }
    return (<img src={thumb}
        alt={file.name}
        className="img-thumbnail mt-2 me-2"
        style={{ objectFit: "contain", height: 100, width: 100 }} />);
}

export default Thumb