import {  useMemo } from 'react'
import clsx from 'clsx'
import _pickBy from "lodash-es/pickBy"
import _identity from "lodash-es/identity"
import _isEmpty from "lodash-es/isEmpty"

import { getTotalPage, ID, KTSVG, toAbsoluteUrl } from 'helpers'
import useArtworkList from 'data/hooks/use-artwork-list'
import { ArtworkListRequest } from 'apis/get-artworks/interface';
import { createSearchParams, useNavigate, useSearchParams } from 'react-router-dom'
import { Artwork } from 'interfaces/artwork'
import Pagination from 'components/common/pagination'


import Loading from '../components/loading'
import useConfirmDialog from 'hooks/use-confirm-dialog'
import removeArtwork from 'apis/remove-artworks'
import { toast } from 'react-toastify'
import EmptyData from 'components/common/empty-data'
import InputEdit from 'components/common/Input-edit'
import { useMutation } from 'react-query'
import { UpdateArtworkRequest } from 'apis/update-artwork/interface'
import PageLoading from 'components/common/page-loading'
import updateArtwork from 'apis/update-artwork'


const TableArtwork = () => {

  const [searchParams] = useSearchParams();
  const navigate = useNavigate();
  const { getDialogConfirmResult } = useConfirmDialog();
  const useMutationUpdateArtwork = useMutation((payload: UpdateArtworkRequest) => updateArtwork(payload))
  const useMutationRemoveArtwork = useMutation((payload: ID) => removeArtwork(payload))
 
  const artWorkQuery: ArtworkListRequest = useMemo(() => {
    // const categoryId = (searchParams.get("id") === 'All') ? 0 : parseInt(searchParams.get("id"))
    return {
      search: searchParams.get('search'),
      page: searchParams.get('page') ? parseInt(searchParams.get('page')) : 1,
      per_page: searchParams.get('per_page') ? parseInt(searchParams.get('per_page')) : 10,
      category: parseInt(searchParams.get("id")) ? parseInt(searchParams.get("id")) : 0,
    }
  }, [searchParams])

  const { data: dataArtwork, isLoading, isFetched, refetch } = useArtworkList(artWorkQuery);

  const totalArtwork = dataArtwork?.total ?? 0
  const totalPage = getTotalPage(totalArtwork, artWorkQuery.per_page);

  const handleChangePage = (page: any) => {
    const newArtworkQuery = _pickBy(
      {
        ...artWorkQuery,
        page: page + 1
      },
      _identity()
    )
    navigate({
      pathname: '/artworks',
      search: `?${createSearchParams(newArtworkQuery)}`,
    });
  }

  const handleDelete = async (items: Artwork) => {
    const isConfirmOk = await getDialogConfirmResult({
      title: "Delete Artwork",
      description: "Are you sure delete artwork " + items.name + "?"
    })
    try {
      if (isConfirmOk) {
        const result = await useMutationRemoveArtwork.mutateAsync(items.id);
        if (result.code === 200) {
          toast.success("Remove artworks success");
          refetch()
        } else {
          toast.error("Remove artworks success");
        }
      }
    } catch (error) {
      console.log(error);
      toast.error("Something went wrong. Please try again!")
    }
  }

  const handleChangeName = async (items: Artwork, value) => {
    try {
      const result = await useMutationUpdateArtwork.mutateAsync({
        id: items.id,
        data: {
          name: value
        }
      })
      if (result.code === 200) {
        toast.success("Update name artwork success.")
      } else {
        toast.error(result.message)
      }
    } catch (error) {
      console.log(error);
      toast.error("Something went wrong. Please try again!")
    }
  }

  return (
    <div className='row gx-3 gy-3 position-relative min-h-500px'>
      {
        (useMutationUpdateArtwork.isLoading || useMutationRemoveArtwork.isLoading) &&
        <PageLoading />
      }
      {
        isFetched && _isEmpty(dataArtwork?.data) &&
        <EmptyData />
      }
      {isFetched && !isLoading && !_isEmpty(dataArtwork?.data) &&
        dataArtwork?.data?.map((items: Artwork) => <div key={items.id} className={clsx('col-lg-3')}>
          <div className='card card-bordered shadow-sm'>
            <div className='card-body p-2'>
              <img src={toAbsoluteUrl(items.thumbnail)} alt={items.name} className="w-100" />
            </div>
            <div className='card-footer'>
            <div className='d-flex justify-content-between'>
              <div><InputEdit value={items.name} onChange={(value: string) => handleChangeName(items, value)} /></div>
              <div className='d-flex g-2'>
                <a href={'/admin/fe-artworks/edit/' + items.id}  >
                  <KTSVG
                    path='/media/icons/duotune/art/art005.svg'
                    className='svg-icon-3 w-24 h-24'
                  />
                </a>
                <a onClick={() => handleDelete(items)}>
                  <KTSVG path='/media/icons/duotune/general/gen027.svg'

                    className='svg-icon-3' />
                </a>
              </div>
            </div>

          </div>
          </div>
     
        </div>)
      }
      {/* <Pagination id={"table_artwork"} 
       isLoading={isLoading}
        updateState={handleChangePage}
        pagination={paginationState}   /> */}
      {isLoading && <Loading />}
      {
        totalPage > 1 &&
        <Pagination onPageChange={handleChangePage} pageCount={totalPage} forcePage={artWorkQuery.page - 1} />
      }
    </div>
  )
}

export default TableArtwork