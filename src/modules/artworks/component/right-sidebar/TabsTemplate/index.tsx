import { Grid } from '@mui/material'
import ItemInlineText from 'components/common/item-inline-text'
import { TypeDisplayOptionTempLate } from 'constants/enum'
import { useAppSelector } from 'hooks'
import React, { useCallback } from 'react'
import { useDispatch } from 'react-redux'
import { Template } from 'store/template-reducer/interface'
import { onActiveTemplate } from 'store/template-reducer/templateSlice'
import Select from "antd/lib/select";
import { updateDrawer } from 'store/drawer-menu/drawerSlice'
import { closeReposition } from 'store/re-position-reducer/RePositionSlice'
type Props = {}

const TabsTemplate = (props: Props) => {
    const { templates, templateActive } = useAppSelector(state => state.template);
    const { open } = useAppSelector(state => state.drawerMenu)
    const { isReposition } = useAppSelector(state => state.rePosition)
    const { setting } = useAppSelector(state => state.artwork)
    const dispatch = useDispatch()

    const validateBeforeActiveTemplate = useCallback((templateIsActive) => {
        if (open) {
            dispatch(
                updateDrawer({
                    open: false,
                    data: null,
                })
            );
        }
        if (isReposition) {
            dispatch(closeReposition())
        }
        dispatch(onActiveTemplate(templateIsActive))
    }, [dispatch, isReposition, open])

    const handleClickTab = useCallback((checked, data: Template) => {
        validateBeforeActiveTemplate(data)
    }, [validateBeforeActiveTemplate])

    const handleChangeSelect = useCallback((value) => {
        const templateIsActive = templates.find((template: Template) => template.id === value);
        validateBeforeActiveTemplate(templateIsActive)
    }, [templates, validateBeforeActiveTemplate])

    if (templates?.length === 1) {
        return null
    }
    return (
        <div>
            <p>{setting?.template.label}</p>
            {setting?.template?.displayOption === TypeDisplayOptionTempLate.Radio &&
                <ItemInlineText
                    isShowHover={true}
                    data={templates}
                    onChecked={handleClickTab}
                    filedNames={{
                        name: "name",
                        value: "id",
                        id: "id"
                    }}
                    minWidth={120}
                    value={templateActive.id}
                />
            }
            {
                setting?.template?.displayOption === TypeDisplayOptionTempLate.Dropdown &&
                <Grid item xs={12}>
                    <Select
                        fieldNames={{ label: "name", value: "id" }}
                        options={templates}
                        style={{ width: "100%" }}
                        value={templateActive.id}
                        onChange={handleChangeSelect}
                    />
                </Grid>
            }
        </div>
    )
}

export default TabsTemplate