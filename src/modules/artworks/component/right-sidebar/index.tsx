import { useState } from "react";
import { useParams } from "react-router-dom";
import ContentReOrder from "./content-re-order";
import { ButtonGroup, Button } from "@mui/material";

import ContentPreview from "./content-preview";
import styles from "./styles.module.scss";
import { ContentPreviewProvider } from "context/content-preview.context";

const RightSidebarComponent = ({ openSidebar, visible }: any) => {
    const [open, setOpen] = useState(visible);

    const [tab, setTab] = useState<any>(1);
    const params = useParams();

    const handleOpenMenu = () => {
        setOpen(!open);
        openSidebar(!open);
    };

    return (
        <ContentPreviewProvider >
            <div className={`${styles.btnPreview} ${open ? styles.active : ""}`} onClick={handleOpenMenu}>
                Preview options
            </div>
            <div className={`${styles.previewArea} ${open ? styles.active : ""}`}>
                <div className={` ${styles.contentPreview}`}>
                    <div style={{ height: "80px", textAlign: "right", marginTop: 15 }}>
                        <div>
                            Mode :{" "}
                            <ButtonGroup>
                                <Button
                                    size="small"
                                    variant={tab === 1 ? "contained" : "outlined"}
                                    onClick={() => setTab(1)}
                                >
                                    Preview
                                </Button>
                                <Button
                                    size="small"
                                    variant={tab === 2 ? "contained" : "outlined"}
                                    onClick={() => setTab(2)}
                                >
                                    Re-order
                                </Button>
                            </ButtonGroup>
                        </div>
                        <small style={{ textAlign: "right" }}>
                            {tab === 1 && <i>* Preview form only. Live preview does not work in artwork editor.</i>}
                            {tab === 2 && <i>* Re-order applies to all campaigns.</i>}
                        </small>
                        <div>
                            {params?.idArtwork && (
                                <span style={{ cursor: 'pointer' }} onClick={() => {
                                    window.open(`${process.env.REACT_APP_PUBLIC_PREVIEW_URL}artworkId=${params?.idArtwork}`, '_blank')
                                }}

                                >
                                    Preview on new tab
                                </span>
                            )}
                        </div>
                    </div>
                    <div style={{ display: open && tab === 1 ? "block" : "none" }}>
                        <ContentPreview />
                    </div>
                    <div style={{ display: open && tab === 2 ? "block" : "none" }}>
                        <ContentReOrder />
                    </div>
                </div>
            </div>
        </ContentPreviewProvider>
    );
};

export default RightSidebarComponent;
