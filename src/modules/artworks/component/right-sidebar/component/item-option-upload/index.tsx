import React from "react";

import { styled } from "@mui/material/styles";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";

import ImageOutlinedIcon from "@mui/icons-material/ImageOutlined";

interface IProps {}
const UploadItem = (props: IProps) => {
    return (
        <Grid item xs={12}>
            <Box
                sx={{
                    width: 120,
                    height: 120,
                    background: "#f5f5f5",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    border: "1px solid #f6f6f6",
                    margin: "8px 0",
                }}
            >
                <ImageOutlinedIcon sx={{ width: 60, height: 60 }} color="secondary" />
            </Box>
            <label htmlFor="contained-button-file">
                <Input accept="image/*" id="contained-button-file" type="file" />

                <Button variant="contained" component="span">
                    Upload
                </Button>
            </label>
        </Grid>
    );
};

export default UploadItem;
const Input = styled("input")({
    display: "none",
});
