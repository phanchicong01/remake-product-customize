import { Grid } from '@mui/material'
import { PersonalizationOption, TypeLayer } from 'constants/enum'
import { DataSelectedState } from 'context/content-preview.context'
import useContentPreview from 'hooks/use-content-preview'
import { useCallback, useMemo } from 'react'
import {  ConditionSetting, Layer } from 'store/template-reducer/interface'
import { ClipArtCategory } from 'interfaces/clipart-category'

import ItemOptionImage from '../item-option-image'
import ItemOptionText from '../item-option-text'

type Props = {
    layer: Layer;
}

const ItemShowOptionByLayer = ({ layer }: Props) => {
    const { optionSelected, removeOptionSelected, onUpdateState } = useContentPreview()
    const handleChangeItemLayer = useCallback((field: string, value: any) => {
        const dataSelected: DataSelectedState = {
            layerId: layer.id,
            dataSelectOption: {
                [field]: value
            }
        }
        onUpdateState(dataSelected, layer.id)
    }, [layer.id, onUpdateState])

    const isShowByConditionSetting = useMemo(() => {
        const conditionLayer = layer.configuration?.conditionalSetting
        if (conditionLayer?.layerShowOptionWhen?.id) {
            const conditionSetting: ConditionSetting = layer.configuration.conditionalSetting
            const layerConditionIsSelect = optionSelected.find((option: DataSelectedState) => option.layerId === conditionSetting.layerShowOptionWhen?.id);
            if (!!layerConditionIsSelect) {
                if (conditionLayer.layerShowOptionWhen?.configuration.personalization.option === PersonalizationOption.GroupClipart &&
                    conditionSetting.clipartMatchShow?.length > 0 && conditionSetting.clipartMatchShow.some((clipArt: ClipArtCategory) => clipArt.id === layerConditionIsSelect.dataSelectOption?.clipartMatchShowId)) {
                    return true
                }
                if (conditionLayer.layerShowOptionWhen?.configuration?.personalization.option === PersonalizationOption.Clipart && conditionSetting.itemMatchShow?.id === layerConditionIsSelect.dataSelectOption?.itemMatchShowId) {
                    return true
                }
                return false
            }
            return false
        }
        return true
        // return "123"
    }, [layer, optionSelected])
    const handleRemove = useCallback(() => {
        removeOptionSelected(layer.id)
    }, [layer.id, removeOptionSelected])
    return (
        <Grid container>
            {layer.type === TypeLayer.Text && isShowByConditionSetting &&
                <Grid item xs={12}>
                    <ItemOptionText data={layer?.configuration} name={layer.name} onChange={handleChangeItemLayer} />
                </Grid>
            }
            {layer.type === TypeLayer.Image && isShowByConditionSetting &&
                <Grid item xs={12}>
                    <ItemOptionImage data={layer?.configuration} name={layer.name} onRemove={handleRemove} onChange={handleChangeItemLayer} />
                </Grid>
            }
        </Grid>
    )
}

export default ItemShowOptionByLayer