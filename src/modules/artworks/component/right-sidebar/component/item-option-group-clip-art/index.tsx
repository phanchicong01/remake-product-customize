import React, { useCallback, useEffect, useState } from "react";

import Grid from "@mui/material/Grid";

import ItemChecked from "components/common/item-checked";
import {  GroupClipArtSetting } from "store/template-reducer/interface";
import { InputAdornment, MenuItem, Select, SelectChangeEvent, Typography } from "@mui/material";
import { PersonalizationOption } from "constants/enum";
import ClearIcon from "@mui/icons-material/Clear"
import { CustomInput } from "components/common/custom-input";
import { ClipArtCategory } from "interfaces/clipart-category";

// const { Option } = Select;
interface IProps {
    data: GroupClipArtSetting;
    onChange: (field: string, value: number) => void;
    onRemove: () => void

}
const ItemOptionGroupClipArt = ({ data, onChange, onRemove }: IProps) => {
    const [dataSelect, setDataSelect] = useState<any>(null);
    const [idChecked, setIdChecked] = useState<any>(null);

    const handleChange = useCallback((event: SelectChangeEvent<number>) => {
        const {
            target: { value },
        } = event;

        const findItem = data?.clipArtCategory?.children?.find((item) => item.id === value);
        if (!!findItem) {
            setDataSelect(findItem);
            // onChange('clipartMatchShowId', value as number)
        }
        // firstChange.current = true;
    }, [data?.clipArtCategory?.children])

    const handleChangeChecked = (idChecked: any) => {
        setIdChecked(idChecked);
    };

    useEffect(() => {
        if (data?.defaultOptionParent) {
            setDataSelect(data.defaultOptionParent)
            if (data?.defaultOption) {
                setIdChecked(data?.defaultOption?.id)
            } else {
                setIdChecked(null)
            }
        }
        if (!data?.defaultOptionParent) {
            setDataSelect(null);
            if (!data?.defaultOption) {
                setIdChecked(null)
            }
        }
    }, [data?.defaultOption, data?.defaultOptionParent, data?.clipArtCategory])

    const handleOnChange = useCallback((name: PersonalizationOption, value: number) => {
        // return onChange(name, value)
    }, [])

    useEffect(() => {
        if (!!dataSelect) {
            handleOnChange(PersonalizationOption.GroupClipart, dataSelect?.id)
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [dataSelect])

    const handleClearOptionLayer = () => {
        setDataSelect(null)
        setIdChecked(null)
        onRemove()
    }
    return (
        <Grid item xs={12}>
            <Select
                value={dataSelect?.id || "placeholder"}
                fullWidth
                size="small"
                sx={{ background: "#fff", color: "#222", marginBottom: "10px" }}
                onChange={handleChange}
                input={<CustomInput size={"small"} endAdornment={<InputAdornment position="end">
                    {
                        dataSelect?.id && <ClearIcon className='btn-clear-icon' onClick={handleClearOptionLayer} />
                    }
                </InputAdornment>} />}
            >
                {
                    !dataSelect && !!data?.placeholder &&
                    <MenuItem disabled value="">
                        {data.placeholder}
                    </MenuItem>
                }
                {
                    !data?.clipArtCategory &&
                    <MenuItem disabled value="">
                        choose something
                    </MenuItem>
                }
                {data?.clipArtCategory?.children?.map((child: ClipArtCategory, index: number) => {
                    return (
                        <MenuItem key={child.id} value={child.id}>
                            {child.name}
                        </MenuItem>

                    );
                })}
            </Select>
            {Array.isArray(dataSelect?.items) && dataSelect?.items.length > 0 && (
                <ItemChecked
                    max_width={dataSelect?.max_width}
                    data={dataSelect?.items}
                    value={idChecked}
                    onChange={handleChangeChecked}
                    id={`preview_group_clip_art`}
                />
            )}

            {(!Array.isArray(dataSelect?.items) || dataSelect?.items?.length <= 0) && (
                <Typography sx={{ padding: 1, marginTop: 15 }} color={"gray"} component={"span"} >
                    Empty items
                </Typography>
            )}
        </Grid>
    );
};

export default ItemOptionGroupClipArt
