import React, { useEffect, useMemo, useState } from "react";
import OutlinedInput from "@mui/material/OutlinedInput";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Switch from "@mui/material/Switch";
import FormControlLabel from "@mui/material/FormControlLabel";

import { IConfigurationText } from "store/template-reducer/interface";
import { PersonalizationOption } from "constants/enum";
type Props = {
    data: IConfigurationText,
    name: string
    onChange: (field: string, data) => void
}

const ItemOptionText = ({ data, name, onChange }: Props) => {
    const [value, setValue] = useState<any>("");

    const handleChangeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = event.target.value;
        if (!data.personalization.limitCharacter || value.length <= data.personalization.limitCharacter) {
            setValue(value);
            onChange("text", value)
        }
    };

    const [toggleShow, setToggleShow] = useState(data?.personalization?.toggleShowLayer);

    const handleChangeToggleShow = (event: React.ChangeEvent<HTMLInputElement>) => {
        const checked = event.target.checked;
        setToggleShow(checked);
        onChange("toggleShow", checked)
    };

    useEffect(() => {
        if (data?.personalization?.toggleShowLayer && data?.personalization?.defaultValueToggleShow) {
            setToggleShow(data?.personalization?.defaultValueToggleShow);
            onChange("toggleShow", data?.personalization?.defaultValueToggleShow)
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [data?.personalization?.defaultValueToggleShow, data?.personalization?.toggleShowLayer]);

    const titlePersonalization = useMemo(() => {
        if (data?.personalization?.title) {
            return data?.personalization?.title
        }
        return name
    }, [data?.personalization?.title, name])

    return (
        <Grid container sx={{ py: 2 }}>
            <Grid item xs={12}>
                {<Typography sx={{ fontWeight: "bold", mb: 1 }} component={"h3"}>
                    {titlePersonalization} {data.personalization.require && <small style={{ color: "red" }}>*</small>}
                </Typography>}
            </Grid>
            {data?.personalization?.toggleShowLayer && (
                <Grid item xs={12}>
                    <FormControlLabel
                        sx={{ width: "100%", textAlign: "left" }}
                        value={toggleShow}
                        onChange={handleChangeToggleShow}
                        checked={toggleShow}
                        control={<Switch />}
                        label={`show (${titlePersonalization})`}
                    />
                </Grid>
            )}
            {
                data?.personalization.option === PersonalizationOption.EnablePersonalization &&
                <Grid item xs={12}>
                    {" "}
                    <OutlinedInput
                        value={value}
                        onChange={handleChangeInput}
                        size="small"
                        fullWidth
                        placeholder={data?.personalization?.placeholder}
                        endAdornment={
                            !!data?.personalization?.limitCharacter ? `${value?.length}/${data?.personalization?.limitCharacter}` : undefined
                        }
                    />
                </Grid>
            }
        </Grid >
    )
}

export default ItemOptionText