import React, { useCallback, useMemo } from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { Grid, List, ListItemIcon, ListItemButton, ListItemText } from "@mui/material";
import DragIndicatorIcon from "@mui/icons-material/DragIndicator";

import sortBy from "lodash/sortBy"

import { useAppSelector, useAppDispatch } from "hooks";
import { Layer } from "store/template-reducer/interface";
import { onReOrderLayerConfig } from "store/template-reducer/templateSlice"

import { checkShowingConfig } from "../constants";
import { MappingPersonalizationImage } from "constants/const";
import { TypeLayer } from "constants/enum";
import TabsTemplate from "../TabsTemplate";

import { styled } from "@mui/styles";
import styles from "./styles.module.scss";

const ContentReOrder = ({ data }: any) => {
    const { layers } = useAppSelector((state) => state.template.templateActive);
    const dispatch = useAppDispatch();

    const layerActiveConfig: Layer[] = useMemo(() => {
        const result = layers.map((layer: Layer, index: number) => {
            const newLayer = { ...layer }
            if (checkShowingConfig(newLayer)) {
                return newLayer
            }
            return null
        }).filter(layer => !!layer)
        return sortBy(result, o => o.configuration.reOrder)
    }, [layers])

    const onDragEnd = useCallback((result) => {
        if (!result.destination) {
            return;
        }

        const [sourceItem] = layerActiveConfig.splice(result.source.index, 1);
        layerActiveConfig.splice(result.destination.index, 0, sourceItem);
        const newLayerActiveConfig = layerActiveConfig.map((item: any, index: number) => {
            const itemTemp = {
                ...item, configuration: {
                    ...item.configuration,
                    reOrder: index + 1
                }
            };
            return itemTemp
        })

        // const sourceItem = items[result.source.index]
        // const destinationItem = items[result.destination.index];
        dispatch(onReOrderLayerConfig(newLayerActiveConfig))

    }, [dispatch, layerActiveConfig]);

    const getTitlePersonalization = useCallback((layer: Layer) => {
        if (layer.type === TypeLayer.Text) {
            return layer.configuration?.personalization?.title || ""
        } else if (layer.type === TypeLayer.Image) {
            const configuration = layer.configuration;
            const personalization = layer.configuration.personalization;
            if (!!configuration && configuration[MappingPersonalizationImage[personalization?.option]]?.title) {
                return configuration[MappingPersonalizationImage[personalization.option]]?.title
            }
        }
        return ''
    }, [])

    return (
        <>
            <Grid container rowGap={2} sx={{ p: 1 }}>
                <TabsTemplate />
            </Grid>
            <DragDropContext onDragEnd={(event) => onDragEnd(event)}>
                <Droppable droppableId="droppable-preview">
                    {(provided, snapshot) => (
                        <CustomList
                            {...provided.droppableProps}
                            ref={provided.innerRef}
                            sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}
                            component="nav"
                            aria-labelledby="nested-list-subheader"
                        >
                            {layerActiveConfig?.map((layer: Layer, index: number) => {
                                return (
                                    <Draggable key={layer.id} draggableId={`${layer.id}`} index={index}>
                                        {(provided, snapshot) => (
                                            <ListItemButton
                                                ref={provided.innerRef}
                                                className={styles.customListItem}
                                                {...provided.draggableProps}
                                                {...provided.dragHandleProps}
                                                key={layer.id}
                                            >

                                                <ListItemText
                                                    primary={getTitlePersonalization(layer)}
                                                    secondary={`${layer.name}`}
                                                />

                                                <ListItemIcon>
                                                    <DragIndicatorIcon />
                                                </ListItemIcon>
                                            </ListItemButton>
                                        )}
                                    </Draggable>
                                );
                            })}
                        </CustomList>
                    )}
                </Droppable>
            </DragDropContext>
        </>
    );
};

export default ContentReOrder;
const CustomList = styled(List)<{ component?: React.ElementType }>({
    "& .MuiListItemButton-root": {
        backgroundColor: "#f2f2f2",
        border: "1px solid #d3d3d3",
    },
    "& .MuiListItemIcon-root": {
        minWidth: 0,
        marginRight: 16,
    },
    "& .MuiSvgIcon-root": {
        fontSize: 20,
    },
});
