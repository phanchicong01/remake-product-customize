
import {  useMemo } from "react";
import Box from "@mui/material/Box";
import { useAppSelector } from "hooks";

import sortBy from "lodash/sortBy"
import cloneDeep from "lodash/cloneDeep"

import TabsTemplate from "../TabsTemplate";
import { Layer } from "store/template-reducer/interface";
import { checkShowingConfig } from "../constants";

import ItemShowOptionByLayer from "../component/item-show-option-by-layer";

import styles from "./styles.module.scss";


const ContentPreview = () => {
    const { layers } = useAppSelector(state => state.template.templateActive)

    const isShowConfig = layers.some((layer: any) => {
        return checkShowingConfig(layer)
    })
    const layerActiveConfig: Layer[] = useMemo(() => {
        // setLayerActiveConfig(layers.filter((layer) => (checkShowingConfig(layer))))
        const result = layers.map((layer: Layer, index: number) => {
            const newLayer = { ...layer }
            if (checkShowingConfig(layer) && layer.configuration.reOrder === null) {
                const newLayer = cloneDeep(layer)
                newLayer.configuration['reOrder'] = index + 1
                return { ...newLayer }
            }
            if (checkShowingConfig(newLayer)) {
                return newLayer
            }
            return null
        }).filter(layer => !!layer)
        return sortBy(result, o => o.configuration.reOrder)

    }, [layers])

    return <Box
        sx={{
            backgroundColor: "#f2f2f2",
            border: "1px solid #f4f4f4",
            p: 1,
            mt: 3,
            textAlign: "left",
        }}
    >
        <TabsTemplate />
        {
            layerActiveConfig?.map((layer: Layer) => {
                return <div key={layer.id}>
                    <ItemShowOptionByLayer layer={layer} />
                </div>
            })
        }
        {
            !isShowConfig && <span className={`p-3 ${styles.textEmpty}`}>
                No options to preview yet.
            </span>
        }

    </Box>
};

export default ContentPreview;
