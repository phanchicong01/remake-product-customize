import React, { useEffect } from "react";
import IconButton from "@mui/material/IconButton";
import Menu from "@mui/material/Menu";
import ToggleButton from "@mui/material/ToggleButton";
import ToggleButtonGroup from "@mui/material/ToggleButtonGroup";

import MoreVertIcon from "@mui/icons-material/MoreVert";
import FormatAlignLeftIcon from "@mui/icons-material/FormatAlignLeft";
import FormatAlignCenterIcon from "@mui/icons-material/FormatAlignCenter";
import FormatAlignRightIcon from "@mui/icons-material/FormatAlignRight";
import FormatAlignJustifyIcon from "@mui/icons-material/FormatAlignJustify";

const toggleData = [
    {
        icon: <FormatAlignLeftIcon />,
        value: "left",
        name: "align left",
    },
    {
        icon: <FormatAlignCenterIcon />,
        value: "center",
        name: "align center",
    },
    {
        icon: <FormatAlignRightIcon />,
        value: "right",
        name: "align right",
    },
    {
        icon: <FormatAlignJustifyIcon />,
        value: "justify",
        name: "align justify",
    },
];

const MenuAlign = ({ data, onChangeAttr }: any) => {
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const [iconActive, setActive] = React.useState<any>(<MoreVertIcon />);
    const open = Boolean(anchorEl);
    const handleClick = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleChangeAlign = (event: React.MouseEvent<HTMLElement>, newAlignment: string) => {
        const newAttrs = { ...data, align: newAlignment };

        getItemActive(newAlignment, (item) => item && setActive(item.icon));
        onChangeAttr(newAttrs);
    };

    const getItemActive = (value, callback) => {
        const index = toggleData.findIndex((item) => item.value === value);

        if (index !== -1) {
            callback(toggleData[index]);
        }
    };

    useEffect(() => {
        getItemActive(data.align, (item) => item && setActive(item.icon));
        // eslint-disable-next-line
    }, []);

    return (
        <div>
            <IconButton
                aria-label="more"
                id="long-button"
                aria-controls="long-menu"
                aria-expanded={open ? "true" : undefined}
                aria-haspopup="true"
                onClick={handleClick}
            >
                {iconActive}
            </IconButton>
            <Menu
                id="long-menu"
                MenuListProps={{
                    "aria-labelledby": "long-button",
                }}
                anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "left",
                }}
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                PaperProps={{
                    style: {
                        //   maxHeight: ITEM_HEIGHT * 4.5,
                        width: "40ch",
                        //   display:"flex",
                    },
                }}
            >
                <ToggleButtonGroup size="small" value={data.align} onChange={handleChangeAlign} exclusive={true}>
                    {toggleData.map((toggleItem, index: number) => (
                        <ToggleButton value={toggleItem.value} key={toggleItem.value}>
                            {toggleItem.icon}
                        </ToggleButton>
                    ))}
                    {/* <ToggleButton value="left" key="left">
                    <FormatAlignLeftIcon />
                </ToggleButton>,
                <ToggleButton value="center" key="center">
                    <FormatAlignCenterIcon />
                </ToggleButton>,
                <ToggleButton value="right" key="right">
                    <FormatAlignRightIcon />
                </ToggleButton>,
                <ToggleButton value="justify" key="justify">
                    <FormatAlignJustifyIcon />
                </ToggleButton>, */}
                </ToggleButtonGroup>
            </Menu>
        </div>
    );
};

export default MenuAlign;
