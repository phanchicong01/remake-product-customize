import React from "react";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";

import { roughScale } from "utils";
import { styled } from "@mui/styles";
import { useAppDispatch } from "hooks";
import { onUpdatePositionLayerSelected } from "store/template-reducer/templateSlice";
import { PositionLayer } from "constants/enum";

interface IProps {
    data: any;
    onChangeAttr?: (data) => void;
}
const Position = ({ data, onChangeAttr }: IProps) => {
    const dispatch = useAppDispatch();

    const handleChangeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        const name = event.target.name as PositionLayer;
        const value = event.target.value;
        // if (name === "strokeWidth" || name === "fontSize") {
        //     const newValue = roughScale(value);
        //     return handleChangeAttrs({ [name]: newValue });
        // }
        const distance = roughScale(value) - data[name]
        return dispatch(onUpdatePositionLayerSelected({ position: name, value: distance }))
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps


    return (
        <Grid item xs={12} sx={{ py: 1, px: 2 }}>
            <Grid container rowGap={2}>
                <TextFieldCustom
                    label="X (px)"
                    id="outlined-x-small"
                    value={roughScale(data?.x)}
                    name="x"
                    style={{ maxWidth: 100 }}
                    sx={{ mr: 1 }}
                    type="number"
                    onChange={handleChangeInput}
                    size="small"
                    inputProps={{ inputMode: "numeric" }}
                />
                <TextFieldCustom
                    label="Y (px)"
                    name="y"
                    sx={{ mr: 1 }}
                    id="outlined-y-small"
                    onChange={handleChangeInput}
                    style={{ maxWidth: 100 }}
                    type="number"
                    value={roughScale(data?.y)}
                    size="small"
                    inputProps={{ inputMode: "numeric" }}
                />
                <TextFieldCustom
                    label="W (px)"
                    id="outlined-w-small"
                    value={roughScale(data?.width)}
                    name="width"
                    style={{ maxWidth: 100 }}
                    sx={{ mr: 1 }}
                    type="number"
                    onChange={handleChangeInput}
                    size="small"
                    inputProps={{ inputMode: "numeric" }}
                />
                <TextFieldCustom
                    label="H (px)"
                    name="height"
                    sx={{ mr: 1 }}
                    id="outlined-h-small"
                    onChange={handleChangeInput}
                    style={{ maxWidth: 100 }}
                    type="number"
                    value={roughScale(data?.height)}
                    size="small"
                    inputProps={{ inputMode: "numeric" }}
                />
                <TextFieldCustom
                    label="rotate (deg)"
                    name="rotation"
                    sx={{ mr: 1 }}
                    id="outlined-r-small"
                    onChange={handleChangeInput}
                    style={{ maxWidth: 100 }}
                    type="number"
                    value={roughScale(data?.rotation)}
                    size="small"
                    inputProps={{ inputMode: "numeric" }}
                />
            </Grid>
        </Grid>
    );
};

export default Position;

const TextFieldCustom = styled(TextField)({
    // '& label.Mui-focused': {
    //   color: 'green',
    // },
    // '& .MuiInput-underline:after': {
    //   borderBottomColor: 'green',
    // },
    "& .MuiOutlinedInput-root": {
        color: "white",
        "& fieldset": {
            borderColor: "white",
        },
        "&:hover fieldset": {
            borderColor: "white",
        },
        "&.Mui-focused fieldset": {
            borderColor: "white",
        },
    },
    "& .MuiInputLabel-root": {
        color: "white",
    },
});
