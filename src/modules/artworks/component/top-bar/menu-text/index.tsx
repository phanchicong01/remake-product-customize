import React, { useState, useCallback } from "react";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";

import Divider from "@mui/material/Divider";

import { roughScale } from "utils";

import styles from "./styles.module.scss";
import MenuFontStyle from "./menu-font-style";
import MenuAlign from "./menu-align";
import { useAppSelector } from "hooks";
import { LayerText } from "store/template-reducer/interface";
import Position from "./postion"
interface IProps {
    data: LayerText;
    onChangeAttr: (data) => void;
}
const MenuText = ({ data, onChangeAttr }: IProps) => {


    const handleChangeAttr = useCallback(
        (dataChange: any) => {
            console.log(dataChange)
        },
        [],
    )


    if (!data) {
        return <div> ...loading </div>;
    }

    return (
        <Grid item xs={12} className={styles.menuText} sx={{ py: 1, px: 2 }}>
            <Grid container rowGap={2}>
                <Position data={data} onChangeAttr={handleChangeAttr} />
            </Grid>
        </Grid>
    );
};

export default MenuText;
