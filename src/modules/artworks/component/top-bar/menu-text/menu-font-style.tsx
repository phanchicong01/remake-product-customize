import React from "react";
import IconButton from "@mui/material/IconButton";

import Stack from "@mui/material/Stack";

import FormatBoldIcon from "@mui/icons-material/FormatBold";
import FormatItalicIcon from "@mui/icons-material/FormatItalic";
import FormatUnderlinedIcon from "@mui/icons-material/FormatUnderlined";

import styles from "./styles.module.scss";

const fontStyles: any[] = [
    {
        icon: <FormatBoldIcon />,
        name: "bold",
        value: "bold",
        key: "fontStyle",
    },
    {
        icon: <FormatItalicIcon />,
        name: "italic",
        value: "italic",
        key: "fontStyle",
    },
    {
        icon: <FormatUnderlinedIcon />,
        name: "underline",
        value: "underline",
        key: "textDecoration",
    },
];

const menuFontStyle = ({ data, onChangeAttr }: any) => {
    const onUpdateFontStyle = (key, value) => {
        const oldFontStyle = data[key] || "";
        if (oldFontStyle?.includes(value)) {
            const newAttrs = { ...data, [key]: oldFontStyle.split(value)[0] };
            onChangeAttr(newAttrs);
        } else {
            const newAttrs = { ...data, [key]: oldFontStyle + " " + value };
            onChangeAttr(newAttrs);
        }
    };

    return (
        <Stack direction="row" spacing={1} sx={{ mr: 1 }}>
            {fontStyles.map((option, index) => {
                return (
                    // <MenuItem key={option} selected={option === 'Pyxis'} onClick={handleClose}>
                    <IconButton
                        key={index}
                        aria-label={option.name}
                        style={{ borderRadius: 5 }}
                        onClick={() => onUpdateFontStyle(option.key, option.value)}
                        className={data[option.key]?.includes(option.value) ? styles.activeIcon : ""}
                    >
                        {option.icon}
                    </IconButton>
                    // </MenuItem>
                );
            })}
        </Stack>
    );
};

export default menuFontStyle;
