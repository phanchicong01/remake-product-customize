import React from 'react'
import IconButton from '@mui/material/IconButton';
import Menu from '@mui/material/Menu';
import Stack from "@mui/material/Stack";
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';

import MoreVertIcon from '@mui/icons-material/MoreVert';
import Divider from '@mui/material/Divider';


import FormatBoldIcon from "@mui/icons-material/FormatBold";
import FormatItalicIcon from "@mui/icons-material/FormatItalic";
import FormatUnderlinedIcon from "@mui/icons-material/FormatUnderlined";
import FormatAlignLeftIcon from '@mui/icons-material/FormatAlignLeft';
import FormatAlignCenterIcon from '@mui/icons-material/FormatAlignCenter';
import FormatAlignRightIcon from '@mui/icons-material/FormatAlignRight';
import FormatAlignJustifyIcon from '@mui/icons-material/FormatAlignJustify';

import styles from "./styles.module.scss";

const MenuComponent = ({data, onChangeAttr}:any) => {
    const fontStyles:any[] = [
        {
            icon:<FormatBoldIcon />,
            name:'bold',
            value:'bold',
            key:'fontStyle'
        }, {
            icon:<FormatItalicIcon />,
            name:'italic',
            value:'italic',
            key:'fontStyle'
        }, {
            icon:<FormatUnderlinedIcon />,
            name:'underline',
            value:'underline',
            key:'textDecoration'
        },
    ]
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

    const open = Boolean(anchorEl);
    const handleClick = (event: React.MouseEvent<HTMLElement>) => {
      setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
      setAnchorEl(null);
    };
    const onUpdateFontStyle = (key , value)=>{
        const oldFontStyle =  data[key] || '';
        if(oldFontStyle?.includes(value)){
            const newAttrs = { ...data, [key]: oldFontStyle.split(value)[0] };
            onChangeAttr(newAttrs)
        }else{
            const newAttrs = { ...data, [key]: oldFontStyle + ' ' +  value };
            onChangeAttr(newAttrs)
        }

    }
    const handleChangeAlign = (
        event: React.MouseEvent<HTMLElement>,
        newAlignment: string,
      ) => {
        const newAttrs = { ...data, align: newAlignment };
        onChangeAttr(newAttrs)
      };
  
    return (
        <div>
        <IconButton
          aria-label="more"
          id="long-button"
          aria-controls="long-menu"
          aria-expanded={open ? 'true' : undefined}
          aria-haspopup="true"
          onClick={handleClick}
        >
          <MoreVertIcon />
        </IconButton>
        <Menu
          id="long-menu"
          MenuListProps={{
            'aria-labelledby': 'long-button',
          }}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          anchorEl={anchorEl}
          open={open}
          onClose={handleClose}
          PaperProps={{
            style: {
            //   maxHeight: ITEM_HEIGHT * 4.5,
              width: '40ch',
            //   display:"flex",
            },
          }}
        >
             <Stack direction="row" spacing={1} sx={{mr:1}}>
                {fontStyles.map((option) =>{
                  return  (
                    // <MenuItem key={option} selected={option === 'Pyxis'} onClick={handleClose}>
                        <IconButton
                                aria-label={option.name}
                                style={{ borderRadius: 5 }}
                                onClick={()=>onUpdateFontStyle(option.key , option.value)}
                                className={data[option.key]?.includes(option.value) ? styles.activeIcon : ""}
                                >
                                    {option.icon}
                            </IconButton>
                    // </MenuItem>
                )
                })}
                <Divider orientation="vertical" flexItem />
                <ToggleButtonGroup size="small" value={data.align} onChange={handleChangeAlign} exclusive={true} >
                    <ToggleButton value="left" key="left">
                        <FormatAlignLeftIcon />
                    </ToggleButton>,
                    <ToggleButton value="center" key="center">
                        <FormatAlignCenterIcon />
                    </ToggleButton>,
                    <ToggleButton value="right" key="right">
                        <FormatAlignRightIcon />
                    </ToggleButton>,
                    <ToggleButton value="justify" key="justify">
                        <FormatAlignJustifyIcon />
                    </ToggleButton>,
                </ToggleButtonGroup>
            </Stack>
        </Menu>
      </div>
    )
}

export default MenuComponent
