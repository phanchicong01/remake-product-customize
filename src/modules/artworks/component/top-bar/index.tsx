import {  useAppSelector } from "hooks";

import Position from "./menu-text/postion";
import styles from "./styles.module.scss";

const TopBar = () => {
    const layerSelected = useAppSelector((state) => state.template.templateActive.layerSelected);

    if (layerSelected.length > 1) {
        return null
    }
    return (
        <div className={styles.topBar}>

            {layerSelected && layerSelected[0] && (
                <Position data={layerSelected[0]} />
            )}
            
        </div>
    );
};

export default TopBar;
