import React, { useState } from 'react'
import IconButton from '@mui/material/IconButton';
import Menu from '@mui/material/Menu';
import Stack from "@mui/material/Stack";
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';

import MoreVertIcon from '@mui/icons-material/MoreVert';
import Divider from '@mui/material/Divider';


import OpenInFullIcon from '@mui/icons-material/OpenInFull';
import FullscreenExitIcon from '@mui/icons-material/FullscreenExit';

import styles from "./styles.module.scss";
import { useAppSelector } from 'hooks';

const MenuComponent = ({ data, onChangeAttr }: any) => {
  const artwork = useAppSelector((state) => state.artwork);
  const [flagFullScreen, setFlagFullScreen] = useState(false);
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const onUpdateFontStyle = (key, value) => {
    const oldFontStyle = data[key] || '';
    if (oldFontStyle?.includes(value)) {
      const newAttrs = { ...data, [key]: oldFontStyle.split(value)[0] };
      onChangeAttr(newAttrs)
    } else {
      const newAttrs = { ...data, [key]: oldFontStyle + ' ' + value };
      onChangeAttr(newAttrs)
    }

  }
  const handleChangeAlign = (
    event: React.MouseEvent<HTMLElement>,
    newAlignment: string,
  ) => {
    const newAttrs = { ...data, align: newAlignment };
    onChangeAttr(newAttrs)
  };
  const handleFullScreen = () => {
    const newAttrs = { ...data, width: artwork.size.width, height: artwork.size.height, x: 0, y: 0 };
    onChangeAttr(newAttrs)
    setFlagFullScreen(true)
  }
  const handleDeScreen = () => {
    const newAttrs = { ...data, width: data.widthImage, height: data.heightImage, x: (artwork.size.width) / 2 - (data.widthImage / 2), y: (artwork.size.height - data.height) / 2 };
    onChangeAttr(newAttrs)
    setFlagFullScreen(false)

  }
  return (
    <div>
      <IconButton
        aria-label="more"
        id="long-button"
        aria-controls="long-menu"
        aria-expanded={open ? 'true' : undefined}
        aria-haspopup="true"
        onClick={handleClick}
      >
        <MoreVertIcon />
      </IconButton>
      <Menu
        id="long-menu"
        MenuListProps={{
          'aria-labelledby': 'long-button',
        }}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        PaperProps={{
          style: {
            //   maxHeight: ITEM_HEIGHT * 4.5,
            width: '40ch',
            //   display:"flex",
          },
        }}
      >
        <Stack direction="row" spacing={1} sx={{ mr: 1 }}>
          {
            !flagFullScreen ?
              <IconButton
                // aria-label={option.name}
                style={{ borderRadius: 5 }}
                onClick={handleFullScreen}
              // className={data[option.key]?.includes(option.value) ? styles.activeIcon : ""}
              >
                <OpenInFullIcon />
              </IconButton> :
              <IconButton
                // aria-label={option.name}
                style={{ borderRadius: 5 }}
                onClick={handleDeScreen}
              // className={data[option.key]?.includes(option.value) ? styles.activeIcon : ""}
              >
                <FullscreenExitIcon />
              </IconButton>
          }


        </Stack>
      </Menu>
    </div>
  )
}

export default MenuComponent
