import React, { useState, useEffect } from "react";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import { styled } from "@mui/styles";

import { roughScale } from "utils";

interface IProps {
    data: any;
    onChangeAttr: (data) => void;
}
const Menu = ({ data, onChangeAttr }: IProps) => {
    const onUpdateData = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = event.target.value;
        const name = event.target.name;
        const newValue = roughScale(value);
        // if (name === "x" || name === "y" || name === "fontSize") {
        //     newValue = roughScale(value);
        // }
        const newAttrs = { ...data, [name]: newValue };
        onChangeAttr(newAttrs);
    };

    return (
        <Grid item xs={12} sx={{ py: 1, px: 2 }}>
            <Grid container rowGap={2}>
                <TextFieldCustom
                    label="X (px)"
                    id="outlined-x-small"
                    value={data?.x || 0}
                    name="x"
                    style={{ maxWidth: 100 }}
                    sx={{ mr: 1 }}
                    type="number"
                    onChange={onUpdateData}
                    size="small"
                />
                <TextFieldCustom
                    label="Y (px)"
                    name="y"
                    sx={{ mr: 1 }}
                    id="outlined-y-small"
                    onChange={onUpdateData}
                    style={{ maxWidth: 100 }}
                    type="number"
                    value={data?.y || 0}
                    size="small"
                />
                <TextFieldCustom
                    label="W (px)"
                    id="outlined-x-small"
                    value={data?.width || 0}
                    name="width"
                    style={{ maxWidth: 100 }}
                    sx={{ mr: 1 }}
                    type="number"
                    onChange={onUpdateData}
                    size="small"
                />
                <TextFieldCustom
                    label="H (px)"
                    name="height"
                    sx={{ mr: 1 }}
                    id="outlined-y-small"
                    onChange={onUpdateData}
                    style={{ maxWidth: 100 }}
                    type="number"
                    value={data?.height || 0}
                    size="small"
                />
                <TextFieldCustom
                    label="rotate (deg)"
                    name="rotation"
                    sx={{ mr: 1 }}
                    id="outlined-y-small"
                    onChange={onUpdateData}
                    style={{ maxWidth: 100 }}
                    type="text"
                    value={data?.rotation || 0}
                    size="small"
                />
            </Grid>
        </Grid>
    );
};

export default Menu;

const TextFieldCustom = styled(TextField)({
    // '& label.Mui-focused': {
    //   color: 'green',
    // },
    // '& .MuiInput-underline:after': {
    //   borderBottomColor: 'green',
    // },
    "& .MuiOutlinedInput-root": {
        color: "white",
        "& fieldset": {
            borderColor: "transparent",
        },
        "&:hover fieldset": {
            borderColor: "white",
        },
        "&.Mui-focused fieldset": {
            borderColor: "white",
        },
    },
    "& .MuiInputLabel-root": {
        color: "white",
    },
});
