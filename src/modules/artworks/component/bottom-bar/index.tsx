import React, { useState, useEffect } from "react";
import { useAppDispatch, useAppSelector } from "hooks";

import ButtonGroup from "@mui/material/ButtonGroup";
import LoadingButton from "@mui/lab/LoadingButton";
import MenuZoom from "./menu-zoom";
import MenuSetting from "./menu-setting-gird";

import styles from "./styles.module.scss";
import { Grid } from "@mui/material";
import { setModal } from "store/modal-reducer/modalSlice";
import { ModalKey } from "constants/enum";
import { useNavigate } from "react-router-dom";
import { ARTWORKS } from "constants/path";
import { resetArtwork } from "store/artwork-reducer/artworkSlice";
import { resetFonts } from "store/fonts-family/fontsFamilySlice";
import { resetPreview } from "store/preview-product/previewProductSlice";
import { resetTemplate } from "store/template-reducer/templateSlice";

const BottomBar = () => {
    const { key } = useAppSelector((state) => state.modal);
    const [loadingSaveData, setLoadingSave] = useState<any>(false);
    const dispatch = useAppDispatch();
    const navigate =  useNavigate()
    const handleSaveData = (data) => {
        setLoadingSave(true);
        dispatch(
            setModal({
                data: true,
                key: ModalKey.SaveData,
            })
        );
    };

    const handleCancel = () => {
        dispatch(resetArtwork())
        dispatch(resetFonts())
        dispatch(resetPreview())
        dispatch(resetTemplate())
        navigate(ARTWORKS)
    };
    
    useEffect(() => {
        if (!key && loadingSaveData) {
            setLoadingSave(false)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [key]);
    return (
        <div className={`${styles.bottomBar}`}>
            <Grid container spacing={1} justifyContent={"left"} alignItems="center">
                <Grid item xs={"auto"} textAlign={"left"}>
                    {" "}
                    <MenuZoom />{" "}
                </Grid>
                <Grid item xs={"auto"} textAlign={"left"}>
                    {" "}
                    <MenuSetting />{" "}
                </Grid>

                <Grid item xs={9} textAlign="center" alignItems="center">
                    <ButtonGroup variant="outlined" aria-label="outlined button group">
                        <LoadingButton
                            loading={loadingSaveData}
                            onClick={handleSaveData}
                            size="small"
                            variant="contained"
                        >
                            Save
                        </LoadingButton>
                        <LoadingButton onClick={handleCancel} size="small">
                            Cancel
                        </LoadingButton>
                    </ButtonGroup>
                </Grid>
                {/* )} */}
            </Grid>
        </div>
    );
};

export default BottomBar;
