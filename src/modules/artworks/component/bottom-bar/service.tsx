import { callApiFuc } from "utils/fetcher";

export const saveDataArtworkService = async (data: any): Promise<any> => {
    const response = await callApiFuc("/api/artworks", "POST", data)
        .then((rs) => {
            return rs.data;
        })
    return response;
};
export const updateDataArtworkService = async (data: any, id: any): Promise<any> => {
    const response = await callApiFuc(`/api/artworks/${id}`, "PUT", data)
        .then((rs) => {
            return rs.data;
        })

    return response;
};
