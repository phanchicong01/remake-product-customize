import React from "react";
import Menu from "@mui/material/Menu";
// import ToggleButton from "@mui/material/ToggleButton";

import SettingsIcon from "@mui/icons-material/Settings";
import Switch from "@mui/material/Switch";
import IconButton from "@mui/material/IconButton";
import Card from "@mui/material/Card";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import { useAppDispatch, useAppSelector } from "hooks";
import {updateArtwork} from "store/artwork-reducer/artworkSlice";

const toggleData = ["show gird"];

const MenuSetting = () => {
    const artwork = useAppSelector((state) => state.artwork);
    const dispatch = useAppDispatch();
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

    const open = Boolean(anchorEl);
    const handleClick = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleChangeSwitch = (event: React.ChangeEvent<HTMLInputElement>) => {
        const checked = event.target.checked;
        const newAttrs = {  setting:{...artwork.setting, showGrid: checked } };
        dispatch(updateArtwork(newAttrs));
    };

    return (
        <Card sx={{ backgroundColor: "#595959" }}>
            <IconButton
                aria-label="more"
                id="menu-setting-button"
                aria-controls="long-menu"
                aria-expanded={open ? "true" : undefined}
                aria-haspopup="true"
                onClick={handleClick}
            >
                <SettingsIcon sx={{ color: "white" }} fontSize="small" />
            </IconButton>
            <Menu
                id="menu-setting"
                MenuListProps={{
                    "aria-labelledby": "long-button",
                }}
                anchorOrigin={{
                    vertical: "top",
                    horizontal: "center",
                }}
                transformOrigin={{
                    vertical: "bottom",
                    horizontal: "center",
                }}
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                PaperProps={{
                    style: {
                        // maxHeight: ITEM_HEIGHT * 4.5,
                        width: "auto",
                        //   display:"flex",
                    },
                }}
            >
                {toggleData.map((toggleItem: string, index: number) => (
                    <FormGroup key={index}>
                        <FormControlLabel
                            control={<Switch checked={!!artwork.setting?.showGrid} onChange={handleChangeSwitch} />}
                            label={toggleItem}
                        />
                    </FormGroup>
                ))}
            </Menu>
        </Card>
    );
};

export default MenuSetting;
