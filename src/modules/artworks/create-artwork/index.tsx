import  {  useCallback, useEffect,  useState } from "react";
import _isEmpty from "lodash-es/isEmpty"



import { useAppDispatch, useAppSelector } from "hooks";

import { onRemoveLayerByID, onUpdateInitDataTemplate, onUpdatePositionLayerSelected } from "store/template-reducer/templateSlice";

import { useParams } from "react-router-dom";
import { createCustomFonts, intiFontsSelected } from "store/fonts-family/fontsFamilySlice";
import { updateArtwork } from "store/artwork-reducer/artworkSlice"
import { Layer } from "store/template-reducer/interface";
import { PositionLayer, TypeLayer } from "constants/enum";
import { IFontSelected } from "store/fonts-family/interface";
import FontFamilyImport from "components/font-family";
import useFonts from "data/hooks/user-get-fonts";
import useDetailArtwork from "data/hooks/use-detail-artwork";
import TopBar from "../component/top-bar";
import RightSidebarComponent from "../component/right-sidebar";
import BottomBar from "../component/bottom-bar";
import ArtworkComponent from "components/widgets/artwork-konva";
import LeftSideBar from "components/widgets/artwork-konva/left-sidebar";
import EditSidebar from "components/widgets/artwork-konva/edit-sidebar";

declare module "@mui/material/styles" {
    interface Palette {
        dimgray: Palette["primary"];
    }

    // allow configuration using `createTheme`
    interface PaletteOptions {
        dimgray?: PaletteOptions["primary"];
    }
}

// Update the Button's color prop options
declare module "@mui/material/Button" {
    interface ButtonPropsColorOverrides {
        dimgray: true;
    }
}

const Home = () => {

    const [openPreview, setOpenPreview] = useState(false);

    const drawerMenu = useAppSelector((state) => state.drawerMenu);
    const { customFonts  } = useAppSelector((state) => state.fontsFamily);
    const { layers, layerSelected } = useAppSelector((state) => state.template.templateActive);

    const params = useParams();
    const dispatch = useAppDispatch();

    const handleRemoveObject = (idLayer) => {
        dispatch(onRemoveLayerByID({ idLayer }));
    };
    const handleUpdatePosition = (position: PositionLayer, value: number) => {
        dispatch(onUpdatePositionLayerSelected({ position, value }));
    };
    const handleEventListener = () => {
        window.addEventListener("keydown", (event: any) => {
            const layerActive = window.layerActive;
            if (!!event.target.value) {
                return;
            }
            switch (event.code) {
                case "Delete":
                    return layerActive.map(item => handleRemoveObject(item.id))
                case "ArrowRight":
                    return handleUpdatePosition(PositionLayer.X, +1)
                case "ArrowLeft":
                    return handleUpdatePosition(PositionLayer.X, -1)
                case "ArrowDown":
                    return handleUpdatePosition(PositionLayer.Y, +1)
                case "ArrowUp":
                    return handleUpdatePosition(PositionLayer.Y, -1)

                default:
                    return;
            }
        })

    }
    const handleOpenSideBar = (flag) => {
        return setOpenPreview(flag)
    }
    const {data:dataFonts , isFetched:isFetchedFonts} = useFonts();

    useEffect(() => {
        if(isFetchedFonts && !_isEmpty(dataFonts)){
            const newRes = dataFonts.map((item) => { 
            return {
                ...item,
                family: item.name,
                kind: "customfonts#customfontList",
            };
        });
        dispatch(createCustomFonts(newRes));
    }
    }, [dataFonts, dispatch, isFetchedFonts]) 
    
       

    const {data:DataArtworkDetail , isFetched:isFetchedArtworkDetail , isLoading:isLoadingArtworkDetail} = useDetailArtwork(parseInt(params.idArtwork));
   
    const handleImportFont = useCallback((listFontSelected) => {
        const listGoogleFontSelected = []
        const listCustomFontSelected = []
        listFontSelected.forEach( (layer: Layer) => {
            if (layer.fontKind === 'customfonts#customfontList') {
                const findItem = customFonts?.items?.find(item => layer.fontFamily === item.family);
                if(!_isEmpty(findItem)){
                    listCustomFontSelected.push(findItem)
                }
            }
            if (layer.fontKind === 'webfonts#webfontList') {
                listGoogleFontSelected.push(layer.fontFamily)
            }

        })
        const fontSelected: IFontSelected = {
            customFonts: listCustomFontSelected,
            googleFonts: listGoogleFontSelected
        }
        dispatch(intiFontsSelected(fontSelected))
    }, [customFonts?.items, dispatch])


    useEffect(()=>{
        if(isFetchedArtworkDetail && !_isEmpty(DataArtworkDetail)){
            const data = DataArtworkDetail
            try {
                let parseDataContent = JSON.parse(data.content);
                const initTemplates = parseDataContent?.templates

                if (typeof parseDataContent === "string") {
                    parseDataContent = JSON.parse(parseDataContent)
                }
                if(initTemplates?.templates?.length > 0){
                    const firstTemplate = initTemplates?.templates[0];
                    if(!!firstTemplate){
                        const listFontSelected = firstTemplate.layersActive?.filter((layer: Layer) => layer.type === TypeLayer.Text);
                        handleImportFont(listFontSelected)
                    }
                }
                dispatch(updateArtwork(parseDataContent))
                
                dispatch(onUpdateInitDataTemplate(initTemplates))
            } catch (error) {
                console.error(error);
            }
        }
    }, [DataArtworkDetail, dispatch, handleImportFont, isFetchedArtworkDetail])

    useEffect(() => {
        handleEventListener();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        window.layers = layers;
        window.layerActive = layerSelected
    }, [layers, layerSelected]);

    if (!isFetchedArtworkDetail && isLoadingArtworkDetail ) {
        return (
            <div
                style={{
                    position: "fixed",
                    top: 0,
                    height: "100vh",
                    width: "100%",
                    left: 0,
                    background: "#f2f2f2",
                    justifyContent: "center",
                    display: "flex",
                    alignItems: "center",
                }}
            >
                Loading....
            </div>
        );
    }

    return (
            <div className="App">
                <FontFamilyImport />
                {
                    // (isReadyFont) &&
                    <div className="page d-flex flex-row flex-column-fluid">
                      <div className=" w-100" style={{ backgroundColor: "#f2f2f2" }}>
                        <div className="row g-0">
                            <div className="col-lg-3"  
                                style={{height:"auto",  overflow:"auto",
                                position: "relative" }}>
                                <div>{drawerMenu.open ? <EditSidebar /> : <LeftSideBar />}</div>
                            </div>
                            <div className="col-lg-9 col-xs-8" >
                                <div className={"row g-0"} style={{ background: "#232323" }}>
                                    <div className={`col-lg-${openPreview ?  8 : 12}`} style={{height:"100vh", position: "relative" }}>
                                        <TopBar />
                                        <ArtworkComponent />
                                        <BottomBar />
                                    </div>
                                    <div className={`col-lg-${openPreview ?  4 : 0}`} >
                                        <RightSidebarComponent visble={openPreview} openSidebar={handleOpenSideBar} />
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                }
                
            </div>
    );
};

export default Home;
