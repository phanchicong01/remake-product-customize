import  {   useEffect } from "react";
import _isEmpty from "lodash-es/isEmpty"


import { useAppDispatch } from "hooks";


import { useParams } from "react-router-dom";
import useMockupDetail from "data/hooks/use-mockup-detail";
import { onInitMockup } from "store/mockup-reducer/mockupSlice";
import LeftSideBar from "components/widgets/mockup-konva/component/left-sidebar";
import BottomBar from "components/widgets/mockup-konva/component/bottom-bar";
import MockupKonva from "components/widgets/mockup-konva"


const EditMockupKonva = () => {
    const params = useParams();
    const dispatch = useAppDispatch();

    const {data:DataMockupDetail , isFetched:isFetchedMockupDetail , isLoading:isLoadingMockupDetail} = useMockupDetail(parseInt(params.idMockup));
   
    useEffect(()=>{
        if(isFetchedMockupDetail && !_isEmpty(DataMockupDetail)){
            const data = DataMockupDetail
            try {
                let parseDataContent = JSON.parse(data.content);

                if (typeof parseDataContent === "string") {
                    parseDataContent = JSON.parse(parseDataContent)
                }
             
                dispatch(onInitMockup(parseDataContent))
            } catch (error) {
                console.error(error);
            }
        }
    }, [DataMockupDetail, dispatch, isFetchedMockupDetail])

    if (!isFetchedMockupDetail && isLoadingMockupDetail ) {
        return (
            <div
                style={{
                    position: "fixed",
                    top: 0,
                    height: "100vh",
                    width: "100%",
                    left: 0,
                    background: "#f2f2f2",
                    justifyContent: "center",
                    display: "flex",
                    alignItems: "center",
                }}
            >
                Loading....
            </div>
        );
    }
    return (
            <div className="App">
                {
                    // (isReadyFont) &&
                    <div className="page d-flex flex-row flex-column-fluid">
                      <div className=" w-100" style={{ backgroundColor: "#f2f2f2" }}>
                        <div className="row g-0">
                            <div className="col-lg-3"  
                                style={{height:"100vh",
                                position: "relative" }}>
                                 <LeftSideBar />
                            </div>
                            <div className="col-lg-9 col-xs-8" >
                                <div className={"row g-0"} style={{ background: "#232323" }}>
                                    <div className={`w-100 d-flex  align-items-center justify-content-center`} style={{height:"100vh", position: "relative" }}>
                                        <MockupKonva />
                                        <BottomBar />
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                }
            </div>
    );
};

export default EditMockupKonva;
