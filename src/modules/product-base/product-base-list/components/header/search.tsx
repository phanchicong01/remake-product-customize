/* eslint-disable react-hooks/exhaustive-deps */
import { useState,  useCallback } from 'react'
import {  KTSVG } from 'helpers'
import _debounce from "lodash-es/debounce"

import { useQueryRequest } from '../../core/QueryRequestProvider'


const ProductSearchComponent = () => {
  const {query , updateQuery} = useQueryRequest()
  const [searchTerm, setSearchTerm] = useState<string>(query.search);

  const handleChangeSearchValue = useCallback(
    _debounce((value) => {
      updateQuery({search:value})
    }, 500),
    [],
  )

  const handleChangeInputSearch = (event:React.ChangeEvent<HTMLInputElement>) => {
    const  {value} = event.target
    setSearchTerm(value);
    handleChangeSearchValue(value)
  }
  return (
    <div className='card-title'>
      {/* begin::Search */}
      <div className='d-flex align-items-center position-relative my-1'>
        <KTSVG
          path='/media/icons/duotune/general/gen021.svg'
          className='svg-icon-1 position-absolute ms-6'
        />
        <input
          type='text'
          data-kt-user-table-filter='search'
          className='form-control form-control-solid w-100 ps-14'
          placeholder='Search Product Base'
          value={searchTerm}
          onChange={handleChangeInputSearch}
        />
      </div>
      {/* end::Search */}
    </div>
  )
}

export { ProductSearchComponent }
