import { KTSVG } from 'helpers'
import { useListView } from '../../core/ListViewProvider';

const ProductBaseToolbar = () => {

  const {setModalData} = useListView()

  const handleProductBase = () => {
    setModalData({
      visible:true,
      data:null
    })
  }

  return (
    <div className='d-flex justify-content-end' data-kt-user-table-toolbar='base'>
      {/* <UsersListFilter /> */}
   
      {/* begin::Add user */}
      {
        <button type='button' className='btn btn-primary' onClick={handleProductBase} >
        <KTSVG path='/media/icons/duotune/arrows/arr075.svg' className='svg-icon-2' />
        Create New
      </button>
      }
     
      {/* end::Add user */}
    </div>
  )
}

export { ProductBaseToolbar }
