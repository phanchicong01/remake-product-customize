

import { ProductSearchComponent } from './search'
import { ProductBaseToolbar } from './toolbar'


const ProductBaseHeader = () => {
  
  return (
    <div className='card-header border-0 pt-6'>
      <div className='d-flex flex-column'>
        <ProductSearchComponent />
      </div>
      {/* begin::Card toolbar */}
      <div className='card-toolbar'>
        {/* begin::Group actions */}
        <div className='d-flex '>
          <ProductBaseToolbar />
        </div>
        {/* <ClipartToolbar /> */}
        {/* end::Group actions */}
      </div>
      {/* end::Card toolbar */}
    </div>
  )
}

export { ProductBaseHeader }
