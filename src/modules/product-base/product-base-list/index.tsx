
import { ProductBaseHeader } from './components/header';
import {ProductBaseTable} from './table/ProductBaseTable';

import { PageLink, PageTitle } from 'layout/core';
import { ModalCreateProductBase } from '../component/modal-create-product-base';

type Props = {}

const breadcrumbs: Array<PageLink> = [
    {
        title: 'Home',
        path: '/',
        isSeparator: false,
        isActive: false,
        isRedirect: true
    },
    {
        title: '',
        path: '',
        isSeparator: true,
        isActive: false,
    },
]

const ListProductBase = (props: Props) => {
    return (
        <>
            <PageTitle breadcrumbs={breadcrumbs}>Product Base</PageTitle>
            <div className='card'>
                <div className='card-body'>
                    <div className='row'>
                        <div className='col-lg-12'>
                            <ProductBaseHeader />
                        </div>
                        
                        <div className='col-lg-12'>
                            <ProductBaseTable />
                        </div>
                    </div>
                    <ModalCreateProductBase  />
                </div>
            </div>
        </>
    )
}

export default ListProductBase