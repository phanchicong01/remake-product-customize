/* eslint-disable react-hooks/exhaustive-deps */
import { FC, useContext} from 'react'

import { useQueryRequest } from './QueryRequestProvider'
import useProductBases from 'data/hooks/use-product-bases'
import { getTotalPage } from 'helpers'
import { ProductBaseResponse } from 'apis/get-product-bases/interface'
import { createResponseContext, initialQueryResponse } from 'helpers/crud-helper/models-v2'


const QueryResponseContext = createResponseContext<ProductBaseResponse>(initialQueryResponse)
const QueryResponseProvider: FC = ({ children }) => {
  const { query } = useQueryRequest()
  const {isRefetching, refetch, data:response, isFetched , isLoading } = useProductBases(query);

  const totalProductBase = response?.data?.total || 0
  const totalPage = getTotalPage(totalProductBase, query.per_page);
  return (
    <QueryResponseContext.Provider value={{ 
      refetch,
      isRefetching,
      isLoading,
      isFetched,
      totalPage,
      response
      }}>
      {children}
    </QueryResponseContext.Provider>
  )
}

const useQueryResponse = () => useContext(QueryResponseContext)

const useQueryResponseData = () => {
  const { response } = useQueryResponse();
  if (!response) {
    return []
  }
  if(Array.isArray(response?.data)){
    return response?.data
  }
  
  return response?.data?.data || []
}

export {
  QueryResponseProvider,
  useQueryResponse,
  useQueryResponseData
}
