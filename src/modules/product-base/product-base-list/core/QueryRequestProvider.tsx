import {FC, createContext, useContext, useMemo, useCallback} from 'react'
import _pickBy from 'lodash-es/pickBy';
import _identity from 'lodash-es/identity';

import { ProductBaseRequest } from 'apis/get-product-bases/interface'
import { createSearchParams, useLocation, useNavigate, useSearchParams } from 'react-router-dom'
import { initialQueryState } from 'helpers/crud-helper/models-v2';


type QueryRequestContextProps = {
  query: ProductBaseRequest,
  updateQuery:(updateData:Partial<ProductBaseRequest>)=>void
}
const QueryRequestContext = createContext<QueryRequestContextProps>({
  query:initialQueryState,
  updateQuery:(updateData:Partial<ProductBaseRequest>)=>{}
})

const QueryRequestProvider: FC = ({children}) => {
  const [searchParams] = useSearchParams();
  const navigate = useNavigate();
  const location = useLocation()
  
  const query: ProductBaseRequest = useMemo(() => {
    return {
      search: searchParams.get('search'),
      page: searchParams.get('page') ? parseInt(searchParams.get('page')) : 1,
      per_page: searchParams.get('per_page') ? parseInt(searchParams.get('per_page')) : 10
    }
  }, [searchParams])
  
  const updateQuery = useCallback(
    (updateData: Partial<ProductBaseRequest>) => {
      const newArtworkQuery = _pickBy(
        {
          ...query,
          ...updateData
        },
        _identity()
      )
      navigate({
        pathname: location.pathname,
        search: `?${createSearchParams(newArtworkQuery)}`,
      });
    },
    [location.pathname, navigate, query],
  )
  return (
    <QueryRequestContext.Provider value={{query , updateQuery}}>
      {children}
    </QueryRequestContext.Provider>
  )
}

const useQueryRequest = () => useContext(QueryRequestContext)
export {QueryRequestProvider, useQueryRequest}
