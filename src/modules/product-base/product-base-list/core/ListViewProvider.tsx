import {FC, useState, createContext, useContext, useMemo} from 'react'
import {
  ID,
  calculatedGroupingIsDisabled,
} from 'helpers'
import { useQueryResponse, useQueryResponseData } from './QueryResponseProvider'
import { initialListView, ListViewContextProps } from 'helpers/crud-helper/models-v2'

const ListViewContext = createContext<ListViewContextProps>(initialListView)

const ListViewProvider: FC = ({children}) => {
  const [modalData, setModalData] = useState<{
    visible:boolean,
    data:ID
  }>({
    visible:false,
    data:null
  })
  const {isLoading} = useQueryResponse()
  const data = useQueryResponseData()
  const disabled = useMemo(() => calculatedGroupingIsDisabled(isLoading, data), [isLoading, data])

  return (
    <ListViewContext.Provider
      value={{
        modalData,
        disabled,
        setModalData,
      }}
    >
      {children}
    </ListViewContext.Provider>
  )
}

const useListView = () => useContext(ListViewContext)

export {ListViewProvider, useListView}
