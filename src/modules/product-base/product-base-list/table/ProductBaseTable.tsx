import {  useMemo } from 'react'
import { useTable, ColumnInstance, Row } from 'react-table'
import { CustomHeaderColumn } from './columns/CustomHeaderColumn'
import { CustomRow } from './columns/CustomRow'
import { productBaseColumns } from './columns/_columns'
import {  KTCardBody } from 'helpers'
import PaginationCommon from 'components/common/pagination-common'
import Loading from 'components/common/loading-metronic-style'
import { ProductBase } from 'interfaces/product-base'
import { useQueryResponse, useQueryResponseData } from '../core/QueryResponseProvider'
import { useQueryRequest } from '../core/QueryRequestProvider'

const ProductBaseTable = () => {
  const data = useQueryResponseData();
  const {query} = useQueryRequest()
  const {isLoading , totalPage , isFetched} = useQueryResponse()
  

  const columns = useMemo(() => productBaseColumns, [])
  const { getTableProps, getTableBodyProps, headers, rows, prepareRow } = useTable({
    columns,
    data: data,
  })

  return (
    <KTCardBody className='py-4'>
     
      <div className='table-responsive'>
        <table
          id='kt_table_users'
          className='table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer'
          {...getTableProps()}
        >
          <thead>
            <tr className='text-start text-muted fw-bolder fs-7 text-uppercase gs-0'>
              {headers.map((column: ColumnInstance<ProductBase>) => (
                <CustomHeaderColumn key={column.id} column={column} />
              ))}
            </tr>
          </thead>
          {isFetched && !isLoading && <tbody className='text-gray-600 fw-bold' {...getTableBodyProps()}>
            {rows.length > 0 ? (
              rows.map((row: Row<ProductBase>, i) => {
                prepareRow(row)
                return <CustomRow row={row} key={`row-${i}-${row.id}`} />
              })
            ) : (
              <tr>
                <td colSpan={7}>
                  <div className='d-flex text-center w-100 align-content-center justify-content-center'>
                    No matching records found
                  </div>
                </td>
              </tr>
            )}
          </tbody>}

        </table>
      </div>
      {totalPage > 1 ? <PaginationCommon pathname='admin/users' pageCount={totalPage} forcePage={query.page - 1} /> : ''}

      {isLoading && <Loading   />}
    </KTCardBody>
  )
}

export { ProductBaseTable }
