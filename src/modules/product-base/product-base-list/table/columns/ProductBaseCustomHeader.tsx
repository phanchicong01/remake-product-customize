import clsx from 'clsx'
import {FC, PropsWithChildren, useMemo} from 'react'
import {HeaderProps} from 'react-table'
import { useQueryRequest} from '../../core/QueryRequestProvider'
import { ProductBase } from 'interfaces/product-base'
import { initialQueryState } from 'helpers/crud-helper/models-v2'


type Props = {
  className?: string
  title?: string
  tableProps: PropsWithChildren<HeaderProps<ProductBase>>
}
const ProductBaseCustomHeader: FC<Props> = ({className, title, tableProps}) => {
  const id = tableProps.column.id
  const {query , updateQuery} = useQueryRequest()

  const isSelectedForSorting = useMemo(() => {
    return query.sort && query.sort === id
  }, [query, id]);

  const order: 'asc' | 'desc' | undefined = useMemo(() => query.order, [query])

  const sortColumn = () => {
    // avoid sorting for these columns
    if (id === 'actions' || id === 'selection') {
      return
    }

    if (!isSelectedForSorting) {
      // enable sort asc
      updateQuery({sort: id, order: 'asc', ...initialQueryState})
      return
    }

    if (isSelectedForSorting && order !== undefined) {
      if (order === 'asc') {
        // enable sort desc
        updateQuery({sort: id, order: 'desc', ...initialQueryState})
        return
      }

      // disable sort
      updateQuery({sort: undefined, order: undefined, ...initialQueryState})
    }
  }

  return (
    <th
      {...tableProps.column.getHeaderProps()}
      className={clsx(
        className,
        isSelectedForSorting && order !== undefined && `table-sort-${order}`
      )}
      style={{cursor: 'pointer'}}
      onClick={sortColumn}
    >
      {title}
    </th>
  )
}

export {ProductBaseCustomHeader}
