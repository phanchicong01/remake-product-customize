/* eslint-disable jsx-a11y/anchor-is-valid */
import { FC, useCallback, useEffect } from 'react'
import { MenuComponent } from 'assets/ts/components'
import { ID, KTSVG } from 'helpers'
import { useNavigate } from 'react-router-dom'
import {  PRODUCT_BASE_DETAIL } from 'constants/path'
import useConfirmDialog from 'hooks/use-confirm-dialog'
import { useMutation } from 'react-query'
import { toast } from 'react-toastify'
import removeProductBase from 'apis/remove-product-base'
import { useQueryResponse } from '../../core/QueryResponseProvider'
import PageLoading from 'components/common/page-loading'

type Props = {
  id: ID
}

const ProductBaseActionsCell: FC<Props> = ({ id }) => {
  const navigate = useNavigate()
  const {refetch} = useQueryResponse()
  const  {getDialogConfirmResult} = useConfirmDialog()

  const useMutationRemove = useMutation((id: ID) =>
  removeProductBase(id),
)
  useEffect(() => {
    MenuComponent.reinitialization()
  }, [])

  const openEditModal = () => {
    navigate(PRODUCT_BASE_DETAIL + "/" + id)
  }

  const handleRemoveItem = useCallback(async ()=>{
    const isConfirmOk = await  getDialogConfirmResult({
      title:"Delete Product Base",
      description:"Are you sure delete this Product Base?"
    })
    if(isConfirmOk){
      try {
       
          const result = await  useMutationRemove.mutateAsync(id);
          if(result.code === 200){
            toast.success(result.message);
            refetch()
          }else{
            toast.error(result.message);
          }
        
      } catch (error) {
        console.error(error)
        toast.error("Something went wrong. Please try again!")
      }
    }
   
  },[getDialogConfirmResult, id, refetch, useMutationRemove])

  return (
    <>
    {
      useMutationRemove.isLoading && <PageLoading />
    }
      <a
        href='#'
        className='btn btn-light btn-active-light-primary btn-sm'
        data-kt-menu-trigger='click'
        data-kt-menu-placement='bottom-end'
      >
        Actions
        <KTSVG path='/media/icons/duotune/arrows/arr072.svg' className='svg-icon-5 m-0' />
      </a>
      {/* begin::Menu */}
      <div
        className='menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4'
        data-kt-menu='true'
      >
        {/* begin::Menu item */}
        <div className='menu-item px-3'>
          <a className='menu-link px-3' onClick={openEditModal}>
            Edit
          </a>
        </div>
        {/* end::Menu item */}

        {/* begin::Menu item */}
        <div className='menu-item px-3'>
          <a
            className='menu-link px-3'
            data-kt-users-table-filter='delete_row'
            onClick={handleRemoveItem}
          >
            Delete
          </a>
        </div>
        {/* end::Menu item */}
      </div>
      {/* end::Menu */}
    </>
  )
}

export { ProductBaseActionsCell }
