import { Column } from 'react-table'
import moment from 'moment'

import { ProductBaseActionsCell } from './ProductBaseActionsCell'
import { ProductBaseCustomHeader } from './ProductBaseCustomHeader'
import { ProductBase } from 'interfaces/product-base'

const productBaseColumns: ReadonlyArray<Column<ProductBase>> = [
 
  {
    Header: (props) => (
      <ProductBaseCustomHeader tableProps={props} title='Name' className='min-w-125px' />
    ),
    id: 'name',
    accessor: 'name',
  },
  {
    Header: (props) => (
      <ProductBaseCustomHeader tableProps={props} title='created at' className='min-w-125px' />
    ),
    id: 'created_at',
    Cell: ({ ...props }) => moment(props.data[props.row.index].created_at).format('dddd, MMMM Do YYYY'),
  },
  {
    Header: (props) => (
      <ProductBaseCustomHeader tableProps={props} title='Actions' className='text-end min-w-100px' />
    ),
    id: 'actions',
    Cell: ({ ...props }) => <ProductBaseActionsCell id={props.data[props.row.index].id} />,
  },
]

export { productBaseColumns }
