/* eslint-disable jsx-a11y/anchor-is-valid */
import clsx from 'clsx'
import { ProductBase } from 'interfaces/product-base'
import {FC} from 'react'
// import {User} from '../../core/_models'
interface newProductBase extends ProductBase {
  initials?:{
    state:string
    label:string
  }
}

type Props = {
  data: newProductBase 
}

const ProductBaseInfoCell: FC<Props> = ({data}) => (
  <div className='d-flex align-items-center'>
    {/* begin:: Avatar */}
    <div className='symbol symbol-circle symbol-50px overflow-hidden me-3'>
      <a href='#'>
        {data.mockup ? (
          <div className='symbol-label'>
            <img src={data.mockup} alt={data.name} className='w-100' />
          </div>
        ) : (
          <div
            className={clsx(
              'symbol-label fs-3',
              `bg-light-${data.initials?.state}`,
              `text-${data.initials?.state}`
            )}
          >
            {data.initials?.label}
          </div>
        )}
      </a>
    </div>
    <div className='d-flex flex-column'>
      <a href='#' className='text-gray-800 text-hover-primary mb-1'>
        {data.name}
      </a>
      <span>{data.name}</span>
    </div>
  </div>
)

export {ProductBaseInfoCell}
