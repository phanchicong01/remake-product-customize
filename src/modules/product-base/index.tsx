import { lazy } from 'react'
import { Route, Routes,  Navigate } from 'react-router-dom'
import { ListViewProvider } from './product-base-list/core/ListViewProvider'
import { QueryRequestProvider } from './product-base-list/core/QueryRequestProvider'
import { QueryResponseProvider } from './product-base-list/core/QueryResponseProvider'

type Props = {}
const ProductBasePageList = lazy(()=>  import('modules/product-base/product-base-list'))
const ProductBasePageDetail = lazy(()=>  import('modules/product-base/product-base-detail'))

const ProductBase = (props: Props) => {
  return (
    <Routes>
    <Route>
    <Route>
      <Route
        path='view/:productBaseId'
        element={
          <ProductBasePageDetail />
        }
      />
    </Route>
      <Route
        path='list'
        element={
          <QueryRequestProvider>
            <QueryResponseProvider>
              <ListViewProvider>
                <ProductBasePageList />
              </ListViewProvider>
            </QueryResponseProvider>
          </QueryRequestProvider>
        }
      />
    </Route>
    

    <Route index element={<Navigate to='list' />} />
  </Routes>
  )
}

export default ProductBase