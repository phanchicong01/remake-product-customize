/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react'
import { Modal } from 'antd'
import { useFormik } from 'formik'
// import {ListsWidget4, ListsWidget5} from '../widgets'
import * as Yup from 'yup'
import clsx from 'clsx'
import _pickBy from "lodash-es/pickBy"
import _identity from "lodash-es/identity"
import { useMutation } from 'react-query'

import { toast } from 'react-toastify'
import { CreateProductBaseRequest } from 'apis/create-product-base/inteface'
import createProductBase from 'apis/create-product-base'
import { ProductBase } from 'interfaces/product-base'
import { useListView } from 'modules/product-base/product-base-list/core/ListViewProvider'
import { useQueryResponse } from 'modules/product-base/product-base-list/core/QueryResponseProvider'

type Props = {
    show?: boolean
    handleClose?: (isReload?:boolean) => void,
    dataCategory?: any[],
    data?: ProductBase
}

const schema = Yup.object().shape({
    name: Yup.string()
        .min(3, 'Minimum 3 symbols')
        .max(50, 'Maximum 50 symbols')
        .required('Name is required'),
})
const ModalCreateProductBase: React.FC<Props> = ({ show, handleClose,  data }) => {
    const {modalData , setModalData}  =  useListView()
    const {refetch} = useQueryResponse()

    const useMutatonCreate = useMutation((payload: CreateProductBaseRequest) => createProductBase(payload))
    const formik = useFormik({
        initialValues: {
            name: data?.name || "",
        },
        validationSchema: schema,
        onSubmit: async (values, { setSubmitting }) => {
            setSubmitting(true)
            try {
                const request = await useMutatonCreate.mutateAsync(
                    _pickBy({
                        name: values.name,
                    } , _identity())
                )
                if (request.code === 200) {
                    toast.success(request.message);
                } else {
                    toast.error(request.message)
                }
            } catch (ex) {
                console.error(ex)
            } finally {
                setSubmitting(true)
                handleCancel(true)
            }
        },

    })
 
    
    const handleCancel =  (flag:boolean) => {
        refetch();
        setModalData({
            visible:false,
            data:null
        })
        formik.resetForm()
    }
    
    return (
        <>
            <Modal
                title={"Create Product Base"}
                visible={modalData.visible}
                // onOk={() => formik.handleSubmit()}
                // confirmLoading={useMutatonCreateClipartCategory.isLoading}
                onCancel={()=>handleCancel(false)}
                destroyOnClose
                footer={null}
            // footer={[

            // ]}
            >

                <form onSubmit={formik.handleSubmit} className='form pb-10' id="form-artwork-category">
                    <div className='col-lg-12 fv-row'>
                        <div className='row'>
                            <label className='col-lg-12 col-form-label required fw-bold fs-6'>Name Product Base</label>
                            <input
                                placeholder='Full name'
                                {...formik.getFieldProps('name')}
                                type='text'
                                name='name'
                                className={clsx(
                                    'form-control form-control-solid mb-3 ',
                                    { 'is-invalid': formik.touched.name && formik.errors.name },
                                    {
                                        'is-valid': formik.touched.name && !formik.errors.name,
                                    }
                                )}
                                autoComplete='off'
                                disabled={formik.isSubmitting}
                            />
                            {formik.touched.name && formik.errors.name && (
                                <div className='fv-plugins-message-container'>
                                    <div className='fv-help-block'>
                                        <span role='alert'>{formik.errors.name}</span>
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>
                  
                    <div className='pt-15'>
                        <button
                            type='reset'
                            onClick={()=>handleCancel(false)}
                            className='btn btn-light me-3'
                            data-kt-users-modal-action='cancel'
                            disabled={formik.isSubmitting}
                        >
                            Discard
                        </button>

                        <button
                            type='submit'
                            className='btn btn-primary'
                            data-kt-users-modal-action='submit'
                            disabled={formik.isSubmitting || !formik.isValid || !formik.touched}
                        >
                            <span className='indicator-label'>Submit</span>
                            {(formik.isSubmitting) && (
                                <span className='indicator-progress'>
                                    Please wait...{' '}
                                    <span className='spinner-border spinner-border-sm align-middle ms-2'></span>
                                </span>
                            )}
                        </button>
                    </div>
                </form>
            </Modal>
        </>
    )
}

export { ModalCreateProductBase }
