import React from 'react'

import { useFormik } from 'formik'
import * as Yup from 'yup'
import clsx from 'clsx'
import { useMutation } from 'react-query'
import { UpdateProductBaseRequest } from 'apis/update-product-base/interface'
import { toast } from 'react-toastify'
import updateProductBase from 'apis/update-product-base'
import { ID } from 'helpers'


type Props = {
    data: any
    productBaseId:ID
}


const schema = Yup.object().shape({
    name: Yup.string()
        .min(3, 'Minimum 3 symbols')
        .max(50, 'Maximum 50 symbols')
        .required('Name is required'),
})

const FormProductBaseInfo = ({ data ,productBaseId}: Props) => {
    const useMutationUpdate = useMutation((dataUpdate:UpdateProductBaseRequest) => updateProductBase(dataUpdate))
    const formik = useFormik({
        initialValues: {
            name: data?.name || "",
        },
        validationSchema: schema,
        onSubmit: async (values, { setSubmitting }) => {
            // setSubmitting(true)
            try {
                const newRequest:UpdateProductBaseRequest = {
                    id:productBaseId,
                    data:{
                        name: values.name
                    }
                }
                const request = await useMutationUpdate.mutateAsync(newRequest)
                if (request.code === 200) {
                    toast.success(request.message);
                } else {
                    toast.error(request.message)
                }
            } catch (ex) {
                console.error(ex)
            } finally {
                setSubmitting(true)
            }
        },

    })
    return (
        <>
            <form onSubmit={formik.handleSubmit} className='form pb-10' id="form-artwork-category">
                <div className='col-lg-12 fv-row'>
                    <div className='row'>
                        <label className='col-lg-12 col-form-label required fw-bold fs-6'>Name Product Base</label>
                        <input
                            placeholder='Full name'
                            {...formik.getFieldProps('name')}
                            type='text'
                            name='name'
                            className={clsx(
                                'form-control form-control-solid mb-3 ',
                                { 'is-invalid': formik.touched.name && formik.errors.name },
                                {
                                    'is-valid': formik.touched.name && !formik.errors.name,
                                }
                            )}
                            autoComplete='off'
                            disabled={formik.isSubmitting}
                        />
                        {formik.touched.name && formik.errors.name && (
                            <div className='fv-plugins-message-container'>
                                <div className='fv-help-block'>
                                    <span role='alert'>{formik.errors.name}</span>
                                </div>
                            </div>
                        )}
                    </div>
                </div>

                <div className='pt-15'>

                    <button
                        type='submit'
                        className='btn btn-primary'
                        data-kt-users-modal-action='submit'
                        disabled={formik.isSubmitting || !formik.isValid || !formik.touched}
                    >
                        <span className='indicator-label'>Submit</span>
                        {(formik.isSubmitting) && (
                            <span className='indicator-progress'>
                                Please wait...{' '}
                                <span className='spinner-border spinner-border-sm align-middle ms-2'></span>
                            </span>
                        )}
                    </button>
                </div>
            </form>
        </>
    )
}

export default FormProductBaseInfo