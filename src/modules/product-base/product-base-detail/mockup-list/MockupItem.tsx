import { useMutation } from 'react-query'
import { toast } from 'react-toastify'

import removeMockup from 'apis/remove-mockup'

import { getUrl, ID, KTSVG } from 'helpers'
import useConfirmDialog from 'hooks/use-confirm-dialog'
import { Mockup } from 'interfaces/product-base'

type Props = {
  data:Mockup,
  refetch:()=>void
}

const MockupItem = ({data , refetch}: Props) => {
  const { getDialogConfirmResult } = useConfirmDialog();

  const useMutationRemove = useMutation((id: ID) => removeMockup(id))

  const handleDeleteMockup = async () => {
    const isConfirmed = await getDialogConfirmResult({
      title: "Remove Mockup",
      description: "Are you sure delete this item?"
    })
    if (isConfirmed) {
      try {
        const result = await useMutationRemove.mutateAsync(data.id);
        if (result.code === 200) {
          toast.success(result.message)
          refetch()
        } else {
          toast.error(result.message)
        }

      } catch (error) {
        console.error(error)
        toast.error("Something went wrong. Please try again!")
      }
    }
  }
  let  content = null;
  try {
    content = JSON.parse(data?.content)
  } catch (error) {
    console.error(error)
  }

  return (
    <div className='card card-bordered shadow-sm h-100' style={{}}>
    <div className='card-body p-2 d-flex align-items-center'>
      <img className='card-img-top' src={getUrl(content.thumbnail)} alt="" />
    </div>
    <div className='card-footer p-2'>
      <div className='d-flex g-2 justify-content-end'>

        <a href={'/admin/fe-mockups/edit/' + data.id}  >

          <KTSVG
            path='/media/icons/duotune/art/art005.svg'
            className='svg-icon-3 w-24 h-24'
          />
        </a>
        {
          useMutationRemove.isLoading ?
            <div className="spinner-border text-primary" role="status">
            </div> :
            <a onClick={handleDeleteMockup}>
              <KTSVG path='/media/icons/duotune/general/gen027.svg'
                className='svg-icon-3' />
            </a>
        }
      </div>

    </div>
  </div>
  )
}

export default MockupItem