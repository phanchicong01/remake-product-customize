import React from 'react'

import { useParams } from 'react-router-dom'
import useMockupList from 'data/hooks/use-mockup-list'
import { Mockup } from 'interfaces/product-base'
import MockupItem from './MockupItem'


type Props = {
}


const MockupList = (props: Props) => {
  const { productBaseId } = useParams();


  const { data, isLoading, isFetched, refetch } = useMockupList({
    productBaseId: parseInt(productBaseId)
  });


  return (
    <div className='row align-items-stretch'>
      {
        isFetched && !isLoading && data?.map((item: Mockup) => {
          
          return <div className='col-lg-2'>
           <MockupItem data={item} refetch={refetch}/>
          </div>
        })
      }
    </div>
  )
}

export default MockupList