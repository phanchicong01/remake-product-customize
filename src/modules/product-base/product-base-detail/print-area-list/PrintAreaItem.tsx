import removePrintArea from 'apis/remove-print-area'
import updatedPrintArea from 'apis/update-print-area'
import { UpdatePrintAreaRequest } from 'apis/update-print-area/inteface'
import InputEdit from 'components/common/Input-edit'
import { ID } from 'helpers'
import useConfirmDialog from 'hooks/use-confirm-dialog'
import { PrintArea } from 'interfaces/product-base'
import React from 'react'
import { useMutation } from 'react-query'
import { toast } from 'react-toastify'

type Props = {
    data: PrintArea,
    refetch?: () => void,
}

const PrintAreaItem = ({ data, refetch }: Props) => {
    const { getDialogConfirmResult } = useConfirmDialog();
    
    const useMutationRemove = useMutation((id: ID) => removePrintArea(id))
    const useMutationUpdate = useMutation((payload:UpdatePrintAreaRequest ) => updatedPrintArea(payload))
    
    const handleChangeInputEdit = async (value: any, name: any) => {
        try {
            const newDataUpdate:UpdatePrintAreaRequest = {
                printAreaId:data.id,
                data:{
                    [name]:value
                }
            }
            const result = await useMutationUpdate.mutateAsync(newDataUpdate);
            if (result.code === 200) {
                toast.success(result.message)
                refetch()
            } else {
                toast.error(result.message)
            }
        } catch (error) {
            console.error(error)
            toast.error("Something went wrong. Please try again!")
        }
    }

    const handleRemove = async (event:any) => {
        const isRemove =await getDialogConfirmResult({
            title: "Remove Print Area",
            description: "Are you sure delete this item?"
        })
        if (isRemove) {
            try {
                const result = await useMutationRemove.mutateAsync(data.id);
                if (result.code === 200) {
                    toast.success(result.message)
                    refetch()
                } else {
                    toast.error(result.message)
                }

            } catch (error) {
                console.error(error)
                toast.error("Something went wrong. Please try again!")
            }
        }

    }
    return (
        <div className="row py-3 border-secondary border-bottom">
            <div className='col-lg-4'>
                <InputEdit name='name' value={data.name} onChange={handleChangeInputEdit} isShowConfirm={true} size="sm" />
            </div>
            <div className='col-lg-3'>
                <InputEdit name='width' value={data.width} onChange={handleChangeInputEdit} isShowConfirm={true} size="sm" />
            </div>
            <div className='col-lg-3'>
                <InputEdit name='height' value={data.height} onChange={handleChangeInputEdit} isShowConfirm={true} size="sm" />
            </div>
            <div className='w-auto ml-auto'>
                <div className='row g-2 justify-content-end'>
                        {
                       useMutationRemove.isLoading ? 
                        <div className="spinner-border text-primary" role="status">
                        </div>:
                        <div onClick={handleRemove} className='btn btn-icon btn-active-color-primary btn-sm'>
                      
                        <i role="button" className="w-auto fas fa-trash-alt "></i>
                    </div>
                       }
                  
                    {/* <div className='btn btn-icon btn-active-color-primary btn-sm' >
                        <i role="button" className="w-auto fa fa-grip-vertical"></i>
                    </div> */}
                </div>
            </div>
        </div>
    )
}

export default PrintAreaItem