import { PrintArea } from 'interfaces/product-base'
import React from 'react'
import PrintAreaItem from './PrintAreaItem'

type Props = {
  data:PrintArea[],
  refetch?:()=>void
}

const PrintAreaList = ({data, refetch}: Props) => {
  return (
    <div className='col-lg-12'>
      <div className="row fw-bolder fs-5 border-bottom border-dark pb-2">
        <div className="col-lg-4">Name</div>
        <div className="col-lg-3">Width</div>
        <div className="col-lg-3">Heihgt</div>
        <div className="w-auto font-bold text-right">Action</div>
      </div>
      {
       data.map((item: PrintArea) => {
          return  <PrintAreaItem data={item} key={item.id} refetch={()=>refetch()}/>
        })
      }
    </div>
  )
}

export default PrintAreaList