import createMockup from 'apis/create-mockup'
import uploadFileWithThumbnail from 'apis/upload-file-with-thumbnail'
import Loading from 'components/common/loading-metronic-style'
import PageLoading from 'components/common/page-loading'
import usePrintAreas from 'data/hooks/use-print-areas'
import useProductBaseDetail from 'data/hooks/use-product-base-detail'
import { getUrl} from 'helpers'
import React, { useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { toast } from 'react-toastify'
import { IMockup } from 'store/mockup-reducer/interface'
import FormProductBaseInfo from './component/form-product-base-info'
import MockupList from './mockup-list'
import PrintAreaList from './print-area-list'

type Props = {}

const ProductBaseDetail = (props: Props) => {
    const { productBaseId } = useParams();
    const [loading, setLoading] = useState<boolean>(false);

    const navigate = useNavigate();
    const { data, isLoading, isFetched } = useProductBaseDetail(parseInt(productBaseId));

    const { data: dataPrintArea, isLoading: isLoadingPrintArea, isFetched: isFetchedPrintArea, refetch: refechPrintArea } = usePrintAreas();

    const handleUploadMockup = async (event: React.ChangeEvent<HTMLInputElement>) => {
        setLoading(true);
        const { files } = event.target
        try {
            const newFromData = new FormData();
            newFromData.append("file", files[0])
            newFromData.append("folder", "media")
         
            const response = await uploadFileWithThumbnail(newFromData);
            if (response.code === 200) {
                const { path, thumbnail } = response.data;
                const image = new Image();
                image.src = getUrl(path);
                const dataContent:Partial<IMockup> ={
                    backgroundImage:path,
                    thumbnail:thumbnail,
                    size:{
                        width:350,
                        height:350
                    },
                    productBaseId:parseInt(productBaseId),
                    backgroundVariantColor:false
                }
               const newPromise =  new Promise((rs, rj)=> {
                image.onload = function () {
                    dataContent.size.width = image.width;
                    dataContent.size.height = image.height;
                    rs(dataContent)
                };
            
               }) 
               newPromise.then(async ()=> {
                const resultCreateMockup = await createMockup({
                    content: JSON.stringify(dataContent),
                    is_background_by_variable_color: 0,
                    product_base_id: parseInt(productBaseId)
                })
                if (resultCreateMockup.code === 200) {
                    navigate("/admin/fe-mockups/edit/" + resultCreateMockup.data.id);
                } else {
                    toast.error(resultCreateMockup.message)
                }
                setLoading(false)
               })
               
            }
        } catch (error) {
            console.error(error);
            toast.error("Something")

        }
    }
    return (
        <div>
            <div className='row'>
                {
                    loading && <PageLoading />
                }
                <div className='col-lg-4'>
                    <div className='card'>
                        <div className='card-body position-relative'>
                            {
                                isFetched && !isLoading && <FormProductBaseInfo data={data} productBaseId={parseInt(productBaseId)}/>
                            }
                            {
                                isLoading && <Loading />
                            }
                        </div>
                    </div>
                </div>
                <div className='col-lg-8'>
                    <div className='card mb-4'>
                        <div className='card-header'>
                            <div className='row g-2 w-100  justify-content-between align-items-center'>
                                <h2 className='col'>Mockup</h2>
                                <div className='row w-auto align-items-center'>
                                <input
                                    className='form-control  w-300px'
                                    type='file'
                                    id='file-upload'
                                    multiple
                                    accept='.jpeg,.jpg,.png'
                                    onChange={handleUploadMockup}
                                    disabled={loading}
                                />
                                </div>
                            </div>
                        </div>
                        <div className='card-body'>
                            <MockupList />
                        </div>
                    </div>
                    <div className='card'>
                        <div className="card-header">
                            <div className='row g-2 w-100  justify-content-between align-items-center'>
                                <h2 className='col'>Print Areas</h2>
                            </div>
                        </div>
                        <div className="card-body position-relative">
                            {
                                isFetchedPrintArea && !isLoadingPrintArea &&
                                <PrintAreaList data={dataPrintArea} refetch={refechPrintArea}/>
                            }
                            {isLoadingPrintArea && <Loading />}
                        </div>
                    </div>
                </div>
            </div>
          
        </div>
    )
}

export default ProductBaseDetail