import { useMemo } from 'react'
import _isEmpty from "lodash-es/isEmpty"
import {  useSearchParams } from 'react-router-dom'
import FormCreateClipart from './form-create-clipart'
import { ClipArtItemsTable } from './table/clipart-items-table'
import {  KTSVG } from 'helpers'
import useDetailClipartCategories from 'data/hooks/use-detail-clipart-categories'
import { useClipartCategory } from './context'
import EmptyData from 'components/common/empty-data'
import Loading from 'components/common/loading-metronic-style'

type Props = {}

const ClipartCategories = (props: Props) => {
    const [searchParams] = useSearchParams();
    const { isCreate, onCreate } = useClipartCategory()
    const categroyId = parseInt(searchParams.get("category"))
  
    const { data, isFetched, isLoading, refetch,  isError } = useDetailClipartCategories(categroyId)
    const handleCreate = () => {
        onCreate()
    }

    useMemo(() => {
        if (!isNaN(categroyId)) {
            if (isCreate) {
                onCreate(false)
            }
        }
    }, [isCreate, onCreate, categroyId])



    return (
        <div className='col-lg-9'>
            <div className='card'>
                {
                   (isFetched &&  (_isEmpty(data))) && 
                   <EmptyData title='Not found clipart category'/>
                }
                 {       
                    isLoading &&  <Loading />
                 }
                <div className='card-body'>
                    {
                        !searchParams.get("category") &&
                        <>
                            {
                                isCreate ? <div className='col-lg-6'>
                                    <FormCreateClipart clipart={null} />
                                </div> :
                                    <div className='d-flex justify-content-center align-items-center'>
                                        <span className='me-3'>Select one category on the left or</span>
                                        <button type='button' className='btn btn-primary' onClick={handleCreate} >
                                            <KTSVG path='/media/icons/duotune/arrows/arr075.svg' className='svg-icon-2' />
                                            Create a Clipart
                                        </button>
                                    </div>
                            }
                        </>
                    }
                 
                    {((isFetched && !isLoading  && !_isEmpty(data))) &&
                        <div className='row  justify-content-between'>
                            <div className='col-lg-6 '>
                                <FormCreateClipart clipart={data} />
                            </div>
                            <div className='col-lg-6 rounded-1 py-4' style={{background:"#f1f1f1"}}>
                                <ClipArtItemsTable categoryId={data?.id} isLoading={isLoading} items={data?.items} key={"ClipArtItemsTable"} onRefetch={refetch} />
                            </div>
                        </div>
                    }
                </div>
            </div>
        </div>
    )
}

export default ClipartCategories
export { ClipartCategories }