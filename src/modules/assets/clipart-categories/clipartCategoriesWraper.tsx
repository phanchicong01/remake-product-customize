import React, { useCallback, useMemo, useRef, useState } from 'react'
import { createSearchParams, Outlet, useSearchParams } from 'react-router-dom'
import TreeAntd from 'components/common/tree-antd'
import _isEmpty from 'lodash-es/isEmpty'

import { ClipartHeader } from './components/header/ClipartHeader'
import { useNavigate } from "react-router-dom";
import { ID } from 'helpers'

import useClipartCategories from 'data/hooks/use-clipart-categories'
import Loading from 'components/common/loading-metronic-style'
import { ClipArtCategoryRequest } from 'apis/get-clipart-categories/interface'
import _pickBy from "lodash-es/pickBy"
import _identity from "lodash-es/identity"
import EmptyData from 'components/common/empty-data'

const ClipartCategoriesWrapper = () => {
    const [selected, setSelected] = useState<Array<ID>>([])
  
    const navigate = useNavigate();
    const [searchParams] = useSearchParams();

    const clipartCategoriesQuery: ClipArtCategoryRequest = useMemo(() => {
        return {
          search: searchParams.get('search'),
          category: searchParams.get("category") ? parseInt(searchParams.get("category")) : undefined,
        }
      }, [searchParams])
  
    const {data , isLoading , isFetched, refetch, isRefetching} = useClipartCategories(clipartCategoriesQuery)

  
    const handleOnSelect = useCallback((nodeData) => {
        if(!!nodeData[0]){
            const newClipartCategoriesQuery = _pickBy(
                {
                  ...clipartCategoriesQuery,
                  category:nodeData[0]
                },
                _identity()
              )
            navigate({
                pathname: '/assets/clipart-categories',
                search: `?${createSearchParams(newClipartCategoriesQuery)}`,
              }, {replace:true} );
        }
    }, [clipartCategoriesQuery, navigate])

    const handleOnRefetch = (isRemove) => {
        if(isRemove){
            setSelected([])
        }
        refetch()
    }
    const refAntd = useRef(null)
    return (
        <div className='card'>
            <div className='card-body'>
                <div className='row'>
                    <div className='col-lg-12'>
                        {
                            <ClipartHeader clipartCategoriesQuery={clipartCategoriesQuery} onRefetch={handleOnRefetch}  selected={selected}/>
                        }
                    </div>
                    <div className='col-lg-3 position-relative border-left' style={{maxHeight:"80vh" , overflow:"auto"}}>
                    {
                        (isRefetching) && <Loading title='Loading...' />
                    }
                    {
                       !isRefetching && isFetched && !isLoading && !_isEmpty(data) ?
                        <TreeAntd treeData={data?.map((item: any) => (
                            { ...item, key: item.id, title: item.name }
                        ))}
                            
                            onSelect={handleOnSelect}
                            selectedKeys={[parseInt(searchParams.get('category'))]}
                            ref={refAntd}
                            defaultExpandAll
                            fieldNames={{
                                key: "id",
                                title: "name",
                                children: "children"
                            }}
                        />: <EmptyData title='No category' />
                    }
                    </div>
                    <Outlet context={{onRefetch:refetch }} />
                </div>

            </div>
        </div>
    )
}

export default ClipartCategoriesWrapper
export { ClipartCategoriesWrapper }