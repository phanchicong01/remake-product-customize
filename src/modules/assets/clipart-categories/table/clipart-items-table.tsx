import ItemClipart from '../components/item-clipart'
import React, { useCallback, useState } from 'react'
import {  ClipartItem } from 'interfaces/clipart-category'
import uploadFile from 'apis/upload-files/uploadFile'
import { toast } from 'react-toastify'
import createClipartItems from 'apis/created-clipart-items'
import { ID } from 'helpers'
import Loading from 'components/common/loading-metronic-style'

type Props = {
  items: ClipartItem[],
  isLoading: boolean,
  onRefetch:()=>void,
  categoryId:ID
}
const ClipArtItemsTable = ({ items, isLoading , categoryId, onRefetch}: Props) => {
  // const [listImage, setListImage] = useState<any[]>([]);
  const [loading, setLoading] = useState<boolean>(false)

  const handleChangeItems = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      const { files } = event.target;
      const newFiles = Array.from(files)
      const arrayPromise: any[] = []

      newFiles.forEach((files: File) => {
        setLoading(true)
        const newPromise = new Promise(async (resolve, reject) => {
          const newFromData = new FormData();
          newFromData.append("file", files)
          newFromData.append("folder", "media")
          const response = await uploadFile(newFromData)
          if (response.success) {
            const result = await createClipartItems({
              category_id:categoryId,
              url:response.data.path,
              name:files.name
            }) ;
            if(result.code === 200){  
              resolve(response.data.path)
            }
          } else {
            reject(response)
          }
        })
        arrayPromise.push(newPromise)
      })
      Promise.all(arrayPromise)
        .then((values) => {
          // setListImage(values)
          onRefetch();
          setLoading(false)
        })
        .catch((err) => {
          console.log(err)
          toast.error("Something went wrong. Please try again!")
          setLoading(false)
        })
    },
    [categoryId, onRefetch],
  )
  return (
    <>
      <div className='d-flex mb-4 align-items-center'>
        <label className='me-4 col-form-label font-bold fw-bold fs-6 '>Clipart Items:</label>
        <div className='d-flex align-items-center w-100'>
          <input
            className='form-control'
            type='file'
            id='file-upload'
            multiple
            accept='.jpeg,.jpg,.png'
            onChange={handleChangeItems}
          />
        </div>
      </div>
      <div className='row' style={{
        maxHeight: 500,
        overflow: "auto",
        position:"relative"
      }}>
       
        {
          items.map((item: ClipartItem) => {
            return <div className='col-12 g-4'>
              <ItemClipart onRefetch={onRefetch} data={item}  />
            </div>
          })
        }
       <div>
       {
         loading && <Loading /> 
        }
       </div>
      </div>
    </>
  )
}

export { ClipArtItemsTable }
