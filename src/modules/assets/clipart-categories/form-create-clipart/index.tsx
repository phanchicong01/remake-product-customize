import { useEffect, useState } from 'react'
import { Select } from 'antd'
import clsx from 'clsx'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import _isEmpty from "lodash-es/isEmpty"
import _pick from "lodash-es/pick"

import { useNavigate, useOutletContext } from 'react-router-dom';
import TreeSelectComponent from 'components/common/tree-select-component'
import { ClipArtCategory } from 'interfaces/clipart-category'

import { useMutation } from 'react-query'
import updatedClipartCategory from 'apis/update-clipart-categoy'
import { UpdateClipartCategoryRequest } from 'apis/update-clipart-categoy/inteface'
import { ID } from 'helpers'
import { TypeDisplayClipart } from 'constants/enum'
import CreatedClipartCategory from 'apis/created-clipart-categoy'
import UploadImage from 'components/common/upload-image'
import { toast } from 'react-toastify'
import { UploadFileResponse } from 'apis/upload-files/interfaces'
import _pickBy from "lodash-es/pickBy"
import _identity from "lodash-es/identity"
import PageLoading from 'components/common/page-loading'
import useClipartCategorySelect from 'data/hooks/use-clipart-category-select'

type Props = {
    clipart: ClipArtCategory,
    children?: any
}

const clipartSchema = Yup.object().shape({
    name: Yup.string()
        .min(3, 'Minimum 3 symbols')
        .max(50, 'Maximum 50 symbols')
        .required('Name is required'),
        parent_category_id: Yup.string().nullable().required('Parent category is required'),
    max_width: Yup.number().nullable().required('Max Width display is required'),

})
const firstClipart:ClipArtCategory = {
    id: 0,
    name: "Root",
    thumbnail: "string",
    order:1,
    parent_category_id: 0,
    max_width: 100,
    display:TypeDisplayClipart.thumbnail,
    is_show_name_on_hover: false,
    created_at: "",
    updated_at: "",
    items: [],
    children:[]
}

const FormCreateClipart = ({clipart}: Props) => {
    const {onRefetch} = useOutletContext<any>();
    const navigate = useNavigate()
    // const {listClipartCategory} = useOutletContext<any>()
    const {data :listClipartCategory} = useClipartCategorySelect()

    const [clipartState ] = useState<Partial<ClipArtCategory>>({
        ...clipart,
        name: clipart?.name || "",
        parent_category_id: clipart?.parent_category_id || '0',
        max_width: clipart?.max_width || 100,
        display: clipart?.display || TypeDisplayClipart.thumbnail,
        is_show_name_on_hover: clipart?.is_show_name_on_hover || !!0,
        order:clipart?.order || 0,
        items: clipart?.items || [],
    })

    useEffect(() => {
        if(!!clipart){
            formik.setFieldValue('name' , clipart?.name)
            formik.setFieldValue('parent_category_id' , clipart?.parent_category_id)
            formik.setFieldValue('max_width' , clipart?.max_width)
            formik.setFieldValue('display' , clipart?.display || TypeDisplayClipart.thumbnail)
            formik.setFieldValue('is_show_name_on_hover' , clipart?.is_show_name_on_hover || 0)
            formik.setFieldValue('order' , clipart?.order || 0)
            formik.setFieldValue('items' , clipart?.items || [])
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [clipart])
    
    
    const useMutationUpdateClipartCategory = useMutation((payload:{
        clipartCategoryId:ID,
        data:Partial<UpdateClipartCategoryRequest>
    }) =>  updatedClipartCategory(payload.clipartCategoryId , payload.data))
    
    const useMutationCreateClipartCategory = useMutation((data:any) =>  CreatedClipartCategory(data))
    
    const formik  = useFormik({
        initialValues: clipartState,
        validationSchema: clipartSchema,
        onSubmit: async (values, { setSubmitting }) => {
            setSubmitting(true)
            try {
                if (values.id) {
                    const dataUpdated:Partial<UpdateClipartCategoryRequest> = {
                        id:values.id,
                        display: values.display,
                        is_show_name_on_hover:values.is_show_name_on_hover ?  1 : 0,
                        max_width:values.max_width,
                        name:values.name,
                        order:values.order,
                        parent_category_id:values.parent_category_id,
                        thumbnail:values.thumbnail
                    }
                    const result =  await useMutationUpdateClipartCategory.mutateAsync({
                        clipartCategoryId:values.id,
                        data: _pickBy(dataUpdated, _identity())
                    })
                    if(result.code === 200){
                        toast.success(result.message)
                        
                       }else{
                        toast.error(result.message)
                       }
                } else {
                    const newValues:UpdateClipartCategoryRequest = _pick(values ,["display" , "is_show_name_on_hover", "max_width" , "name", "order", "parent_category_id", "thumbnail"] )
                    newValues.parent_category_id = values.parent_category_id + ''
                    newValues.is_show_name_on_hover = values.is_show_name_on_hover ?  1 : 0
                    const result =   await useMutationCreateClipartCategory.mutateAsync(newValues);
                    if(result.code === 200){
                        toast.success(result.message)
                        formik.resetForm();
                        // cancel(true)
                        navigate("/assets/clipart-categories?category="+result.data.id , {replace:true})
                       }else{
                        toast.error(result.message)
                       }
                }
            } catch (ex) {
                console.error(ex);
                toast.error("Something went wrong. Please try again!")
            } finally {
                setSubmitting(true)
                cancel(true)
            }
        },
    })

    const cancel = (withRefresh?: boolean) => {
        if (withRefresh) {
            onRefetch()
        }
        // setItemIdForUpdate(undefined)
    }
    
    return (
        <form className='form' onSubmit={formik.handleSubmit}  >
            {
               (useMutationCreateClipartCategory?.isLoading || useMutationUpdateClipartCategory?.isLoading) && 
               <PageLoading />
            }
            {/* begin::Scroll */}
            <div
                className='d-flex flex-column scroll-y me-n7 pe-7'
            >
                {/* begin::Input name */}
                <div className='row mb-6'>
                    {/* begin::Label */}
                    <label className='col-lg-12 col-form-label required fw-bold fs-6'>Name</label>
                    {/* end::Label */}

                    {/* begin::Input */}
                    <div className='col-lg-12 fv-row'>
                        <div className='row align-items-center me-auto'>
                            <div className='col-lg-8'>
                                <input
                                    placeholder='Full name'
                                    {...formik.getFieldProps('name')}
                                    type='text'
                                    name='name'
                                    className={clsx(
                                        'form-control form-control-solid mb-3 ',
                                        { 'is-invalid': formik.touched.name && formik.errors.name },
                                        {
                                            'is-valid': formik.touched.name && !formik.errors.name,
                                        }
                                    )}
                                    autoComplete='off'
                                    disabled={formik.isSubmitting}
                                />
                            </div>
                        </div>
                        {formik.touched.name && formik.errors.name && (
                            <div className='fv-plugins-message-container'>
                                <div className='fv-help-block'>
                                    <span role='alert'>{formik.errors.name}</span>
                                </div>
                            </div>
                        )}
                    </div>


                    {/* end::Input */}
                </div>
                {/* end::Input name */}
                {/* begin::Upload thumbnail */}
                <div className='form-check mb-6 form-check-solid form-switch fv-row d-flex align-items-center'>
                    <UploadImage  
                    classNameWrapper='me-2' 
                    id={"thumbnail-clipart-" + formik.values.id} 
                    src={formik.values.thumbnail}
                
                    onChange={(data:UploadFileResponse) => {
                        console.log(data)
                        formik.setFieldValue("thumbnail", data.path);
                     
                    }} 
                    title="Thumbnail Clipart" />
                </div>
                {/* end:: Upload thumbnail */}
                {/* begin::Checkbox */}
                <div className='form-check mb-6 form-check-solid form-switch fv-row d-flex align-items-center'>
                    <input
                        className='form-check-input w-45px h-30px me-3'
                        type='checkbox'
                        id='is_show_name_on_hover'
                        checked={formik.values.is_show_name_on_hover }
                        onChange={() => formik.setFieldValue("is_show_name_on_hover", !formik.values.is_show_name_on_hover)}
                    />
                    <label className='form-check-label' htmlFor='is_show_name_on_hover'>
                        Show name on hover
                        <i className='d-block '>Display clipart name as a tooltip when hovering on clipart thumbnail.</i>
                    </label>
                </div>
                {/* end::Checkbox */}
                <div className='row  mb-6'>
                    {/* begin::Parent category */}
                    <div className='col-lg-12'>
                        <div className='row '>
                            {/* begin::Label */}
                            <label className='col-lg-12 col-form-label required fw-bold fs-6'>Parent category</label>
                            {/* end::Label */}

                            {/* begin::Input */}
                            <div className='col-lg-12 fv-row'>
                                
                                {
                                   !_isEmpty(listClipartCategory) ? 
                                        <TreeSelectComponent
                                        {...formik.getFieldProps('parent_category_id')}
                                        size="large"
                                        treeIcon={false}
                                        id='parent_category_id'
                                        value={formik.values.parent_category_id || '0'}
                                        onChange={(value) => {
                                            formik.setFieldValue("parent_category_id", value);
                                        }}
                                        className={clsx(
                                            'form-select-tree form-select-tree-solid ',
                                            { 'form-control is-invalid': formik.touched.parent_category_id && formik.errors.parent_category_id },
                                            {
                                                'form-control is-valid': formik.touched.parent_category_id && !formik.errors.parent_category_id,
                                            }
                                        )}
                                        showSearch
                                        treeData={[firstClipart].concat(listClipartCategory)}
                                        fieldNames={{
                                            label: 'name',
                                            children: 'children',
                                            value: 'id'
                                        }}
                                        dropdownClassName="form-select-tree-dropdown"
                                        placeholder="Please parent"
                                        disabled={formik.isSubmitting}
                                    />: <Select
                                
                                    showSearch
                                    // value={formik.values.maxWidth}
                                    size='large'
                                    placeholder="Select clipart category"
                                    id='max_width'> </Select>
                                }
                                {/* end::Input */}
                                {formik.touched.parent_category_id && formik.errors.parent_category_id && (
                                    <div className='fv-plugins-message-container'>
                                        <div className='fv-help-block'>
                                            <span role='alert'>{formik.errors.parent_category_id}</span>
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                    {/* end::Parent category */}
                </div>
                {/* begin::Max Width */}
                <div className='col-lg-12'>
                        <div className='row '>
                            <label className='col-lg-12 col-form-label required fw-bold fs-6'>Max Width Display</label>
                            <div className='col-lg-12 fv-row'>
                                <Select
                                    {...formik.getFieldProps('max_width')}
                                    showSearch
                                    // value={formik.values.maxWidth}
                                    size='large'
                                    placeholder="Select max width"
                                    id='max_width'
                                    onChange={(value) => {
                                        formik.setFieldValue("max_width", value);
                                        // formik.setFieldTouched("maxWidth", true)
                                    }}
                                    dropdownClassName="form-custom-select-dropdown"
                                    className={clsx('form-custom-select form-custom-select-solid',
                                        { 'form-control is-invalid': formik.touched.max_width && formik.errors.max_width },
                                        {
                                            'form-control is-valid': formik.touched.max_width && !formik.errors.max_width,
                                        })}

                                >
                                    <Select.Option value={100}>100px</Select.Option>
                                    <Select.Option value={150}>150px</Select.Option>
                                    <Select.Option value={200}>200px</Select.Option>
                                    <Select.Option value={250}>250px</Select.Option>
                                    <Select.Option value={300}>300px</Select.Option>
                                    <Select.Option value={350}>350px</Select.Option>
                                    <Select.Option value={400}>400px</Select.Option>
                                    <Select.Option value={450}>450px</Select.Option>
                                    <Select.Option value={500}>500px</Select.Option>
                                </Select>
                                {formik.touched.max_width && formik.errors.max_width && (
                                    <div className='fv-plugins-message-container'>
                                        <div className='fv-help-block'>
                                            <span role='alert'>{formik.errors.max_width}</span>
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                    {/* end::Max Width */}
                {/* begin::Input group */}
                <div className='row  mb-6'>
                    <label className='col-lg-12 col-form-label fw-bold fs-6'>Display cliparts on personalization form as:
                    </label>
                    <div className='col-lg-12 fv-row'>
                        <label className='d-flex  mb-5 cursor-pointer'>
                            <span className='form-check form-check-custom form-check-solid'>
                                <input
                                    className='form-check-input'
                                    type='radio'
                                    name='display'
                                    value={TypeDisplayClipart.thumbnail}
                                    checked={formik.values.display === TypeDisplayClipart.thumbnail}
                                    onChange={() =>
                                        formik.setFieldValue("display", TypeDisplayClipart.thumbnail)
                                    }
                                />

                            </span>
                            <span className='d-flex align-items-center mx-2'>
                                Thumbnail Images
                            </span>
                        </label>

                        <label className='d-flex  mb-5 cursor-pointer'>

                            <span className='form-check form-check-custom form-check-solid'>
                                <input
                                    className='form-check-input'
                                    type='radio'
                                    name='display'
                                    value={TypeDisplayClipart.dropdown}
                                    checked={formik.values.display === TypeDisplayClipart.dropdown}
                                    onChange={() =>
                                        formik.setFieldValue("display", TypeDisplayClipart.dropdown)
                                    }
                                />

                            </span>
                            <span className='d-flex align-items-center mx-2'>
                                Dropdown list of Clipart name
                            </span>
                        </label>
                        <label className='d-flex  mb-5 cursor-pointer'>

                            <span className='form-check form-check-custom form-check-solid'>
                                <input
                                    className='form-check-input'
                                    type='radio'
                                    name='display'
                                    value={TypeDisplayClipart.inline}
                                    checked={formik.values.display === TypeDisplayClipart.inline}
                                    onChange={() =>
                                        formik.setFieldValue("display", TypeDisplayClipart.inline)
                                    }
                                />

                            </span>
                            <span className='d-flex align-items-center mx-2'>
                                Inline button of Clipart name
                            </span>
                        </label>
                    </div>
                </div>
                {/* end::Input group */}

            </div>
            {/* end::Scroll */}
      

            {/* begin::Actions */}
            <div className='text-center pt-15'>
                <button
                    type='reset'
                    onClick={() => cancel()}
                    className='btn btn-light me-3'
                    data-kt-users-modal-action='cancel'
                    disabled={formik.isSubmitting}
                >
                    Discard
                </button>

                <button
                    type='submit'
                    className='btn btn-primary'
                    data-kt-users-modal-action='submit'
                    disabled={formik.isSubmitting || !formik.isValid || !formik.touched}
                >
                    <span className='indicator-label'>Submit</span>
                    {(formik.isSubmitting) && (
                        <span className='indicator-progress'>
                            Please wait...{' '}
                            <span className='spinner-border spinner-border-sm align-middle ms-2'></span>
                        </span>
                    )}
                </button>
            </div>
            {/* end::Actions */}
        </form >
    )
}

export default FormCreateClipart