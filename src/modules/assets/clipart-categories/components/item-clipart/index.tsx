import {Tooltip} from 'antd'
import _pickBy from 'lodash-es/pickBy'
import _identity from 'lodash-es/identity'

import updateClipartItems from 'apis/update-clipart-items'
import {UpdateClipartItemRequest} from 'apis/update-clipart-items/inteface'
import {UploadFileResponse} from 'apis/upload-files/interfaces'
// import Loading from 'components/common/loading-metronic-style'
import PickColor from 'components/common/pick-color'
import UploadImage from 'components/common/upload-image'
import {ID} from 'helpers'
import {ClipartItem} from 'interfaces/clipart-category'
import {useCallback} from 'react'
import {useMutation} from 'react-query'
import {toast} from 'react-toastify'
import removeClipartItem from 'apis/remove-clipart-item'
import InputEdit from 'components/common/Input-edit'
import useConfirmDialog from 'hooks/use-confirm-dialog'
import clsx from 'clsx'

import styles from './styles.module.scss'
import Loading from 'components/common/loading'

type Props = {
  data: ClipartItem
  onRefetch: () => void
}

const ItemClipart = ({data, onRefetch}: Props) => {
  const {getDialogConfirmResult} = useConfirmDialog()
  const useMuationUpdate = useMutation((payload: UpdateClipartItemRequest) =>
    updateClipartItems(payload)
  )
  const useMuationRemove = useMutation((payload: ID) => removeClipartItem(payload))

  const handleChangeColor = useCallback(
    async (color: string) => {
      const result = await useMuationUpdate.mutateAsync({
        id: data.id,
        data: {..._pickBy(data, _identity()), color: color},
      })
      if (result.code === 200) {
        onRefetch()
        // toast.success(result.message)
      } else {
        toast.error(result.message)
      }
    },
    [data, onRefetch, useMuationUpdate]
  )

  const handleChangeThumb = useCallback(
    async (dataThumb: UploadFileResponse) => {
      await useMuationUpdate.mutateAsync({
        id: data.id,
        data: {..._pickBy(data, _identity()), thumbnail_url: dataThumb.path},
      })
    },
    [data, useMuationUpdate]
  )

  const handleChangeName = async (value) => {
    try {
      await useMuationUpdate.mutateAsync({
        id: data.id,
        data: {..._pickBy(data, _identity()), name: value},
      })
    } catch (err) {
      console.error(err)
      toast.error('Something went wrong. Please try again!')
    }
  }
  const handleRemoveClipartItem = async () => {
    const isConfirm = await getDialogConfirmResult({
      title: 'Delete item clipart',
      description: 'Are you sure delete this item?',
    })
    if (isConfirm) {
      try {
        const result = await useMuationRemove.mutateAsync(data.id)
        if (result.code === 200) {
          onRefetch()
          toast.success(result.message)
        } else {
          toast.error(result.message)
        }
      } catch (error) {
        console.log(error)
        toast.error('Something went wrong. Please try again!')
      }
    }
  }
  return (
    <div className={clsx({
        [styles.disabled]:useMuationRemove.isLoading
    })}>
      <div className={clsx('d-flex gap-2 justify-content-around mb-2',styles.itemClipart,{
         [styles.pointerNone]:useMuationRemove.isLoading
      })}>
      <div className={clsx('d-flex me-2')}>
        <Tooltip placement='topLeft' title={'Origin image'}>
          <div role={'button'}>
            <UploadImage
              readOnly={true}
              classNameWrapper='me-2 shadow-none'
              id={'origin-image-' + data.id}
              src={data.url}
              title='origin image'
            />
          </div>
        </Tooltip>
        <UploadImage
          classNameWrapper='shadow-none'
          id={'thumbnail-image-' + data.id}
          src={data.thumbnail_url}
          onChange={handleChangeThumb}
        />
      </div>
      <div className='d-flex flex-column col-lg-6'>
        <div className='position-relative  col-12 mb-2'>
          {
            <InputEdit
              loading={useMuationUpdate.isLoading}
              classNames='text-truncate'
              classNameLabel='w-100'
              onChange={handleChangeName}
              value={data.name}
            />
          }
        </div>
        <PickColor
          width='auto'
          color={data.color}
          id={'clipart-color' + data.id}
          onChange={handleChangeColor}
        />
      </div>
      <div
        className='btn btn-icon btn-sm btn-active-icon-primary h-auto'
        data-kt-users-modal-action='close'
        onClick={handleRemoveClipartItem}
        style={{cursor: 'pointer'}}
      >
        {useMuationRemove.isLoading ? (
          <div
            style={{width: '14px', height: '14px'}}
            className='spinner-border  mr-1 text-primary'
            role='status'
          ></div>
        ) : (
          <i className='fas fa-trash-alt fa-2x'></i>
        )}
      </div>
      </div>
    </div>
  )
}

export default ItemClipart
