import {  useMutation } from 'react-query'
import { ID } from 'helpers'

import { useCallback } from 'react'
import removeClipartCategory from 'apis/remove-clipart-category'
import { toast } from 'react-toastify'
import { createSearchParams, useNavigate,  useSearchParams } from 'react-router-dom'
import PageLoading from 'components/common/page-loading'
import useConfirmDialog from 'hooks/use-confirm-dialog'
// import { deleteSelectedUsers } from '../../core/_requests'

const ClipartGrouping = ({onRefetch, selected}) => {
  const [searchParams] = useSearchParams();
  const navigate = useNavigate()
  const {getDialogConfirmResult} = useConfirmDialog() 
  const useMutationRemoveClipartItem = useMutation((clipartItemId: ID) =>
    removeClipartCategory(clipartItemId),
)

 
  const handleRemoveItem = useCallback(async ()=>{
    const isConfirm = await getDialogConfirmResult({
      title:"Delete Clipart Category",
      description:"Are you sure to delete this item?"
    })
    if(isConfirm){
      try {
        const categoryID = parseInt(searchParams.get("category"))
        if(!isNaN(categoryID) ){
          const result = await  useMutationRemoveClipartItem.mutateAsync(categoryID);
          if(result.code === 200){
            toast.success(result.message);
            if(searchParams.get("search")){
              navigate({
                pathname: '/assets/clipart-categories',
                search: `?${createSearchParams({search:searchParams.get("search")})}`,
              });
            }else{
              navigate({
                pathname: '/assets/clipart-categories',
              });
            }
            onRefetch(true)
          }else{
            toast.error(result.message);
          }
        }
      } catch (error) {
        console.error(error)
        toast.error("Something went wrong. Please try again!")
      } 
    }
  
  },[getDialogConfirmResult, navigate, onRefetch, searchParams, useMutationRemoveClipartItem])

  return (
    <div className='d-flex justify-content-end align-items-center'>
      {
       ( useMutationRemoveClipartItem.isLoading ) && <PageLoading />
      }
      {/* <div className='fw-bolder me-5'>
        <span className='me-2'>{selected.length}</span> Selected
      </div> */}
      <button
        type='button'
        disabled={useMutationRemoveClipartItem.isLoading || isNaN(parseInt(searchParams.get("category"))) }
        className='btn btn-danger'
        onClick={handleRemoveItem}
      >
        Delete
      </button>
    </div>
  )
}

export { ClipartGrouping }
