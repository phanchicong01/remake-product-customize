
import { ClipartToolbar } from './ClipartToolbar'
import { ClipartGrouping } from './ClipartGrouping'
import { ClipartSearchComponent } from './ClipartSearchComponent'

import { useCallback } from 'react';
import { createSearchParams, useNavigate } from 'react-router-dom';
import _pickBy from "lodash-es/pickBy"
import _identity from "lodash-es/identity"

const ClipartHeader = ({ onRefetch, selected, clipartCategoriesQuery }: any) => {
  const navigate = useNavigate()
  const handleChangeSearch = useCallback((newSearch: string) => {
    const newClipartCategoriesQuery = _pickBy(
      {
        ...clipartCategoriesQuery,
        search: newSearch
      },
      _identity()
    )
    navigate({
      pathname: '/assets/clipart-categories',
      search: `?${createSearchParams(newClipartCategoriesQuery)}`,
    });
  }, [clipartCategoriesQuery, navigate])

  return (
    <div className='card-header border-0 pt-6'>
      <ClipartSearchComponent onChange={handleChangeSearch} />
      {/* begin::Card toolbar */}

      <div className='card-toolbar'>
        {/* begin::Group actions */}
        <div className='d-flex gap-2'>
          <ClipartToolbar />
          <ClipartGrouping onRefetch={onRefetch} selected={selected} />
        </div>
        {/* end::Group actions */}
      </div>
      {/* end::Card toolbar */}
    </div>
  )
}

export { ClipartHeader }
