import { KTSVG } from 'helpers'
import { useNavigate, useSearchParams } from 'react-router-dom'
import { useClipartCategory } from '../../context';

const ClipartToolbar = () => {

  const navigate = useNavigate();
  const [searchParams] = useSearchParams()
  const {onCreate} = useClipartCategory()
  const handleCreated = () => {
    onCreate(true)
    navigate('/assets/clipart-categories' )
  }


  return (
    <div className='d-flex justify-content-end' data-kt-user-table-toolbar='base'>
      {/* <UsersListFilter /> */}
   
      {/* begin::Add user */}
      {
        searchParams.get("category") &&  <button type='button' className='btn btn-primary' onClick={handleCreated} >
        <KTSVG path='/media/icons/duotune/arrows/arr075.svg' className='svg-icon-2' />
        Create a Clipart
      </button>
      }
      {/* end::Add user */}
    </div>
  )
}

export { ClipartToolbar }
