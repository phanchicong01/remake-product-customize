/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState,useCallback } from 'react'
import {  KTSVG } from 'helpers'
import {  useSearchParams } from 'react-router-dom'
import _debounce from "lodash-es/debounce"

const ClipartSearchComponent = ({onChange}) => {
  const [searchParams] = useSearchParams()
  // const { updateState } = useQueryRequest()
  const [searchTerm, setSearchTerm] = useState<string>(searchParams.get("search"));
  // useEffect(() => {
  //   setSearchTerm(searchParams.get("search"))
  // }, [searchParams.get("search")])
  
  const handleChangeSearchValue = useCallback(
    _debounce((value) => {
      onChange(value)
    }, 500),
    [],
  )
  const handleChangeInputSearch = (event:React.ChangeEvent<HTMLInputElement>) => {
    const  {value} = event.target
    setSearchTerm(value);
    handleChangeSearchValue(value)
  }
  
  return (
    <div className='card-title'>
      {/* begin::Search */}
      <div className='d-flex align-items-center position-relative my-1'>
        <KTSVG
          path='/media/icons/duotune/general/gen021.svg'
          className='svg-icon-1 position-absolute ms-6'
        />
        <input
          type='text'
          data-kt-user-table-filter='search'
          className='form-control form-control-solid w-250px ps-14'
          placeholder='Search Clipart'
          value={searchTerm}
          onChange={handleChangeInputSearch}
        />
      </div>
      {/* end::Search */}
    </div>
  )
}

export { ClipartSearchComponent }
