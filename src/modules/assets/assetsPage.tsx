import { Route, Routes,  Navigate } from 'react-router-dom'
import { PageLink, PageTitle } from 'layout/core'
import ClipartCategoriesWrapper from './clipart-categories/clipartCategoriesWraper'
import { lazy } from 'react'
import AdditionalOptionsWrapper from './additional-options/additionalOptionsWrapper'
import FontsWrapper from './fonts/fontsWrapper'
import ClipartCategories from './clipart-categories/clipartCategories'
import { ClipartCategoryProvider } from './clipart-categories/context'
import { AdditionalOptionsProvider } from './additional-options/context'
const AdditionalOptions = lazy(() => import("./additional-options/additionalOptions"))
const Fonts = lazy(() => import("./fonts"))

const usersBreadcrumbs: Array<PageLink> = [
  {
    title: 'Home',
    path: '/',
    isSeparator: false,
    isActive: false,
    isRedirect: true
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
    isRedirect: true
  },
  {
    title: 'Assets',
    path: '',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const AssetsPage = () => {

  return (
    <Routes>
      <Route element={
        <ClipartCategoryProvider>
          <ClipartCategoriesWrapper />
        </ClipartCategoryProvider>

      }>
        <Route
          path='clipart-categories/*'
          element={
            <>
              <PageTitle breadcrumbs={usersBreadcrumbs}>Clipart Categories</PageTitle>
              <ClipartCategories />
            </>
          }
        />
        
      </Route>
      <Route element={
      <AdditionalOptionsProvider >
          <AdditionalOptionsWrapper />
      </AdditionalOptionsProvider>}>
        <Route
          path='additional-option/*'
          element={
            <>
              <AdditionalOptions />
            </>
          }
        />
      </Route>
      <Route element={<FontsWrapper />}>
        <Route
          path='fonts/*'
          element={
          <>  
              <Fonts />
            </>
          }
        />
      </Route>

      <Route index element={<Navigate to='clipart-categories' />} />
    </Routes>
  )
}

export default AssetsPage
