/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState,  useMemo, useCallback } from 'react'
import {  KTSVG } from 'helpers'
import _debounce from 'lodash-es/debounce'
import { BaseParamRequest } from 'interfaces/common.interface';
import { createSearchParams, useNavigate, useSearchParams } from 'react-router-dom';
import _identity from 'lodash-es/identity';
import _pickBy from 'lodash-es/pickBy';

const SearchComponent = () => {

  const [searchParams] = useSearchParams();
  const navigate = useNavigate();

  const query: BaseParamRequest = useMemo(() => {
    return {
      search: searchParams.get('search'),
      page: 1,
      per_page: searchParams.get('per_page') ? parseInt(searchParams.get('per_page')) : 10
    }
  }, [searchParams])

  const [searchTerm, setSearchTerm] = useState<string>(query.search);
 
  const onSearch = useCallback(_debounce((search: string) => {
    const newFontsQuery = _pickBy(
      {
        ...query,
        search: search
      },
      _identity()
    )
    navigate({
      pathname: 'fonts',
      search: `?${createSearchParams(newFontsQuery)}`,
    });
  },300), [])

  const handleChangeInput = (event:React.ChangeEvent<HTMLInputElement>) =>{
    const {value} =  event.target
    setSearchTerm(value);
    onSearch(value)
  }
  return (
    <div className='card-title'>
      {/* begin::Search */}
      <div className='d-flex align-items-center position-relative my-1'>
        <KTSVG
          path='/media/icons/duotune/general/gen021.svg'
          className='svg-icon-1 position-absolute ms-6'
        />
        <input
          type='text'
          data-kt-user-table-filter='search'
          className='form-control form-control-solid w-250px ps-14'
          placeholder='Search Font'
          value={searchTerm}
          onChange={handleChangeInput}
        />
      </div>
      {/* end::Search */}
    </div>
  )
}

export { SearchComponent }
