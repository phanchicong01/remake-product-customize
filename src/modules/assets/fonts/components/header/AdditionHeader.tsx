import { AdditionToolbar } from './AdditionToolbar'
import { SearchComponent } from './SearchComponent'

const AdditionHeader = () => {
  return (
    <div className='card-header border-0 pt-6'>
    
      {/* begin::Card toolbar */}
      <div className='card-toolbar'>
        {/* begin::Group actions */}
        <AdditionToolbar />
        {/* end::Group actions */}
      </div>
      {/* end::Card toolbar */}
    </div>
  )
}

export { AdditionHeader }
