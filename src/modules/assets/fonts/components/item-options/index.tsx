import clsx from 'clsx'
import Thumb from 'components/common/thumb'
import React, { useState } from 'react'
import styles from "./styles.module.scss"
type Props = {
    data: any
}

const ItemOptions = ({ data }: Props) => {
    const [color, setColor] = useState('')
    const [thumbnail, setThumnail] = useState('')
    const [file, setFile] = useState(null)
    const handleChangeColor = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        setColor(value)
    }
    const handleChangeFile = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, files } = event.target;
        
        setFile(files[0])
    }
    return (
        <div className='row mb-4 g-4'>
            <div className='col-lg-9'>
                <div>{data.name}</div>
                <div className='d-flex justify-content-between g-2 align-items-center'>
                    <div  className='position-relative'>
                        <label className='d-flex' htmlFor={'color-item-option-' + data.id} role="button">
                            <div className='input-group w-200px input-group-sm' >
                                <input onChange={handleChangeColor} value={color} type="text" className='form-control ' aria-describedby="inputGroup-sizing-sm" />
                                <div className='input-group-text p-1' style={{
                                    backgroundImage: !color ? "url('/bg-transparent.png')" : undefined
                                }} >
                                    <span className={clsx("w-25px h-25px rouded-sm", {})}
                                        style={{
                                            backgroundColor: color
                                        }}></span>
                                </div>
                            </div>
                        </label>
                        <input onChange={handleChangeColor} value={color} type={"color"} id={'color-item-option-' + data.id} className="position-absolute top-0 start-0 opacity-0 " />
                    </div>
                    <div >
                        Feel month ago
                    </div>
                </div>
            </div>
            <div className='col-lg-3'>
                <div className={styles.thumbnail} style={{ backgroundImage: "url('/bg-transparent.png')"}} role="button">
                    <label role="button" className={styles.labelThumbnail} htmlFor={'thumbail-item-option-' + data.id} >
                        Change thumbnail
                    </label>
                    <input onChange={handleChangeFile} className='d-none' id={'thumbail-item-option-' + data.id} type="file" accept='image/png,image/jpg,image/jpeg' />
                    {
                        file && <label className='position-absolute top-0 start-0 w-100 h-100'><Thumb width={"100%"} height="100%" file={file} /></label>
                    }
                </div>

            </div>
        </div>
    )
}

export default ItemOptions