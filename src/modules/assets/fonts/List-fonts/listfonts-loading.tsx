import styles from './styles.module.scss'

interface ListFontLoadingProps {
    line?: number
}
const ListFontLoading = ({ line = 1 }: ListFontLoadingProps) => {


    return <>
        {Array(line).fill(null).map((_, i) => <div className='row gx-3 gy-3 justify-content-around' key={i}>
            <div className={styles.placeholderContentItem}></div>
            <div className={styles.placeholderContentItem}></div>
            <div className={styles.placeholderContentItem}></div>
            <div className={styles.placeholderContentItem}></div>
        </div>)}
    </>
}

export { ListFontLoading }