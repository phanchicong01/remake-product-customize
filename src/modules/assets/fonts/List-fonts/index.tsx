import clsx from 'clsx'
import React, { useEffect, useMemo, useState } from 'react'

import deleteFont from 'apis/delete-font'
import useFonts from 'data/hooks/user-get-fonts'
import { ID } from 'helpers'
import { BaseParamRequest } from 'interfaces/common.interface'
import { FontInterface } from 'interfaces/font'
import { Helmet } from 'react-helmet'
import { useMutation } from 'react-query'
import {  useOutletContext, useSearchParams } from 'react-router-dom'
import { toast } from 'react-toastify'
import { OutletContextInterface } from '../fontsWrapper'
import { ListFontLoading } from './listfonts-loading'
import PaginationCommon from 'components/common/pagination-common'
import PageLoading from 'components/common/page-loading'
import useConfirmDialog from 'hooks/use-confirm-dialog'

type Props = {
  dataArtwork?: Array<any>
}

const ListFont = React.memo(({ dataArtwork }: Props) => {
  const { refetchListFonts, setRefetchListFonts }: OutletContextInterface = useOutletContext();
  const [searchParams] = useSearchParams();

  const { getDialogConfirmResult } = useConfirmDialog()
  const query: BaseParamRequest = useMemo(() => {
    return {
      search: searchParams.get('search'),
      page: searchParams.get('page') ? parseInt(searchParams.get('page')) : 1,
      per_page: searchParams.get('per_page') ? parseInt(searchParams.get('per_page')) : 10
    }
  }, [searchParams])

  const { data, isFetched, isLoading, refetch } = useFonts(query)
  const { mutateAsync: deleteFontAsync, isLoading: isDeletFontLoading } = useMutation(async (id: ID) => await deleteFont(id))

  useEffect(() => {
    if (refetchListFonts) {
      refetch()
      setRefetchListFonts(false)
    }
  }, [isFetched, refetch, refetchListFonts, setRefetchListFonts])

  const memoizedData = useMemo(() => !!data && data?.data && data?.data?.length > 0 ? data.data : [], [data])
  const [listMarkupFont, setListMarkupFont] = useState(null)


  const handleLoadFont = async (list) => {
    let fonts = ''
    if (!!list && list.length > 0) {
      list.map((fontItem) => {
        fonts += ([
          '@font-face {\n',
          '\tfont-family: \'', fontItem.name, '\';\n',
          '\tfont-style: \'normal\';\n',
          '\tfont-weight: \'normal\';\n',
          '\tsrc: url(\'', process.env.REACT_APP_PUBLIC_URL + '/storage/' + fontItem.url, '\');\n',
          '}\n'
        ].join(''))
        return fontItem
      })
    }
    return fonts

  }

  const handleDeleteFont = async (id:ID) => {
    const isConfirm = await getDialogConfirmResult({
      title: "Delete Font",
      description: "Are you sure delete this item?"
    })
    if (isConfirm) {
      let result = await deleteFontAsync(id)
      if (result && result.success) {
        refetch()
        toast.success('Delete Font success')
      } else {
        toast.error(result.message)
      }
    }
  }

  useEffect(() => {
    let fontNames = []
    if (data?.data && data?.data?.length > 0) {
      handleLoadFont(data?.data).then((res) => setListMarkupFont(res))
      data.data.map((item) => fontNames.push(item.name))
    }

  }, [data])


  return (
    <>
      <Helmet >
        <style type="text/css" >
          {listMarkupFont}
        </style>
      </Helmet>
      {
        isDeletFontLoading && <PageLoading />
      }
      {isFetched && !isLoading ? <ListFontComponent
        memoizedData={memoizedData}
        handleDelete={handleDeleteFont} /> : <ListFontLoading />}

      {data && data.last_page > 1 ? <PaginationCommon pathname='fonts' pageCount={data && data.last_page ? data.last_page : 0} forcePage={data && data.page ? data.page - 1 : 0} /> : ''}

      {/* <ConfirmDialog show={onDeleteFont}
        onClose={() => {
          setOnDeleteFont(false)
          setFontSelected(null)
        }}
        next={handleDeleteFont} /> */}
    </>
  )
})

export default ListFont

const ListFontComponent = React.memo(({ memoizedData, handleDelete }: any) => {
  return <div className='row gx-3 gy-3'>
    {
      memoizedData.map((item: FontInterface, index: number) => <div key={index} className={clsx('col-lg-3')}>
        <div className='card card-bordered shadow-sm' >
          <div className='card-header  '>
            <div className="card-title font-bold">{item.name || 'Font Name'}</div>
          </div>
          <div className='card-body fs-1' style={{ fontFamily: item.name }}>
          {`Aa Bb Cc Dd Ee Ff Gg Hh Ii Jj Kk Ll Mm Nn Oo Pp Qq Rr Ss Tt Uu Vv Ww Xx Yy Zz 1234567890 \`~!@#%^&*()-_=+{}[]|;:'“<>,.?/$€£`}
          </div>
          <div className='card-footer'>
            <div className='d-flex justify-content-between'>
              <button className='btn btn-danger' onClick={() => handleDelete(item.id)}>Delete</button>
            </div>

          </div>
        </div>
      </div>)
    }
  </div>
})