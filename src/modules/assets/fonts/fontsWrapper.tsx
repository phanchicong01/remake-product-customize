
import { PageLink, PageTitle } from 'layout/core'
import { Outlet } from 'react-router-dom'
import * as Yup from 'yup'
import _get from "lodash-es/get"
import CreateFont from 'apis/createFont'
import uploadFont from 'apis/upload-files/uploadFont'
import clsx from 'clsx'
import { useFormik } from 'formik'
import { FontInterface, FontUploadInterface } from 'interfaces/font'
import { useRef, useState } from 'react'
import { useMutation } from 'react-query'
import { toast } from 'react-toastify'
import { SearchComponent } from './components/header/SearchComponent'
import PageLoading from 'components/common/page-loading'


const breadcrumbs: Array<PageLink> = [
  {
    title: 'Home',
    path: '/',
    isSeparator: false,
    isActive: false,
    isRedirect: true
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
    isRedirect: true
  },
  {
    title: 'Assets',
    path: '',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

export interface OutletContextInterface {
  refetchListFonts: boolean,
  setRefetchListFonts: (value: boolean) => any
}
const initalData: FontUploadInterface = {
  folder: '',
  name: '',
  file: null,
}
const FontsWrapper = () => {
  const uploadFontRef = useRef(null)

  const [refetchListFonts, setRefetchListFonts] = useState(false)
  const { mutateAsync: uploadFontAsync, isLoading: isUploadFontLoading } = useMutation(async (file: File) => await uploadFont(file))
  const { mutateAsync: createFontAsync, isLoading: isCreateFontLoading } = useMutation(async (body: FontInterface) => await CreateFont(body))
  const fontSchema = Yup.object().shape({

    file: Yup.mixed()
      .required('Font file is required'),
    folder: Yup.string(),
    name: Yup.string().min(3, 'Minimum 1 symbols')
      .max(50, 'Maximum 20 symbols')
      .required('Name is required'),

  })
  const formik = useFormik({
    initialValues: initalData,
    validationSchema: fontSchema,
    onSubmit: async (values, { setSubmitting, resetForm }) => {
      setSubmitting(true)
      try {
        let response = await uploadFontAsync(values.file)
        if (!!response && response.success) {
          let path = response.data?.path
          let result = await createFontAsync({ name: values.name, url: path, folder: values.folder })
          if (!!result && result.success) {
            toast.success('Create font successfully !')
            resetForm()
            uploadFontRef.current.value = null
            setRefetchListFonts(true)
          } else {
            toast.warning(result.message)
          }
        } else {
          toast.warning('Upload Font Fail !')
        }
      } catch (ex) {
        console.error(ex)
        setSubmitting(false)
      } finally {
        setSubmitting(false)
      }
    },
  })


  return (
    <>
      <PageTitle breadcrumbs={breadcrumbs}>Fonts</PageTitle>
      <div className='card'>
        {
         ( isCreateFontLoading || isUploadFontLoading ) && <PageLoading />
        }
        <div className='card-body'>
          <div className='row'>
            <div className='col-lg-3'>
              <div className='card'>
                <div className='card-header'>  <SearchComponent /></div>
                <div className='card-body'>
                  <form onSubmit={formik.handleSubmit} noValidate>
                    {/* begin::Input group */}
                    <div className={` mb-7`}>
                      {/* begin::Label */}
                      <label className='required fw-bold fs-6 mb-2'>Font Name</label>
                      {/* end::Label */}

                      {/* begin::Input */}
                      <input
                        placeholder='Font name'
                        {...formik.getFieldProps('name')}
                        type='text'
                        name='name'
                        className={clsx(
                          'form-control form-control-solid mb-3 mb-lg-0',
                          { 'is-invalid': formik.touched.name && formik.errors.name },
                          {
                            'is-valid': formik.touched.name && !formik.errors.name,
                          }
                        )}
                        autoComplete='off'
                        disabled={formik.isSubmitting}
                      />
                      {formik.touched.name && formik.errors.name && (
                        <div className='fv-plugins-message-container'>
                          <div className='fv-help-block'>
                            <span role='alert'>{formik.errors.name}</span>
                          </div>
                        </div>
                      )}
                      {/* end::Input */}
                    </div>
                    <div className={` mb-7`}>
                      {/* begin::Label */}
                      <label className=' fw-bold fs-6 mb-2'>Folder</label>
                      {/* end::Label */}

                      {/* begin::Input */}
                      <input
                        placeholder='Folder'
                        {...formik.getFieldProps('folder')}
                        type='text'
                        name='folder'
                        className={clsx(
                          'form-control form-control-solid mb-3 mb-lg-0',
                          { 'is-invalid': formik.touched.folder && formik.errors.folder },
                          {
                            'is-valid': formik.touched.folder && !formik.errors.folder,
                          }
                        )}
                        autoComplete='off'
                        disabled={formik.isSubmitting}
                      />
                      {/* end::Input */}
                    </div>
                    <div className={` mb-7`}>
                      <label htmlFor="file" className='required fw-bold fs-6 mb-2'>Upload yours fonts here:</label>
                      <input id="file" name='file' className='form-control'
                        ref={uploadFontRef}
                        onChange={(event) => {
                          formik.setFieldValue("file", _get(event , 'currentTarget.files[0]'));
                          formik.setFieldValue("name",  _get(event , 'currentTarget.files[0].name'));
                        }} type={"file"} accept=".otf,.ttf,.woff"
                      />
                      {formik.touched.file && formik.errors.file && (
                        <div className='fv-plugins-message-container'>
                          <div className='fv-help-block'>
                            <span role='alert'>{formik.errors.file}</span>
                          </div>
                        </div>
                      )}
                    </div>
                    {/* end::Input group */}
                    <button
                      type='submit'
                      className='btn btn-primary'
                      data-kt-users-modal-action='submit'
                      disabled={formik.isSubmitting || !formik.isValid}
                    >
                      <span className='indicator-label'>Submit</span>
                      {(formik.isSubmitting) && (
                        <span className='indicator-progress'>
                          Please wait...{' '}
                          <span className='spinner-border spinner-border-sm align-middle ms-2'></span>
                        </span>
                      )}
                    </button>
                  </form>

                </div>

              </div>
            </div>
            <Outlet context={{ refetchListFonts  , setRefetchListFonts}} />
          </div>
        </div>
      </div>
    </>
  )
}


export default FontsWrapper