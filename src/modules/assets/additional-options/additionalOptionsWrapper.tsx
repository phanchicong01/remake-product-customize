

import { createSearchParams, Outlet,  useNavigate, useSearchParams } from 'react-router-dom'
import { ID, KTSVG } from 'helpers'
import { PageLink, PageTitle } from 'layout/core'
import _isEmpty from 'lodash-es/isEmpty'
import _pickBy from "lodash-es/pickBy"
import _identity from "lodash-es/identity"
import clsx from 'clsx'

import { AdditionalOption } from 'interfaces/additional-options'
import styles from "./styles.module.scss"
import { SearchComponent } from './components/header/SearchComponent'
import useAdditionOptions from 'data/hooks/use-addition-options'
import React, { useCallback, useMemo } from 'react'
import Loading from 'components/common/loading-metronic-style'
import { useMutation } from 'react-query'
import removeAdditionalOption from 'apis/remove-additional-option'
import { toast } from 'react-toastify'
import PageLoading from 'components/common/page-loading'
import { AdditionalOptionsRequest } from 'apis/get-additional-options/interface'
import useConfirmDialog from 'hooks/use-confirm-dialog'

const breadcrumbs: Array<PageLink> = [
  {
    title: 'Home',
    path: '/',
    isSeparator: false,
    isActive: false,
    isRedirect: true
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
    isRedirect: true
  },
  {
    title: 'Assets',
    path: '',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]


const AdditionalOptionsWrapper = () => {
  const [searchParams] = useSearchParams();
  const navigate = useNavigate();
  const {getDialogConfirmResult} = useConfirmDialog()
  const additionalOptionQuery: AdditionalOptionsRequest = useMemo(() => {
    return {
      search: searchParams.get('search'),
      id: searchParams.get("id") ? parseInt(searchParams.get("id")) : undefined,
    }
  }, [searchParams])

  const { data: dataAddition, isLoading, isFetched,  refetch } = useAdditionOptions(additionalOptionQuery)
  const useMutationRemoveAdditionOption = useMutation((id: ID) => removeAdditionalOption(id))
 

  const handleOnSelect = useCallback((id: ID) => {
    if (!!id) {
      const newAdditionalOptionQuery = _pickBy(
        {
          ...additionalOptionQuery,
          id
        },
        _identity()
      )
      navigate({
        pathname: '/assets/additional-option',
        search: `?${createSearchParams(newAdditionalOptionQuery)}`,
      }, {replace:true} );
    }
  }, [navigate , additionalOptionQuery])


  const handleRemove = useCallback(async (id: ID) => {
    const isConfirm = await getDialogConfirmResult({
      title:"Delete Additional options",
      description:"Are you sure delete this item?"
    })
    if(isConfirm) {
      try {
        const result = await useMutationRemoveAdditionOption.mutateAsync(id);
        if (result.code === 200) {
          toast.success(result.message)
          refetch()
          if(parseInt(searchParams.get('id')) === id){
            navigate("/assets/additional-option");
          }
        } else {
          toast.error(result.message)
        }
      } catch (error) {
        console.error(error)
        toast.error("Something went wrong. Please try again!")
      }
    }
    
  }, [getDialogConfirmResult, navigate, refetch, searchParams, useMutationRemoveAdditionOption])

  return (
    <>
      <PageTitle breadcrumbs={breadcrumbs}>Additional options</PageTitle>
      <div className='card'>
       {
        useMutationRemoveAdditionOption.isLoading &&  <PageLoading />
       }
        <div className='card-body'>
          <div className='row'>
            <div className='col-lg-3'>
              <div className='card'>
                <div className='card-header'>  <SearchComponent  additionalOptionQuery={additionalOptionQuery}/></div>
                <div className='card-body position-relative'>
                  {
                    isLoading  && <div className='mt-4'><Loading /></div>
                  }
                  {
                    isFetched && !isLoading && !_isEmpty(dataAddition) &&
                    dataAddition.map((item: AdditionalOption) => {
                  
                      const isActive = parseInt(searchParams.get("id")) === item.id
                      return <div className={""} key={item.id}>
                        <div className={clsx('menu-item me-lg-1', styles.categoryItem)}>
                          <div
                            id={item.id + ''}
                            className={clsx('menu-link py-3', styles.menuLink, {
                              [styles.active]: isActive,
                            })}
                            // to={to}
                            onClick={() => handleOnSelect(item.id)}
                          >
                            {/* <span className='menu-bullet'>
                                              <span className='bullet bullet-dot'></span>
                                            </span> */}
                            <span className={styles.menuTitle}>{item.name}</span>
                          </div>
                          <div className={clsx('', styles.action)}>
                            <a onClick={()=>handleRemove(item.id)}>
                              <KTSVG path='/media/icons/duotune/general/gen027.svg'
                                className='svg-icon-3' />
                            </a>
                          </div>
                        </div>

                      </div>
                    })
                  }
                </div>

              </div>
            </div>
            <Outlet context={{
              handleOnRefetch:()=>refetch()
            }} />
          </div>

        </div>
      </div>
    </>
  )
}


export default AdditionalOptionsWrapper