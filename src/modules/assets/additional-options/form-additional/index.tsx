import {  useEffect, useState } from 'react'
import clsx from 'clsx'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import _pick from "lodash-es/pick"
import { toast } from 'react-toastify'


import { useNavigate, useOutletContext } from 'react-router-dom';
import { AdditionalOption } from 'interfaces/additional-options'
import { TypeDisplayClipart } from 'constants/enum'
import { useMutation } from 'react-query'
import createdAdditionalOption from 'apis/created-addtional-option'
import { CreateAdditionalOptionRequest } from 'apis/created-addtional-option/inteface'
import updatedAdditionalOption from 'apis/update-addtional-option'
import { UpdatedAdditionalOptionRequest } from 'apis/update-addtional-option/inteface'
import PageLoading from 'components/common/page-loading'


type Props = {
    data: AdditionalOption,
    children?: any
}

const clipartSchema = Yup.object().shape({

    name: Yup.string()
        .min(3, 'Minimum 3 symbols')
        .max(50, 'Maximum 50 symbols')
        .required('Name is required'),

})

const FormAddition = ({ data, children }: Props) => {

    const navigate = useNavigate();

    const  {handleOnRefetch} = useOutletContext<any>()
    const useMutationCreatedAdditionalOption = useMutation((payload: CreateAdditionalOptionRequest) => createdAdditionalOption(payload))
    const useMutationUpdatedAdditionalOption = useMutation((payload: UpdatedAdditionalOptionRequest) => updatedAdditionalOption(payload))


    const [additionOptionData] = useState<AdditionalOption>({
        ...data,
        is_show_clipart_name_on_hover: data?.is_show_clipart_name_on_hover || 0,
        name: data?.name,
        type: data?.type || TypeDisplayClipart.thumbnail,
    })

    useEffect(() => {
        if(!!data){
            formik.setFieldValue('is_show_clipart_name_on_hover', data?.is_show_clipart_name_on_hover || 0)
            formik.setFieldValue('name' ,data?.name )
            formik.setFieldValue('type', data?.type || TypeDisplayClipart.thumbnail)
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [data])

    const formik = useFormik({
        initialValues: additionOptionData,
        validationSchema: clipartSchema,
        onSubmit: async (values, { setSubmitting }) => {
            setSubmitting(true)
            try {
                if (values.id) {
                    const dataUpdated = _pick(values, ['name', 'is_show_name_on_hover', 'type']);
                    const result = await useMutationUpdatedAdditionalOption.mutateAsync({
                        additionId:values.id,
                        dataUpdated:dataUpdated
                    })
                    if (result.code === 200) {
                        toast.success(result.message)
                        // formik.resetForm()
                    } else {
                        toast.error(result.message)
                    }


                } else {
                    const dataCreated = _pick(values, ['name', 'is_show_name_on_hover', 'type'])
                    const result = await useMutationCreatedAdditionalOption.mutateAsync(dataCreated)
                    if (result.code === 200) {
                        toast.success(result.message)
                        navigate("/assets/additional-option?id="+result.data.id)
                    } else {
                        toast.error(result.message)
                    }
                }
            } catch (ex) {
                console.error(ex)
                toast.error("Something went wrong. Please try again!")
            } finally {
                setSubmitting(true)
                cancel(true)
            }
        },
    })

    const cancel = (withRefresh?: boolean) => {
        if (withRefresh) {
            handleOnRefetch()
        }
        // setItemIdForUpdate(undefined)
    }


    return (
        <form className='form' onSubmit={formik.handleSubmit} >
            {/* begin::Scroll */}
            {
                useMutationUpdatedAdditionalOption.isLoading && <PageLoading />
            }
            <div
                className='d-flex flex-column scroll-y me-n7 pe-7'
            >
                {/* begin::Input name */}
                <div className='row mb-6'>
                    {/* begin::Label */}
                    <label className='col-lg-12 col-form-label required fw-bold fs-6'>Name</label>
                    {/* end::Label */}

                    {/* begin::Input */}
                    <div className='col-lg-12 fv-row'>
                        <div className='row align-items-center me-auto'>
                            <div className='col-lg-8'>
                                <input
                                    placeholder='Full name'
                                    {...formik.getFieldProps('name')}
                                    type='text'
                                    name='name'
                                    className={clsx(
                                        'form-control form-control-solid mb-3 ',
                                        { 'is-invalid': formik.touched.name && formik.errors.name },
                                        {
                                            'is-valid': formik.touched.name && !formik.errors.name,
                                        }
                                    )}
                                    autoComplete='off'
                                    disabled={formik.isSubmitting}
                                />
                            </div>
                        </div>
                        {formik.touched.name && formik.errors.name && (
                            <div className='fv-plugins-message-container'>
                                <div className='fv-help-block'>
                                    <span role='alert'>{formik.errors.name}</span>
                                </div>
                            </div>
                        )}
                    </div>


                    {/* end::Input */}
                </div>
                {/* end::Input name */}

                {/* begin::Checkbox */}
                <div className='form-check mb-6 form-check-solid form-switch fv-row d-flex align-items-center'>
                    <input
                        className='form-check-input w-45px h-30px me-3'
                        type='checkbox'
                        id='is_show_name_on_hover'
                        checked={!!formik.values.is_show_clipart_name_on_hover}
                        onChange={() => formik.setFieldValue("is_show_clipart_name_on_hover", !formik.values.is_show_clipart_name_on_hover)}
                    />
                    <label className='form-check-label' htmlFor='is_show_name_on_hover'>
                        Show clipart name on hover
                        <i className='d-block '>Display clipart name as a tooltip when hovering on clipart thumbnail.

                            .</i>
                    </label>
                </div>
                {/* end::Checkbox */}

                {/* begin::Input group */}
                <div className='row  mb-6'>
                    <label className='col-lg-12 col-form-label fw-bold fs-6'>Display cliparts on personalization form as:
                    </label>
                    <div className='col-lg-12 fv-row'>
                        <label className='d-flex  mb-5 cursor-pointer'>
                            <span className='form-check form-check-custom form-check-solid'>
                                <input
                                    className='form-check-input'
                                    type='radio'
                                    name='type'
                                    checked={formik.values.type === TypeDisplayClipart.thumbnail}
                                    onChange={() =>
                                        formik.setFieldValue("type", 'image')
                                    }
                                />

                            </span>
                            <span className='d-flex align-items-center mx-2'>
                                Thumbnail Images
                            </span>
                        </label>
                        <label className='d-flex  mb-5 cursor-pointer'>

                            <span className='form-check form-check-custom form-check-solid'>
                                <input
                                    className='form-check-input'
                                    type='radio'
                                    name='type'
                                    value={TypeDisplayClipart.thumbnail}
                                    checked={formik.values.type === TypeDisplayClipart.dropdown}
                                    onChange={() =>
                                        formik.setFieldValue("type", TypeDisplayClipart.dropdown)
                                    }
                                />

                            </span>
                            <span className='d-flex align-items-center mx-2'>
                                Dropdown list of Clipart name
                            </span>
                        </label>
                        <label className='d-flex  mb-5 cursor-pointer'>

                            <span className='form-check form-check-custom form-check-solid'>
                                <input
                                    className='form-check-input'
                                    type='radio'
                                    name='type'
                                    value={TypeDisplayClipart.inline}
                                    checked={formik.values.type === TypeDisplayClipart.inline}
                                    onChange={() =>
                                        formik.setFieldValue("type", TypeDisplayClipart.inline)
                                    }
                                />

                            </span>
                            <span className='d-flex align-items-center mx-2'>
                                Inline button of Clipart name
                            </span>
                        </label>
                        <i className='d-block '>Note: If color is set for 1 item, all items will be displayed as color swatches
                        </i>
                    </div>
                </div>
                {/* end::Input group */}




            </div>
            {/* end::Scroll */}
            {/* begin::items */}
            <div className="row mb-6">
                {children}
            </div>
            {/* end::items */}

            {/* begin::Actions */}
            <div className='pt-15'>
                <button
                    type='reset'
                    onClick={() => cancel()}
                    className='btn btn-light me-3'
                    data-kt-users-modal-action='cancel'
                    disabled={formik.isSubmitting}
                >
                    Discard
                </button>

                <button
                    type='submit'
                    className='btn btn-primary'
                    data-kt-users-modal-action='submit'
                    disabled={formik.isSubmitting || !formik.isValid || !formik.touched}
                >
                    <span className='indicator-label'>Submit</span>
                    {(formik.isSubmitting) && (
                        <span className='indicator-progress'>
                            Please wait...{' '}
                            <span className='spinner-border spinner-border-sm align-middle ms-2'></span>
                        </span>
                    )}
                </button>
            </div>
            {/* end::Actions */}
        </form >
    )
}

export default FormAddition