import { KTSVG } from 'helpers'
import { useNavigate, useSearchParams } from 'react-router-dom'

const AdditionToolbar = () => {

  const navigate = useNavigate();
  const [searchParams] = useSearchParams()
  const handleCreate = () => {
    navigate('/assets/additional-option' )
  }

  return (
    <div className='d-flex justify-content-end' data-kt-user-table-toolbar='base'>
      {/* <UsersListFilter /> */}
   
      {/* begin::Add user */}
      {
         <button type='button' className='btn btn-primary' onClick={handleCreate} >
        <KTSVG path='/media/icons/duotune/arrows/arr075.svg' className='svg-icon-2' />
        Create new addition
      </button>
      }
     
      {/* end::Add user */}
    </div>
  )
}

export { AdditionToolbar }
