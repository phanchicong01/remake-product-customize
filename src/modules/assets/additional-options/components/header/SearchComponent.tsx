/* eslint-disable react-hooks/exhaustive-deps */
import { useState,  useCallback } from 'react'
import { KTSVG } from 'helpers'
import _debounce from "lodash-es/debounce"
import _pickBy from "lodash-es/pickBy"
import _identity from "lodash-es/identity"
import { createSearchParams,  useNavigate } from 'react-router-dom'
import { AdditionalOptionsRequest } from 'apis/get-additional-options/interface'

const SearchComponent = ({additionalOptionQuery }:{additionalOptionQuery:AdditionalOptionsRequest}) => {
  const navigate = useNavigate()
  const [searchTerm, setSearchTerm] = useState<string>(additionalOptionQuery.search)
  
  const handleChangeSearchValue = useCallback(
    _debounce((value) => {
        const newAdditionalOptionQuery = _pickBy(
          {
            ...additionalOptionQuery,
            search:value
          },
          _identity()
        )
       navigate({
          pathname: '/assets/additional-option',
          search: `?${createSearchParams(newAdditionalOptionQuery)}`,
        });
    }, 500),
    [],
  )
  const handleChangeInputSearch = (event:React.ChangeEvent<HTMLInputElement>) => {
    const  {value} = event.target
    setSearchTerm(value);
    handleChangeSearchValue(value)
  }
  return (
    <div className='card-title'>
      {/* begin::Search */}
      <div className='d-flex align-items-center position-relative my-1'>
        <KTSVG
          path='/media/icons/duotune/general/gen021.svg'
          className='svg-icon-1 position-absolute ms-6'
        />
        <input
          type='text'
          data-kt-user-table-filter='search'
          className='form-control form-control-solid w-250px ps-14'
          placeholder='Search Clipart'
          value={searchTerm}
          onChange={handleChangeInputSearch}
        />
      </div>
      {/* end::Search */}
    </div>
  )
}

export { SearchComponent }
