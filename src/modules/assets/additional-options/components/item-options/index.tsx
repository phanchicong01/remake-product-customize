import updateAdditionalOptionItem from 'apis/update-addtional-option-item'
import { UploadFileResponse } from 'apis/upload-files/interfaces'
import clsx from 'clsx'
import _pickBy from "lodash-es/pickBy"
import _indentity from  "lodash-es/identity"
import UploadImage from 'components/common/upload-image'
import { AdditionalOptionItem } from 'interfaces/additional-options'
import { toast } from 'react-toastify'

import styles from "./styles.module.scss"
import PickColor from 'components/common/pick-color'
import InputEdit from 'components/common/Input-edit'
import useConfirmDialog from 'hooks/use-confirm-dialog'
import { useMutation } from 'react-query'
import removeAdditionalOptionItems from 'apis/remove-additional-option-items'
import { ID } from 'helpers'
import Loading from 'components/common/loading-metronic-style'

type Props = {
    data: AdditionalOptionItem;
    onRefetch:()=>void
}

const ItemOptions = ({ data, onRefetch  }: Props) => {
    const {getDialogConfirmResult} = useConfirmDialog()
   
    const useMuationRemove = useMutation((payload: ID) => removeAdditionalOptionItems(payload))

    const handleChangeColor = async (color:string) => {
        const result = await updateAdditionalOptionItem(_pickBy({
            additional_option_id: data.additional_option_id,
            id:data.id,
            color,
            name:data.name,
            thumbnail:data.thumbnail
        }, _indentity()))
        if (result.code === 200) {
            onRefetch()
        } else {
            toast.error(result.message)
        }
    }
    const handleChangeFile = async (dataUpload:UploadFileResponse) => {
        const result = await updateAdditionalOptionItem(_pickBy({
            additional_option_id: data.additional_option_id,
            id:data.id,
            thumbnail:dataUpload.thumbnail,
            color:data.color,
            name:data.name

        } , _indentity()))

        if (result.code === 200) {
            onRefetch()
        } else {
            toast.error(result.message)
        }
    }
    const handleChangeName = async (newName:string) => {
        const result = await updateAdditionalOptionItem(_pickBy({
            additional_option_id: data.additional_option_id,
            id:data.id,
            name:newName,
            color:data.color,
            thumbnail:data.thumbnail,
        } , _indentity()))

        if (result.code === 200) {
            onRefetch()
        } else {
            toast.error(result.message)
        }
    }

    const handleRemove = async () => {
        const isConfirm = await getDialogConfirmResult({
            title:"Delete item additional",
            description:"Are you sure delete this item?"
        })
        if(isConfirm) {
            try {
                const result = await useMuationRemove.mutateAsync(data.id);
                if (result.code === 200) {
                    onRefetch()
                    toast.success(result.message)
                } else {
                    toast.error(result.message)
                }
            } catch (error) {
                console.log(error)
                toast.error("Something went wrong. Please try again!")
            }
        }
       
    }

    return (
        <div className={clsx({
            [styles.disabled]:useMuationRemove.isLoading
        })}>
 <div className={clsx('row mb-4 g-4 align-items-stretch justify-content-between' , styles.itemOptions , {
    [styles.pointerNone]:useMuationRemove.isLoading
 })}>
            <div className='col-lg-6 mt-auto'>
                <div className='me-2 mb-2'>

                    <InputEdit value={data.name}  onChange={handleChangeName}/>
                </div>
                    <PickColor width={"100"} color={data.color} onChange={handleChangeColor}   id={'color-item-option-' + data.id}  />
            </div>
            {/* <div className='col-lg-2'>
                    <small className='text-sencodary mt-auto' >
                        Feel month ago
                    </small>
            </div> */}
          
            <div className='w-auto float-md-end '>
                <UploadImage  
                    size={{
                        width:60,
                        height:60
                    }}
                    classNameWrapper='shadow-none ml-auto' 
                    id={'thumbail-item-option-' + data.id} 
                    src={data.thumbnail}
                    onChange={(file:UploadFileResponse) => {
                        handleChangeFile(file)
                    }} 
                    title="Thumbnail Clipart" />
            </div>
            <div
                 className='btn btn-icon btn-sm btn-active-icon-primary  h-auto'
                data-kt-users-modal-action='close'
                onClick={handleRemove}
                style={{cursor: 'pointer'}}
            >
            <i className="fas fa-trash-alt"></i>
            </div>
            {
                useMuationRemove.isLoading && <Loading />
            }
        </div>
            
        </div>
       
    )
}

export default ItemOptions