import Loading from 'components/common/loading-metronic-style';
import useDetailAdditionOptions from 'data/hooks/use-detail-addition-options';
import { ID, KTSVG } from 'helpers';
import _isEmpty from "lodash-es/isEmpty"

import { useCallback, useEffect, useMemo } from 'react'
import { useSearchParams,  useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useAdditionalOptions } from './context';
import FormAddition from './form-additional'
import FormOptions from './form-options';

type Props = {}

const AdditionalOptions = (props: Props) => {
  const [searchParams] = useSearchParams();
  const navigate = useNavigate();
  const additionalID = parseInt(searchParams.get("id")) as ID

  const { data , isFetched, isLoading,  refetch } = useDetailAdditionOptions(additionalID)

  const { isCreate, onCreate } = useAdditionalOptions();

  useMemo(() => {
    if (!isNaN(additionalID)) {
      if (isCreate) {
        onCreate(false)
      }
   
    }
  }, [isCreate, onCreate,  additionalID])

  const handleCreate = useCallback(() => {
    navigate("/assets/additional-option" , {replace:true})
    onCreate(true)
  }, [onCreate, navigate])

  const handleOnRefetch = () => {
    refetch()
  }

  useEffect(() => {
    if(!!data && !data?.success ){
        toast.error(data.message);
        setTimeout(()=> {
            navigate("/assets/additional-option")
        }, 300)
    }
}, [data, navigate])

  return (
    <div className='col-lg-9'>
      <div className='card'>
        <div className='card-header justify-content-end'>
          {
            searchParams.get("id") &&
            <div className='card-title ' >
              <button type='button' className='btn btn-primary' onClick={handleCreate} >
                <KTSVG path='/media/icons/duotune/arrows/arr075.svg' className='svg-icon-2' />
                Create new addition
              </button>
            </div>
          }
        </div>
        <div className='card-body position-relative'>

          <div className='row'>
            {
              !searchParams.get("id") &&
              <div className='col-lg-12'>
                {
                  isCreate ? <FormAddition data={null} /> :
                    <div className='d-flex justify-content-center align-items-center'>
                      <span className='me-3'>Select one category on the left or</span>
                      <button type='button' className='btn btn-primary' onClick={() => onCreate(true)} >
                        <KTSVG path='/media/icons/duotune/arrows/arr075.svg' className='svg-icon-2' />
                        Create a addition option
                      </button>
                    </div>
                }
              </div>
            }

          </div>
        
          {
           isFetched && !isLoading && !_isEmpty(data?.data) &&
            <div className='row'>
              <div className='col-lg-6'>
                <FormAddition data={data?.data} />
              </div>
              <div className='col-lg-6 rounded-1 py-4'   style={{background:"#f1f1f1" }}>
                <FormOptions data={data?.data?.items} onRefetch={handleOnRefetch} additional_option_id={data?.data?.id} />
              </div>
            </div>

          }
            {(isLoading ) &&
            <Loading />
          }
        </div>

      </div>
    </div>

  )
}

export default AdditionalOptions