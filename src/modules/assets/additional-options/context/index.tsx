import {FC, useState, createContext, useContext,  useCallback} from 'react'
import _isEmpty from "lodash-es/isEmpty"
import { ID } from 'helpers'
type ContextProps = {
  selected: Array<ID>
  onSelect: (selectedKeys: ID[]) => void
  isCreate:boolean,
  onCreate:(flag?:boolean)=>void
}
const initContext:ContextProps = {
        selected: [],
     onSelect: (selectedKeys: ID[]) => {},
     isCreate:false,
     onCreate(flag?:boolean) {
         
     },
    }
const AdditionalOptionsContext = createContext<ContextProps>(initContext)

const AdditionalOptionsProvider: FC = ({children}) => {
  const [selected, setSelected] = useState<Array<ID>>([])
  const [isCreate, setIsCreate] = useState<boolean>(false);
  // const [itemIdForUpdate, setItemIdForUpdate] = useState<ID>(initialListView.itemIdForUpdate)

  // const disabled = useMemo(() => calculatedGroupingIsDisabled(isLoading, data), [isLoading, data])
  // const isAllSelected = useMemo(() => calculateIsAllDataSelected(data, selected), [data, selected])
  const handelSelect = useCallback( (data:ID[]) =>{
    setSelected(data)
  },[])
  const handleCreate = useCallback((flag?:boolean) =>{
    if(_isEmpty(flag)){
      setIsCreate(!isCreate)
    }else{
      setIsCreate(flag)
    }
  }, [isCreate])
  return (
    <AdditionalOptionsContext.Provider
      value={{
        selected,
        onSelect:handelSelect,
        isCreate,
        onCreate:handleCreate
        
      }}
    >
      {children}
    </AdditionalOptionsContext.Provider>
  )
}

const useAdditionalOptions = () => useContext(AdditionalOptionsContext)

export {AdditionalOptionsProvider, useAdditionalOptions}
