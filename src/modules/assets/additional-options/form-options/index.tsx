import React from 'react'
import createdAdditionalOptionItem from 'apis/created-item-addtional-option'
import clsx from 'clsx'
import Loading from 'components/common/loading-metronic-style'
import { useFormik } from 'formik'
import _isEmpty from "lodash-es/isEmpty"
import { toast } from 'react-toastify'

import { ID, KTSVG } from 'helpers'
import { AdditionalOptionItem } from 'interfaces/additional-options'
import * as Yup from 'yup'
import ItemOptions from '../components/item-options'
import { useMutation } from 'react-query'
import { CreateAdditionalOptionItemRequest } from 'apis/created-item-addtional-option/inteface'
import EmptyData from 'components/common/empty-data'

type Props = {
    data:AdditionalOptionItem[]
    additional_option_id: ID,
    onRefetch:()=>void
}

const schema = Yup.object().shape({

    name: Yup.string()
        .min(3, 'Minimum 3 symbols')
        .max(50, 'Maximum 50 symbols')
        .required('Name is required'),
})

const FormOptions = ({ data,  additional_option_id,onRefetch }: Props) => {
   
    const useMutationCreate = useMutation((payload:CreateAdditionalOptionItemRequest) => createdAdditionalOptionItem(payload))
    

    const formik = useFormik({
        initialValues: { name: '' },
        validationSchema: schema,
        onSubmit: async (values, { setSubmitting }) => {
            setSubmitting(true)
            try {
                const result = await useMutationCreate.mutateAsync({
                    additional_option_id,
                    name: values.name
                })
                if (result.code === 200) {
                    toast.success(result.message)
                    formik.resetForm()
                } else {
                    toast.error(result.message)
                }
            } catch (ex) {
                console.error(ex)
            } finally {
                setSubmitting(false)
                cancel(true)
            }
        },
    })
    const cancel = (withRefresh?: boolean) => {
        if (withRefresh) {
            onRefetch()
        }
        // setItemIdForUpdate(undefined)
    }
    return (
        <div className='position-relative w-100  h-100
        ' style={{
        maxHeight:"500px" , overflow:"auto"
        }}>
            <form onSubmit={formik.handleSubmit} className=' form mb-6'>
                <div className='row'>
                    <label className='col-lg-12 col-form-label required fw-bold fs-6'>Name option</label>
                   <div className='d-flex'>
                   <input
                        {...formik.getFieldProps('name')}
                        placeholder='Full name'
                        type='text'
                        name='name'
                        className={clsx(
                            'form-control form-control-solid me-4 bg-white',
                            { 'is-invalid': formik.touched.name && formik.errors.name },
                            {
                                'is-valid': formik.touched.name && !formik.errors.name,
                            }
                        )}
                        autoComplete='off'
                        disabled={formik.isSubmitting}
                    />
                    <button className='btn btn-sm btn-flex btn-primary'
                     disabled={formik.isSubmitting || !formik.isValid || !formik.touched}
                    >
                        <KTSVG
                            path='/media/icons/duotune/arrows/arr075.svg'
                            className='svg-icon-2 svg-icon-gray-500 me-1'
                        />
                        Add
                    </button>
                 
                   </div>
                   {formik.touched.name && formik.errors.name && (
                            <div className='fv-plugins-message-container'>
                                <div className='fv-help-block'>
                                    <span role='alert'>{formik.errors.name}</span>
                                </div>
                            </div>
                        )}
                </div>
            </form>
            <div className='position-relative'>
                {
                    !_isEmpty(data) ? 
                    data?.map((item: AdditionalOptionItem) => <ItemOptions onRefetch={onRefetch} data={item} key={item.id} />)
                    :
                    <EmptyData title='No data options' />
                }
            </div>
            {useMutationCreate.isLoading && <Loading />}
        </div>
    )
}

export default FormOptions