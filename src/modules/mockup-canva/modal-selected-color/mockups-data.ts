export const dataProduct = [
  {
    id: 1,
    title: 'Unisex T-Shirt',
    design: '2400 x 3200',
    url: '/media/products-mockup/unisex.png',
  },
  {
    id: 2,
    title: 'Hoodie',
    design: '2400 x 3200',
    url: '/media/products-mockup/hoodie.png',
  },
  {
    id: 3,
    title: 'Women T-Shirt',
    design: '2400 x 3200',
    url: '/media/products-mockup/women-t-shirt.png',
  },
  {
    id: 4,
    title: 'Tank Top',
    design: '2400 x 3200',
    url: '/media/products-mockup/tank-top.png',
  },
  {
    id: 5,
    title: 'Sweatshirt',
    design: '2400 x 3200',
    url: '/media/products-mockup/sweatshirt.png',
  },
  {
    id: 6,
    title: 'Long Sleeve Tee',
    design: '2400 x 3200',
    url: '/media/products-mockup/long-sleeve-see.png',
  },
  {
    id: 7,
    title: 'Youth T-Shirt',
    design: '2400 x 3200',
    url: '/media/products-mockup/youth-t-shirt.png',
  },
  {
    id: 8,
    title: 'Youth Hoodie',
    design: '2400 x 3200',
    url: '/media/products-mockup/youth-hoodie.png',
  },
  {
    id: 9,
    title: 'Softstyle V-Neck T-Shirt',
    design: '2400 x 3200',
    url: '/media/products-mockup/softstyle.png',
  },
  {
    id: 10,
    title: "Women's V-Neck T-Shirt",
    design: '2400 x 3200',
    url: '/media/products-mockup/women-v-neck.png',
  },
  {
    id: 11,
    title: 'Ceramic Mugs - Normal',
    design: '2400 x 3200',
    url: '/media/products-mockup/ceramic-mugs-normal.png',
  },
  {
    id: 12,
    title: 'Baby Onesie',
    design: '2400 x 3200',
    url: '/media/products-mockup/baby-onesie.png',
  },
]
