/* eslint-disable react-hooks/exhaustive-deps */
import React , { useState} from 'react'
import {Modal} from 'antd'



type Props = {
  modalData: any
  setModalData: any
  onSelectColor: (e: React.ChangeEvent<HTMLInputElement>, products: any) => void
  // dataSelected:any[]
}

const ModalSelectedColor = ({modalData, setModalData, onSelectColor}) => {
  const [colorSelected, setColorSelected] = useState<String[]>(modalData?.data?.color);
  const handleCancel = (flag: boolean) => {
    // refetch();
    setModalData({
      visible: false,
      data: null,
    })
  }
const handleSelectedColor = (isActive , colorItem) =>{
  const tempSelected = [...colorSelected]

  if(isActive){
   const  index  = tempSelected.findIndex(itemSelect => itemSelect === colorItem);
   if(index !== -1){
     tempSelected.slice(0 , 1);
   }
  }else{
   tempSelected.push(colorItem)
  }
  setColorSelected([...tempSelected]);
  onSelectColor &&  onSelectColor(tempSelected)
}

// useEffect(() => {
//   const data = modalData?.data
//   if(!!data && Array.isArray(data.color) && !_isEqual(data.color , colorSelected)){
//    setColorSelected(data.color)
//   }
//  }, [modalData]);
  return (
    <>
      <Modal
        title={'Select Color '}
        visible={modalData.visible}
        onCancel={() => handleCancel(false)}
        destroyOnClose
        // footer={}
       onOk={() => handleCancel(false)}
        width={800}
      >
        <div className='row g-2'>
          {[
            'rgb(22, 37, 71)',
            'rgb(207, 201, 202)',
            'rgb(37, 40, 43)',
            'rgb(54, 69, 79)',
            'rgb(70, 50, 46)',
            'rgb(16, 39, 27)',
            'rgb(252, 177, 41)',
          ].map((color: any) => {
            const active = colorSelected?.findIndex((colorItem: any) => colorItem === color) !== -1
            return (
              <a
                type='color'
                onClick={() => handleSelectedColor(active, color)}
                className='w-25px h-25px m-2'
                style={{background: color, color: !!active ? 'silver' : 'transparent'}}
              >
                {!!active && <i className='fa fa-check'></i>}
              </a>
            )
          })}
        </div>
      </Modal>
    </>
  )
}

export default ModalSelectedColor
