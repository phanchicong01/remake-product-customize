import React, {  useState } from 'react'
import { useParams } from 'react-router-dom'

import { CAMPAIGNS } from 'constants/path';
import useCampaignDetail from 'data/hooks/use-campaign-detail';
import { PageLink, PageTitle } from 'layout/core';
import { ModalSelectArtwork } from '../modal-select-artwork';
import ItemArtworkSelected from './ItemArtworkSelected';
import Loading from 'components/common/loading-metronic-style';

type Props = {}
const breadcrumbs: Array<PageLink> = [
  {
    title: 'Home',
    path: '/',
    isSeparator: false,
    isActive: false,
    isRedirect: true
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  }, {
    title: 'Campaigns',
    path: CAMPAIGNS,
    isSeparator: false,
    isActive: false,
    isRedirect: true
  }, {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  }
]

const CampaignDetail = (props: Props) => {
  const { campaignId } = useParams();
  const [dataModalArtwork, setDataModalArtwork] = useState({
    visibe:false,
    name:""
  })

  const { data, isLoading,  refetch } = useCampaignDetail(parseInt(campaignId));

  const handleCloseModal = (isReload:boolean) =>{
    if(isReload){
      refetch()
    }
    setDataModalArtwork({
      visibe:false,
      name:""
    })
  }

  return (
    <div className="card">
      {isLoading && <Loading />}
      <PageTitle breadcrumbs={breadcrumbs}>{`Campaign ${data?.name}`}</PageTitle>
      <div className='card-header '>
        <div className="d-flex align-items-center p-4">
          <span>Print Areas of  <b>{data?.name}</b></span>
        </div>
      </div>
      <div className='card-body'>
        <div className='row align-items-lg-stretch'>
          <div className='col-lg-6 border'>
              <div className='row'>
                <div className='col-4 bg-secondary d-flex align-items-center '>
                  <h4>
                    Font Area
                  </h4>
                </div>
                <div className='col-8'>
                  <ItemArtworkSelected onSelectArtwork={()=>setDataModalArtwork({
                  visibe:true,
                  name:"front_artwork_id"
                })} id={data?.front_artwork_id} name="front_artwork_id"
                refetch={refetch} />
                </div>
              </div>
          </div>
          <div className='col-lg-6 border'>
              <div className='row '>
              <div className='col-4 bg-secondary d-flex align-items-center'>
                  <h4>
                    Back Area
                  </h4>
                </div>
                <div className='col-8'>
                <ItemArtworkSelected onSelectArtwork={()=>setDataModalArtwork({
                  visibe:true,
                  name:"back_artwork_id"
                })} id={data?.back_artwork_id} name="back_artwork_id"
                refetch={refetch}
                />

                </div>
              </div>
          </div>
        </div>
      </div>
      <ModalSelectArtwork show={dataModalArtwork.visibe} handleClose={handleCloseModal} name={dataModalArtwork.name} />
    </div>
  )
}

export default CampaignDetail