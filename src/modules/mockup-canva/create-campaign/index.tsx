import Thumb from 'components/common/thumb'
import MockupCanvaComponent from 'components/widgets/mockup-canva'

import {useState} from 'react'
import { MockupProductCategory } from 'store/mockup-canva-design/interface'
import { useMockupCanva } from '../core/MockupCanvaProvider'
import {ModalCreateMockupCampaign} from '../modal-create-campaign'
import ProductCategoryItem from './product-category-item.tsx'
import styles from './styles.module.scss'
type Props = {}

const MockupCreateCampagin = (props: Props) => {
  const {productCategorySelected , updateProductCategory} = useMockupCanva()
  const [dataModal, setModalData] = useState<any>({
    visible: false,
    data: null,
  })
 
  // const [productSelected, setProductSelected] = useState<any[]>([])
  const [productIsActive, setProductIsActive] = useState<MockupProductCategory & {colorSelected?:string}>(null)
  const handleAddProductCampaign = () => {
    setModalData({
      visible: true,
      data: null,
    })
  }

  const handleSelectColor = (colors: any, index:number ) => {
    if(!!colors && Array.isArray(colors)){
      const tempProductSelected = [...productCategorySelected]
      tempProductSelected[index].color = colors
      updateProductCategory([...tempProductSelected])
    }
  }
  // const handleSelectProduct = (e: React.ChangeEvent<HTMLInputElement>, productSelect: any) => {
  //   const checked = e.target.checked
  //   // const isExsit =  productSelected.find((item:any)=> item.id === productSelect.id)
  //   if (!checked) {
  //     return
  //   } else {
  //     setProductSelected((prev: any) => [...prev, productSelect])
  //   }
  // }
  const handleSelectProductActive = (item) => {
    setProductIsActive(item);
    const flagScrollto = document.getElementById("detail-mockup");
    if(!!flagScrollto){
      flagScrollto.scrollIntoView({
        behavior:"smooth",
        block:"start"
      })
    }

  }
 
  const handleChangeFile = (event) => {
    const indexProduct = productCategorySelected?.findIndex((item: any) => item.id === productIsActive?.id)
    if (indexProduct !== -1) {
      const tempFront = productCategorySelected[indexProduct].artwork.front;
      const files = event.target.files
      const file = Array.from(files)[0]
      tempFront.src = file;
      const tempProductSelected = productCategorySelected
      tempProductSelected[indexProduct].artwork.front = tempFront;
      updateProductCategory([...tempProductSelected])
      setProductIsActive((prev) => ({...prev, front:tempFront}))
    }
   
  }
  const handleChangeFileBack = (event) => {
    const indexProduct = productCategorySelected?.findIndex((item: any) => item.id === productIsActive?.id)
    if (indexProduct !== -1) {
      const tempBack = productCategorySelected[indexProduct].artwork.back;
      const files = event.target.files
      const file = Array.from(files)[0]
      tempBack.src = file;
      const tempProductSelected = productCategorySelected
      tempProductSelected[indexProduct].artwork.back = tempBack;
      updateProductCategory([...tempProductSelected])
      setProductIsActive((prev) => ({...prev, back:tempBack}))
    }
  }
  const handleSelectCategory  = (items:MockupProductCategory[]) => {
    updateProductCategory(items);
    if(items?.length  > 0 && !productIsActive){
      setProductIsActive(items[0])
    }
  }
  const handleRemoveProductCategory  = (id:MockupProductCategory) => {
    debugger
    const index =  productCategorySelected.findIndex((items:any) => items.id === id);
    if(index !== -1){
      const tempProduct:any =  [...productCategorySelected];
      tempProduct.splice(index , 1);
      updateProductCategory(tempProduct)
      if(tempProduct?.length > 0){
        setProductIsActive(tempProduct[0])
      }
    }
  }
  const handleSubmit = () => {
    console.log(productCategorySelected , 'data - submit');
    
  }
  return (
    <div className='container m-auto mt-auto'>
      <div className='card card-bordered shadow-sm'>
        <div className='card-header'>
          <h2 className='card-title'>1. Design </h2>
        </div>

        <div className='card-body'>
          <div className='row'>
            <div className='col-3'>
              {productCategorySelected?.map((item: any , index:number) => {
                return (
                  <div className='d-flex' key={item.id}>
                    <div className='col'>
                      <ProductCategoryItem 
                        dataProduct={item}
                        onRemove={handleRemoveProductCategory}
                        onSelectedProduct={handleSelectProductActive}
                        onSelectedColors={(colors)=>handleSelectColor(colors ,index )}
                        />
                      
                    </div>
                  </div>
                )
              })}
              <div className='row'>
                <button
                  type='button'
                  className='btn btn-primary'
                  onClick={handleAddProductCampaign}
                >
                  Add product
                </button>
              </div>
            </div>
            <div className='col-9'>
              {!!productIsActive && (
                <div className='card card-bordered shadow-sm'>
                  <div id="detail-mockup"></div>
                  <div className='card-title p-5'>
                    Design ({productIsActive?.design?.width} x {productIsActive?.design?.height} 150DPI) {productIsActive?.title}
                  </div>
                  <div className='card-body'>
                      <div className='row '>
                        <div className='col'>
                          <p className='text-center  font-bold border bg-gray-100'><b>Front artwork</b></p>
                          <div className='d-flex align-items-center flex-column'>
                            <div className=''>
                              <div
                                className={styles.thumbnail}
                                style={{backgroundImage: "url('/bg-transparent.png')" ,
                               width:120,
                               height:160
                            }}
                                role='button'
                              >
                                <label
                                  role='button'
                                  className={styles.labelThumbnail}
                                  htmlFor={'thumbail-item-option-front'}
                                >
                                  Front artwork
                                </label>
                                <input
                                  onChange={handleChangeFile}
                                  className='d-none'
                                  id={'thumbail-item-option-front'}
                                  type='file'
                                  accept='image/png,image/jpg,image/jpeg'
                                />
                                {productIsActive?.artwork?.front && (
                                  <label className='position-absolute top-0 start-0 w-100 h-100'>
                                    <Thumb width={'100%'} height='100%' file={productIsActive?.artwork?.front?.src } />
                                  </label>
                                )}
                              </div>
                            </div>
                            <MockupCanvaComponent
                              color={productIsActive?.colorSelected}
                              dataMockup={productIsActive?.artwork?.front}
                              imgSrc={productIsActive.srcFront}
                              // productIsActive={productIsActive}
                            />
                          </div>
                        </div>
                        <div className='col'>
                          <p className='text-center  font-bold border bg-gray-100 '>
                            <b>Back Artwork</b>
                          </p>
                          <div className='d-flex align-items-center flex-column'>
                            <div className=''>
                              <div
                                className={styles.thumbnail}
                                style={{backgroundImage: "url('/bg-transparent.png')" , 
                                width:120,
                                height:160
                              }}
                                role='button'
                              >
                                <label
                                  role='button'
                                  className={styles.labelThumbnail}
                                  htmlFor={'thumbail-item-option-back'}
                                >
                                  Back artwork
                                </label>
                                <input
                                  onChange={handleChangeFileBack}
                                  className='d-none'
                                  id={'thumbail-item-option-back'}
                                  type='file'
                                  accept='image/png,image/jpg,image/jpeg'
                                />
                                {productIsActive?.artwork?.back  && (
                                  <label className='position-absolute top-0 start-0 w-100 h-100'>
                                    <Thumb width={'100%'} height='100%' file={productIsActive?.artwork?.back?.src } />
                                  </label>
                                )}
                              </div>
                            </div>
                            <MockupCanvaComponent
                              color={productIsActive?.colorSelected}
                              dataMockup={productIsActive?.artwork?.back}
                              imgSrc={productIsActive.srcBack}
                              // productIsActive={productIsActive}
                            />
                          </div>
                        </div>
                      </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
        <div className='card-footer'>
          <button onClick={handleSubmit} type="submit" className='btn btn-primary fl-right'>Submit</button>
        </div>
      </div>
   
      <ModalCreateMockupCampaign
      dataSelected={productCategorySelected}
      onSelected={handleSelectCategory}
      modalData={dataModal}
      setModalData={setModalData}
      />
    </div>
  )
}

export default MockupCreateCampagin
