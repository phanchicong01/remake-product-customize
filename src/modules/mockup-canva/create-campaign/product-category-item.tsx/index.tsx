import { toAbsoluteUrl } from 'helpers'
import ModalSelectedColor from 'modules/mockup-canva/modal-selected-color'
import {useState , useEffect, useCallback} from 'react'
import { MockupProductCategory } from 'store/mockup-canva-design/interface'
import _isEqual from "lodash-es/isEqual";
import useConfirmDialog from 'hooks/use-confirm-dialog';

type Props = {
    dataProduct:MockupProductCategory
    onSelectedProduct:(productCategory:MockupProductCategory &  {colorSelected?:string}) => void
    onSelectedColors:(colors:string[]) => void
    onRemove:(idProductCategory:any) => void 
}

const ProductCategoryItem = ({dataProduct , onSelectedProduct, onSelectedColors,onRemove}: Props) => {
  const  {getDialogConfirmResult} = useConfirmDialog()  
  const [dataModalColor, setDataModalColor] = useState<any>({
        visible: false,
        data: null,
    })
    const [colorIsActive , setColorActive] = useState<string>('')

    const handleSelectedProduct = ()=> {
        onSelectedProduct({...dataProduct,colorSelected:colorIsActive})
    }
    const handleSelectColor = (colors:any) => {
        // setDataModalColor((prev:any)=> ({...prev , color:colors}))
        onSelectedColors(colors)
        
    }
    const handleSelectedSingleColorProduct =(color:any) => {
        setColorActive(color)
        onSelectedProduct({...dataProduct,colorSelected:color})
    }
   
    const handleRemove = useCallback(async ()=>{
      const isConfirmOk = await  getDialogConfirmResult({
        title:"Delete Product Mockups",
        description:"Are you sure delete this products "+ dataProduct?.title+"?"  
      })
      if(isConfirmOk){
        try {
         
            // const result = await  useMutationRemove.mutateAsync(id);
            // if(result.code === 200){
            //   toast.success(result.message);
            //   refetch()
            // }else{
            //   toast.error(result.message);
            // }
            onRemove(dataProduct?.id)
          
        } catch (error) {
          // console.error(error)
          // toast.error("Something went wrong. Please try again!")
        }
      }
     
    },[dataProduct?.id, dataProduct?.title, getDialogConfirmResult, onRemove])

    useEffect(() => {
        if(dataProduct?.color){
            setColorActive(dataProduct?.color[0])
        }
    }, [dataProduct])
    

  return (
    <div className='card card-bordered shadow-sm'>
    <div className='card-header '>
      <div className='d-flex w-100 justify-content-between align-items-center'>
        <span>{dataProduct.title}</span>
        <i className='fa fa-times fa-2x' onClick={handleRemove}></i>
      </div>
    </div>
    <div className='card-body'>
      <div className='d-flex' >
        <img
          onClick={handleSelectedProduct}
          style={{backgroundColor:colorIsActive ? colorIsActive: "transparent"}}
          src={toAbsoluteUrl(dataProduct.url)}
          alt={dataProduct.title}
          className='w-150px h-150px'
        //   onClick={handleSelectedProduct}
        />
        <div>
          <div className='d-flex flex-wrap mb-2'>
            {dataProduct?.color?.map((color: any) => (
              <a
              type='color'
              style={{background: color, color: !!colorIsActive === color ? 'silver' : 'transparent'}}
                className='border w-25px h-25px m-1 d-flex justify-content-center align-items-center'
                onClick={()=>handleSelectedSingleColorProduct(color)}
              >
               {
                colorIsActive === color &&
                 <i className='fa fa-check'></i>
               }
              </a>
            ))}
            <span
              onClick={() => {
                setDataModalColor({
                  visible: true,
                  data: dataProduct,
                })
                
              }}
              className=' w-25px h-25px bg-white border m-1
            shadow-sm d-flex align-items-center justify-content-center'
            >
              <i className='fa fa-plus'></i>
            </span>
          </div>
        </div>
      </div>
    </div>
    <div className='card-footer'>  
    View detail  <a onClick={handleSelectedProduct} 
        className='fl-right'><i className='fa fa-arrow-right'></i></a>
    </div>
   {dataModalColor.visible &&
     <ModalSelectedColor
        onSelectColor={handleSelectColor}
        modalData={dataModalColor}
        setModalData={setDataModalColor}
    />
   }
  </div>
  )
}

export default ProductCategoryItem