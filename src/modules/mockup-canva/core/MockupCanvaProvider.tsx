import { ID } from 'helpers'
import {FC, useState, createContext, useContext ,Dispatch ,useCallback} from 'react'

import { MockupProductCategory } from 'store/mockup-canva-design/interface'

const MockupCanvaContext = createContext<{
  productCategorySelected:Array<MockupProductCategory>,
  updateProductCategory:(item:MockupProductCategory[]) => void
}>({
  productCategorySelected:[],
  updateProductCategory: (item:MockupProductCategory[]) => {}
})

const MockupCanvaProvider: FC = ({children}) => {
  const [productCategorySelected , setProductCategorySelected] =  useState<MockupProductCategory[]>([])
 const handleUpdateProductCategory  =
   (dataUpdate:MockupProductCategory[]) => {
    setProductCategorySelected([...dataUpdate])
   }
  return (
    <MockupCanvaContext.Provider
      value={{
        productCategorySelected,
        updateProductCategory:handleUpdateProductCategory
      }}
    >
      {children}
    </MockupCanvaContext.Provider>
  )
}

const useMockupCanva = () => useContext(MockupCanvaContext)

export {MockupCanvaProvider, useMockupCanva}
