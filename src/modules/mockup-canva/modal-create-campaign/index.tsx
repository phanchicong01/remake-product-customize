/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useState} from 'react'
import {Modal} from 'antd'
import _isEqual from 'lodash-es/isEqual'

import {dataProduct, dataProduct3d} from '../../../data/mockup-data/mockup-products'
import Switch from 'components/common/switch'
import {MockupProductCategory} from 'store/mockup-canva-design/interface'
import {toAbsoluteUrl} from 'helpers'
import clsx from 'clsx'

type Props = {
  modalData: any
  setModalData: any
  onSelected: (items: any) => void
  dataSelected: any[]
}

const ModalCreateMockupCampaign: React.FC<Props> = ({
  modalData,
  setModalData,
  dataSelected,
  onSelected,
}) => {
  const [activeTab , setActiveTab] = useState("2d")
  const [productCategorySelected, setProductCategorySelected] = useState<MockupProductCategory[]>(
    []
  )

  useEffect(() => {
    if (
      !!dataSelected &&
      Array.isArray(dataSelected) &&
      !_isEqual(dataSelected, productCategorySelected)
    ) {
      setProductCategorySelected(dataSelected)
    }
  }, [dataSelected])

  const handleCancel = (flag: boolean) => {
    // refetch();
    setModalData({
      visible: false,
      data: null,
    })
  }
  const handleChangeSwitch = (isActive, item) => {
    const tempSelected = [...productCategorySelected]

    if (isActive) {
      const index = tempSelected.findIndex((itemSelect) => itemSelect.id === item.id)
      if (index !== -1) {
        tempSelected.slice(0, 1)
      }
    } else {
      tempSelected.push(item)
    }
    setProductCategorySelected([...tempSelected])
    onSelected && onSelected(tempSelected)
  }

  return (
    <>
      <Modal
        title={'Create Campaign'}
        visible={modalData.visible}
        onCancel={() => handleCancel(false)}
        destroyOnClose
        footer={null}
        width={800}
      >
        <ul className='nav nav-tabs nav-line-tabs mb-5 fs-6'>
          <li className='nav-item'>
            <a className={clsx('nav-link' , {
            'active': activeTab === "2d"
          })}  onClick={()=>setActiveTab('2d')} data-bs-toggle='tab' href='#kt_tab_pane_2d'>
              2D
            </a>
          </li>
          <li className='nav-item'>
            <a className={clsx('nav-link' , {
            'active': activeTab === "3d"
          })} onClick={()=>setActiveTab('3d')} data-bs-toggle='tab' href='#kt_tab_pane_3d'>
              3D
            </a>
          </li>
        </ul>
        <div className='tab-content '>
          <div className={clsx('tab-pane' , {
            ' fade show active': activeTab === "2d"
          })} id='kt_tab_pane_2d' role='tabpanel'>
            <div className='row g-2 '>
              {dataProduct.map((item) => {
                const isActive =
                  productCategorySelected.findIndex(
                    (itemSelected) => itemSelected.id === item.id
                  ) !== -1

                return (
                  <div className='col-3' key={item.id}>
                    <div className='card card-bordered shadow-sm h-100'>
                      <div className='card-body p-2'>
                        <img src={toAbsoluteUrl(item.url)} alt={item.title} className='w-100' />
                        {/* <div style={{height:"300px"}}>{item}</div> */}
                      </div>
                      <div className='card-footer'>
                        <h4>{item.title}</h4>
                        <h4>
                          <i>
                            {item.design?.width} X {item.design.height}
                          </i>
                        </h4>
                        <div className=''>
                          <Switch
                            checked={isActive}
                            onChange={() => handleChangeSwitch(isActive, item)}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                )
              })}
            </div>
          </div>
          <div className={clsx('tab-pane' , {
            ' fade show active': activeTab === "3d"
          })} id='kt_tab_pane_3d' role='tabpanel'>

            <div className='row g-2'>
              {dataProduct3d.map((item) => {
                const isActive =
                  productCategorySelected.findIndex(
                    (itemSelected) => itemSelected.id === item.id
                  ) !== -1

                return (
                  <div className='col-3' key={item.id}>
                    <div className='card card-bordered shadow-sm h-100'>
                      <div className='card-body p-2'>
                        <img src={toAbsoluteUrl(item.url)} alt={item.title} className='w-100' />
                        {/* <div style={{height:"300px"}}>{item}</div> */}
                      </div>
                      <div className='card-footer'>
                        <h4>{item.title}</h4>
                        <h4>
                          <i>
                            {item.design?.width} X {item.design.height}
                          </i>
                        </h4>
                        <div className=''>
                          <Switch
                            checked={isActive}
                            onChange={() => handleChangeSwitch(isActive, item)}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                )
              })}
            </div>
          </div>
        </div>
      </Modal>
    </>
  )
}

export {ModalCreateMockupCampaign}
