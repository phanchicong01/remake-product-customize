import { lazy } from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'
import { MockupCanvaProvider } from './core/MockupCanvaProvider'
import { QueryRequestProvider } from './core/QueryRequestProvider'
import { QueryResponseProvider } from './core/QueryResponseProvider'

type Props = {}
const MockupCampaignPageList = lazy(()=>  import('modules/mockup-canva/campaign-list'))
const MockupCampaignPageDetail = lazy(()=>  import('modules/mockup-canva/campaign-detail'))
const MockupCanva = (props: Props) => {
  return (
    <Routes>
    <Route>
      <Route>
        <Route
          path='view/:campaignId'
          element={
            <MockupCampaignPageDetail />
          }
        />
     </Route>
      <Route
        path='list'
        element={
          <QueryRequestProvider>
            <QueryResponseProvider>
              <MockupCanvaProvider>
                <MockupCampaignPageList />
              </MockupCanvaProvider>
            </QueryResponseProvider>
          </QueryRequestProvider>
        }
      />
    </Route>
    

    <Route index element={<Navigate to='list' />} />
  </Routes>
  )
}

export default MockupCanva