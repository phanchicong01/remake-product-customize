
import { CampaignHeader } from './components/header';
import {CampaignTable} from './table/CampaignTable';

import { PageLink, PageTitle } from 'layout/core';
// import { ModalCreateCampaign } from '../modal-create-campaign';


type Props = {}

const breadcrumbs: Array<PageLink> = [
    {
        title: 'Home',
        path: '/',
        isSeparator: false,
        isActive: false,
        isRedirect: true
    },
    {
        title: 'Mockup canva',
        path: '',
        isSeparator: false,
        isActive: false,
        isRedirect: false
    },
    {
        title: '',
        path: '',
        isSeparator: true,
        isActive: false,
    }
]

const ListCampaign = (props: Props) => {
    return (
        <>
            <PageTitle breadcrumbs={breadcrumbs}>Campaign</PageTitle>
            <div className='card'>
                <div className='card-body'>
                    <div className='row'>
                        <div className='col-lg-12'>
                            <CampaignHeader />
                        </div>
                        
                        <div className='col-lg-12'>
                            <CampaignTable />
                        </div>
                    </div>
                    {/* <ModalCreateCampaign  /> */}
                </div>
            </div>
        </>
    )
}

export default ListCampaign