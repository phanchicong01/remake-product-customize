/* eslint-disable jsx-a11y/anchor-is-valid */
import { FC, useCallback } from 'react'

import { Campaign } from 'interfaces/campaign'
import InputEdit from 'components/common/Input-edit'
import updateCampaign from 'apis/update-campaign'
import { useQueryResponse } from '../../../core/QueryResponseProvider'
import { toast } from 'react-toastify'

type Props = {
  data: Campaign
}

const CampaignCellName: FC<Props> = ({ data }) => {
  const {refetch} = useQueryResponse()
  const handleUpdateName = useCallback(async (value:string)=>{
    const {id} = data
    try {
      const result = await updateCampaign({
        id,
        data:{
          name:value
        }
      })
      if (result.code === 200) {
        toast.success(result.message)
        refetch()// handleClose(true);
      } else {
        toast.error(result.message);
      }
    } catch (error) {
      console.error(error);
      toast.error("Something went wrong. Please try again!")
    }
  }, [data, refetch])

  return (
    <InputEdit name='name' value={data.name} onChange={handleUpdateName} isShowConfirm={true} size="sm" />

  )
}

export { CampaignCellName }
