import clsx from 'clsx'
import { ProductBase } from 'interfaces/product-base'
import {FC} from 'react'
import {Row} from 'react-table'

type Props = {
  row: Row<ProductBase>
}

const CustomRow: FC<Props> = ({row}) => (
  <tr {...row.getRowProps()}>
    {row.cells.map((cell) => {
      return (
        <td
          {...cell.getCellProps()}
          className={clsx({'text-end min-w-100px': cell.column.id === 'actions'})}
        >
          {cell.render('Cell')}
        </td>
      )
    })}
  </tr>
)

export {CustomRow}
