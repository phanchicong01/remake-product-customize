/* eslint-disable jsx-a11y/anchor-is-valid */
import { FC } from 'react'

import { Campaign } from 'interfaces/campaign'
import { getUrl } from 'helpers'

type Props = {
  data: Campaign
}

const CampaignCellThumbnail: FC<Props> = ({ data }) => {

  return (
    <div className='row g-2'>
      <div className='col'>
        <div className='card card-bordered shadow-sm min-h-100px'>
          <div className="card-body p-2"><img className='card-img-top' src={getUrl(data.thumbnail_front)} alt={data.name + '_front'} /></div>
        </div>
      </div>
      <div className='col'>
        <div className='card card-bordered shadow-sm min-h-100px'>
          <div className="card-body p-2">
            <img className='card-img-top' src={getUrl(data.thumbnail_back)} alt={data.name + '_back'} />
          </div>
        </div>
      </div>
    </div>
  )
}

export { CampaignCellThumbnail }
