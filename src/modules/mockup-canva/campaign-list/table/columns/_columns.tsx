import { Column } from 'react-table'
import moment from 'moment'

import { CampaignActionsCell } from './CampaignActionsCell'
import { CampaignCustomHeader } from './CampaignCustomHeader'
import { Campaign } from 'interfaces/campaign'
import { CampaignCellThumbnail } from './CampaignCellThumbnail'
import { CampaignCellName } from './CampaignCellName'

const campaignColumns: ReadonlyArray<Column<Campaign>> = [
  {
    Header: (props) => (
      <CampaignCustomHeader tableProps={props} title='Thumbnail' className='w-350px' />
    ),
    id: 'thumbnail',
    Cell: ({ ...props }) => <CampaignCellThumbnail data={props.data[props.row.index]}/>

  },
  {
    Header: (props) => (
      <CampaignCustomHeader tableProps={props} title='Name' className='min-w-300px' />
    ),
    id: 'name',
    accessor: 'name',
    Cell:({...props}) => <CampaignCellName data={props.data[props.row.index]} />
  },
  {
    Header: (props) => (
      <CampaignCustomHeader tableProps={props} title='created at' className='min-w-125px' />
    ),
    id: 'created_at',
    Cell: ({ ...props }) => moment(props.data[props.row.index].created_at).format('dddd, MMMM Do YYYY'),
  },
  {
    Header: (props) => (
      <CampaignCustomHeader tableProps={props} title='Actions' className='text-end min-w-100px' />
    ),
    id: 'actions',
    Cell: ({ ...props }) => <CampaignActionsCell id={props.data[props.row.index].id} />,
  },
]

export { campaignColumns }
