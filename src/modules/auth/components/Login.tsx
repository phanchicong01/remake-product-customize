/* eslint-disable jsx-a11y/anchor-is-valid */
import * as Yup from 'yup'
import clsx from 'clsx'
import {useMutation} from 'react-query'
import {Link} from 'react-router-dom'
import {useFormik} from 'formik'

import login from 'apis/login'
import {LoginPayload} from 'apis/login/interface'
import {useAuth} from '../core/Auth'
import getAuthMemberByToken from 'apis/get-auth-member-by-token'

const loginSchema = Yup.object().shape({
  email: Yup.string()
    .email('Wrong email format')
    .min(3, 'Minimum 3 symbols')
    .max(50, 'Maximum 50 symbols')
    .required('Email is required'),
  password: Yup.string()
    .min(3, 'Minimum 3 symbols')
    .max(50, 'Maximum 50 symbols')
    .required('Password is required'),
})

const initialValues = {
  email: '',
  password: '',
}

export function Login() {
  const {saveAuth, setCurrentUser} = useAuth()
  const loginMutation = useMutation((payload: LoginPayload) => login(payload))

  const formik = useFormik({
    initialValues,
    validationSchema: loginSchema,
    onSubmit: async (values, {setStatus, setSubmitting}) => {
      try {
        // const result = await loginMutation.mutateAsync({ email: values.email, password: values.password })
        const result = {
          success: true,
          data: {
            access_token:
              'eyJhbGciOiJIUzI1NiIsImtpZCI6IkhGUFZVMCtOdUVleDd4VVciLCJ0eXAiOiJKV1QifQ.eyJhdWQiOiJhdXRoZW50aWNhdGVkIiwiZXhwIjoxNzEzMjQ0ODk5LCJpYXQiOjE3MTMyNDEyOTksImlzcyI6Imh0dHBzOi8vYmNmb3RiY2J3emdyanJ4c2Jrc2Iuc3VwYWJhc2UuY28vYXV0aC92MSIsInN1YiI6IjU5ZjdlYTBiLTMyZTUtNDFjOS1hZTBlLWM2ZTE4Y2IyNTMwMyIsImVtYWlsIjoicm9vdEBjYW5hd2FuLmNvbSIsInBob25lIjoiIiwiYXBwX21ldGFkYXRhIjp7InByb3ZpZGVyIjoiZW1haWwiLCJwcm92aWRlcnMiOlsiZW1haWwiXX0sInVzZXJfbWV0YWRhdGEiOnt9LCJyb2xlIjoiYXV0aGVudGljYXRlZCIsImFhbCI6ImFhbDEiLCJhbXIiOlt7Im1ldGhvZCI6InBhc3N3b3JkIiwidGltZXN0YW1wIjoxNzEzMjQxMjk5fV0sInNlc3Npb25faWQiOiI3MmY2MjA4My00MzAwLTQ5OWYtYjc5Ni1kMjE0ZDRkYTM1NmUiLCJpc19hbm9ueW1vdXMiOmZhbHNlfQ.oIQvyHeyUg87i-KiS1XMm-nl8XUXYVhXNhsqO6vqFIY',
            expires_in: '1713244899',
            token_type: 'bearer',
            user: {
              created_at: '2021-02-12T07:38:19.000000Z',
              email: 'admin@gmail.com',
              email_verified_at: '2021-02-12T07:38:19.000000Z',
              avatar: ' https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_960_720.png',
              id: 1,
              name: 'admin',
              updated_at: '2021-02-12T07:38:19.000000Z',
            },
          },
          message: 'Login successfully',
        }
        if (result.success && !!result.data) {
          saveAuth(result.data)
          // const data = await getAuthMemberByToken(result.data.access_token)
          const data = {
            created_at: '2021-02-12T07:38:19.000000Z',
            email: 'admin@gmail.com',
            email_verified_at: '2021-02-12T07:38:19.000000Z',
            avatar: ' https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_960_720.png',
            id: 1,
            name: 'admin',
            updated_at: '2021-02-12T07:38:19.000000Z',
          }
          if (data) {
            setCurrentUser(data)
          }
        } else {
          setStatus(result.message)
        }
      } catch (error) {
        console.error(error)
        saveAuth(undefined)
        setStatus('The login detail is incorrect')
        setSubmitting(false)
      }
    },
  })

  return (
    <form
      className='form w-100'
      onSubmit={formik.handleSubmit}
      noValidate
      id='kt_login_signin_form'
    >
      {/* begin::Heading */}
      <div className='text-center mb-10'>
        <h1 className='text-dark mb-3'>Sign In to Custom Product</h1>
        <div className='text-gray-400 fw-bold fs-4'>
          New Here?{' '}
          <Link to='/auth/registration' className='link-primary fw-bolder'>
            Create an Account
          </Link>
        </div>
      </div>
      {/* begin::Heading */}

      {formik.status ? (
        <div className='mb-lg-15 alert alert-danger'>
          <div className='alert-text font-weight-bold'>{formik.status}</div>
        </div>
      ) : (
        <div className='mb-10 bg-light-info p-8 rounded'>
          <div className='text-info'>
            Use <strong>CANAWAN account</strong> to continue.
          </div>
        </div>
      )}

      {/* begin::Form group */}
      <div className='fv-row mb-10'>
        <label className='form-label fs-6 fw-bolder text-dark'>Email</label>
        <input
          placeholder='Email'
          {...formik.getFieldProps('email')}
          className={clsx(
            'form-control form-control-lg form-control-solid',
            {'is-invalid': formik.touched.email && formik.errors.email},
            {
              'is-valid': formik.touched.email && !formik.errors.email,
            }
          )}
          type='email'
          name='email'
          autoComplete='off'
        />
        {formik.touched.email && formik.errors.email && (
          <div className='fv-plugins-message-container'>
            <span role='alert'>{formik.errors.email}</span>
          </div>
        )}
      </div>
      {/* end::Form group */}

      {/* begin::Form group */}
      <div className='fv-row mb-10'>
        <div className='d-flex justify-content-between mt-n5'>
          <div className='d-flex flex-stack mb-2'>
            {/* begin::Label */}
            <label className='form-label fw-bolder text-dark fs-6 mb-0'>Password</label>
            {/* end::Label */}
            {/* begin::Link */}
            <Link
              to='/auth/forgot-password'
              className='link-primary fs-6 fw-bolder'
              style={{marginLeft: '5px'}}
            >
              Forgot Password ?
            </Link>
            {/* end::Link */}
          </div>
        </div>
        <input
          type='password'
          autoComplete='off'
          {...formik.getFieldProps('password')}
          className={clsx(
            'form-control form-control-lg form-control-solid',
            {
              'is-invalid': formik.touched.password && formik.errors.password,
            },
            {
              'is-valid': formik.touched.password && !formik.errors.password,
            }
          )}
        />
        {formik.touched.password && formik.errors.password && (
          <div className='fv-plugins-message-container'>
            <div className='fv-help-block'>
              <span role='alert'>{formik.errors.password}</span>
            </div>
          </div>
        )}
      </div>
      {/* end::Form group */}

      {/* begin::Action */}
      <div className='text-center'>
        <button
          type='submit'
          id='kt_sign_in_submit'
          className='btn btn-lg btn-primary w-100 mb-5'
          disabled={formik.isSubmitting || !formik.isValid}
        >
          {!loginMutation.isLoading && <span className='indicator-label'>Continue</span>}
          {loginMutation.isLoading && (
            <span className='indicator-progress' style={{display: 'block'}}>
              Please wait...
              <span className='spinner-border spinner-border-sm align-middle ms-2'></span>
            </span>
          )}
        </button>
      </div>
      {/* end::Action */}
    </form>
  )
}
