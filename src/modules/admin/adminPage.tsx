import { Route, Routes,  Navigate } from 'react-router-dom'
import { lazy } from 'react'

const UsersPage = lazy(()=> import("./user-management/UsersPage"))

const AdminPage = () => {

  return (
    <Routes>
        <Route
          path='users/*'
          element={
            <UsersPage />
          }
        />

      <Route index element={<Navigate to='users' />} />
    </Routes>
  )
}

export default AdminPage
