import {  useMemo } from 'react'
import { useTable, ColumnInstance, Row } from 'react-table'
import { CustomHeaderColumn } from '../table/columns/CustomHeaderColumn'
import { CustomRow } from '../table/columns/CustomRow'
import { usersColumns } from './columns/_columns'
import { User } from '../core/_models'
import { UsersListLoading } from '../components/loading/UsersListLoading'
import { KTCardBody } from 'helpers'
import useUsersList from 'data/hooks/use-users-list'
import {   useSearchParams } from 'react-router-dom'
import { BaseParamRequest } from 'interfaces/common.interface'
import PaginationCommon from 'components/common/pagination-common'

const UsersTable = () => {
  const [searchParams] = useSearchParams();
  const query: BaseParamRequest = useMemo(() => {
    return {
      search: searchParams.get('search'),
      page: searchParams.get('page') ? parseInt(searchParams.get('page')) : 1,
      per_page: searchParams.get('per_page') ? parseInt(searchParams.get('per_page')) : 10
    }
  }, [searchParams])
  const { data, isFetched } = useUsersList(query)

  const columns = useMemo(() => usersColumns, [])
  const { getTableProps, getTableBodyProps, headers, rows, prepareRow } = useTable({
    columns,
    data: data?.data && data?.data?.length > 0 ? data.data : [],
  })

  return (
    <KTCardBody className='py-4'>
      <div className='table-responsive'>
        <table
          id='kt_table_users'
          className='table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer'
          {...getTableProps()}
        >
          <thead>
            <tr className='text-start text-muted fw-bolder fs-7 text-uppercase gs-0'>
              {headers.map((column: ColumnInstance<User>) => (
                <CustomHeaderColumn key={column.id} column={column} />
              ))}
            </tr>
          </thead>
          {isFetched && <tbody className='text-gray-600 fw-bold' {...getTableBodyProps()}>
            {rows.length > 0 ? (
              rows.map((row: Row<User>, i) => {
                prepareRow(row)
                return <CustomRow row={row} key={`row-${i}-${row.id}`} />
              })
            ) : (
              <tr>
                <td colSpan={7}>
                  <div className='d-flex text-center w-100 align-content-center justify-content-center'>
                    No matching records found
                  </div>
                </td>
              </tr>
            )}
          </tbody>}

        </table>
      </div>
      {data && data.last_page > 1 ? <PaginationCommon pathname='admin/users' pageCount={data && data.last_page ? data.last_page : 0} forcePage={data && data.current_page ? data.current_page - 1 : 0} /> : ''}

      {!isFetched && <UsersListLoading line={3} />}
    </KTCardBody>
  )
}

export { UsersTable }
