import { ID, Response } from 'helpers'
import { UserModel } from 'interfaces/auth'
export type User = {
  id?: ID
  name?: string
  avatar?: string
  email?: string
  position?: string
  role?: string
  last_login?: string
  two_steps?: boolean
  created_at?: string
  online?: boolean
  initials?: {
    label: string
    state: string
  }
}

export type UsersQueryResponse = Response<Array<UserModel>>

export const initialUser: User = {
  avatar: '',
  name: '',
  email: '',
  id: null
}
