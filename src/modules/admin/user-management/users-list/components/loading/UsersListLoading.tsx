import styles from './style.module.scss'

interface UsersListLoadingProps {
  line?: number
}
const UsersListLoading = ({ line = 2 }: UsersListLoadingProps) => {


  return <>
    {Array(line).fill(null).map((_, i) => <div className={styles.placeholderContent} key={i}>
      <div className={styles.placeholderContentItem}></div>
      <div className={styles.placeholderContentItem}></div>
      <div className={styles.placeholderContentItem}></div>
      <div className={styles.placeholderContentItem}></div>
    </div>)}
  </>
}

export { UsersListLoading }
