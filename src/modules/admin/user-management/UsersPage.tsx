import {PageLink, PageTitle} from 'layout/core'
import {UsersListWrapper} from './users-list/UsersList'

const usersBreadcrumbs: Array<PageLink> = [
  {
    title: 'Home',
    path: '/',
    isSeparator: false,
    isActive: false,
    isRedirect: true,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
    isRedirect: true,
  },
  {
    title: 'User Management',
    path: '',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const UsersPage = () => {
  return (
     <>
     <PageTitle breadcrumbs={usersBreadcrumbs}>Users list</PageTitle>
      <UsersListWrapper />
     </>
  )
}

export default UsersPage
