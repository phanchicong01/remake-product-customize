import { lazy } from 'react'
import { Route, Routes,  Navigate } from 'react-router-dom'
import { ListViewProvider } from './campaign-list/core/ListViewProvider'
import { QueryRequestProvider } from './campaign-list/core/QueryRequestProvider'
import { QueryResponseProvider } from './campaign-list/core/QueryResponseProvider'

type Props = {}
const CampaignPageList = lazy(()=>  import('modules/campaigns/campaign-list'))
const CampaignPageDetail = lazy(()=>  import('modules/campaigns/campaign-detail'))
// path = /campaigns/list
// path = /campaigns/:id
// path = /campaigns/create
const Campaigns = (props: Props) => {
  return (
    <Routes>
      <Route>
        <Route>
          {/* /campaign/view/:campaignId */}
          <Route
            path='view/:campaignId'
            element={
              <CampaignPageDetail />
            }
          />
      </Route>
        <Route
          path='list'
          element={
            <QueryRequestProvider>
              <QueryResponseProvider>
                <ListViewProvider>
                  <CampaignPageList />
                </ListViewProvider>
              </QueryResponseProvider>
            </QueryRequestProvider>
          }
        />
      </Route>

      <Route index element={<Navigate to='list' />} />
  </Routes>
  )
}

export default Campaigns