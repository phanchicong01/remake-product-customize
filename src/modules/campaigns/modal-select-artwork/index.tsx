/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState } from 'react'
import { Modal } from 'antd'
import clsx from 'clsx'
import _isEmpty from "lodash-es/isEmpty"

import { getTotalPage, toAbsoluteUrl } from 'helpers'
import { ArtworkListRequest } from 'apis/get-artworks/interface';
import EmptyData from 'components/common/empty-data'
import { Artwork } from 'interfaces/artwork'
import Loading from 'components/common/loading-metronic-style'
import Pagination from 'components/common/pagination'
import useArtworks from 'data/hooks/use-artworks'
import { useMutation } from 'react-query'
import { UpdateCampaignRequest } from 'apis/update-campaign/interface'
import updateCampaign from 'apis/update-campaign'
import { useParams } from 'react-router-dom'
import { toast } from 'react-toastify'


type Props = {
    show: boolean,
    handleClose: (isReload: boolean) => void;
    name:"back_artwork_id" | "front_artwork_id" | string
}

const ModalSelectArtwork: React.FC<Props> = ({ show, handleClose , name }) => {
    const {campaignId} = useParams()
    const [stateQuery, setStateQuery] = useState<ArtworkListRequest>({
        search: '',
        page: 1,
        per_page: 20
    })
    
    const { data: dataArtwork, isLoading, isFetched, refetch } = useArtworks(stateQuery , show);
    const useMutationUpdate = useMutation((payload:UpdateCampaignRequest)=> updateCampaign(payload))

    const totalArtwork = dataArtwork?.total ?? 0
    const totalPage = getTotalPage(totalArtwork, stateQuery.per_page);

    const handleCancel = (flag: boolean) => {
        refetch();
        handleClose(flag)
    }
    const handleChangePage = (pageCurrent: any) => {
        setStateQuery((prev) => ({ ...prev, page: pageCurrent + 1 }))
    }
    const handleSelect = async (dataArtwork:Artwork) => {
        try {
            const nameThumb = (name === "front_artwork_id") ? "thumbnail_front" :"thumbnail_back"
            const payload:UpdateCampaignRequest = {
                id:parseInt(campaignId) ,
                data:{
                    [name]:dataArtwork.id,
                    [nameThumb]:dataArtwork.thumbnail,
                }
            }
            const result = await useMutationUpdate.mutateAsync(payload)
            if(result.code === 200){
                    handleClose(true);
            }else{
                toast.error(result.message);
            }
        } catch (error) {
            console.error(error);
            toast.error("Something went wrong. Please try again!")
        }
    }
    return (
        <>
            <Modal
                title={"Create Campaign"}
                visible={show}
                onCancel={() => handleCancel(false)}
                destroyOnClose
                footer={null}
                width={1000}
                confirmLoading={true}
                
            >
                <div className='row gx-3 gy-3 position-relative min-h-500px position-relative'>
                   { useMutationUpdate.isLoading && <>
                     <Loading />
                     <div className={"top-0 left-0 w-100 h-100 position-absolute"} style={{background:"#f1f1f1",zIndex:1 , opacity:0.5}}></div>
                     </>
                   }
                   {
                        isFetched && _isEmpty(dataArtwork?.data) &&
                        <EmptyData />
                    }
                    {isFetched && !isLoading && !_isEmpty(dataArtwork?.data) &&
                        dataArtwork?.data?.map((item: Artwork) => <div key={item.id} className={clsx('col-lg-4')}>
                            <div className='card card-bordered shadow-sm btn btn-active-light-primary' onClick={()=>handleSelect(item)}>
                                <div className='card-body p-2'>
                                    <img src={toAbsoluteUrl(item.thumbnail)} alt={item.name} className="w-100" />
                                </div>
                                <div className='card-footer'>
                                    <div className='d-flex justify-content-between'>
                                        {item.name}
                                    </div>
                                </div>
                            </div>

                        </div>)
                    }
                    {isLoading && <Loading />}
                    {
                        totalPage > 1 &&
                        <Pagination onPageChange={handleChangePage} pageCount={totalPage} forcePage={stateQuery.page - 1} />
                    }
                </div>

            </Modal>
        </>
    )
}

export { ModalSelectArtwork }
