import React, { Dispatch, SetStateAction } from 'react'
import useDetailArtwork from 'data/hooks/use-detail-artwork'
import _isNumber from 'lodash-es/isNumber';
import _isEmpty from 'lodash-es/isEmpty';
import { ID, KTSVG } from 'helpers';
import { UpdateCampaignRequest } from 'apis/update-campaign/interface';
import updateCampaign from 'apis/update-campaign';
import { useParams } from 'react-router-dom';
import { useMutation } from 'react-query';
import { toast } from 'react-toastify';
import { ARTWORKS_EDIT } from 'constants/path';
import Loading from 'components/common/loading-metronic-style';
import useConfirmDialog from 'hooks/use-confirm-dialog';

type Props = {
  id: ID,
  onSelectArtwork: Dispatch<SetStateAction<boolean>>
  name: "front_artwork_id" | "back_artwork_id"
  refetch:()=>void
}

const ItemArtworkSelected = ({ id, onSelectArtwork, name , refetch }: Props) => {
  const { campaignId } = useParams();
  const  {getDialogConfirmResult} = useConfirmDialog()

  const useMutationUpdate = useMutation((payload: UpdateCampaignRequest) => updateCampaign(payload))
  const { data, isLoading,isRefetching,  isFetched  } = useDetailArtwork(id);
  
  const handleRemove = async () => {
    const isConfirm = await getDialogConfirmResult({
      title: "Delete Artwork",
      description: "Are you sure delete this item?"
    })
    if(isConfirm){
      try {
        const nameThumb = (name === "front_artwork_id") ? "thumbnail_front" : "thumbnail_back"
        const payload: UpdateCampaignRequest = {
          id: parseInt(campaignId),
          data: {
            [name]: null,
            [nameThumb]: null,
          }
        }
        const result = await useMutationUpdate.mutateAsync(payload)
        if (result.code === 200) {
          refetch()// handleClose(true);
        } else {
          toast.error(result.message);
        }
      } catch (error) {
        console.error(error);
        toast.error("Something went wrong. Please try again!")
      }
    }
    
  }

  if (!_isNumber(id)) {
    return <div role={'button'} onClick={() => onSelectArtwork(true)} className={"d-flex  min-h-70px align-items-center nav-link text-active-primary"}>
      <KTSVG path='/media/icons/duotune/arrows/arr075.svg' className='svg-icon-2 text-primary' />
      <span>Select Artwork</span>
    </div>
  }
  if (isLoading || isRefetching || useMutationUpdate.isLoading) {
    return <div className='min-h-70px position-relative d-flex align-items-center justify-content-center'>
      <Loading />
    </div>
  }
  return (isFetched && !isLoading && !_isEmpty(data) &&
    <div className='row justify-content-around py-2 min-h-70px'>
      <div className='col-lg-2 px-4 border'>
        <img className='card-img' src={data.thumbnail} alt={data.name} />
      </div>
      <h5 className='col-lg-6 d-flex align-items-center text-truncate'>
        {data.name}
      </h5>
      <div className='w-auto d-flex justify-content-end align-items-center'>
        <a href={ARTWORKS_EDIT + "/" + id} className="btn btn-icon btn-active-light-primary">
          <KTSVG path='/media/icons/duotune/art/art005.svg' />

        </a>
        <div className='btn btn-icon btn-active-light-primary' onClick={handleRemove}>
          <KTSVG path='/media/icons/duotune/arrows/arr061.svg' />
        </div>

      </div>
    </div>
  )
}

export default ItemArtworkSelected