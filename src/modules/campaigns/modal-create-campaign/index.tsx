/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react'
import { Modal } from 'antd'
import { useFormik } from 'formik'
// import {ListsWidget4, ListsWidget5} from '../widgets'
import * as Yup from 'yup'
import clsx from 'clsx'
import _pickBy from "lodash-es/pickBy"
import _identity from "lodash-es/identity"
import { useMutation } from 'react-query'

import { toast } from 'react-toastify'

import createCampaign from 'apis/create-campaign'
import { CreateCampaignRequest } from 'apis/create-campaign/inteface'
import { useListView } from '../campaign-list/core/ListViewProvider'
import { useQueryResponse } from '../campaign-list/core/QueryResponseProvider'


type Props = {}
const schema = Yup.object().shape({
    name: Yup.string()
        .min(3, 'Minimum 3 symbols')
        .max(50, 'Maximum 50 symbols')
        .required('Name is required'),
})
const ModalCreateCampaign: React.FC<Props> = () => {
    const {modalData , setModalData}  =  useListView()
    const {refetch} = useQueryResponse()

    const useMutatonCreate = useMutation((payload: CreateCampaignRequest) => createCampaign(payload))
    const formik = useFormik({
        initialValues: {
            name:  "",
        },
        validationSchema: schema,
        onSubmit: async (values, { setSubmitting }) => {
            setSubmitting(true)
            try {
                const request = await useMutatonCreate.mutateAsync(
                    _pickBy({
                        name: values.name,
                    } , _identity())
                )
                if (request.code === 200) {
                    toast.success(request.message);
                } else {
                    toast.error(request.message)
                }
            } catch (ex) {
                console.error(ex)
            } finally {
                setSubmitting(true)
                handleCancel(true)
            }
        },

    })
 
    
    const handleCancel =  (flag:boolean) => {
        refetch();
        setModalData({
            visible:false,
            data:null
        })
        formik.resetForm()
    }
    
    return (
        <>
            <Modal
                title={"Create Campaign"}
                visible={modalData.visible}
                onCancel={()=>handleCancel(false)}
                destroyOnClose
                footer={null}
            >

                <form onSubmit={formik.handleSubmit} className='form pb-10' id="form-artwork-category">
                    <div className='col-lg-12 fv-row'>
                        <div className='row'>
                            <label className='col-lg-12 col-form-label required fw-bold fs-6'>Name Campaign</label>
                            <input
                                placeholder='Full name'
                                {...formik.getFieldProps('name')}
                                type='text'
                                name='name'
                                className={clsx(
                                    'form-control form-control-solid mb-3 ',
                                    { 'is-invalid': formik.touched.name && formik.errors.name },
                                    {
                                        'is-valid': formik.touched.name && !formik.errors.name,
                                    }
                                )}
                                autoComplete='off'
                                disabled={formik.isSubmitting}
                            />
                            {formik.touched.name && formik.errors.name && (
                                <div className='fv-plugins-message-container'>
                                    <div className='fv-help-block'>
                                        <span role='alert'>{formik.errors.name}</span>
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>
                    <div className='pt-15'>
                        <button
                            type='reset'
                            onClick={()=>handleCancel(false)}
                            className='btn btn-light me-3'
                            data-kt-users-modal-action='cancel'
                            disabled={formik.isSubmitting}
                        >
                            Discard
                        </button>

                        <button
                            type='submit'
                            className='btn btn-primary'
                            data-kt-users-modal-action='submit'
                            disabled={formik.isSubmitting || !formik.isValid || !formik.touched}
                        >
                            <span className='indicator-label'>Submit</span>
                            {(formik.isSubmitting) && (
                                <span className='indicator-progress'>
                                    Please wait...{' '}
                                    <span className='spinner-border spinner-border-sm align-middle ms-2'></span>
                                </span>
                            )}
                        </button>
                    </div>
                </form>
            </Modal>
        </>
    )
}

export { ModalCreateCampaign }
