/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react'
import TreeSelectComponent from 'components/common/tree-select-component'
import { Modal } from 'antd'
import { useFormik } from 'formik'
// import {ListsWidget4, ListsWidget5} from '../widgets'
import * as Yup from 'yup'
import clsx from 'clsx'
import _isEmpty from "lodash-es/isEmpty"
import _pickBy from "lodash-es/pickBy"
import _identity from "lodash-es/identity"
import createArtworkCategory from 'apis/create-artwork-category'
import { useMutation } from 'react-query'
import { CreateArtworkCategoryRequest } from 'apis/create-artwork-category/inteface'
import { ArtworkCategory } from 'interfaces/artwork'
import { toast } from 'react-toastify'
import updateArtworkCategories from 'apis/update-artwork-categories'
import { UpdateArtworkCategoriesRequest } from 'apis/update-artwork-categories/interface'

type Props = {
    show: boolean
    handleClose: (isReload?:boolean) => void,
    dataCategory: any[],
    data: ArtworkCategory
}

const schema = Yup.object().shape({
    name: Yup.string()
        .min(3, 'Minimum 3 symbols')
        .max(50, 'Maximum 50 symbols')
        .required('Name is required'),
    parent_id: Yup.string().nullable().required('Parent category is required'),
})
const CreateModal: React.FC<Props> = ({ show, handleClose, dataCategory, data }) => {

    const useMutatonCreateClipartCategory = useMutation((payload: CreateArtworkCategoryRequest) => createArtworkCategory(payload))
    const useMutatonUpdateClipartCategory = useMutation((payload: UpdateArtworkCategoriesRequest) =>
     updateArtworkCategories(payload))
    const formik = useFormik({
        initialValues: {
            name: data?.name || "",
            parent_id: data?.parent_id || "0"
        },
        validationSchema: schema,
        onSubmit: async (values, { setSubmitting }) => {
         
            setSubmitting(true)
            try {
                if (data?.id) {
                    const request = await useMutatonUpdateClipartCategory.mutateAsync({
                        id:data?.id,
                        data:_pickBy({
                            name: values.name,
                            parent_id: parseInt(values.parent_id) === 0 ? null : parseInt(values.parent_id)
                        } , _identity())
                    })
                    if (request.code === 200) {
                        toast.success(request.message);
                    } else {
                        toast.error(request.message)
                    }
                } else {
                    const request = await useMutatonCreateClipartCategory.mutateAsync(
                        _pickBy({
                            name: values.name,
                            parent_id: parseInt(values.parent_id) === 0 ? null : parseInt(values.parent_id)
                        } , _identity())
                    )
                    if (request.code === 200) {
                        toast.success(request.message);
                    } else {
                        toast.error(request.message)
                    }
                }
            } catch (ex) {
                console.error(ex)
            } finally {
                setSubmitting(true)
                handleCancel(true)
            }
        },

    })
    useEffect(()=>{
        if(!!data){
            formik.setFieldValue("name" , data.name);
            formik.setFieldValue("parent_id" , data.parent_id);
        }
        // @ts-ignore
    }, [data, formik.setFieldValue])
    
    const handleCancel =  (flag:boolean) => {
        handleClose(flag)
        formik.resetForm()
    }

    
    return (
        <>
            <Modal
                title={data?.id ? "Update category" :"Create category"}
                visible={show}
                // onOk={() => formik.handleSubmit()}
                // confirmLoading={useMutatonCreateClipartCategory.isLoading}
                onCancel={()=>handleCancel(false)}
                destroyOnClose
                footer={null}
            // footer={[

            // ]}
            >

                <form onSubmit={formik.handleSubmit} className='form pb-10' id="form-artwork-category">
                    <div className='col-lg-12 fv-row'>
                        <div className='row'>
                            <label className='col-lg-12 col-form-label required fw-bold fs-6'>Name category</label>

                            <input
                                placeholder='Full name'
                                {...formik.getFieldProps('name')}
                                type='text'
                                name='name'
                                className={clsx(
                                    'form-control form-control-solid mb-3 ',
                                    { 'is-invalid': formik.touched.name && formik.errors.name },
                                    {
                                        'is-valid': formik.touched.name && !formik.errors.name,
                                    }
                                )}
                                autoComplete='off'
                                disabled={formik.isSubmitting}
                            />
                            {formik.touched.name && formik.errors.name && (
                                <div className='fv-plugins-message-container'>
                                    <div className='fv-help-block'>
                                        <span role='alert'>{formik.errors.name}</span>
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>
                    <div className='col-lg-12 '>
                        <div className='row'>
                            <label className='col-lg-12 col-form-label required fw-bold fs-6'>Parent category</label>

                            {!_isEmpty(dataCategory) &&
                                <TreeSelectComponent
                                    {...formik.getFieldProps('parent_id')}
                                    size="large"
                                    treeIcon={false}
                                    id='parent_id'
                                    value={formik.values.parent_id || 0}
                                    onChange={(value) => {
                                        formik.setFieldValue("parent_id", value);
                                    }}
                                    className={clsx(
                                        'form-select-tree form-select-tree-solid ',
                                        { 'form-control is-invalid': formik.touched.parent_id && formik.errors.parent_id },
                                        {
                                            'form-control is-valid': formik.touched.parent_id && !formik.errors.parent_id,
                                        }
                                    )}
                                    showSearch
                                    treeData={
                                        [{ name: "Root", id: 0, key: 0, title: "All", children: [], checkable: false }].concat(dataCategory)
                                    }
                                    fieldNames={{
                                        label: 'name',
                                        children: 'children',
                                        value: 'id'
                                    }}
                                    dropdownClassName="form-select-tree-dropdown"
                                    placeholder="Please parent"
                                    disabled={formik.isSubmitting} />
                            }
                            {formik.touched.parent_id && formik.errors.parent_id && (
                                <div className='fv-plugins-message-container'>
                                    <div className='fv-help-block'>
                                        <span role='alert'>{formik.errors.parent_id}</span>
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>
                    <div className='pt-15'>
                        <button
                            type='reset'
                            onClick={()=>handleCancel(false)}
                            className='btn btn-light me-3'
                            data-kt-users-modal-action='cancel'
                            disabled={formik.isSubmitting}
                        >
                            Discard
                        </button>

                        <button
                            type='submit'
                            className='btn btn-primary'
                            data-kt-users-modal-action='submit'
                            disabled={formik.isSubmitting || !formik.isValid || !formik.touched}
                        >
                            <span className='indicator-label'>Submit</span>
                            {(formik.isSubmitting) && (
                                <span className='indicator-progress'>
                                    Please wait...{' '}
                                    <span className='spinner-border spinner-border-sm align-middle ms-2'></span>
                                </span>
                            )}
                        </button>
                    </div>
                </form>
            </Modal>
        </>
    )
}

export { CreateModal }
