

import { CampaignSearchComponent } from './search'
import { CampaignToolbar } from './toolbar'


const CampaignHeader = () => {
  
  return (
    <div className='card-header border-0 pt-6'>
      <div className='d-flex flex-column'>
        <CampaignSearchComponent />
      </div>
      {/* begin::Card toolbar */}
      <div className='card-toolbar'>
        {/* begin::Group actions */}
        <div className='d-flex '>
          <CampaignToolbar />
        </div>
        {/* <ClipartToolbar /> */}
        {/* end::Group actions */}
      </div>
      {/* end::Card toolbar */}
    </div>
  )
}

export { CampaignHeader }
