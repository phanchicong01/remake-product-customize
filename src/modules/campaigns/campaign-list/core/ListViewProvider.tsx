import {FC, useState, createContext, useContext} from 'react'
import {
  ID,
} from 'helpers'
import { initialListView, ListViewContextProps } from 'helpers/crud-helper/models-v2'

const ListViewContext = createContext<ListViewContextProps>(initialListView)

const ListViewProvider: FC = ({children}) => {
  const [modalData, setModalData] = useState<{
    visible:boolean,
    data:ID
  }>({
    visible:false,
    data:null
  })
 
  return (
    <ListViewContext.Provider
      value={{
        modalData,
        setModalData,
      }}
    >
      {children}
    </ListViewContext.Provider>
  )
}

const useListView = () => useContext(ListViewContext)

export {ListViewProvider, useListView}
