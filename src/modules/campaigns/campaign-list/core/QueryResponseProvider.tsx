/* eslint-disable react-hooks/exhaustive-deps */
import { FC, useContext} from 'react'

import { useQueryRequest } from './QueryRequestProvider'
import { getTotalPage } from 'helpers'
import useCampaigns from 'data/hooks/use-campaigns'
import { createResponseContext, initialQueryResponse } from 'helpers/crud-helper/models-v2'
import { GetCampaignResponse } from 'apis/get-campaigns/interface'


const QueryResponseContext = createResponseContext<GetCampaignResponse>(initialQueryResponse)
const QueryResponseProvider: FC = ({ children }) => {
  const { query } = useQueryRequest()
  const { refetch,isRefetching,  data:response, isFetched , isLoading } = useCampaigns(query);

  const totalProductBase = response?.data?.total || 0
  const totalPage = getTotalPage(totalProductBase, query.per_page);
  return (
    <QueryResponseContext.Provider value={{ 
      refetch,
      isRefetching,
      isLoading,
      isFetched,
      totalPage,
      response
      }}>
      {children}
    </QueryResponseContext.Provider>
  )
}

const useQueryResponse = () => useContext(QueryResponseContext)

const useQueryResponseData = () => {
  const { response } = useQueryResponse();
  if (!response) {
    return []
  }
  if(Array.isArray(response?.data)){
    return response?.data
  }
  
  return response?.data?.data || []
}

export {
  QueryResponseProvider,
  useQueryResponse,
  useQueryResponseData
}
