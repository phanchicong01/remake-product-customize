import { PersonalizationOption } from "./enum";

export const MappingPersonalizationImage = {
    [PersonalizationOption.Clipart]: 'clipArtSetting',
    [PersonalizationOption.GroupClipart]: 'groupClipArtSetting',
    [PersonalizationOption.UploadPhoto]: 'uploadPhotoSetting',
}
