export enum TypeDisplayOptionTempLate {
    Radio = "Radio" ,
    Dropdown = "Dropdown"
  }
  export enum PersonalizationOption {
    None= "none" ,
    Clipart = "clipart" ,
    GroupClipart = "group-clipart" ,
    UploadPhoto = "upload-photo" ,
    Maps = "maps",
    EnablePersonalization = "enable"
  }
  export enum TypeLayer {
    Image = "image" ,
    Text = "text"
  }
  export enum ModalKey {
    ModalImage = "modal-image",
    ModalReplaceImage = "modal-replace-image",
    ModalSettingArtwork = "modal-setting-artwork",
    ModalSettingTemplate = "modal-setting-template",
    SaveData = "save-data",
    SavedData = "saved-data"
  }

  export enum PositionLayer {
    X = "x",
    Y = "y",
  
  }