export const QUERY_KEYS = {
  MENU: 'menu',
  MEMBER: "member",
  ARTWORK_CATEGORIES: "artwork-categories",
  CLIPART_CATEGORIES: "clipart-categories",
  CLIPART_CATEGORIES_SELECT: "clipart-categories-select",
  DETAIL_CLIPART_CATEGORIES: "detail-clipart-categories",
  ARTWORK: "artwork",
  ARTWORK_LIST: "artwork-list",
  ARTWORK_DETAIL: "artwork-detail",
  ADDITION_OPTION: "addition-option",
  ADDITION_OPTION_ITEMS: "addition-option-item",
  DETAIL_ADDITION_OPTION: "detail-addition-option",
  FONTS: "fonts",
  USER: "user",
  UPLOAD_FONT: "upload_fonts",
  PRODUCT_BASE: "product_base",
  PRODUCT_BASE_DETAIL: "product_base_detail",
  MOCKUP_LIST:"mockup-list",
  MOCKUP_LIST_DETAIL:"mockup-detail",
  PRINT_AREAS:"print-areas",
  PRINT_AREA_DETAIL:"print-area-detail",
  CAMPAIGNS:"campaigns",
  CAMPAIGN_DETAIL:"campaign-detail",
}
