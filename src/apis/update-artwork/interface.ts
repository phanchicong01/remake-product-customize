import { ID } from "helpers";

export interface UpdateArtworkRequest {
    id:ID,
    data:{
        artwork_category_id?:ID,
        name?:string;
        content?:string,
        thumbnail?:string
    }
}