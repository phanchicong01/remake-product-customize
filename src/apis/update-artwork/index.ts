import { Artwork } from 'interfaces/artwork'
import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'
import {UpdateArtworkRequest  } from './interface'


const updateArtwork = async ( payload:UpdateArtworkRequest): Promise<FetcherResponse<Artwork>> => {
    const { data: responseData } = await axiosInstance.put<
    FetcherResponse<Artwork>
  >('artworks/'+ payload.id, payload.data )

  return responseData
}

export default updateArtwork