import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'
import { UpdateAdditionalOptionItemRequest, UpdateAdditionalOptionItemResponse } from './inteface'


const updateAdditionalOptionItem = async ( payload:UpdateAdditionalOptionItemRequest): Promise<FetcherResponse<UpdateAdditionalOptionItemResponse>> => {
  const {id , ...otherPayload } = payload
    const { data: responseData } = await axiosInstance.put<
    FetcherResponse<UpdateAdditionalOptionItemResponse>
  >('additional-option-items/'+ id,otherPayload )

  return responseData
}

export default updateAdditionalOptionItem