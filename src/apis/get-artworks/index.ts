import {axiosInstance} from 'utils/fetcher'
import _pick from "lodash-es/pick"

import { FetcherResponse } from 'interfaces/fetcher'
import { ArtworkListResponse , ArtworkListRequest } from './interface'

const getArtworks = async ( params:ArtworkListRequest): Promise<ArtworkListResponse> => {
  const { data: responseData } = await axiosInstance.get<
    FetcherResponse<ArtworkListResponse>
  >('get-artworks', {
    params:_pick(params, ["search","per_page","page"])
  })

  return responseData.data
}

export default getArtworks
