import { Artwork } from "interfaces/artwork"

export interface ArtworkListResponse  {
    "current_page": number,
    "last_page": number,
    "per_page": number,
    "total": number,
    "data": Artwork[]
};
export interface ArtworkListRequest {
    search?:string,
    per_page?:number,
    page?:number,
    category?:number
}