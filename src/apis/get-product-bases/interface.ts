import { ProductBase } from "interfaces/product-base"

export interface ProductBaseResponse {
    "current_page": number,
    "last_page": number,
    "per_page": number,
    "total": number,
    "data": ProductBase[]
}

export interface ProductBaseRequest {
    per_page:number
    page:number,
    sort?:string,
    order?:'desc' | 'asc',
    search?:string,
}