
import { axiosInstance } from 'utils/fetcher'

import { FetcherResponse } from 'interfaces/fetcher'
import { ProductBaseRequest , ProductBaseResponse } from './interface';

const getProductBases = async (params: ProductBaseRequest): Promise<FetcherResponse<ProductBaseResponse>> => {
  const { data: responseData } = await axiosInstance.get<
    FetcherResponse<ProductBaseResponse>
  >('product-bases', {
    params
  })

  return responseData
}

export default getProductBases
