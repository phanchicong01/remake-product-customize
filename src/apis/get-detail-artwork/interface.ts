import { ID } from 'helpers';

export interface ArtworkResponse {
    id:ID,
    name:string,
    content:string,
    artwork_category_id:ID,
    created_at:string,
    updated_at:string,
    thumbnail:string
}