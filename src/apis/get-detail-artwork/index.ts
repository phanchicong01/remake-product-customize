import { ID } from 'helpers';
import {axiosInstance} from 'utils/fetcher'

import { FetcherResponse } from 'interfaces/fetcher'
import { ArtworkResponse } from './interface';

const getDetailArtwork = async (ID:ID): Promise<ArtworkResponse> => {
  const { data: responseData } = await axiosInstance.get<
    FetcherResponse<ArtworkResponse>
  >('artwork/'+ID)

  return responseData.data
}

export default getDetailArtwork
