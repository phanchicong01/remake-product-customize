import {axiosInstance} from 'utils/fetcher'

import { FetcherResponse } from 'interfaces/fetcher'

const logout = async (): Promise<boolean> => {
  const { data: responseData } = await axiosInstance.post<
    FetcherResponse<null>
  >('auth/logout')

  return responseData.success
}

export default logout
