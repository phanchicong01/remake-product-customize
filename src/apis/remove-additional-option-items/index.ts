import { ID } from 'helpers';
import {  AdditionalOptionItem } from 'interfaces/additional-options';
import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'


const removeAdditionalOptionItems = async ( payload:ID): Promise<FetcherResponse<AdditionalOptionItem>> => {
    const { data: responseData } = await axiosInstance.delete<
    FetcherResponse<AdditionalOptionItem>
  >('additional-option-items/'+ payload )

  return responseData
}

export default removeAdditionalOptionItems