import { FetcherResponse } from 'interfaces/fetcher'
import { ProductBase } from 'interfaces/product-base'
import { axiosInstance } from 'utils/fetcher'
import {UpdateProductBaseRequest  } from './interface'


const updateProductBase = async ( payload:UpdateProductBaseRequest): Promise<FetcherResponse<ProductBase>> => {
  const { data: responseData } = await axiosInstance.put<
    FetcherResponse<ProductBase>
  >('product-bases/'+ payload.id, payload.data )

  return responseData
}

export default updateProductBase