import { ID } from "helpers";

export interface UpdateProductBaseRequest {
    id:ID,
    data:{
        name:string;
    }
}