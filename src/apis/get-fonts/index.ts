import { axiosInstance } from 'utils/fetcher'

import { FetcherResponse } from 'interfaces/fetcher'
import { BaseParamRequest } from 'interfaces/common.interface';

const getFonts = async (params: BaseParamRequest): Promise<any> => {
  const { data: responseData } = await axiosInstance.get<
    FetcherResponse<any>
  >('fonts', {
    params
  })

  return responseData.data
}

export default getFonts
