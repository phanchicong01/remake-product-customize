import { ID } from 'helpers';

import { axiosInstance } from 'utils/fetcher'

import { FetcherResponse } from 'interfaces/fetcher'
import {  MockupListResponse  } from './interface';

const getMockupDetail = async (Id:ID): Promise<MockupListResponse> => {
  const { data: responseData } = await axiosInstance.get<
    FetcherResponse<MockupListResponse>
  >('mockups/'+Id)

  return responseData.data
}

export default getMockupDetail
