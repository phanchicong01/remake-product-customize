import { ID } from '../../helpers/crud-helper/models';

export interface CreateArtworkRequest {
    "artwork_category_id":ID,
    "content":string,
    "name":string,
    "thumbnail"?:string
}
export interface CreateArtworkResponse {}