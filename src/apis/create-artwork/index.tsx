import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'
import { CreateArtworkRequest } from './inteface'
import { Artwork } from 'interfaces/artwork'


const createArtwork = async ( payload:CreateArtworkRequest): Promise<FetcherResponse<Artwork>> => {

    const { data: responseData } = await axiosInstance.post<
    FetcherResponse<Artwork>
  >('artworks', payload)

  return responseData
}

export default createArtwork