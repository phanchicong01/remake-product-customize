import { ID } from 'helpers';
import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'

const removeCampaign = async ( payload:ID): Promise<FetcherResponse<null>> => {
    const { data: responseData } = await axiosInstance.delete<
    FetcherResponse<null>
  >('campaigns/'+ payload )

  return responseData
}

export default removeCampaign