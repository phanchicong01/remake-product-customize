
import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'
import { CreateClipartItemRequest, ClipartItemResponse } from './inteface'


const createClipartItems = async ( body:CreateClipartItemRequest): Promise<FetcherResponse<ClipartItemResponse>> => {

    const { data: responseData } = await axiosInstance.post<
    FetcherResponse<ClipartItemResponse>
  >('cliparts', body)

  return responseData
}

export default createClipartItems