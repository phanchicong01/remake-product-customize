import { ID } from '../../helpers/crud-helper/models';
import {  ClipartItem } from 'interfaces/clipart-category'

export interface CreateClipartItemRequest {
    "name":string,
    "category_id":ID ,
    "url":string,
    "color"?:string,
    'thumbnail_url'?:any
}
export interface ClipartItemResponse extends ClipartItem{}