import { ID } from 'helpers'

export interface UpdatePrintAreaRequest {
    printAreaId:ID,
    data:Partial<UpdatePrintAreaData>
}
export interface UpdatePrintAreaData {
    "name"?: string,
    "width"?: number,
    "height"?: number,
    "position"?: number
}
 