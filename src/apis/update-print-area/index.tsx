
import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'
import { UpdatePrintAreaRequest } from './inteface'
import { PrintArea } from 'interfaces/product-base'


const updatedPrintArea = async (payload:UpdatePrintAreaRequest): Promise<FetcherResponse<PrintArea>> => {
    const { data: responseData } = await axiosInstance.put<
    FetcherResponse<PrintArea>
  >('print-areas/'+payload.printAreaId, payload.data )

  return responseData
}

export default updatedPrintArea