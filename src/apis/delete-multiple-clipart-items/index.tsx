import { ID } from 'helpers'
import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'


const deleteClipartItem = async ( clipartItemId:ID, selected:ID[]): Promise<null> => {

    const { data: responseData } = await axiosInstance.post<
    FetcherResponse<null>
  >('cliparts/'+clipartItemId, {ids:selected})

  return responseData.data
}

export default deleteClipartItem