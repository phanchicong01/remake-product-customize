import { ID } from 'helpers';
import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'


const removeClipartCategory = async ( payload:ID): Promise<FetcherResponse<null>> => {
    const { data: responseData } = await axiosInstance.delete<
    FetcherResponse<null>
  >('clipart-categories/'+ payload )

  return responseData
}

export default removeClipartCategory