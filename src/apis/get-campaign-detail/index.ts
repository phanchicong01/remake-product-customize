import { ID } from 'helpers';
import {axiosInstance} from 'utils/fetcher'

import { FetcherResponse } from 'interfaces/fetcher'
import { Campaign } from 'interfaces/campaign';

const getCampaignDetail = async (ID:ID): Promise<Campaign> => {
  const { data: responseData } = await axiosInstance.get<
    FetcherResponse<Campaign>
  >('campaigns/'+ID)

  return responseData.data
}

export default getCampaignDetail
