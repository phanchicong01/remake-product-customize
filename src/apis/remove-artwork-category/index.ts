import { ID } from 'helpers';
import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'


const removeArtworkCategory = async ( payload:ID): Promise<FetcherResponse<null>> => {
    const { data: responseData } = await axiosInstance.delete<
    FetcherResponse<null>
  >('artwork-categories/'+ payload )

  return responseData
}

export default removeArtworkCategory