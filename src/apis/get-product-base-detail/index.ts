import { ID } from 'helpers';
import { axiosInstance } from 'utils/fetcher'

import { FetcherResponse } from 'interfaces/fetcher'
import { ProductBase } from 'interfaces/product-base';

const getProductBaseDetail = async (ID:ID): Promise<ProductBase> => {
  const { data: responseData } = await axiosInstance.get<
    FetcherResponse<ProductBase>
  >('product-bases/'+ID)

  return responseData.data
}

export default getProductBaseDetail
