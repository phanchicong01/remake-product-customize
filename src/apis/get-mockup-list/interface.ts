import { ID } from "helpers"
import { Mockup } from "interfaces/product-base"

export interface MockupListResponse extends Mockup{}
export interface MockupListRequest {
    productBaseId:ID,
}
