
import { axiosInstance } from 'utils/fetcher'

import { FetcherResponse } from 'interfaces/fetcher'
import { MockupListRequest, MockupListResponse  } from './interface';

const getMockupList = async (params: MockupListRequest): Promise<MockupListResponse[]> => {
  const { data: responseData } = await axiosInstance.get<
    FetcherResponse<MockupListResponse[]>
  >('mockups', {
    params
  })

  return responseData.data
}

export default getMockupList
