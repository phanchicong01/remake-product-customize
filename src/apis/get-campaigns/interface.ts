import { Campaign } from "interfaces/campaign"

export interface GetCampaignResponse  {
    "current_page": number,
    "last_page": number,
    "per_page": number,
    "total": number,
    "data": Campaign[]
};
export interface GetCampaignRequest {
    search?:string,
    per_page?:number,
    page?:number,
}