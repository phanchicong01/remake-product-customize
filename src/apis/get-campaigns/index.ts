import {axiosInstance} from 'utils/fetcher'
import _pick from "lodash-es/pick"

import { FetcherResponse } from 'interfaces/fetcher'
import { GetCampaignResponse , GetCampaignRequest } from './interface'

const getCampaigns = async ( params:GetCampaignRequest): Promise<FetcherResponse<GetCampaignResponse>> => {
  const { data: responseData } = await axiosInstance.get<
    FetcherResponse<GetCampaignResponse>
  >('campaigns', {
    params:_pick(params, ["search","per_page","page"])
  })

  return responseData
}

export default getCampaigns
