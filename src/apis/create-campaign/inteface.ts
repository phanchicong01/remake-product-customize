import { Campaign } from "interfaces/campaign";


export interface CreateCampaignRequest {
    "name":string,
    "front_artwork_id"?: number,
    "back_artwork_id"?: number
}
export interface CreateCampaignResponse extends Campaign{}