import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'
import { CreateCampaignRequest, CreateCampaignResponse } from './inteface'


const createCampaign = async ( payload:CreateCampaignRequest): Promise<FetcherResponse<CreateCampaignResponse>> => {

    const { data: responseData } = await axiosInstance.post<
    FetcherResponse<CreateCampaignResponse>
  >('campaigns', payload)

  return responseData
}

export default createCampaign