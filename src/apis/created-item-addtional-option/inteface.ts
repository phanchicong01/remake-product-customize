import { ID } from '../../helpers/crud-helper/models';
import { ClipArtCategory } from 'interfaces/clipart-category'


export interface CreateAdditionalOptionItemRequest {
    additional_option_id:ID
    "name"?:string,
    "thumbnail"?:string,
    "color"?:string
}
export interface CreateAdditionalOptionItemResponse extends ClipArtCategory{}