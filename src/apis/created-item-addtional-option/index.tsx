import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'
import { CreateAdditionalOptionItemRequest, CreateAdditionalOptionItemResponse } from './inteface'


const createdAdditionalOptionItem = async ( payload:CreateAdditionalOptionItemRequest): Promise<FetcherResponse<CreateAdditionalOptionItemResponse>> => {
    const { data: responseData } = await axiosInstance.post<
    FetcherResponse<CreateAdditionalOptionItemResponse>
  >('additional-option-items',payload )

  return responseData
}

export default createdAdditionalOptionItem