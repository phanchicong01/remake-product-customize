import {axiosInstance} from 'utils/fetcher'

import { FetcherResponse } from 'interfaces/fetcher'
import { AuthMember } from './interface'

const getAuthMemberByToken = async (token:string): Promise<AuthMember> => {
  const { data: responseData } = await axiosInstance.get<
    FetcherResponse<AuthMember>
  >('auth/user-profile', {
    headers:{
      'Authorization': `Bearer ${token}`
    }
  })

  return responseData.data
}

export default getAuthMemberByToken
