import { ID } from 'helpers';
import { AdditionalOptionItem } from 'interfaces/additional-options';

export interface AdditionalOptionsResponse extends AdditionalOptionItem  {}