import { ID } from 'helpers';
import {axiosInstance} from 'utils/fetcher'

import { FetcherResponse } from 'interfaces/fetcher'
import { AdditionalOptionsResponse } from './interface'

const getAdditionalOptionItems = async (additionalId:ID): Promise<AdditionalOptionsResponse[]> => {
  const { data: responseData } = await axiosInstance.get<
    FetcherResponse<AdditionalOptionsResponse[]>
  >('additional-option-items/'+additionalId)

  return responseData.data
}

export default getAdditionalOptionItems
