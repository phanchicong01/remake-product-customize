import { ID } from 'helpers';
import {axiosInstance} from 'utils/fetcher'

import { FetcherResponse } from 'interfaces/fetcher'
import {  DetailClipartCategoryResponse } from './interface'

const getDetailClipartCategory = async (params:ID): Promise<DetailClipartCategoryResponse> => {
  const { data: responseData } = await axiosInstance.get<
    FetcherResponse<DetailClipartCategoryResponse>
  >('clipart-categories/'+params)

  return responseData.data
}

export default getDetailClipartCategory
