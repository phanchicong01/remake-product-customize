
import { FetcherResponse } from 'interfaces/fetcher'
import { FontInterface } from 'interfaces/font'
import { axiosInstance } from 'utils/fetcher'



const CreateFont = async (body: FontInterface): Promise<FetcherResponse<FontInterface>> => {

    const { data: responseData } = await axiosInstance.post<
        FetcherResponse<FontInterface>
    >('/font', body)

    return responseData
}

export default CreateFont