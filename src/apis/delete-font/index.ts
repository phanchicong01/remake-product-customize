import { ID } from 'helpers';
import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'


const deleteFont = async (id: ID): Promise<FetcherResponse<any>> => {

    const { data: responseData } = await axiosInstance.delete<
        FetcherResponse<any>
    >(`/font/${id}`)

    return responseData
}

export default deleteFont