import { ID } from 'helpers';
import {axiosInstance} from 'utils/fetcher'

import { FetcherResponse } from 'interfaces/fetcher'
import {  CategoriesResponse } from './interface'

const getDetailArtworkCategories = async (ID:ID): Promise<CategoriesResponse[]> => {
  const { data: responseData } = await axiosInstance.get<
    FetcherResponse<CategoriesResponse[]>
  >('artwork-categories/'+ID)

  return responseData.data
}

export default getDetailArtworkCategories
