import { ID } from 'helpers';
import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'


const removeProductBase = async ( id:ID): Promise<FetcherResponse<null>> => {
    const { data: responseData } = await axiosInstance.delete<
    FetcherResponse<null>
  >('product-bases/'+ id )

  return responseData
}

export default removeProductBase