import { AdditionalOption } from 'interfaces/additional-options';

export interface CreateAdditionalOptionRequest {
    
    "name":string,
    "type":any ,
}
export interface AdditionalOptionResponse extends AdditionalOption{}