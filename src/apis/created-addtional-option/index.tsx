import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'
import { CreateAdditionalOptionRequest, AdditionalOptionResponse } from './inteface'


const createdAdditionalOption = async ( payload:CreateAdditionalOptionRequest): Promise<FetcherResponse<AdditionalOptionResponse>> => {

    const { data: responseData } = await axiosInstance.post<
    FetcherResponse<AdditionalOptionResponse>
  >('additional-options', payload)

  return responseData
}

export default createdAdditionalOption