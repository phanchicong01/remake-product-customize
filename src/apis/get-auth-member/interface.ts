

export interface AuthMember {
  id: number
  created_at: string
  updated_at: string
  name: string
  email: string
  email_verified_at:null
  avatar?: string
}

