import {axiosInstance} from 'utils/fetcher'

import { FetcherResponse } from 'interfaces/fetcher'
import { AuthMember } from './interface'

const getAuthMember = async (): Promise<AuthMember> => {
  const { data: responseData } = await axiosInstance.get<
    FetcherResponse<AuthMember>
  >('auth/user-profile')

  return responseData.data
}

export default getAuthMember
