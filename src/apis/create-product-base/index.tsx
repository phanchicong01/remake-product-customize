import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'
import { CreateProductBaseRequest , CreateProductBaseResponse } from './inteface'


const createProductBase = async ( payload:CreateProductBaseRequest): Promise<FetcherResponse<CreateProductBaseResponse>> => {

    const { data: responseData } = await axiosInstance.post<
    FetcherResponse<CreateProductBaseResponse>
  >('product-bases', payload)

  return responseData
}

export default createProductBase