
import { ProductBase } from 'interfaces/product-base';

export interface CreateProductBaseRequest {
    "name":string,
}
export interface CreateProductBaseResponse extends ProductBase{}