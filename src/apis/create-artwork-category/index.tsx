import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'
import { CreateArtworkCategoryResponse , CreateArtworkCategoryRequest } from './inteface'


const createArtworkCategory = async ( payload:CreateArtworkCategoryRequest): Promise<FetcherResponse<CreateArtworkCategoryResponse>> => {

    const { data: responseData } = await axiosInstance.post<
    FetcherResponse<CreateArtworkCategoryResponse>
  >('artwork-categories', payload)

  return responseData
}

export default createArtworkCategory