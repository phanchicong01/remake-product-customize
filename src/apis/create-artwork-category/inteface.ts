import { ID } from './../../helpers/crud-helper/models';
import { ArtworkCategory } from 'interfaces/artwork';

export interface CreateArtworkCategoryRequest {
    "name":string,
    "parent_id":ID ,
}
export interface CreateArtworkCategoryResponse extends ArtworkCategory{}