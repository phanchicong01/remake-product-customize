import { ClipArtCategory } from "interfaces/clipart-category";


export interface ClipArtCategoryRequest  {
    search?:string
}

export interface ClipArtCategoryResponse extends ClipArtCategory {}