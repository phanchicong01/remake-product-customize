import {axiosInstance} from 'utils/fetcher'

import { FetcherResponse } from 'interfaces/fetcher'
import { ClipArtCategoryRequest, ClipArtCategoryResponse } from './interface'

const getClipartCategories = async (params:ClipArtCategoryRequest): Promise<ClipArtCategoryResponse[]> => {
  const { data: responseData } = await axiosInstance.get<
    FetcherResponse<ClipArtCategoryResponse[]>
  >('clipart-categories', {
    params
  })

  return responseData.data || []
}

export default getClipartCategories
