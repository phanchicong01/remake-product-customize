
import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'
import { UpdatedAdditionalOptionRequest, AdditionalOptionResponse } from './inteface'


const updatedAdditionalOption = async ( payload:UpdatedAdditionalOptionRequest): Promise<FetcherResponse<AdditionalOptionResponse>> => {

    const { data: responseData } = await axiosInstance.post<
    FetcherResponse<AdditionalOptionResponse>
  >('additional-options/'+payload.additionId, payload.dataUpdated)

  return responseData
}

export default updatedAdditionalOption