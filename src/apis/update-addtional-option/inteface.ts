import { ID } from '../../helpers/crud-helper/models';
import { ClipArtCategory } from 'interfaces/clipart-category'
import { TypeDisplayClipart } from 'constants/enum';

export interface UpdatedAdditionalOptionRequest {
    additionId:ID,
    dataUpdated:{
        "name"?:string,
        "type"?:TypeDisplayClipart ,
        is_show_clipart_name_on_hover?:number,
        placeholder?:string
    }
 
}
export interface AdditionalOptionResponse extends ClipArtCategory{}