import { ID } from "helpers";

export interface UpdateCampaignRequest {
    id:ID,
    data:{
        name?:string;
        "front_artwork_id"?: number,
        "back_artwork_id"?: number,
        "thumbnail_front"?:string,
        "thumbnail_back"?:string
    }
}