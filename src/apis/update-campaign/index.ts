import { Campaign } from 'interfaces/campaign'
import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'
import {UpdateCampaignRequest  } from './interface'


const updateCampaign = async ( payload:UpdateCampaignRequest): Promise<FetcherResponse<Campaign>> => {
    const { data: responseData } = await axiosInstance.put<
    FetcherResponse<Campaign>
  >('campaigns/'+ payload.id, payload.data )

  return responseData
}

export default updateCampaign