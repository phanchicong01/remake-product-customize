import { ID } from 'helpers';
import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'


const removeMultipleArtworkCategory = async ( payload:string[]): Promise<FetcherResponse<null>> => {
    const { data: responseData } = await axiosInstance.post<
    FetcherResponse<null>
  >('artwork-categories', {ids:payload} )

  return responseData
}

export default removeMultipleArtworkCategory