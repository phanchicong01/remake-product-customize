import { ID } from 'helpers';
import { ArtworkCategory } from "interfaces/artwork";

export interface ArtworkCategoriesRequest  {
    per_page?:number,
    page?:number,
    search?:string,
    id?:ID
}
export interface CategoriesResponse extends ArtworkCategory {}