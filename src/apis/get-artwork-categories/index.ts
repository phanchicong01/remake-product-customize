import {axiosInstance} from 'utils/fetcher'

import { FetcherResponse } from 'interfaces/fetcher'
import { ArtworkCategoriesRequest, CategoriesResponse } from './interface'

const getArtworkCategories = async (params:ArtworkCategoriesRequest): Promise<CategoriesResponse[]> => {
  const { data: responseData } = await axiosInstance.get<
    FetcherResponse<CategoriesResponse[]>
  >('artwork-categories', {
    params
  })

  return responseData.data
}

export default getArtworkCategories
