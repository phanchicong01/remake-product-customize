import { ID } from 'helpers';
import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'


const removeClipartItem = async ( id:ID): Promise<FetcherResponse<null>> => {
    const { data: responseData } = await axiosInstance.delete<
    FetcherResponse<null>
  >('cliparts/'+ id )

  return responseData
}

export default removeClipartItem