export interface UploadFileRequest {
    file:File,
    folder:string
}
export interface UploadFileResponse {
    path:string, thumbnail:string
}