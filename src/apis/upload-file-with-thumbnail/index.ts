import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'
import {  UploadFileResponse } from './interfaces'


const uploadFileWithThumbnail = async ( formData:any) => {
   
    const { data: responseData } = await axiosInstance.post<
    FetcherResponse<UploadFileResponse>
  >('resource/upload-image-with-thumbnail', formData)
  return responseData
}

export default uploadFileWithThumbnail