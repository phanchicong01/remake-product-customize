import {axiosInstance} from 'utils/fetcher'

import { FetcherResponse } from 'interfaces/fetcher'
import { AdditionalOptionsRequest, AdditionalOptionsResponse } from './interface'

const getAdditionalOptions = async (params:AdditionalOptionsRequest): Promise<AdditionalOptionsResponse[]> => {
  const { data: responseData } = await axiosInstance.get<
    FetcherResponse<AdditionalOptionsResponse[]>
  >('additional-options' , {
    params
  })

  return responseData.data
}

export default getAdditionalOptions
