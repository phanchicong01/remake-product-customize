import { ID } from 'helpers';

export interface AdditionalOptionsResponse  {
    "id": ID,
    "name": string,
    "type": string,
    "is_show_clipart_name_on_hover": number,
    "placeholder": string,
    "created_at": string,
    "updated_at": string
}
export interface AdditionalOptionsRequest  {
   search?:string
}