import {axiosInstance} from 'utils/fetcher'

import { FetcherResponse } from 'interfaces/fetcher'
import { PrintArea } from 'interfaces/product-base';

const getPrintAreas = async (): Promise<PrintArea[]> => {
  const { data: responseData } = await axiosInstance.get<
    FetcherResponse<PrintArea[]>
  >('print-areas')

  return responseData.data
}

export default getPrintAreas
