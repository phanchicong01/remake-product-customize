import { ID } from 'helpers';

export interface GetPrintAreasRequest {
    product_base_id:ID
}