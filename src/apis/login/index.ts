import {axiosInstance} from 'utils/fetcher'

import { FetcherResponse } from 'interfaces/fetcher'
import { LoginPayload } from './interface'
import { AuthDataModel } from 'interfaces/auth'

const login = async (
  loginPayload: LoginPayload,
): Promise<FetcherResponse<AuthDataModel>> => {
  const { data: responseData } = await axiosInstance.post<
    FetcherResponse<AuthDataModel>
  >('/auth/login', loginPayload , {headers: {
    Authorization:null
  }})

  return responseData
}

export default login
