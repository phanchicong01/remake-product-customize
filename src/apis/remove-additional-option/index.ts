import { ID } from 'helpers';
import { AdditionalOption } from 'interfaces/additional-options';
import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'


const removeAdditionalOption = async ( payload:ID): Promise<FetcherResponse<AdditionalOption>> => {
    const { data: responseData } = await axiosInstance.delete<
    FetcherResponse<AdditionalOption>
  >('additional-options/'+ payload )

  return responseData
}

export default removeAdditionalOption