import { ID } from 'helpers';
import { ArtworkCategory } from "interfaces/artwork";

export interface UpdateArtworkCategoriesRequest  {
    id:ID,
    data:{
        name:string,
        parent_id:ID
    }
}
export interface CategoriesResponse extends ArtworkCategory {}