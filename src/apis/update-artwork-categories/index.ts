import {axiosInstance} from 'utils/fetcher'

import { FetcherResponse } from 'interfaces/fetcher'
import { UpdateArtworkCategoriesRequest, CategoriesResponse } from './interface'

const updateArtworkCategories = async (payload:UpdateArtworkCategoriesRequest): Promise<FetcherResponse<CategoriesResponse>> => {
  const { data: responseData } = await axiosInstance.put<
    FetcherResponse<CategoriesResponse>
  >('artwork-categories/'+ payload.id, payload.data)

  return responseData
}

export default updateArtworkCategories
