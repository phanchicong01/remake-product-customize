import { axiosInstance } from 'utils/fetcher'

import { FetcherResponse } from 'interfaces/fetcher'
import { UserResponseModel } from './interface'
import { BaseParamRequest } from 'interfaces/common.interface'

const getListUsers = async (params: BaseParamRequest): Promise<UserResponseModel> => {
  const { data: responseData } = await axiosInstance.get<
    FetcherResponse<UserResponseModel>
  >(`user`, {
    params
  })
  return responseData.data
}

export default getListUsers
