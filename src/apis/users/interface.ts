
import { UserModel } from './../../interfaces/auth';

export interface UserResponseModel {
    current_page: number,
    data: UserModel[],
    last_page: number,
    per_page: number,
    total: number
}
