import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'
import { UpdateClipartItemRequest, ClipartItemResponse } from './inteface'


const updateClipartItems = async ( payload:UpdateClipartItemRequest): Promise<FetcherResponse<ClipartItemResponse>> => {

    const { data: responseData } = await axiosInstance.put<
    FetcherResponse<ClipartItemResponse>
  >('cliparts/'+payload.id, payload.data)

  return responseData
}

export default updateClipartItems