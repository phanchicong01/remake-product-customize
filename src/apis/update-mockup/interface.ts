import { ID } from "helpers";

export interface UpdateMockupRequest {
    id:ID,
    data:{
        "content": string,
        "is_background_by_variable_color": number,
        "product_base_id":number
    }
}