import { FetcherResponse } from 'interfaces/fetcher'
import { Mockup } from 'interfaces/product-base'
import { axiosInstance } from 'utils/fetcher'
import {UpdateMockupRequest  } from './interface'


const updateMockup = async ( payload:UpdateMockupRequest): Promise<FetcherResponse<Mockup>> => {
    const { data: responseData } = await axiosInstance.put<
    FetcherResponse<Mockup>
  >('mockups/'+ payload.id, payload.data )

  return responseData
}

export default updateMockup