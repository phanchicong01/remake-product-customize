import { ID } from 'helpers';
import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'


const removeMultipleClipartCategory = async ( payload:Array<string>): Promise<FetcherResponse<null>> => {
    const { data: responseData } = await axiosInstance.post<
    FetcherResponse<null>
  >('clipart-categories/delete-many', {ids:payload} )

  return responseData
}

export default removeMultipleClipartCategory