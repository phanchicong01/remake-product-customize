import { ID } from '../../helpers/crud-helper/models';
import { ClipArtCategory } from 'interfaces/clipart-category'
import { TypeDisplayClipart } from 'constants/enum';

export interface UpdateClipartCategoryRequest {
    "id":ID,
    "name":string,
    "is_show_name_on_hover":Number ,
    "parent_category_id":number | string,
    "max_width":number,
    "order":number,
    "display":TypeDisplayClipart,
    thumbnail:string
    
}
export interface ClipartCategoryResponse extends ClipArtCategory{}