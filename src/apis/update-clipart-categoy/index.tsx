import { ID } from 'helpers'
import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'
import { UpdateClipartCategoryRequest, ClipartCategoryResponse } from './inteface'


const updatedClipartCategory = async (clipartCategoryId:ID, body:Partial<UpdateClipartCategoryRequest>): Promise<FetcherResponse<ClipartCategoryResponse>> => {

    const { data: responseData } = await axiosInstance.put<
    FetcherResponse<ClipartCategoryResponse>
  >('clipart-categories/'+clipartCategoryId, body)

  return responseData
}

export default updatedClipartCategory