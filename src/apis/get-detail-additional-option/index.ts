import { ID } from 'helpers';
import {axiosInstance} from 'utils/fetcher'

import { FetcherResponse } from 'interfaces/fetcher'
import { AdditionalOptionsResponse } from './interface'

const getDetailAdditionalOptions = async (additionId:ID): Promise<FetcherResponse<AdditionalOptionsResponse>> => {
  const { data: responseData } = await axiosInstance.get<
    FetcherResponse<AdditionalOptionsResponse>
  >('additional-options/'+additionId)

  return responseData
}

export default getDetailAdditionalOptions
