import {axiosInstance} from 'utils/fetcher'
import _pick from "lodash-es/pick"

import { FetcherResponse } from 'interfaces/fetcher'
import { ArtworkListRequest, ArtworkListResponse } from 'apis/get-artworks/interface'

const getArtworkList = async ( params:ArtworkListRequest): Promise<ArtworkListResponse> => {
  const getArtworkString = params.category === 0 ? 'get-artworks' : 'get-artworks/'+params.category
  const { data: responseData } = await axiosInstance.get<
    FetcherResponse<ArtworkListResponse>
  >(getArtworkString, {
    params:_pick(params, ["search","per_page","page"])
  })

  return responseData.data
}

export default getArtworkList
