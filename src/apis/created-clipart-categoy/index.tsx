import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'
import { CreateClipartCategoryRequest, ClipartCategoryResponse } from './inteface'


const CreatedClipartCategory = async ( body:CreateClipartCategoryRequest): Promise<FetcherResponse<ClipartCategoryResponse>> => {

    const { data: responseData } = await axiosInstance.post<
    FetcherResponse<ClipartCategoryResponse>
  >('clipart-categories', body)

  return responseData
}

export default CreatedClipartCategory