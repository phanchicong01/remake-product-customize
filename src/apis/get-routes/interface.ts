export interface RouteItem {
  label: string
  route: string
  icon?: string
  parent_id?: number
  children?: Array<RouteItem>
  id: number
}
