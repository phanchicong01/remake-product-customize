import { axiosInstance } from 'utils/fetcher'
import { FetcherResponse } from 'interfaces/fetcher'
import { RouteItem } from './interface'



const getRoutes = async (): Promise<RouteItem[]> => {
    const { data: responseData } = await axiosInstance.get<
        FetcherResponse<RouteItem[]>
    >(`route`)
    return responseData.data
}

export default getRoutes

