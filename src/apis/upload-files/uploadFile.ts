import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'
import {  UploadFileResponse } from './interfaces'


const uploadFile = async ( formData:any) => {
   
    const { data: responseData } = await axiosInstance.post<
    FetcherResponse<UploadFileResponse>
  >('resource/upload-image', formData)
  return responseData
}

export default uploadFile