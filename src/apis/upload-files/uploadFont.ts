
import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'
import { UploadFileResponseInterface } from './upload-file.interface'


const uploadFont = async (file: File) => {
    let data = new FormData()
    data.append('file', file)
    const { data: responseData } = await axiosInstance.post<
        FetcherResponse<UploadFileResponseInterface>
    >('/resource/upload-font', data)

    return responseData
}

export default uploadFont