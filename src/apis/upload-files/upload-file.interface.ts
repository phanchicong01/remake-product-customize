export interface UploadFileResponseInterface {
    path: string,
    thumbnail?: string
}