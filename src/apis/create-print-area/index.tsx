import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'
import { CreatePrintAreaRequest , CreatePrintAreaResponse } from './inteface'


const createPrintArea = async ( payload:CreatePrintAreaRequest): Promise<FetcherResponse<CreatePrintAreaResponse>> => {
    const { data: responseData } = await axiosInstance.post<
    FetcherResponse<CreatePrintAreaResponse>
  >('print-areas', payload)

  return responseData
}

export default createPrintArea