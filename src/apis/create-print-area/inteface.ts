
import { PrintArea } from 'interfaces/product-base';

export interface CreatePrintAreaRequest {
    "name": string
    "width": number,
    "height": number,
    "position"?: number,
    "product_base_id": 5
}
export interface CreatePrintAreaResponse extends PrintArea{}