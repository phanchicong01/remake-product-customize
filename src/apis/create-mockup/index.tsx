import { FetcherResponse } from 'interfaces/fetcher'
import { axiosInstance } from 'utils/fetcher'
import { CreateMockupRequest , CreateProductBaseResponse } from './inteface'


const createMockup = async ( payload:CreateMockupRequest): Promise<FetcherResponse<CreateProductBaseResponse>> => {

    const { data: responseData } = await axiosInstance.post<
    FetcherResponse<CreateProductBaseResponse>
  >('mockups', payload)

  return responseData
}

export default createMockup