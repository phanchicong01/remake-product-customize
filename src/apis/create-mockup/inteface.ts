
import { ID } from 'helpers';
import { ProductBase } from 'interfaces/product-base';

export interface CreateMockupRequest {
    "content": string,
    "is_background_by_variable_color": number,
    "product_base_id":ID
}
export interface CreateProductBaseResponse extends ProductBase{}