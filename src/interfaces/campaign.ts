import { ID } from "helpers";

export interface Campaign {
    id:ID
    mockup:string,
    name:string,
    front_artwork_id: ID,
    back_artwork_id: ID,
    thumbnail_front: string,
    thumbnail_back: string,
    created_at:string,
    updated_at:string,
}
