import { ID } from "helpers";

export interface FontInterface {
    id?: ID;
    name: string;
    url: string;
    folder?: string;
    created_at?: string;
    updated_at?: string;

}


export interface FontUploadInterface {
    folder?: string;
    name: string;
    file: File;


}