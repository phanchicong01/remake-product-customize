import { ID } from 'helpers';
export interface AdditionalOption {
    "id": ID,
    "name": string,
    "type": string,
    "is_show_clipart_name_on_hover": number,
    "placeholder": string,
    "created_at": string,
    "updated_at": string,
    items:AdditionalOptionItem[]
}

export interface AdditionalOptionItem {
    "id": ID,
    "name": string,
    "color": string,
    "thumbnail": string,
    "additional_option_id": ID,
    "created_at": string,
    "updated_at":string
}