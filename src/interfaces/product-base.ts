import { ID } from "helpers";

export interface ProductBase {
    id:ID
    mockup:string,
    createAt:string,
    name:string,
    printAreas:PrintArea[]
}
export interface PrintArea{
    id:ID,
    name:string,
    product_base_id:string,
    width:number,
    height:number,
    position:number,
    created_at:string,
    updated_at:string
}

export interface Mockup {
    "id": ID,
    "content": string,
    "is_background_by_variable_color": number,
    "product_base_id":number,
    created_at:string,
    updated_at:string
}
