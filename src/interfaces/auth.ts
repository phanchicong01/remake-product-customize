export interface AuthModel {
    access_token: string
    expires_in?: string
  }

  export interface AuthDataModel {
    access_token: string
    expires_in?: string
    token_type?: string
    user: UserModel,
  }

  
  export interface UserModel {
    created_at?: string
    email: string
    email_verified_at?: string
    avatar?:string
    id: number
    name: string
    updated_at?: string
  }
  