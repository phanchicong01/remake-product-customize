import { TypeDisplayClipart } from 'constants/enum';
import { ID } from 'helpers';

export interface ClipArtCategory {
    id: ID;
    name: string;
    thumbnail: string;
    order:number
    parent_category_id: number | string;
    max_width: number;
    display: TypeDisplayClipart;
    is_show_name_on_hover: boolean;
    created_at: string;
    updated_at: string;
    items: ClipartItem[];
    children:ClipArtCategory[]
    url?:string
}

export type ClipartItem = {
    "id": ID,
    "name":string,
    "url": string,
    "thumbnail_url": string,
    "color": string,
    "category_id": ID
   
}


