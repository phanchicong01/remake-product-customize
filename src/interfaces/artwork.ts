import { ID } from "helpers";

export interface ArtworkCategory {
    id: ID;
    name: string;
    sort: number;
    created_at: string;
    updated_at: string;
    parent_id?: any;
    thumbnail?: any;
    children: ArtworkCategory[];
}

export interface Artwork {
    id: ID;
    name: string;
    sort: number;
    createdAt: string;
    updatedAt: string;
    parentId?: any;
    thumbnail?: any;
    children: Artwork[];
}

