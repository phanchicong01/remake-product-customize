export interface FetcherResponse<D = null> {
  success: boolean
  message: string
  code: number
  data: D
}
