export interface BaseParamRequest {
    search: string,
    per_page: number,
    page: number
}