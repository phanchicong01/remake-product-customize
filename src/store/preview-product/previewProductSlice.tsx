import { createSlice, PayloadAction } from "@reduxjs/toolkit";

// Define the initial state using that type
const initialState: any[] = [];
export const previewProductSlice = createSlice({
    name: "previewProductSlice",
    initialState,
    reducers: {
        //action object
        onUpdateDataPreviewProduct: (state: any[], action: PayloadAction<any[]>) => {
            state = action.payload;
            return state;
        },
    },
});

// Action creators are generated for each case reducer function
export const { onUpdateDataPreviewProduct } = previewProductSlice.actions;

export default previewProductSlice.reducer;
