import { RectConfig } from "konva/lib/shapes/Rect";
import { TextConfig } from "konva/lib/shapes/Text";
import { RePosition } from "store/re-position-reducer/interface";
import { fontKind } from "../fonts-family/interface";

type typeImage = "image";
type typeText = "text";
export type typeLayer = typeImage | typeText;
type Merge<X, Y> = {
    [K in (keyof X | keyof Y)]:
    (K extends keyof X ? X[K] : never)
    | (K extends keyof Y ? Y[K] : never)
};
export type ILayer = Merge<LayerImage, LayerText>
export type IConfigState = IConfigurationText | IConfigurationText
export type Layer = LayerCommon & ILayer
export interface LayerCommon {
    id: any,
    type: typeLayer;
    name?: string,
    lock?: boolean,
    visible?: boolean,
    isActiveConfig?: boolean,
}
// export interface Layer extends ILayer {
//     id: any,
//     type: typeLayer;
//     name?: string,
//     lock?: boolean,
//     visible?: boolean,
//     isActiveConfig?: boolean,
// }
export interface LayerImage extends RectConfig {
    src?: string;
    zIndex?: number;
    configuration?: IConfigurationImage
}
export interface IConfigurationImage {
    reOrder: number
    personalization: IPersonalizationImage,
    clipArtSetting: ClipartSetting,
    groupClipArtSetting: GroupClipArtSetting,
    uploadPhotoSetting: {
        title: string,
        cropper: boolean
        liveReview: boolean,
        instruction: string,
        require: boolean
        showCustomization: boolean,
    }
    conditionalSetting: ConditionSetting,
    extra: {
        customClass: string
    }
}
export interface ConditionSetting {
    layerShowOptionWhen: Layer,
    clipartMatchShow: ClipArtCategory[]
    showLayerFirstLoad: boolean,
    itemMatchShow: ClipArtCategoryItem
}
export interface ClipartSetting {
    title: string,
    clipArtCategory: ClipArtCategory,
    rePosition: RePosition[]
    defaultOption: ClipArtCategoryItem
    require: boolean
    showCustomization: boolean,
}
export interface GroupClipArtSetting extends ClipartSetting {
    placeholder: string,
    defaultOptionParent: ClipArtCategory,
}
export interface IPersonalizationImage {
    toggleShowLayer: boolean,
    defaultValueToggleShow: boolean,
    option: "none" | "clipart" | "group-clipart" | "upload-photo" | "maps"
}
export interface LayerText extends TextConfig {
    zIndex?: number;
    fontKind?: fontKind;
    configuration?: IConfigurationText
}
export interface IConfigurationText {
    reOrder: number
    personalization: IPersonalizationText,
    conditionalSetting: ConditionSetting,
    extra: {
        customClass: string
    }
}

export interface IPersonalizationText {
    toggleShowLayer: boolean,
    defaultValueToggleShow: boolean,
    option: "none" | "enable",
    title: string,
    placeholder: string,
    limitCharacter: number,
    require: boolean
    showCustomization: boolean,
}


export interface Template {
    id: any,
    name: string,
    layers: Layer[],
    layerSelected: Layer[]
}

export interface ClipArtCategory {
    id: number;
    name: string;
    thumbnail: string;
    parent_category_id: number;
    order: number;
    max_width: number;
    display: "image" | "text" | "radio"; //text => dropdown , image => thumbnail , radio => inline-button
    is_show_name_on_hover: true;
    items: ClipArtCategoryItem[];
    children: ClipArtCategory[];
    url: string
}
export interface ClipArtCategoryItem {
    category_id: number
    color: null
    id: number
    name: string
    thumbnail_url: string
    url: string
}
