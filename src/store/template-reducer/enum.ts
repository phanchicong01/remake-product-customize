export enum OptionPersonalizationImage  {
   none = "none" ,
   clipart =  "clipart" ,
   groupClipart =  "group-clipart" ,
   uploadPhoto = "upload-photo" ,
   maps = "maps" 
}