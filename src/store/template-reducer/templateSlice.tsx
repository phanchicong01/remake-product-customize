import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { PositionLayer } from "constants/enum";
import { Layer, Template, typeLayer } from "./interface";

// var zIndex = 1;
interface TemplateState {
    templates: Template[];
    templateActive: Template;
}

// Define the initial state using that type
const initialState = {
    templates: [
        {
            id: `template-${Date.now()}`,
            name: "Default  Template",
            layers: [],
            layerSelected: []
        }
    ],
    templateActive: {
        id: `template-${Date.now()}`,
        name: "Default  Template",
        layers: [],
        layerSelected: []
    },
} as TemplateState
export const templateSlice = createSlice({
    name: "templateSlice",
    initialState,
    reducers: {
        // new Action template
        addLayer: (state, action: PayloadAction<{ item: any, type: typeLayer }>) => {
            const { item, type } = action.payload;
            const newTemplateActive: any = state.templateActive;
            const indexLayerById = newTemplateActive.layers.findIndex(i => i.id === item.id);
            if (indexLayerById !== -1) {
                let tempLayer = newTemplateActive.layers[indexLayerById]
                tempLayer = { ...tempLayer, ...item }
                newTemplateActive.layers[indexLayerById] = tempLayer;
                newTemplateActive.layerSelected = [tempLayer];
            } else {
                let newLayer: Layer = {
                    id: item.id || `layer-${Date.now()}`,
                    type: type,
                    visible: true,
                    isActiveConfig: false,
                };
                if (type === "image") {
                    const newLayerImage: Layer = {
                        ...newLayer,
                        src: item.src,
                        name: "Image Layer",
                        visible: true,
                        configuration: {
                            reOrder: null,
                            personalization: null,
                            clipArtSetting: null,
                            groupClipArtSetting: null,
                            uploadPhotoSetting: null,
                            conditionalSetting: null,
                            extra: null
                        },
                        ...item,
                    };
                    newLayer = newLayerImage
                } else {
                    const newLayerText: Layer = {
                        ...newLayer,
                        id: `layer-${Date.now()}`,
                        text: `Text Preview`,
                        type: "text",
                        name: "Text layer",
                        visible: true,
                        fontSize: 100,
                        fill: "#e66465",
                        fontFamily: "Roboto",
                        fontKind: "webfonts#webfontList",
                        align: "center",
                        verticalAlign: "middle",
                        textDecoration: "normal",
                        strokeEnabled: false,
                        stroke: "#000",
                        configuration: {
                            reOrder: null,
                            personalization: null,
                            conditionalSetting: null,
                            extra: null
                        },
                        ...item,
                    };
                    newLayer = newLayerText

                }
                newTemplateActive.layers.push(newLayer);
                newTemplateActive.layerSelected = [newLayer];
            }
            state.templateActive = newTemplateActive;
        },
        // new Action template
        onSelectLayer: (state: TemplateState, action: PayloadAction<{ status: boolean; layerSelected: Layer }>) => {
            const { status, layerSelected } = action.payload;
            const newTemplateActive = state.templateActive;

            newTemplateActive.layerSelected = [layerSelected];
            state.templateActive = newTemplateActive;
        },
        onSelectMultipleLayer: (state: TemplateState, action: PayloadAction<{ status: boolean; layerSelected?: Layer; multipleLayerSelected?: Layer[] }>) => {
            const { status, layerSelected = null, multipleLayerSelected = [] } = action.payload;

            const newTemplateActive: Template = state.templateActive;

            if (multipleLayerSelected.length > 0) {
                if (status) {
                    newTemplateActive.layerSelected = multipleLayerSelected
                } else {
                    multipleLayerSelected.forEach(layer => {
                        const indexSelected = newTemplateActive.layerSelected.findIndex((item) => item.id === layer.id);
                        if (indexSelected !== -1) {
                            newTemplateActive.layerSelected.splice(indexSelected, 1);
                        }
                    })
                }
            }
            if (!!layerSelected) {
                if (status && !!layerSelected?.id) {
                    newTemplateActive.layerSelected.push(layerSelected)
                } else {
                    const indexSelected = newTemplateActive.layerSelected.findIndex((item) => item.id === layerSelected.id);
                    if (indexSelected !== -1) {
                        newTemplateActive.layerSelected.splice(indexSelected, 1);
                    }
                }
            }
            state.templateActive = newTemplateActive;

        },
        //   // new Action template
        onUpdateAttrLayer: (
            state: TemplateState,
            action: PayloadAction<{ newAttrs: Layer | any; }>
        ) => {
            const { newAttrs } = action.payload;
            const newTemplateActive = state.templateActive
            const indexLayer = newTemplateActive.layers.findIndex((layer) => layer.id === newAttrs.id);
            const indexLayerSelected = newTemplateActive.layerSelected.findIndex((layer) => layer.id === newAttrs.id);

            if (indexLayer !== -1) {
                const cpAttrs = newTemplateActive.layers[indexLayer];
                newTemplateActive.layers[indexLayer] = { ...cpAttrs, ...newAttrs };
                if (indexLayerSelected !== -1) {
                    newTemplateActive.layerSelected[indexLayerSelected] = { ...cpAttrs, ...newAttrs }
                }
            }
            state.templateActive = newTemplateActive;
        },
        onUpdateAttrConfig: (
            state: TemplateState,
            action: PayloadAction<{ newAttrs: any, nameAttrs: string }>
        ) => {
            const { newAttrs, nameAttrs } = action.payload;
            const newTemplateActive = state.templateActive
            const currentActiveLayer = state.templateActive.layerSelected[0]
            const indexLayer = newTemplateActive.layers.findIndex((layer) => layer.id === currentActiveLayer.id);
            if (indexLayer !== -1) {
                const cpAttrs = newTemplateActive.layers[indexLayer];
                newTemplateActive.layers[indexLayer] = {
                    ...cpAttrs,
                    configuration: {
                        ...cpAttrs.configuration,
                        [nameAttrs]: {

                            ...newAttrs
                        }
                    },
                };
                newTemplateActive.layerSelected[0] = {
                    ...cpAttrs,
                    configuration: {
                        ...cpAttrs.configuration,
                        [nameAttrs]: {
                            ...newAttrs
                        }
                    },
                }
            }
            state.templateActive = newTemplateActive;
        },
        onUpdatePositionLayerSelected: (state: TemplateState, action: PayloadAction<{ position: PositionLayer, value: number }>) => {
            const { position, value } = action.payload
            const newTemplateActive = state.templateActive
            const newLayerSelected = newTemplateActive.layerSelected.map((layer: Layer) => {
                return {
                    ...layer,
                    x: layer.x + value
                }
            })
            newTemplateActive.layerSelected = newLayerSelected
            const newLayer = newTemplateActive.layers.map((layer: Layer) => {
                const isExist = state.templateActive.layerSelected.find((layerSelected: Layer) => layerSelected.id === layer.id)
                if (isExist) {
                    return {
                        ...layer,
                        [position]: layer[position] + value
                    }
                }
                return { ...layer }
            })
            newTemplateActive.layers = newLayer
            state.templateActive = newTemplateActive
        },
        //    // new Action template
        onRemoveLayerByID: (state: TemplateState, action: PayloadAction<{ idLayer: any }>) => {
            const { idLayer } = action.payload;
            if (!!state.templateActive) {
                const newTemplateActive = state.templateActive;
                const indexObj = newTemplateActive.layers.findIndex((obj) => obj.id === idLayer);
                if (indexObj !== -1) {
                    newTemplateActive.layers.splice(indexObj, 1);
                    newTemplateActive.layerSelected = [];
                    state.templateActive = newTemplateActive;
                }
            }
        },

        // // new action template
        onDragEndLayer: (
            state: TemplateState,
            action: PayloadAction<{ indexStart: any; indexEnd: any }>
        ) => {
            const { indexStart, indexEnd } = action.payload;
            const newLayers = Array.from(state.templateActive.layers);
            newLayers.reverse();

            const [sourceItem] = newLayers.splice(indexStart, 1);

            newLayers.splice(indexEnd, 0, sourceItem);
            // if (state.templateActive.layerSelected.length > 0) state.templateActive.layerSelected = [];
            newLayers.reverse();


            state.templateActive.layers = [...newLayers];

        },
        onReOrderLayerConfig: (state: TemplateState,
            action: PayloadAction<{ destinationItem: Layer; sourceItem: Layer }>) => {
            const { destinationItem, sourceItem } = action.payload;
            state.templateActive.layers.map((layer: Layer) => {
                if (layer.id === destinationItem.id) {
                    layer.configuration.reOrder = destinationItem.configuration.reOrder;
                }
                if (layer.id === sourceItem.id) {
                    layer.configuration.reOrder = sourceItem.configuration.reOrder;
                }
                return layer
            })
        },
        onDuplicateLayer: (
            state: TemplateState,
            action: PayloadAction<{ idLayer: number; newIdLayer: any }>
        ) => {
            const { idLayer, newIdLayer } = action.payload;

            const indexLayerCopy = state.templateActive.layers.findIndex((layer) => layer.id === idLayer)
            if (indexLayerCopy !== -1) {
                const cloneLayerCopy = { ...state.templateActive.layers[indexLayerCopy] }
                cloneLayerCopy.id = newIdLayer;
                if (!cloneLayerCopy.name.includes("copy")) {
                    cloneLayerCopy.name += " copy";
                }
                state.templateActive.layers.splice(indexLayerCopy + 1, 0, cloneLayerCopy);
            }

        },

        // // Action layer
        onAddNewTemplate: (state: TemplateState) => {
            const newTemplate: Template = {
                id: `template-${Date.now()}`,
                name: "Template",
                layers: [],
                layerSelected: [],
            };
            state.templates.push(newTemplate);
        },

        onUpdateTemplateActive: (state: TemplateState, action: PayloadAction<{ newTemplateAttr: Template }>) => {
            const { newTemplateAttr } = action.payload;
            state.templateActive = newTemplateAttr
        },
        onRemoveTemplateByID: (state: TemplateState, action: PayloadAction<string>) => {
            const templateId = action.payload;
            const tempLayers = [...state.templates];
            if (state.templates.length > 0) {
                const indexTemplateRemove = tempLayers.findIndex(temp => temp.id === templateId)
                indexTemplateRemove !== -1 && tempLayers.splice(indexTemplateRemove, 1);
                if (tempLayers[indexTemplateRemove - 1]) {
                    state.templateActive = tempLayers[indexTemplateRemove - 1];
                } else {
                    state.templateActive = tempLayers[0];
                }
            }
            state.templates = tempLayers;

        },
        onDuplicateTemplateByID: (state: TemplateState, action: PayloadAction<{ newId: string }>) => {
            const { newId } = action.payload;
            const newLayer = Object.assign({}, state.templateActive);
            const indexCurrent = state.templates.findIndex(template => template.id === state.templateActive.id)
            newLayer.id = newId;
            if (!newLayer.name.includes("copy")) {
                newLayer.name += " copy";
            }
            state.templates.splice(indexCurrent + 1, 0, newLayer);
        },
        onActiveTemplate: (state: TemplateState, action: PayloadAction<any>) => {
            const newTemplateActive = action.payload;
            assignTemplateActive(state.templateActive, state.templates, newTemplateActive);
            state.templateActive = newTemplateActive;
        },
        onLeaveTemplate: (state: TemplateState, action: PayloadAction<any>) => {
            return state
        },
        onUpdateInitDataTemplate: (state: TemplateState, action: PayloadAction<any[]>) => {
            if (Array.isArray(action.payload)) {
                const newDataPayload = [...action.payload]

                if (newDataPayload[0]) {
                    const newTemplateActive = { ...state.templateActive, ...newDataPayload[0] }
                    if (newTemplateActive.layers?.length > 0) {
                        console.log(newTemplateActive, "newTemplateActive")
                        newTemplateActive.layerSelected = [newDataPayload[0].layers[0]]
                    }
                    state.templateActive = newTemplateActive;
                }
                state.templates = [...newDataPayload]
            }
        },
    },
});

// Action creators are generated for each case reducer function
export const {
    onActiveTemplate,
    // new action layer (template)
    onDuplicateLayer,
    onAddNewTemplate,
    onUpdateTemplateActive,
    onUpdatePositionLayerSelected,
    onRemoveTemplateByID,
    onDuplicateTemplateByID,
    addLayer,
    onSelectLayer,
    onSelectMultipleLayer,
    onRemoveLayerByID,
    onUpdateAttrLayer,
    onUpdateAttrConfig,
    onDragEndLayer,
    onReOrderLayerConfig,
    //template
    onUpdateInitDataTemplate,
} = templateSlice.actions;

export default templateSlice.reducer;

export const assignTemplateActive = (prevActive, targetList, nextActive) => {
    if (prevActive.id !== nextActive.id) {
        const indexTemplate = targetList.findIndex(temp => temp.id === prevActive.id);
        if (indexTemplate !== -1) {
            targetList[indexTemplate] = prevActive
        }
    }
}
