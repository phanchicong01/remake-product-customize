import { createSlice } from "@reduxjs/toolkit";


// Define the initial state using that type
const initialState: any = {
 
};
export const mockupSlice = createSlice({
    name: "mockupSlice",
    initialState,
    reducers: {
        reset: () => initialState
    }
});

// Action creators are generated for each case reducer function
export const {
    reset
} = mockupSlice.actions;

export default mockupSlice.reducer;
