
import { ID } from "helpers";

export interface MockupProductCategory {
    id:ID | string,
    title: string,
    design:{width:number , height:number}
    url: string,
    color:string[],
    srcFront:string
    srcBack:string
    artwork?: {
        front:MockupProductCategoryArtwork,
        back:MockupProductCategoryArtwork
    }
    file?:any
}

export interface MockupProductCategoryArtwork {
        src:any,
        x:number,
        y:number,
        width:number,
        height:number,
        dpi:number,
  
}
   