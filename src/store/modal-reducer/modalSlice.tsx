import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {  KeyModal } from "./interface";

// Define the initial state using that type
interface IState {
    key: KeyModal,
    data: any
}
const initialState = {
    key: null,
    data: null
} 
export const modalSlice = createSlice({
    name: "modalSlice",
    initialState,
    reducers: {
        setModal: (
            state: IState,
            action: PayloadAction<{
                key: KeyModal;
                data: any;
            }>
        ) => {
            const {key , data} = action.payload;
            state.key = key;
            state.data = data
        },
        closeModal: (
            state: IState,
        ) => {
            const {key , data} = initialState;
            state.key = key;
            state.data = data
        },
        
    },
});

// Action creators are generated for each case reducer function
export const {
    setModal,
    closeModal
} = modalSlice.actions;

export default modalSlice.reducer;
