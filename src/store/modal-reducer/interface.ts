
export type KeyModal =
| "modal-image"
| "modal-replace-image"
| "modal-setting-artwork"
| "modal-setting-template"
| "save-data"
| "saved-data"
| null;