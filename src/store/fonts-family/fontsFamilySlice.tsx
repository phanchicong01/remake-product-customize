import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { IFontsState } from "./interface";
// Define the initial state using that type
const initialState: IFontsState = {
    googleFonts: {
        kind: "webfonts#webfontList",
        items: [],
    },
    customFonts: {
        kind: "customfonts#customfontList",
        items: [],
    },
    localFonts: {
        kind: "localfonts#localfontList",
        items: [],
    },
    fontsSelected: [],
};
export const fontsFamilySlice = createSlice({
    name: "fontsFamilySlice",
    initialState,
    reducers: {
        createGoogleFonts: (state: IFontsState, action: PayloadAction<any>) => {
            state.googleFonts.items = action.payload;
        },
        createLocalFonts: (state: IFontsState, action: PayloadAction<any>) => {
            state.localFonts.items = action.payload;
        },
        createCustomFonts: (state: IFontsState, action: PayloadAction<any>) => {
            state.customFonts.items = action.payload;
        },
        updateFontsSelected: (state: IFontsState, action: PayloadAction<any[]>) => {
            state.fontsSelected = action.payload;
        },
    },
});

// Action creators are generated for each case reducer function
export const { createGoogleFonts, createLocalFonts, createCustomFonts, updateFontsSelected } = fontsFamilySlice.actions;

export default fontsFamilySlice.reducer;
