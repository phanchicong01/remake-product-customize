export interface IFontsState {
    googleFonts: {
        kind: string;
        items: any[];
    };
    localFonts: {
        kind: string;
        items: any[];
    };
    customFonts: {
        kind: string;
        items: any[];
    };
    fontsSelected: any[];
}
export type fontKind = "webfonts#webfontList" | "customfonts#customfontList" | "localfonts#localfontList";
export const valueFontKind = {
    googleFonts: "webfonts#webfontList",
    customFonts: "customfonts#customfontList",
    localFonts: "localfonts#localfontList",
};
