import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { TypePrintAreas } from "store/template-reducer/enum";
import { IMockup, ISetting, PrintArea } from "./interface";

interface MockupSliceState  extends IMockup{
    printAreaActive:PrintArea[]
}
// Define the initial state using that type
const initialState: MockupSliceState = {
    backgroundColor:"",
    backgroundImage:"",
    thumbnail:"",
    backgroundVariantColor:false,
    productBaseId:null,
    size:{
        width: 1200,
        height: 1200,
    },
    setting:{
        scale:{x:0.5, y:0.5},
        showGrid:false,
        previewMod:false
    },
    printAreas:[],
    printAreaActive:[],
};
export const mockupSlice = createSlice({
    name: "mockupSlice",
    initialState,
    reducers: {
        onResetMockup:(state) => {
            return initialState
        },
        onInitMockup:(state, action: PayloadAction<MockupSliceState>) => {
            return {
                ...state,
                ...action.payload   
            }
        },
        onUpdateAttrMockup: (state, action: PayloadAction<{name, value}>) => {
            const {name, value} = action.payload
            return {
                ...state,
                [name]:value
            }
        },
        onUpdateSetting: (state, action: PayloadAction<ISetting>) => {
            return {
                ...state,
                setting:{
                    ...state.setting,
                    ...action.payload
                }
            }
        },
       
        addPrintAreas:(state, aciton:PayloadAction<{ item: Partial<PrintArea>, type: TypePrintAreas }>) => {
            const {item , type} =  aciton.payload;
            let newPrintAreaActive: any = state.printAreaActive;
            const printAreas: any = state.printAreas;
            const newImage:Partial<PrintArea>  = {
                id: item.id || `image-${Date.now()}`,
                type: type,
                visible: true,
                options:{
                    enableMasked:false,
                    enablePerspective:false,
                    opacity:1
                },
                width:301,
                height:601,
                x:106,
                y:484,
                ...item,
            };
         
            newPrintAreaActive = [newImage]
            printAreas.push(newImage)
            state.printAreas = printAreas
            state.printAreaActive = newPrintAreaActive
        },
        onSelectLayer: (
            state,
            action: PayloadAction<PrintArea>
        ) => {
                let newPrintAreaActive: any = state.printAreaActive;
                newPrintAreaActive = [action.payload]
                state.printAreaActive = newPrintAreaActive
            
        },
        onUpdateAttrPrintArea: (state, action: PayloadAction<PrintArea>) => {
            const newDataAttr = action.payload;
            let newPrintAreas: any = state.printAreas;
            const index =  newPrintAreas.findIndex((item:any)=> item.id === newDataAttr.id);
            if(index !== -1){
                const cpAttrs = newPrintAreas[index];
                newPrintAreas[index] = {...cpAttrs,...newDataAttr}
                state.printAreaActive = [{...cpAttrs ,...newDataAttr }]
                state.printAreas = newPrintAreas
            }
           
        },
        onRemovePrintArea: (state, action:PayloadAction<string>) => {
            const idRemove  = action.payload ;
            const index = state.printAreas.findIndex((print:any) => print.id ===  idRemove)
            
            if(index !== -1) {
                const newPrintAreas = state.printAreas;
                newPrintAreas.splice(index, 1);
                state.printAreas = newPrintAreas;
                if(state.printAreaActive.some((print:any)=> print.id === idRemove)){
                    state.printAreaActive = []
                }
            } 
        },
        setThumbnail: (state, action: PayloadAction<any>) => {
            state.thumbnail = action.payload;
        },
        reset: () => initialState

    }
});

// Action creators are generated for each case reducer function
export const {
    // updateBackground,
    setThumbnail,
    onResetMockup,
    onInitMockup,
    onUpdateAttrMockup,
    onRemovePrintArea,
    onSelectLayer,
    onUpdateAttrPrintArea,
    addPrintAreas,
    onUpdateSetting,
    reset
} = mockupSlice.actions;

export default mockupSlice.reducer;
