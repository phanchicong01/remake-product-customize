import { TypePrintAreas } from "store/template-reducer/enum"
import { RectConfig } from "konva/lib/shapes/Rect";

export interface IMockup {
    backgroundColor:string,
    backgroundImage:string,
    backgroundVariantColor:boolean,
    thumbnail:string,
    productBaseId:number,
    size:{
        width:number,
        height:number
    },
    setting:ISetting,
    printAreas:PrintArea[]
   
}
export interface PrintArea extends RectConfig {
    name:string,
    type: TypePrintAreas,
    size:{
        width:number,
        height:number
    },
    visible:boolean,
    options:{
        enableMasked:boolean,
        enablePerspective:boolean,
        opacity:number
    }
}
export interface ISetting{
    scale:{x:number, y:number},
    showGrid:boolean,
    previewMod:boolean
}
export type typeDisplayOption = "Radio" | "Dropdown"
   