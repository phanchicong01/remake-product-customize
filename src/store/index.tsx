import { configureStore } from "@reduxjs/toolkit";
import artworkReducer from "./artwork-reducer/artworkSlice";
import templateReducer from "./template-reducer/templateSlice";
import fontsFamilyReducer from "./fonts-family/fontsFamilySlice";
import drawerReducer from "./drawer-menu/drawerSlice";
import previewProductReducer from "./preview-product/previewProductSlice";
import modalReducer from "./modal-reducer/modalSlice";
import rePositionReducer from "./re-position-reducer/RePositionSlice";

const store = configureStore({
    reducer: {
        artwork: artworkReducer,
        template: templateReducer,
        previewProduct: previewProductReducer,
        fontsFamily: fontsFamilyReducer,
        drawerMenu: drawerReducer,
        modal: modalReducer,
        rePosition: rePositionReducer,
    },
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;

export default store;
