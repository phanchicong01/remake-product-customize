import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RePosition } from "./interface";

// Define the initial state using that type
interface IState {
    isReposition: boolean,
    data: RePosition
}
const initialState: IState = {
    isReposition: false,
    data: null

}
export const RePositionSlice = createSlice({
    name: "modalSlice",
    initialState,
    reducers: {
        setReposition: (
            state: IState,
            action: PayloadAction<{
                isReposition: boolean;
                data: RePosition;
            }>
        ) => {
            const { isReposition, data } = action.payload;
            state.isReposition = isReposition;
            state.data = data
        },
        updateDataReposition: (
            state: IState,
            action: PayloadAction<RePosition>
        ) => {

            state.data = action.payload
        },
        closeReposition: (state: IState) => {
            state.isReposition = false
            state.data = null
        }

    },
});

// Action creators are generated for each case reducer function
export const {
    setReposition,
    updateDataReposition,
    closeReposition,
} = RePositionSlice.actions;

export default RePositionSlice.reducer;
