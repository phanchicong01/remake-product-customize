import { ClipArtCategory } from "store/template-reducer/interface";

export interface RePosition {
    clipartItem: ClipArtCategory;
    x: number;
    y: number;
    width: number;
    height: number;
    rotation: number
}