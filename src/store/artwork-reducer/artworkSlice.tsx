import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { IArtwork, typeDisplayOption } from "./interface";

// Define the initial state using that type
const initialState: IArtwork = {
    nameArtwork: "Default Artwork",
    thumbnail: '',
    categoryArtwork: null,
    size: {
        width: 1500,
        height: 1500,
    },
    templates: [],
    setting: {
        template: {
            displayOption: "Radio",
            label: "Default label template"
        },
        background: "",
        scale: { x: 0.33, y: 0.33 },
        showGrid: true,

    },
};
export const artworkSlice = createSlice({
    name: "artworkSlice",
    initialState,
    reducers: {
        updateBackground: (state: IArtwork, action: PayloadAction<string>) => {
            state.setting.background = action.payload;
        },
        updateArtwork: (state: IArtwork, action: PayloadAction<any>) => {
            console.log(action.payload, "action.payload")
            return {
                ...state,
                ...action.payload
            }
        },
        setThumbnail: (state: IArtwork, action: PayloadAction<any>) => {
            state.thumbnail = action.payload;
        },
        onUpdateSettingTemplate: (state: IArtwork, action: PayloadAction<{ displayOption: typeDisplayOption, label: string }>) => {
            state.setting.template.displayOption = action.payload.displayOption
            state.setting.template.label = action.payload.label
        }

    },
});

// Action creators are generated for each case reducer function
export const {
    updateBackground,
    updateArtwork,
    setThumbnail,
    onUpdateSettingTemplate,
} = artworkSlice.actions;

export default artworkSlice.reducer;
