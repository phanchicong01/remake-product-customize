import { Template } from "store/template-reducer/interface"

export interface IArtwork {
    nameArtwork:string,
    thumbnail:string
    categoryArtwork:{
        created_at: Date
        id: number
        name: string
        parent_id: number
        sort: any
        updated_at: Date
    },
    size:{
        width:number,
        height:number
    }
    setting:{
        template:{
            displayOption:typeDisplayOption,
            label:string
        },
        scale:{x:number, y:number},
        background:string
        showGrid:boolean
    },
    templates:Template[]
   
}

export type typeDisplayOption = "Radio" | "Dropdown"
   