import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Layer } from "store/template-reducer/interface";

interface drawerState {
    open: boolean;
    data: Layer;
}

// Define the initial state using that type
const initialState: drawerState = {
    open: false,
    data: null,
};
export const drawerSlice = createSlice({
    name: "drawerSlice",
    initialState,
    reducers: {
        updateDrawer: (state: drawerState, action: PayloadAction<drawerState>) => {
            return (state = { ...action.payload });
        },
    },
});

// Action creators are generated for each case reducer function
export const { updateDrawer } = drawerSlice.actions;

export default drawerSlice.reducer;
