import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "./Home";
import "./App.css";

const App = () => {
    return (
        <Router>
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="admin/fe-artworks/create" element={<Home />} />
                <Route path="admin/fe-artworks/:idArtwork/edit" element={<Home />} />
                <Route path="/:idArtwork/edit" element={<Home />} />
            </Routes>
        </Router>
    );
};

export default App;
