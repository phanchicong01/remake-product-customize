import React, { useState, useEffect } from "react";
import { useAppDispatch, useAppSelector } from "hooks";

import ButtonGroup from "@mui/material/ButtonGroup";
import LoadingButton from "@mui/lab/LoadingButton";
import MenuZoom from "./menu-zoom";
import MenuSetting from "./menu-setting-gird";

import styles from "./styles.module.scss";
import { Grid } from "@mui/material";
import { setModal } from "store/modal-reducer/modalSlice";
import { ModalKey } from "constants/enum";

const BottomBar = () => {
    const { key } = useAppSelector((state) => state.modal);
    const [loadingSaveData, setLoadingSave] = useState<any>(false);
    const dispatch = useAppDispatch();

    const handleSaveData = (data) => {
        setLoadingSave(true);
        dispatch(
            setModal({
                data: true,
                key: ModalKey.SaveData,
            })
        );
    };

    const handleCancel = () => {
        window.location.href = `${process.env.PUBLIC_URL}/admin/artworks`;
    };
    useEffect(() => {
        if (!key && loadingSaveData) {
            setLoadingSave(false)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [key]);
    return (
        <div className={`${styles.bottomBar}`}>
            <Grid container spacing={1} justifyContent={"left"} alignItems="center">
                <Grid item xs={"auto"} textAlign={"left"}>
                    {" "}
                    <MenuZoom />{" "}
                </Grid>
                <Grid item xs={"auto"} textAlign={"left"}>
                    {" "}
                    <MenuSetting />{" "}
                </Grid>

                <Grid item xs={9} textAlign="center" alignItems="center">
                    <ButtonGroup variant="outlined" aria-label="outlined button group">
                        <LoadingButton
                            loading={loadingSaveData}
                            onClick={handleSaveData}
                            size="small"
                            variant="contained"
                        >
                            Save
                        </LoadingButton>
                        <LoadingButton onClick={handleCancel} size="small">
                            Cancel
                        </LoadingButton>
                    </ButtonGroup>
                </Grid>
                {/* )} */}
            </Grid>
        </div>
    );
};

export default BottomBar;
