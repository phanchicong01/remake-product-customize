import { AxiosError } from "axios";
import { callApiFuc } from "server/fetcher";

export const saveDataArtworkService = async (data: any): Promise<any> => {
    const response = await callApiFuc("/api/artworks", "POST", data)
        .then((rs) => {
            return rs.data;
        }).catch((err: any) => console.error(err))
    return response;
};
export const updateDataArtworkService = async (data: any, id: any): Promise<any> => {
    const response = await callApiFuc(`/api/artworks/${id}`, "PUT", data)
        .then((rs) => {
            return rs.data;
        })

    return response;
};
