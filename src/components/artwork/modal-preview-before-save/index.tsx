import { useAppDispatch, useAppSelector } from "hooks";
import { useSelector } from "react-redux";
import { RootState } from "store";
import { closeModal, setModal } from "store/modal-reducer/modalSlice";
import { Box, Button, Modal, Stack } from "@mui/material";
import { ModalKey } from "constants/enum";
import Loading from "components/common/loading";
import cloneDeep from "lodash/cloneDeep"
import { saveDataArtworkService, updateDataArtworkService } from "components/bottom-bar/service";
import { useCallback, useState } from "react";
import styles from "./styles.module.scss"
import { Template } from "store/template-reducer/interface";
import LoadingButton from "@mui/lab/LoadingButton"
import { notification } from "antd";
import { IArtwork } from "store/artwork-reducer/interface";
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { AxiosError } from "axios";

const ModalPreviewBeforeSave = () => {
    const { key, data: dataModal } = useSelector((state: RootState) => state.modal)
    const artwork = useAppSelector((state) => state.artwork);
    const { size, setting, thumbnail } = artwork;
    const templates = useAppSelector((state) => state.template.templates)
    const templateActive = useAppSelector((state) => state.template.templateActive)
    const dispatch = useAppDispatch()
    const params = useParams();
    const navigate = useNavigate();

    const [isLoading, setLoadingSave] = useState(false)

    notification.config({
        maxCount: 3,
    })
    const handleClose = (event: any, reason?: any) => {
        if (reason === "backdropClick") {
            return;
        }
        return dispatch(
            closeModal()
        );
    };
    const handleBeforeSubmit = useCallback(() => {
        const newTemplate = cloneDeep(templates)

        const indexTemplateActive = newTemplate.findIndex((item: Template) => item.id === templateActive.id);
        newTemplate[indexTemplateActive] = templateActive;
        const newData = newTemplate.map((template: Template) => {
            const cloneTemplate = cloneDeep(template)
            delete cloneTemplate.layerSelected;
            return { ...cloneTemplate }
        })
        return newData
    }, [templateActive, templates])

    const handleSubmit = () => {
        setLoadingSave(true)
        const newDataTemplate = handleBeforeSubmit()
        const newArtwork: IArtwork = cloneDeep(artwork);
        newArtwork['templates'] = newDataTemplate
        delete newArtwork.thumbnail;
        const dataRequest = {
            name: newArtwork.nameArtwork,
            content: newArtwork,
            artwork_category_id: newArtwork.categoryArtwork?.id,
            thumbnail: thumbnail,
        }
        callAPISave(dataRequest)
    }
    const callAPISave = (data: {
        name: string,
        content: any,
        artwork_category_id: number,
        thumbnail: string,
    }) => {
        if (!data.name) {
            setLoadingSave(false);
            return notification.error({
                placement: "topRight",
                message: "Some thing went wrong!",
                description: "The field name is require",
            });
        }
        if (!data.artwork_category_id) {
            setLoadingSave(false);
            return notification.error({
                placement: "topRight",
                message: "Some thing went wrong!",
                description: "The field category  is require",
            });
        }

        const formData = new FormData();
        formData.append("name", data.name);
        formData.append("content", JSON.stringify(data.content));
        formData.append("artwork_category_id", data.artwork_category_id + "");
        formData.append("thumbnail", data.thumbnail);
        if (!!params.idArtwork) {
            updateDataArtworkService(data, params.idArtwork)
                .then((res) => {
                    if (!!res) {
                        notification.success({
                            placement: "topRight",
                            message: "Update Artwork!",
                            description: "Create Success",
                        });
                        dispatch(
                            setModal({
                                data: false,
                                key: null,
                            })
                        );
                    }

                    setLoadingSave(false);
                }).catch((err: AxiosError) => {
                    notification.error({
                        placement: "topRight",
                        message: "Some thing was wrong",
                    })
                    setLoadingSave(false);
                    return console.error(err, "err");
                })
        } else {
            saveDataArtworkService(formData)
                .then((res) => {

                    if (res) {
                        setLoadingSave(false);
                        notification.success({
                            placement: "topRight",
                            message: "Create Artwork!",
                            description: "Update Success",
                        });
                        dispatch(
                            setModal({
                                data: false,
                                key: null,
                            })
                        );
                        navigate(`/admin/fe-artworks/${res.id}/edit`);
                    }
                    setLoadingSave(false);
                }).catch((err: AxiosError) => {
                    notification.error({
                        placement: "topRight",
                        message: "Some thing was wrong",
                    })
                    setLoadingSave(false);
                    return console.error(err, "err");
                })

        }
    };

    return <Modal
        open={key === ModalKey.SaveData}
        onClose={handleClose}
        sx={{
            zIndex: 1009
        }}

    >
        <Box sx={style} >
            <h2 id="parent-modal-title">Preview artwork before save </h2>
            {
                !thumbnail && <div className={styles.waitingSaveContent} style={{ width: size.width * setting.scale.x, height: size.height * setting.scale.y }}>
                    <Loading />
                    <div className={styles.backdrop}></div>
                </div>
            }
            {
                !!thumbnail &&
                <div className={styles.waitingSaveContent} style={{ width: size.width * setting.scale.x, height: size.height * setting.scale.y }}>
                    <img src={thumbnail} alt="thumbnail" />
                </div>
            }
            <Stack spacing={2} sx={{ pt: 4, textAlign: "right" }} justifyContent="right" direction="row">
                <LoadingButton loading={isLoading} variant="contained" onClick={handleSubmit}>
                    Confirm
                </LoadingButton>
                <Button variant="outlined" onClick={handleClose}>
                    cancel
                </Button>
            </Stack>
        </Box>
    </Modal >
};

export default ModalPreviewBeforeSave;
const style = {
    position: "absolute" as "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
    // height: 500,
    overflow: "auto",

};
