import React, { useEffect } from "react";

import Divider from "@mui/material/Divider";
import ToggleButton from "@mui/material/ToggleButton";
import ToggleButtonGroup from "@mui/material/ToggleButtonGroup";

import AlignHorizontalCenterIcon from "@mui/icons-material/AlignHorizontalCenter";
import AlignHorizontalLeftIcon from "@mui/icons-material/AlignHorizontalLeft";
import AlignHorizontalRightIcon from "@mui/icons-material/AlignHorizontalRight";
import AlignVerticalBottomIcon from "@mui/icons-material/AlignVerticalBottom";
import AlignVerticalCenterIcon from "@mui/icons-material/AlignVerticalCenter";
import AlignVerticalTopIcon from "@mui/icons-material/AlignVerticalTop";

const toggleData = [
    {
        icon: <AlignHorizontalLeftIcon />,
        value: "left",
        name: "horizontal-left",
    },
    {
        icon: <AlignHorizontalCenterIcon />,
        value: "center",
        name: "horizontal-center",
    },
    {
        icon: <AlignHorizontalRightIcon />,
        value: "right",
        name: "horizontal-right",
    },
];
const toggleData2 = [
    {
        icon: <AlignVerticalTopIcon />,
        value: "top",
        name: "vertical-top",
    },
    {
        icon: <AlignVerticalCenterIcon />,
        value: "middle",
        name: "vertical-center",
    },
    {
        icon: <AlignVerticalBottomIcon />,
        value: "bottom",
        name: "vertical-bottom",
    },
];

const MenuSelection = ({ onChange }: any) => {
    const handleChangeAlign = (event: React.MouseEvent<HTMLElement>, newAlignment: string) => {
        // const newAttrs = { ..., align: newAlignment };

        onChange(newAlignment);
    };

    useEffect(() => {
        // console.log(data.align);
    }, []);

    return (
        <>
            <ToggleButtonGroup size="small" onChange={handleChangeAlign} exclusive={true}>
                {toggleData.map((toggleItem, index: number) => (
                    <ToggleButton value={toggleItem.value} key={toggleItem.value}>
                        {toggleItem.icon}
                    </ToggleButton>
                ))}
            </ToggleButtonGroup>
            <Divider sx={{ mx: 2, background: "#fff" }} orientation="vertical" />
            <ToggleButtonGroup size="small" onChange={handleChangeAlign} exclusive={true}>
                {toggleData2.map((toggleItem, index: number) => (
                    <ToggleButton value={toggleItem.value} key={toggleItem.value}>
                        {toggleItem.icon}
                    </ToggleButton>
                ))}
            </ToggleButtonGroup>
        </>
    );
};

export default MenuSelection;
