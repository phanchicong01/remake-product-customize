import React, { useEffect, useMemo, useState } from "react";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Switch from "@mui/material/Switch";
import FormControlLabel from "@mui/material/FormControlLabel";

import { PersonalizationOption } from "constants/enum";
import { MappingPersonalizationImage } from "constants/const";

import { IConfigurationImage } from "store/template-reducer/interface";
import ItemOptionGroupClipArt from "../item-option-group-clip-art";
import ItemOptionClipArt from "../item-option-clip-art";

type Props = {
    data: IConfigurationImage,
    name: string
}

const ItemOptionImage = ({ data, name }: Props) => {

    const [toggleShow, setToggleShow] = useState(data?.personalization?.toggleShowLayer);

    const handleChangeToggleShow = (event: React.ChangeEvent<HTMLInputElement>) => {
        const checked = event.target.checked;
        setToggleShow(checked);
    };
    useEffect(() => {
        if (data?.personalization?.toggleShowLayer && data?.personalization?.defaultValueToggleShow) {
            setToggleShow(data?.personalization?.defaultValueToggleShow);
        }
    }, [data?.personalization?.defaultValueToggleShow, data?.personalization?.toggleShowLayer]);

    const titlePersonalization = useMemo(() => {
        if (!!data && data[MappingPersonalizationImage[data?.personalization?.option]]?.title) {
            return {
                name: data[MappingPersonalizationImage[data.personalization.option]]?.title,
                require: data[MappingPersonalizationImage[data.personalization.option]]?.require
            }
        }
        return {
            name,
            require: false
        }
    }, [data, name])
    return (
        <Grid container sx={{ py: 2 }}>
            <Grid item xs={12}>
                {<Typography sx={{ fontWeight: "bold", mb: 1 }} component={"h3"}>
                    {titlePersonalization.name} {titlePersonalization.require && <small style={{ color: "red" }}>*</small>}
                </Typography>}
            </Grid>
            {data?.personalization?.toggleShowLayer && (
                <Grid item xs={12}>
                    <FormControlLabel
                        sx={{ width: "100%", textAlign: "left" }}
                        value={toggleShow}
                        onChange={handleChangeToggleShow}
                        checked={toggleShow}
                        control={<Switch />}
                        label={`show (${titlePersonalization.name})`}
                    />
                </Grid>
            )}
            {
                data?.personalization?.option === PersonalizationOption.GroupClipart &&
                <ItemOptionGroupClipArt data={data.groupClipArtSetting} />
            }
            {
                data?.personalization?.option === PersonalizationOption.Clipart &&
                <ItemOptionClipArt data={data.clipArtSetting} />
            }
        </Grid >
    )
}

export default ItemOptionImage