import { useCallback, useEffect, useState } from "react";
import { Typography, Grid } from "@mui/material";

import ItemChecked from "components/common/item-checked";
import { ClipartSetting } from "store/template-reducer/interface";

interface IProps {
    data: ClipartSetting;
    onChange?: () => void;
}
const ItemOptionClipArt = ({ data, onChange }: IProps) => {
    const [idChecked, setIdChecked] = useState<any>(null);

    const handleChangeChecked = useCallback(
        (idChecked) => {
            setIdChecked(idChecked);
        }, [])
    useEffect(() => {
        if (data?.defaultOption) {
            setIdChecked(data.defaultOption?.id)
        } else {
            setIdChecked(null)
        }
    }, [data?.defaultOption])
    return (
        <Grid item xs={12}>
            {Array.isArray(data?.clipArtCategory?.items) && data?.clipArtCategory?.items.length > 0 && (
                <ItemChecked
                    max_width={data?.clipArtCategory?.max_width}
                    data={data?.clipArtCategory?.items}
                    value={idChecked}
                    onChange={handleChangeChecked}
                    id={`preview_clip_art`}
                />
            )}

            {!Array.isArray(data?.clipArtCategory?.items) || data?.clipArtCategory?.items?.length < 0 && (
                <Typography color={"gray"} component={"span"} >
                    Empty items
                </Typography>
            )}
        </Grid>
    );
};

export default ItemOptionClipArt;
