import React, { useCallback, useEffect, useState } from "react";

import Grid from "@mui/material/Grid";

import ItemChecked from "components/common/item-checked";
import { ClipArtCategory, GroupClipArtSetting } from "store/template-reducer/interface";
import { MenuItem, Select, SelectChangeEvent, Typography } from "@mui/material";

// const { Option } = Select;
interface IProps {
    data: GroupClipArtSetting;
    onChange?: () => void;
}
const ItemOptionGroupClipArt = ({ data, onChange }: IProps) => {
    const [dataSelect, setDataSelect] = useState<any>(null);
    const [idChecked, setIdChecked] = useState<any>(null);
    const handleChange = useCallback((event: SelectChangeEvent<number>) => {
        const {
            target: { value },
        } = event;
        const findItem = data?.clipArtCategory?.children?.find((item) => item.id === value);
        if (!!findItem) {
            setDataSelect(findItem);
        }
        // firstChange.current = true;
    }, [data?.clipArtCategory?.children])


    const handleChangeChecked = (idChecked: any) => {
        setIdChecked(idChecked);
    };
    useEffect(() => {
        if (data?.defaultOptionParent) {
            setDataSelect(data.defaultOptionParent)
            if (data?.defaultOption) {
                setIdChecked(data?.defaultOption?.id)
            } else {
                setIdChecked(null)
            }
        }
        if (!data?.defaultOptionParent) {
            setDataSelect(null);
            if (!data?.defaultOption) {
                setIdChecked(null)
            }
        }
    }, [data?.defaultOption, data?.defaultOptionParent, data?.clipArtCategory])
    if (!data) {
        return null
    }

    return (
        <Grid item xs={12}>
            <Select
                value={dataSelect?.id || "placeholder"}
                placeholder="Select an option"
                fullWidth
                size="small"
                sx={{ background: "#fff", color: "#222", marginBottom: "10px" }}
                onChange={handleChange}
            >
                {
                    !dataSelect && !!data?.placeholder &&
                    <MenuItem disabled value="placeholder">
                        {data.placeholder}
                    </MenuItem>
                }
                {
                    !data?.clipArtCategory &&
                    <MenuItem disabled value="empty">
                        choose something
                    </MenuItem>
                }
                {data?.clipArtCategory?.children?.map((child: ClipArtCategory, index: number) => {
                    return (
                        <MenuItem key={child.id} value={child.id}>
                            {child.name}
                        </MenuItem>

                    );
                })}
            </Select>
            {Array.isArray(dataSelect?.items) && dataSelect?.items.length > 0 && (
                <ItemChecked
                    max_width={dataSelect?.max_width}
                    data={dataSelect?.items}
                    value={idChecked}
                    onChange={handleChangeChecked}
                    id={`preview_group_clip_art`}
                />
            )}

            {(!Array.isArray(dataSelect?.items) || dataSelect?.items?.length <= 0) && (
                <Typography sx={{ padding: 1, marginTop: 15 }} color={"gray"} component={"span"} >
                    Empty items
                </Typography>
            )}
        </Grid>
    );
};

export default ItemOptionGroupClipArt;
