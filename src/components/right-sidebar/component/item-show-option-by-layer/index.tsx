import { Grid } from '@mui/material'
import { TypeLayer } from 'constants/enum'
import { Layer } from 'store/template-reducer/interface'
import ItemOptionImage from '../item-option-image'
import ItemOptionText from '../item-option-text'

type Props = {
    layer: Layer
}

const ItemShowOptionByLayer = ({ layer }: Props) => {
    return (
        <Grid container>
            {layer.type === TypeLayer.Text &&
                <Grid item xs={12}>
                    <ItemOptionText data={layer?.configuration} name={layer.name} />
                </Grid>
            }
            {layer.type === TypeLayer.Image &&
                <Grid item xs={12}>
                    <ItemOptionImage data={layer?.configuration} name={layer.name} />
                </Grid>
            }

        </Grid>
    )
}

export default ItemShowOptionByLayer