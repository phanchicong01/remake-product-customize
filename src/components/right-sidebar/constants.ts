import { PersonalizationOption } from "constants/enum"
import { Layer } from "store/template-reducer/interface"

export const checkShowingConfig = (layer: Layer) => {
    if (layer?.isActiveConfig && !!layer?.configuration?.personalization) {
        const personalization = layer?.configuration?.personalization
        if (personalization?.toggleShowLayer || personalization.option === PersonalizationOption.EnablePersonalization) {
            return true
        }
        if (personalization?.option === PersonalizationOption.None) {
            return false
        }
        return true
    }
    return false
}
