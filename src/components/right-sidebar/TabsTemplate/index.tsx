import { Grid } from '@mui/material'
import ItemInlineText from 'components/common/item-inline-text'
import { TypeDisplayOptionTempLate } from 'constants/enum'
import { useAppSelector } from 'hooks'
import React, { useCallback } from 'react'
import { useDispatch } from 'react-redux'
import { Template } from 'store/template-reducer/interface'
import { onActiveTemplate } from 'store/template-reducer/templateSlice'
import Select from "antd/lib/select";
type Props = {}

const TabsTemplate = (props: Props) => {
    const { templates, templateActive } = useAppSelector(state => state.template)
    const { setting } = useAppSelector(state => state.artwork)
    const dispatch = useDispatch()
    const handleClickTab = useCallback((checked, data: Template) => {
        dispatch(onActiveTemplate(data))
    }, [dispatch])
    const handleChangeSelect = useCallback((value) => {
        const findLayer = templates.find((template: Template) => template.id === value);
        dispatch(onActiveTemplate(findLayer))
    }, [dispatch, templates])
    if (templates?.length === 1) {
        return null
    }
    console.log(setting?.template?.displayOption === TypeDisplayOptionTempLate.Radio)
    return (
        <div>
            <p>{setting?.template.label}</p>
            {setting?.template?.displayOption === TypeDisplayOptionTempLate.Radio &&
                <ItemInlineText
                    isShowHover={true}
                    data={templates}
                    onChecked={handleClickTab}
                    filedNames={{
                        name: "name",
                        value: "id",
                        id: "id"
                    }}
                    minWidth={120}
                    value={templateActive.id}
                />
            }
            {
                setting?.template?.displayOption === TypeDisplayOptionTempLate.Dropdown &&
                <Grid item xs={12}>
                    <Select
                        fieldNames={{ label: "name", value: "id" }}
                        options={templates}
                        style={{ width: "100%" }}
                        value={templateActive.id}
                        onChange={handleChangeSelect}
                    />
                </Grid>
            }
        </div>
    )
}

export default TabsTemplate