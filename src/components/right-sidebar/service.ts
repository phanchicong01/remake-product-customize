import { callApiFuc } from "server/fetcher";

export const getDetailCategory = async (id: any): Promise<any> => {
    const response = await callApiFuc(`/api/clipart-categories/${id}`)
        .then((rs) => {
            return rs.data.data;
        })
        .catch((err) => console.error(err));
    return response;
};
