
import Box from "@mui/material/Box";
import { useAppSelector } from "hooks";

import TabsTemplate from "../TabsTemplate";
import { Layer } from "store/template-reducer/interface";
import { PersonalizationOption } from "constants/enum";

import ItemShowOptionByLayer from "../component/item-show-option-by-layer";
import styles from "./styles.module.scss";

const ContentPreview = () => {
    const { layers } = useAppSelector(state => state.template.templateActive)

    const checkShowingConfig = (layer: Layer) => {
        if (layer?.isActiveConfig && !!layer?.configuration?.personalization) {
            const personalization = layer?.configuration?.personalization
            if (personalization?.toggleShowLayer || personalization.option === PersonalizationOption.EnablePersonalization) {
                return true
            }
            if (personalization?.option === PersonalizationOption.None) {
                return false
            }
            return true
        }
        return false
    }
    const isShowConfig = layers.some((layer: any) => {
        return checkShowingConfig(layer)
    })
    return <Box
        sx={{
            backgroundColor: "#f2f2f2",
            border: "1px solid #f4f4f4",
            p: 1,
            mt: 3,
            textAlign: "left",
        }}
    >

        <TabsTemplate />
        {
            layers?.map((layer: Layer) => {
                if (!checkShowingConfig(layer)) {
                    return <div key={layer.id}></div>
                }
                return <div key={layer.id}>
                    <ItemShowOptionByLayer layer={layer} />
                </div>
            })
        }
        {
            !isShowConfig && <span className={`p-3 ${styles.textEmpty}`}>
                No options to preview yet.
            </span>
        }

    </Box>
};

export default ContentPreview;
