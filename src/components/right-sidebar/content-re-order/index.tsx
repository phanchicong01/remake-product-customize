import React, { useCallback, useMemo } from "react";

import Grid from "@mui/material/Grid";
import List from "@mui/material/List";
import ListItemText from "@mui/material/ListItemText";
import ListItemButton from "@mui/material/ListItemButton";

import { useAppSelector, useAppDispatch } from "hooks";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";

import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";

import DragIndicatorIcon from "@mui/icons-material/DragIndicator";
import { ListItemIcon } from "@mui/material";
import ItemInlineText from "components/common/item-inline-text";

import { styled } from "@mui/styles";
import styles from "./styles.module.scss";
import TabsTemplate from "../TabsTemplate";
import { Layer, LayerCommon } from "store/template-reducer/interface";
import { checkShowingConfig } from "../constants";
import { MappingPersonalizationImage } from "constants/const";
import { TypeLayer } from "constants/enum";
import sortBy from "lodash/sortBy"
import cloneDeep from "lodash/cloneDeep"
import { onReOrderLayerConfig } from "store/template-reducer/templateSlice"

const ContentReOrder = ({ data }: any) => {
    const { layers, layerSelected } = useAppSelector((state) => state.template.templateActive);
    const dispatch = useAppDispatch();


    const layerActiveConfig: Layer[] = useMemo(() => {
        // setLayerActiveConfig(layers.filter((layer) => (checkShowingConfig(layer))))
        const result = layers.map((layer: Layer, index: number) => {
            const newLayer = { ...layer }
            if (checkShowingConfig(layer) && layer.configuration.reOrder === null) {
                const newLayer = cloneDeep(layer)
                newLayer.configuration['reOrder'] = index + 1
                return { ...newLayer }
            }
            if (checkShowingConfig(newLayer)) {
                return newLayer
            }
            return null
        }).filter(layer => !!layer)
        return sortBy(result, o => o.configuration.reOrder)

    }, [layers])

    console.log(layerActiveConfig, "sss")
    const onDragEnd = useCallback((result) => {
        if (!result.destination) {
            return;
        }
        const items: Layer[] = cloneDeep(layerActiveConfig);
        console.log(items)
        const sourceItem = items[result.source.index]
        console.log(items, "2")
        const destinationItem = items[result.destination.index];
        console.log(sourceItem, destinationItem, result)
        sourceItem.configuration.reOrder = result.destination.index + 1;
        destinationItem.configuration.reOrder = result.source.index + 1
        dispatch(onReOrderLayerConfig({ destinationItem, sourceItem }))

    }, [dispatch, layerActiveConfig]);

    const getTitlePersonalization = useCallback((layer: Layer) => {
        if (layer.type === TypeLayer.Text) {
            return layer.configuration?.personalization?.title || ""
        } else if (layer.type === TypeLayer.Image) {
            const configuration = layer.configuration;
            const personalization = layer.configuration.personalization;
            if (!!configuration && configuration[MappingPersonalizationImage[personalization?.option]]?.title) {
                return configuration[MappingPersonalizationImage[personalization.option]]?.title
            }
        }
        return ''

    }, [])

    return (
        <>
            <Grid container rowGap={2} sx={{ p: 1 }}>
                <TabsTemplate />
            </Grid>
            <DragDropContext onDragEnd={(event) => onDragEnd(event)}>
                <Droppable droppableId="droppable-preview">
                    {(provided, snapshot) => (
                        <CustomList
                            {...provided.droppableProps}
                            ref={provided.innerRef}
                            sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}
                            component="nav"
                            aria-labelledby="nested-list-subheader"
                        >
                            {layerActiveConfig?.map((layer: Layer, index: number) => {
                                return (
                                    <Draggable key={layer.id} draggableId={`${layer.id}`} index={index}>
                                        {(provided, snapshot) => (
                                            <ListItemButton
                                                ref={provided.innerRef}
                                                className={styles.customListItem}
                                                {...provided.draggableProps}
                                                {...provided.dragHandleProps}
                                                key={layer.id}
                                            >
                                                <ListItemText
                                                    primary={getTitlePersonalization(layer)}
                                                    secondary={`${layer.name}`}
                                                />

                                                <ListItemIcon>
                                                    <DragIndicatorIcon />
                                                </ListItemIcon>
                                            </ListItemButton>
                                        )}
                                    </Draggable>
                                );
                            })}
                        </CustomList>
                    )}
                </Droppable>
            </DragDropContext>
        </>
    );
};

export default ContentReOrder;
const CustomList = styled(List)<{ component?: React.ElementType }>({
    "& .MuiListItemButton-root": {
        backgroundColor: "#f2f2f2",
        border: "1px solid #d3d3d3",
    },
    "& .MuiListItemIcon-root": {
        minWidth: 0,
        marginRight: 16,
    },
    "& .MuiSvgIcon-root": {
        fontSize: 20,
    },
});
