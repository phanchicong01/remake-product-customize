import { useAppDispatch, useAppSelector } from "hooks";
import { useEffect, useMemo, useRef, useState } from "react";
import { Stage, Transformer } from "react-konva";
import LayerComponent from "components/Layer";

import { onUpdateAttrLayer, onSelectLayer, onSelectMultipleLayer } from "store/template-reducer/templateSlice"
import GridView from "components/common/gird-view";

import Konva from "konva";
import SelectionRectangle from "components/common/SelectionRectangle";
import { Layer } from "store/template-reducer/interface"

import styles from "./styles.module.scss";

import { closeReposition, updateDataReposition } from "store/re-position-reducer/RePositionSlice";
import ModalPreviewBeforeSave from "./modal-preview-before-save";
import { ModalKey } from "constants/enum";
import { setThumbnail } from "store/artwork-reducer/artworkSlice";

interface IProps {
    // stageConfig?: StageProps;
    children?: any;
    // idParent: string;
}
let x1, y1, x2, y2;
const initSelectionRectangle = {
    x: 0,
    y: 0,
    width: 0,
    height: 0,
    visible: false,
    selectedNodes: [],
    event: null
};
interface ISelectionRectangle {
    visible: boolean;
    width: number;
    height: number;
    x: number;
    y: number;
    selectedNodes: Array<any>;
    event: "mouseDown" | "mouseMove" | "mouseUp" | ""
}
type currentEvent = "mouseDown" | "mouseMove" | "mouseUp" | ""
const ID_STAGE_AREA = "stage-area";
const ID_STAGE_CONTAINER = "stage-container";

const ArtworkComponent = (props: IProps) => {
    const artwork = useAppSelector((state) => state.artwork);
    const { key } = useAppSelector((state) => state.modal);
    const templateActive = useAppSelector((state) => state.template.templateActive);
    const { data:dataReposition, isReposition } = useAppSelector((state) => state.rePosition)

    const dispatch = useAppDispatch();
    const [selectionRectangle, setSelectedRectangle] = useState<ISelectionRectangle>(initSelectionRectangle);
    
    const [positionStage, setPositionStage] = useState<{ top: number; left: number }>({ top: 0, left: 0 });


    const currentTemplateActive = templateActive

    const currentEvent = useRef<currentEvent>("")
    const trRef = useRef<any>(null);
    const refStage = useRef<any>(null);
    const selectionRectRef = useRef<any>(null);

    const handleUpdateAttrs = (payload: { newAttrs: any; }) => {
        return dispatch(onUpdateAttrLayer(payload));
    };
    const handelSelectRectangle = (e, data) => {
        // do nothing if clicked NOT on our rectangles

        // if (!e.target.hasName("shape") && currentTemplateActive?.layerSelected?.length > 2) {
        //     return;
        // }
        // if (!e.target.visible) {
        //     return;
        // }
        //  else
        if (!!data) {
            if (isReposition) {
                dispatch(closeReposition())
            }
            return dispatch(onSelectLayer({ ...data }));
        }
    };

    const handleOnMouseDown = (e) => {
        if (isReposition) {
            return;
        }
        const stage = refStage.current;

        // deselect when clicked on empty area


        // if (clickedOnEmpty) {
        //     dispatch(onSelect({ status: false, idLayer: layerActive.id, idObj: null }));
        // }
        // do nothing if we mousedown on any shape
        if (e.target !== e.target.getStage()) {
            return;
        }
        e.evt.preventDefault();
        x1 = stage.getPointerPosition().x;
        y1 = stage.getPointerPosition().y;
        x2 = stage.getPointerPosition().x;
        y2 = stage.getPointerPosition().y;
        currentEvent.current = "mouseDown"
        setSelectedRectangle((prev) => ({ ...prev, visible: true, width: 0, height: 0, x1, y1, x2, y2, event: "mouseDown" }));
    };
    
    const handleOnMouseMove = (e) => {
        if (currentEvent.current !== "mouseDown") {
            return;
        }
        const stage = refStage.current;
        // do nothing if we didn't start selection
        if (!selectionRectangle.visible) {
            return;
        }
        e.evt.preventDefault();
        x2 = stage.getPointerPosition().x / stage.scaleX();
        y2 = stage.getPointerPosition().y / stage.scaleY();

        const x1State = x1 / stage.scaleX();
        const y1Stage = y1 / stage.scaleY();
        setSelectedRectangle((prev) => ({
            ...prev,
            x: Math.min(x1State, x2),
            y: Math.min(y1Stage, y2),
            width: Math.abs(x2 - x1State),
            height: Math.abs(y2 - y1Stage),
            // visible: true
        }));

    };

    const handleMouseup = (e) => {
        if (currentEvent.current !== "mouseDown") {
            return;
        }
        // do nothing if we didn't start selection
        if (!selectionRectangle.visible) {
            return;
        }
        e.evt.preventDefault();
        // update visibility in timeout, so we can check it in click event
        setTimeout(() => {
            setSelectedRectangle((prev) => ({ ...prev, visible: false }));

        });

        // // var shapesImage = refStage.current.find(".image");
        // return setSelectedNode(selected);
        currentEvent.current = "mouseUp"
        // setSelectedRectangle((prev) => ({ ...prev, visible: false }));
        var shapes = refStage.current?.find(".shape");
        var box = selectionRectRef.current?.getClientRect();
        var selected = shapes?.filter((shape) => Konva.Util.haveIntersection(box, shape.getClientRect()));
        const multipleLayerSelected = currentTemplateActive.layers.filter(layer => selected.some(node => node.attrs.id === layer.id))
        multipleLayerSelected.length > 0 && dispatch(onSelectMultipleLayer({ status: true, multipleLayerSelected }));


    };

    const handleBatchRawSelectRectangle = (event: currentEvent) => {
        if (event === "mouseDown") return;
        // if (selectionRectangle.visible) return;
        var shapes = refStage.current?.find(".shape");
        var box = selectionRectRef.current?.getClientRect();
        var selected = shapes?.filter((shape) => Konva.Util.haveIntersection(box, shape.getClientRect()));
        // setSelectedNode(selected)
        let filterNodesIsExits = selected
        if (!event || event !== "mouseUp") {
            filterNodesIsExits = selected.filter((node) => {
                return currentTemplateActive.layerSelected.some((obj) => obj.id === node.attrs.id);
            });

        } else {
            currentEvent.current = "";
            // const multipleLayerSelected =  currentTemplateActive.layers.filter(layer => selected.some(node => node.attrs.id === layer.id) )
            // dispatch(onSelectMultipleLayer({status:true , multipleLayerSelected}))
        }

        // we need to attach transformer manually
        // trRef.current?.stopTransform()
        trRef.current?.nodes(filterNodesIsExits);
        trRef.current?.getLayer().batchDraw();
        trRef.current?.setAttrs({
            // zIndex: currentTemplateActive.layers.length + 1,
            visible: true,
        });
        // trRef.current?.forceUpdate();

    }
   
    useMemo(() => {
        if (key === ModalKey.SaveData && refStage.current) {
            trRef?.current.setAttrs({
                visible: false,
            });
            dispatch(setThumbnail(refStage.current.toDataURL()));

            const timeOut = setTimeout(() => {
                trRef?.current.setAttrs({
                    visible: true,
                });
                clearTimeout(timeOut)
            }, 100)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [refStage.current, key]);

    useEffect(() => {
        handleBatchRawSelectRectangle(currentEvent.current)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selectionRectangle])

    useMemo(() => {

        // raw rectangle selection multiple layer
        let minX = 0;
        let minY = 0;
        let maxX = 0;
        let maxY = 0;
        if (currentTemplateActive?.layerSelected?.length > 1) {
            minX = artwork.size.width;
            minY = artwork.size.height;
            maxX = 0;
            maxY = 0;
            currentTemplateActive.layerSelected.forEach((layer: Layer) => {
                if (layer.x < minX) {
                    minX = layer.x;
                }
                if ((layer.x + layer.width) > maxX) {
                    maxX = layer.x + layer.width
                }
                if ((layer.y + layer.height) > maxY) {
                    maxY = layer.y + layer.height
                }
                if (layer.y < minY) {
                    minY = layer.y
                }
            })

        }
        if (currentTemplateActive?.layerSelected?.length === 1) {
            const layerSelected: Layer = currentTemplateActive.layerSelected[0]
            minX = layerSelected.x;
            minY = layerSelected.y;
            maxX = layerSelected.x + layerSelected.width;
            maxY = layerSelected.y + layerSelected.height;
        }
        setSelectedRectangle((prev) => ({
            ...prev,
            x1: minX, y1: minY, x2: maxX, y2: maxY,
            x: minX,
            y: minY,
            width: Math.abs(maxX - minX),
            height: Math.abs(maxY - minY),
            // visible: false
        }));


        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [currentTemplateActive?.layerSelected])

    useEffect(() => {
        // trRef.current?.stopTransform()

        // trRef.current?.forceUpdate();
        // if (trRef.current?.visible) {
        // trRef.current?.setAttrs({
        //     visible: false,
        // });
        handleBatchRawSelectRectangle(currentEvent.current)
        // }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [currentTemplateActive.layers])


    const handleChangeReposition = (newData: any) => {
        dispatch(updateDataReposition(newData))
    }




    const widthStage = artwork.size.width * artwork.setting.scale.x;
    const heightStage = artwork.size.height * artwork.setting.scale.y;

    useEffect(() => {
        var container: any = document.getElementById(ID_STAGE_CONTAINER);
        var containerWidth = container?.offsetWidth;
        var containerHeight = container.offsetHeight;

        const topObject = containerHeight / 2 - heightStage / 2;
        const leftObject = containerWidth / 2 - widthStage / 2;

        setPositionStage({
            top: topObject < 0 ? 0 : topObject,
            left: leftObject < 0 ? 0 : leftObject,
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [artwork.setting.scale]);
    useEffect(() => {
        if (!!isReposition) {
            trRef.current?.setAttrs({
                visible: false,
            })
        } else if (currentTemplateActive.layerSelected.length > 0) {
            trRef.current?.setAttrs({
                visible: true,
            })
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isReposition])



    const refContainer = useRef(null);
    return (
        <div id={ID_STAGE_AREA} className={styles.stageArea} >

            <div className={styles.containerStage} id={ID_STAGE_CONTAINER} ref={refContainer}>
                {artwork.setting.showGrid && (
                    <div
                        className={styles.gridView}
                        style={{
                            width: widthStage,
                            height: heightStage,
                            top: `${positionStage.top}px`,
                        }}
                    >
                        <GridView
                            width={artwork.size.width * artwork.setting.scale.x}
                            height={artwork.size.height * artwork.setting.scale.y}
                        />
                    </div>
                )}

                <Stage
                    width={artwork.size.width * artwork.setting.scale.x}
                    height={artwork.size.height * artwork.setting.scale.y}
                    scale={artwork.setting.scale}
                    ref={refStage}
                    onMouseDown={handleOnMouseDown}
                    onMouseMove={handleOnMouseMove}
                    onMouseUp={handleMouseup}
                    onTap={(e) => handelSelectRectangle(e, null)}
                    onClick={(e) => handelSelectRectangle(e, null)}
                    className={styles.stage}
                    style={{
                        width: artwork.size.width * artwork.setting.scale.x,
                        height: artwork.size.height * artwork.setting.scale.y,

                        top: `${positionStage.top}px`,
                        position: "relative",
                    }}
                // {...stageConfig}
                >
                    {currentTemplateActive && Array.isArray(currentTemplateActive.layers) && (
                        <LayerComponent
                            layers={currentTemplateActive?.layers}
                            layerSelected={currentTemplateActive?.layerSelected}
                            onSelect={handelSelectRectangle}
                            onUpdateAttribute={(data) => handleUpdateAttrs(data)}
                            stageScale={artwork.setting.scale}
                            reposition={dataReposition}
                            isReposition={isReposition}
                            onChangeRepo={handleChangeReposition}
                        >
                            <SelectionRectangle
                                key={"SelectionRectangle"}
                                ref={selectionRectRef}
                                visible={selectionRectangle.visible}
                                width={selectionRectangle.width}
                                height={selectionRectangle.height}
                                fill="rgba(0,0,255,0.5)"
                                name="selected"
                                x={selectionRectangle.x}
                                y={selectionRectangle.y}
                                // selectedNodes={selectedNode}
                                align={selectionRectangle["align"]}
                            ></SelectionRectangle>

                            <Transformer
                                key={'Transformer'}
                                ref={trRef}
                                rotateEnabled={true}
                                name="transformer"
                                keepRatio={true}
                                shouldOverdrawWholeArea
                                enabledAnchors={["top-left", "top-right", "bottom-left", "bottom-right"]}
                                boundBoxFunc={(oldBox, newBox) => {
                                    // limit resize
                                    if (newBox.width < 5 || newBox.height < 5) {
                                        return oldBox;
                                    }
                                    return newBox;
                                }}
                            />



                        </LayerComponent>
                    )}
                </Stage>
            </div>
            <ModalPreviewBeforeSave />

        </div>
    );
};

export default ArtworkComponent;
