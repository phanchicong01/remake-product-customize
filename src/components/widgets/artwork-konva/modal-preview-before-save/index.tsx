import { useAppDispatch, useAppSelector } from "hooks";
import { useSelector } from "react-redux";
import { RootState } from "store";
import {
    closeModal,
    //  setModal 
} from "store/modal-reducer/modalSlice";
import {  Button, Modal, Stack } from "@mui/material";
import { ModalKey } from "constants/enum";
import cloneDeep from "lodash/cloneDeep"
import { useCallback } from "react";
import styles from "./styles.module.scss"
import { Template } from "store/template-reducer/interface";
import LoadingButton from "@mui/lab/LoadingButton"
import { IArtwork } from "store/artwork-reducer/interface";
// import { useParams } from "react-router-dom";
// import { useNavigate } from "react-router-dom";
// import { AxiosError } from "axios";
import Loading from "components/common/loading";
import { useMutation } from "react-query";
import createArtwork from "apis/create-artwork";
import { CreateArtworkRequest } from "apis/create-artwork/inteface";
import { toast } from "react-toastify";
import { useNavigate, useParams } from "react-router-dom";
import { UpdateArtworkRequest } from "apis/update-artwork/interface";
import updateArtwork from "apis/update-artwork";
import {ARTWORKS_EDIT } from "constants/path"

const ModalPreviewBeforeSave = () => {
    const { key } = useSelector((state: RootState) => state.modal)
    const artwork = useAppSelector((state) => state.artwork);
    const { size, thumbnail } = artwork;
    const templates = useAppSelector((state) => state.template.templates)
    const templateActive = useAppSelector((state) => state.template.templateActive)
    const dispatch = useAppDispatch()
    const params = useParams();
    const navigate = useNavigate();

    const useMutationCreate = useMutation((payload: CreateArtworkRequest) => createArtwork(payload))
    const useMutationUpdate = useMutation((payload: UpdateArtworkRequest) => updateArtwork(payload))

  
    const handleClose = (event: any, reason?: any) => {
        if (reason === "backdropClick") {
            return;
        }
        return dispatch(
            closeModal()
        );
    };
    const handleBeforeSubmit = useCallback(() => {
        const newTemplate = cloneDeep(templates)

        const indexTemplateActive = newTemplate.findIndex((item: Template) => item.id === templateActive.id);
        newTemplate[indexTemplateActive] = templateActive;
        const newData = newTemplate.map((template: Template) => {
            const cloneTemplate = cloneDeep(template)
            delete cloneTemplate.layerSelected;
            return { ...cloneTemplate }
        })
        return newData
    }, [templateActive, templates])

    const handleSubmit = async () => {
        const newDataTemplate = handleBeforeSubmit()
        const newArtwork: IArtwork = cloneDeep(artwork);
        newArtwork['templates'] = newDataTemplate
        delete newArtwork.thumbnail;

        const dataRequest: CreateArtworkRequest = {
            name: newArtwork.nameArtwork,
            content: JSON.stringify(newArtwork),
            artwork_category_id: newArtwork.categoryArtwork?.id,
            thumbnail: thumbnail,
        }
        if (!dataRequest.name) {
            return toast.error("Name is require")
        }
        if (!dataRequest.artwork_category_id) {
            return toast.error("Categories is require")
        }
        try {
            if(!params.idArtwork){
                const result = await useMutationCreate.mutateAsync(dataRequest)
                if (result.code === 200) {
                   toast.success("Create Artwork!");
                   navigate(ARTWORKS_EDIT +"/"+ result.data?.id)
                   dispatch(closeModal());
                }else{
                    toast.error(result.message)
                }
            }else{
                const dataUpdate:UpdateArtworkRequest = {
                    id:parseInt(params.idArtwork),
                    data:dataRequest
                }
                const result = await useMutationUpdate.mutateAsync(dataUpdate)
                if (result.code === 200) {
                   toast.success("Update Artwork!");
                   dispatch(closeModal());
                }else{
                    toast.error(result.message)
                }
            }
            
        } catch (error) {
            
        }
        
    }

    
    const isLoading =  useMutationCreate.isLoading || useMutationUpdate.isLoading
    return <Modal
        open={key === ModalKey.SaveData}
        onClose={handleClose}
        sx={{
            zIndex: 1009,
            maxHeight: "100vh",
            overflow: "auto"
        }}

    >
        <div className={styles.modalContent}>
            <h2 id="parent-modal-title">Preview artwork before save </h2>
            {
                !thumbnail && <div className={styles.waitingSaveContent} style={{ width: size.width * .33, height: size.height * .33 }}>
                    <Loading />
                    <div className={styles.backdrop}></div>
                </div>
            }
            {
                !!thumbnail &&
                <div className={styles.waitingSaveContent} style={{ width: size.width * .33, height: size.height * .33 }}>
                    <img src={thumbnail} alt="thumbnail" style={{ width: size.width * .33, height: size.height * .33 }} />
                </div>
            }
            <Stack spacing={2} sx={{ pt: 4, textAlign: "right" }} justifyContent="right" direction="row">
                <LoadingButton loading={isLoading} variant="contained" onClick={handleSubmit}>
                    Confirm
                </LoadingButton>
                <Button variant="outlined" onClick={handleClose}>
                    cancel
                </Button>
            </Stack>
        </div>
    </Modal >
};

export default ModalPreviewBeforeSave;
