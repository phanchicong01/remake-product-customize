import { callApiFuc } from "utils/fetcher";

export const getTreeClipArtCategories = async (): Promise<any> => {
    const response = await callApiFuc("/api/tree-clipart-categories")
        .then((rs) => {
            return rs.data.data;
        })
        .catch((err) => console.error(err));
    return response;
};
export const getAllClipArtCategories = async (): Promise<any> => {
    const response = await callApiFuc("/api/clipart-categories")
        .then((rs) => {
            return rs.data.data;
        })
        .catch((err) => console.error(err));
    return response;
};
