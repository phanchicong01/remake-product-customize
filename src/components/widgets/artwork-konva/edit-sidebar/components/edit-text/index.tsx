import Grid from "@mui/material/Grid";
import FormatTextdirectionRToLIcon from '@mui/icons-material/FormatTextdirectionRToL';
import ColorLensIcon from '@mui/icons-material/ColorLens';
import VisibilityIcon from '@mui/icons-material/Visibility';
import ElectricalServicesIcon from '@mui/icons-material/ElectricalServices';

import { useDispatch } from "react-redux";
import { onUpdateAttrConfig, onUpdateAttrLayer } from "store/template-reducer/templateSlice";
import { LayerCommon, LayerText } from "store/template-reducer/interface";

import CollapseComponent from "../common/collapse-component"
import ConditionSetting from "../common/condition-setting";
import PersonalizationSetting from "./personalization-setting";
import TextConfig from "./text-config";
import InputCustom from "components/common/InputCustom";

type IData = LayerCommon & LayerText
interface IProps {
  data: IData;
}


const EditText = ({ data }: IProps) => {
  const dispatch = useDispatch()

  const handleChangeTextConfig = (newAttrs, nameAttrs: string) => {
    dispatch(onUpdateAttrConfig({ newAttrs, nameAttrs }));

  };

  const handleChangePersonalization = (newAttrs: any) => {
    const newData = {
      ...data,
      configuration: {
        ...data?.configuration,
        personalization: {
          ...data?.configuration?.personalization,
          ...newAttrs
        }
      }
    }
    dispatch(onUpdateAttrLayer({ newAttrs: newData }))
  }

  const handleChangeExtra = (name: string, value: string | number) => {

    dispatch(onUpdateAttrLayer({
      newAttrs: {
        ...data,
        configuration: {
          ...data?.configuration,
          extra: {
            ...data?.configuration?.extra,
            [name]: value
          }
        }
      }
    }))
  }

  return (
    <Grid container >
      <Grid item xs={12}>
        <CollapseComponent title={"GENERAL"} icon={<FormatTextdirectionRToLIcon fontSize="small"/>}>
          <TextConfig data={data} />
        </CollapseComponent>
        <CollapseComponent title={"PERSONALIZATION SETTING"} icon={<ColorLensIcon fontSize="small" />}>
          <PersonalizationSetting data={data?.configuration?.personalization} onChangeAttr={handleChangePersonalization} />
        </CollapseComponent>
        <CollapseComponent title={"CONDITIONAL SETTING"} icon={<VisibilityIcon fontSize="small"/>}>
          <ConditionSetting data={data?.configuration?.conditionalSetting} onChangeAttr={(newAttrs) => handleChangeTextConfig(newAttrs, "conditionalSetting")} />
        </CollapseComponent>
        <CollapseComponent title={"EXTRA"} icon={<ElectricalServicesIcon fontSize="small"/>}>
          <div style={{ padding: " 0 15px" }}>
            <InputCustom
              colLabel={12}
              label="Custom class:"
              onChangeValue={handleChangeExtra}
              value={data?.configuration?.extra}
            />
          </div>
        </CollapseComponent>
      </Grid>
    </Grid>
  );
};

export default EditText;
