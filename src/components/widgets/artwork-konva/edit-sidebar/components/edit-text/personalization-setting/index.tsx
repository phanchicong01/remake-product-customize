import React, {  useEffect,  useState } from 'react'
import Grid from "@mui/material/Grid";
import Switch from "@mui/material/Switch";
import FormControlLabel from "@mui/material/FormControlLabel";
import InputCustom from "components/common/InputCustom";
import { IPersonalizationText } from 'store/template-reducer/interface';
import { FormControl, FormLabel, Radio, RadioGroup } from '@mui/material';
import isEqual from "lodash/isEqual"
import debounce from "lodash/debounce"

type Props = {
    onChangeAttr: (data) => void;
    data: IPersonalizationText
}
const dataSwitch = [

    {
        name: "toggleShowLayer",
        label: "Add toggle to show/hide this layer",
        value: "toggleShowLayer",
    },
    {
        name: "defaultValueToggleShow",
        label: "Pre-enable toggle (show layer by default)",
        value: "defaultValueToggleShow",
    },
];
const options = [
    {
        name: "option",
        label: "No personalization",
        value: "none",
    },
    {
        name: "option",
        label: "Enable personalization",
        value: "enable",
    },
]
const initPersonalization: IPersonalizationText = {
    toggleShowLayer: false,
    defaultValueToggleShow: true,
    option: "none",
    title: null,
    placeholder: null,
    limitCharacter: null,
    require: false,
    showCustomization: false,

}
const PersonalizationSetting = (props: Props) => {
    const { data, onChangeAttr } = props
    const [dataState, setDataState] = useState<IPersonalizationText>(initPersonalization)

    const handleChangeSwitch = (event: React.ChangeEvent<HTMLInputElement>) => {
        const name = event.target.name;
        const checked = event.target.checked;
        return debounceCallChange({ ...dataState, [name]: checked })
        // return setDataState((prev) => ({ ...prev, [name]: checked }))
    };
    const handleChangeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        const name = event.target.name;
        const value = event.target.value;
        if (name === "limitCharacter") {
            return debounceCallChange({ ...dataState, [name]: value })
            // return setDataState((prev) => ({ ...prev, [name]: isNaN(parseInt(value)) ? null : parseInt(value) }))
        }
        return debounceCallChange({ ...dataState, [name]: value })
    };

    const handleChangeRadio = (event) => {
        const value = (event.target as HTMLInputElement).value;
        const name = (event.target as HTMLInputElement).name;
        return debounceCallChange({ ...dataState, [name]: value })
        // return setDataState((prev) => ({ ...prev, [name]: value }));
    }
    useEffect(() => {
        if (!!data) {
            setDataState({ ...data })
        }
    }, [data])



    const debounceCallChange = debounce(
        (dataChange) => onChangeAttr(dataChange),
        0
    )
    return (
        <Grid container rowGap={2} columnSpacing={2} sx={{ px: 2 }}>
            <Grid item xs={12}>
                {dataSwitch.map((switchItem, index) => {
                    if (switchItem.name === "defaultValueToggleShow" && !dataState.toggleShowLayer) {
                        return null
                    }
                    return (
                        <FormControlLabel
                            sx={{ color: "white" }}
                            value={switchItem.value}
                            checked={dataState[switchItem.name]}
                            name={switchItem.name}
                            key={index}
                            onChange={handleChangeSwitch}
                            control={
                                <Switch
                                    sx={{
                                        color: "white",
                                    }}
                                />
                            }
                            label={switchItem.label}
                        />
                    );
                })}
            </Grid>
            <Grid item xs={12}>
                <FormControl component="fieldset" >
                    <FormLabel component="legend" sx={{ fontWeight: 900, color: "white", width: "100%" }}>
                        Personalize Options:
                    </FormLabel>
                    <RadioGroup
                        aria-label="Personalize Options"
                        value={dataState?.option}
                        name={"option"}
                        onChange={handleChangeRadio}
                    >
                        {options.map((item: any, index: number) => (
                            <FormControlLabel
                                key={index}
                                sx={{ color: "white", fontWeight: 600 }}
                                value={item.value}
                                control={
                                    <Radio
                                        size="small"
                                        sx={{
                                            pt: 0,
                                            pb: 0,
                                            color: "white",
                                        }}
                                    />
                                }
                                label={item.label}
                            />
                        ))}
                    </RadioGroup>
                </FormControl>
            </Grid>
            {dataState.option === "enable" && (
                <>
                    <Grid item xs={12} px={2}>
                        <InputCustom
                            onChange={handleChangeInput}
                            label={"Option Title:"}
                            name="title"
                            id="option-title"
                            value={dataState.title}
                            colLabel={12}
                        />
                    </Grid>
                    <Grid item xs={12} px={2}>
                        <InputCustom
                            colLabel={12}
                            onChange={handleChangeInput}
                            label={"Placeholder text:"}
                            name="placeholder"
                            id="placeholder-text"
                            value={dataState.placeholder}
                        />
                    </Grid>
                    <Grid item xs={12} px={2}>
                        <InputCustom
                            onChange={handleChangeInput}
                            label={"Text character limit:"}
                            colLabel={12}
                            name="limitCharacter"
                            id="character-limit"
                            inputProps={{ min: 0 }}
                            type="number"

                            value={dataState.limitCharacter}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <FormControlLabel
                            sx={{ color: "white" }}
                            value={"require"}
                            checked={dataState.require}
                            name={"require"}
                            key={"require"}
                            onChange={handleChangeSwitch}
                            control={
                                <Switch
                                    sx={{
                                        color: "white",
                                    }}
                                />
                            }
                            label={"Mark this option as required"}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <FormControlLabel
                            sx={{ color: "white" }}
                            value={"showCustomization"}
                            checked={dataState.showCustomization}
                            name={"showCustomization"}
                            key={"showCustomization"}
                            onChange={handleChangeSwitch}
                            control={
                                <Switch
                                    sx={{
                                        color: "white",
                                    }}
                                />
                            }
                            label={"Show customization data on cart/checkout"}
                        />
                    </Grid>
                </>
            )}

        </Grid>
    )
}
const areEqual = (prevProps: Props, nextProps: Props) => {
    // render when return false => any prevProps different nextProps => return true => !return true => false => pass condition
    return !(!isEqual(prevProps?.data, nextProps.data))
}

export default React.memo(PersonalizationSetting, areEqual)