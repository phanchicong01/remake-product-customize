import React, { useEffect, useState } from "react";

import { Select } from "antd";
const { Option } = Select;

const VirtualizedSelect = ({ data, value, name, label, onChange, ...orderProps }: any) => {
    const [stateData, setStateData] = useState([]);
    const [valueActive, setValueActive] = useState(value);

    const handleChangeFont = (value, option) => {
        onChange(value);
        setValueActive(value);
    };
    useEffect(() => {
        if (data) {
            setStateData(
                data.map((item: any) => ({
                    ...item,
                    family: item.family || item.name,
                    url: "https://fonts.googleapis.com/css?family=" + item.family.replace(/ /g, "+"),
                }))
            );
        }
    }, [data]);
    useEffect(() => {
        if (value !== valueActive) {
            setValueActive(value);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [value]);

    return (
        <>
            <Select
                showSearch
                onChange={handleChangeFont}
                size={"large"}
                style={{ width: "100%" }}
                value={valueActive}
           
            >
                {stateData?.map((option, index: number) => (
                    <Option key={index} value={option.family}>
                        {option.family}{" "}
                    </Option>
                ))}
            </Select>
        
        </>
    );
};

export default VirtualizedSelect;
