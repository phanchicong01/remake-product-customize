import React, { useState, useEffect, useCallback } from "react";
import axios from "axios";
import Select from "antd/lib/select";

import Grid from "@mui/material/Grid";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import FormControlLabel from "@mui/material/FormControlLabel";
import RadioGroup from "@mui/material/RadioGroup";
import Radio from "@mui/material/Radio";
import Switch from "@mui/material/Switch";
import Typography from "@mui/material/Typography";

import { LayerText } from "store/template-reducer/interface";
import { valueFontKind, fontKind } from "store/fonts-family/interface";

import { onUpdateAttrLayerByKey } from "store/template-reducer/templateSlice";
import { createGoogleFonts } from "store/fonts-family/fontsFamilySlice";

import { useAppDispatch, useAppSelector } from "hooks";
import { API_KEY_GOOGLE_FONTS } from "constants/index";
import VirtualizedSelect from "./component/virtualized-select";
import InputCustom from "components/common/InputCustom";
import MenuFontStyle from "./component/menu-font-style";
import { Helmet } from "react-helmet";
import WebFont from 'webfontloader';
import isEqual from "lodash/isEqual"
import styles from "./styles.module.scss"
import LoadingSelect from "./component/loading";
import PickColor from "components/common/pick-color";
import useFonts from "data/hooks/user-get-fonts";

interface IProps {
    data: LayerText
}

const TextConfig = ({ data }: IProps) => {
    const { customFonts, googleFonts } = useAppSelector((state) => state.fontsFamily);

    const [stateData, setStateData] = useState<LayerText>({
        fontKind: "webfonts#webfontList"
    });
    const [customFontSelected, setCustomFontSelected] = useState(null)
    const [loadingFont, setLoadingFont] = useState(false)
    const { refetch, isRefetching } = useFonts(null, !!customFonts);

    const dispatch = useAppDispatch();

    const handleChangeAttrs = useCallback((payload: { newAttrs: any, nameAttrs: string }) => {
        dispatch(
            onUpdateAttrLayerByKey(payload)
        )
    }, [dispatch])

    const handleChangeInput = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        return handleChangeAttrs({ newAttrs: value, nameAttrs: name });
    }, [handleChangeAttrs]);

    const handleChangeInputNumber = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        return handleChangeAttrs({ newAttrs: parseInt(value), nameAttrs: name });
    }, [handleChangeAttrs]);

    const handleChangePickColor = useCallback((color: string, name: string) => {
        return handleChangeAttrs({ newAttrs: color, nameAttrs: name })
    }, [handleChangeAttrs]);

    const handleChangeSelectFont = useCallback((value, options) => {
        let fontItem = null
        setLoadingFont(true)
        if (stateData.fontKind === "webfonts#webfontList") {
            fontItem = googleFonts?.items?.find(item => value === item.family)
            WebFont.load({
                google: {
                    families: [value]
                },
                fontactive: function () {
                    setLoadingFont(false)
                    handleChangeAttrs({
                        nameAttrs: 'fontFamily',
                        newAttrs: value
                    })
                    handleChangeAttrs({
                        nameAttrs: 'fontKind',
                        newAttrs: stateData.fontKind
                    })
                    // setStateData((prevState) => ({ ...prevState, fontFamily: value }));
                }
            })

        }

        if (stateData.fontKind === "customfonts#customfontList") {
            fontItem = customFonts?.items?.find(item => value === item.family)
            let markup = [
                '@font-face {\n',
                '\tfont-family: \'', fontItem.name, '\';\n',
                '\tfont-style: \'normal\';\n',
                '\tfont-weight: \'normal\';\n',
                '\tsrc: url(\'', fontItem.url, '\');\n',
                '}\n'
            ].join('');

            setCustomFontSelected(markup)

            WebFont.load({
                custom: {
                    families: [value],
                },
                fontactive: function () {
                    setLoadingFont(false)
                    handleChangeAttrs({
                        nameAttrs: 'fontFamily',
                        newAttrs: value
                    })
                    handleChangeAttrs({
                        nameAttrs: 'fontKind',
                        newAttrs: stateData.fontKind
                    })
                    // setStateData((prevState) => ({ ...prevState, fontFamily: value }));
                }
            })
        }

    }, [customFonts?.items, googleFonts?.items, handleChangeAttrs, stateData.fontKind])

    const handleChangeRadio = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = (event.target as HTMLInputElement).value as fontKind;
        return setStateData((prev) => ({ ...prev, fontKind: value }));
    };

    useEffect(() => {
        if (googleFonts.items.length <= 0) {
            axios({
                method: "get",
                url: `https://www.googleapis.com/webfonts/v1/webfonts?key=${API_KEY_GOOGLE_FONTS}`,
                // responseType: "stream",
            }).then(function (response) {
                dispatch(createGoogleFonts(response.data.items));
            });
        }

    }, [dispatch, googleFonts?.items?.length]);

    useEffect(() => {
        if (!!data && !isEqual(data, stateData)) {
            setStateData(data)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [data])

    const handleChangeSwitch = (event: React.ChangeEvent<HTMLInputElement>) => {
        const name = event.target.name;
        const checked = event.target.checked;
        setStateData((prevState) => ({ ...prevState, [name]: checked }));
        handleChangeAttrs({ nameAttrs: name, newAttrs: checked })
    };

    const handleChangeFonStyle = useCallback((payload, name, value) => {
        handleChangeAttrs({ nameAttrs: name, newAttrs: value })
    }, [handleChangeAttrs])

    if (!stateData) {
        return null;
    }

    return (
        <Grid container rowSpacing={2} >
            <Helmet >
                <style type="text/css" >
                    {customFontSelected}
                </style>
            </Helmet>

            <Grid item xs={12} px={2}>
                <InputCustom
                    onChange={handleChangeInput}
                    value={stateData.text}
                    name="text"
                    id="text"
                    label={"Preview text:"}
                    colLabel={12}
                />
            </Grid>
            <Grid item xs={12} px={2}>
                <InputCustom
                    onChange={handleChangeInputNumber}
                    value={stateData?.fontSize}
                    name="fontSize"
                    id="fontSize"
                    type="number"
                    label={"Font Size:"}
                    colLabel={12}
                />
            </Grid>
            <Grid item xs={12} px={2}>
                <PickColor label="Font color"
                    width={"100%"}
                    id={"pick-color-fill-" + stateData.id}
                    onChange={handleChangePickColor}
                    color={stateData.fill}
                    name="fill" />
            </Grid>
            <Grid item xs={6} px={2}>
                <Typography
                    sx={{
                        color: "#fff",
                        fontWeight: "bold",
                        margin: "auto",
                        display: "flex",
                        height: "100%",
                        alignItems: "center",
                    }}
                >
                    Font Style
                </Typography>
            </Grid>
            <Grid item xs={6}>
                <MenuFontStyle data={stateData} onChangeAttr={handleChangeFonStyle} />
            </Grid>

            <Grid item xs={12} px={2}>

                <FormControlLabel
                    sx={{ color: "white" }}
                    value="show-option"
                    checked={!!stateData?.strokeEnabled}
                    onChange={handleChangeSwitch}
                    name={"strokeEnabled"}
                    control={
                        <Switch
                            sx={{
                                color: "white",
                            }}
                        />
                    }
                    label="Enable Stroke"
                />
            </Grid>
            {
                stateData?.strokeEnabled && (
                    <>
                        <Grid item xs={12} px={2}>
                            <PickColor width={"100%"}
                                id={"pick-color-stroke-" + stateData.id}
                                onChange={handleChangePickColor}
                                label="Stroke color:"
                                name="stroke"
                                color={stateData.stroke as string} />
                        </Grid>

                        <Grid item xs={12} px={2}>
                            <InputCustom
                                onChange={handleChangeInputNumber}
                                value={stateData?.strokeWidth || 1}
                                name="strokeWidth"
                                id="strokeWidth"
                                type="number"
                                colLabel={12}
                                label={"Stroke Width:"}
                            />
                        </Grid>
                    </>
                )
            }
            <Grid item xs={12} px={2}>
                <FormControl>
                    <FormLabel component="b" sx={{ fontWeight: 900, color: "white", textAlign: "left" }}>
                        Which kind of fonts do you want to use?:
                    </FormLabel>
                    <RadioGroup
                        aria-label="Personalize Options"
                        value={stateData.fontKind}
                        name={"personalizeOption"}
                        onChange={handleChangeRadio}
                    >
                        {[
                            { name: "Google fonts", value: valueFontKind.googleFonts },
                            { name: "Custom fonts", value: valueFontKind.customFonts },
                        ].map((item: any, index: number) => (
                            <FormControlLabel
                                key={index}
                                sx={{ color: "white", fontWeight: 600 }}
                                value={item.value}
                                control={
                                    <Radio
                                        size="small"
                                        sx={{
                                            pt: 0,
                                            pb: 0,
                                            color: "white",
                                        }}
                                    />
                                }
                                label={item.name}
                            />
                        ))}
                    </RadioGroup>
                </FormControl>
            </Grid>
            <Grid item xs={12} px={2} sx={{ position: "relative" }}>
                {stateData.fontKind === "webfonts#webfontList" && (
                    <div className="position-relative">

                        <VirtualizedSelect
                            label="Font"
                            value={stateData.fontFamily}
                            onChange={handleChangeSelectFont}
                            name="fontFamily"
                            data={googleFonts.items}
                        />
                        {loadingFont &&
                            <div className={styles.loadingFont}>
                                <LoadingSelect width={40} height={40} />
                            </div>
                        }

                    </div>
                )}

            </Grid>
            <Grid item xs={12} px={2} >
                {stateData.fontKind === "customfonts#customfontList" && (
                    <>
                        <div role={'button'} className="text-primary text-end" onClick={() => refetch()}>
                            {
                                isRefetching && <div style={{ width: '14px', height: '14px' }} className="spinner-border  text-primary" role="status">
                                </div>
                            }
                            <span>Refetch {isRefetching && '...'}</span>
                        </div>
                        <div style={{ position: "relative" }}>
                            <Select
                                showSearch
                                optionFilterProp="children"
                                fieldNames={{ label: "family", value: "family" }}
                                onChange={handleChangeSelectFont}
                                // onSearch={onSearch}
                                size={"large"}
                                style={{ width: "100%" }}
                                value={stateData.fontFamily}
                                options={customFonts?.items}
                            />
                            {(loadingFont || isRefetching) &&
                                <div className={styles.loadingFont}><LoadingSelect width={40} height={40} /></div>
                            }
                        </div>
                    </>
                )}

            </Grid>
        </Grid >
    );
};

export default TextConfig;
