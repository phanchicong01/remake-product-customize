import cloneDeep from "lodash/cloneDeep"
import React, { useCallback, useEffect, useRef, useState } from 'react'
import { Card, CardMedia, Divider, FormControlLabel, Grid, InputBase, Paper,   Switch, Typography } from '@mui/material'

import InputCustom from 'components/common/InputCustom'
import { GroupClipArtSetting } from 'store/template-reducer/interface'
import isEqual from "lodash/isEqual"
import { debounce } from 'utils'

import GeneralItemClipArt from 'components/common/GeneralItemClipArt'
import { OptionPersonalizationImage } from 'store/template-reducer/enum'
import { RePosition } from 'store/re-position-reducer/interface'

import ItemChecked from 'components/common/item-checked'
import { useAppDispatch, useAppSelector } from 'hooks'
import { closeReposition, setReposition } from 'store/re-position-reducer/RePositionSlice'
import { ClipArtCategory } from 'interfaces/clipart-category'

import useClipartCategories from 'data/hooks/use-clipart-categories'
import useDetailClipartCategories from 'data/hooks/use-detail-clipart-categories'

import styles from "./styles.module.scss"
import { styled } from '@mui/styles'
import TreeSelectComponent from "components/common/tree-select-component"
import clsx from "clsx"
import { Select } from "antd"
import { getUrl } from "helpers"

type Props = {
    data: GroupClipArtSetting,
    onChangeAttr: (data) => void
}
const initClipartSetting: GroupClipArtSetting = {
    title: "",
    defaultOption: null,
    defaultOptionParent: null,
    clipArtCategory: null,
    rePosition: [],
    require: false,
    showCustomization: false,
    placeholder: ""
}

const GroupClipartCategory = (props: Props) => {
    const { data, onChangeAttr } = props;
    const [dataState, setDataState] = useState<GroupClipArtSetting>(initClipartSetting);

    const [isShowDefaultOption, setIsShowDefaultOption] = useState<boolean>(false)
    const { data: daRepositionSlice, isReposition } = useAppSelector((state) => state.rePosition)

    const dispatch = useAppDispatch();
    const {
        data: dataTreeCategories,
        isFetched,
        isLoading,
        isRefetching,
        refetch,
    } = useClipartCategories({ search: '' })

    const {
        isFetched: isFetchedDetailClipart,
        isLoading: isLoadingDetailClipart,
    } = useDetailClipartCategories(dataState?.clipArtCategory?.id);

    const handleChangeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        const name = event.target.name;
        const value = event.target.value;
        if (name === "limitCharacter") {
            return setDataState((prev) => ({ ...prev, [name]: isNaN(parseInt(value)) ? null : parseInt(value) }))
        }
        return setDataState((prev) => ({ ...prev, [name]: value }))
    };


    const handleChangeSelectCategory = (value: number, option: any) => {
        if (isReposition) {
            dispatch(closeReposition());
        }
        setDataState((prev) => ({ ...prev, rePosition: null, clipArtCategory: option, defaultOption: null, defaultOptionParent: null }))
    };
    const handleClearCategory = () => {
        if (isReposition) {
            dispatch(closeReposition());
        }
        setDataState((prev) => ({ ...prev, rePosition: null, clipArtCategory: null, defaultOption: null, defaultOptionParent: null }))
    };
    const handleChangeSwitch = (event: React.ChangeEvent<HTMLInputElement>) => {
        const name = event.target.name;
        const checked = event.target.checked;
        return setDataState((prev) => ({ ...prev, [name]: checked }))
    };


    const isFirstSetState = useRef<boolean>(false)

    useEffect(() => {


        if (!!data) {
            setDataState({ ...data })

        }
        isFirstSetState.current = true

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    const debounceCallChange = debounce(useCallback((dataChange) => onChangeAttr(dataChange), [onChangeAttr]), 500)

    useEffect(() => {
        if (isFirstSetState.current) {
            debounceCallChange({ ...dataState })
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [dataState])
    const handleChangeSelectDefaultOption = (value:any) => {
     
        const dataClipartSelected = dataState.clipArtCategory
        const findItem = dataClipartSelected?.children.find((clipart: ClipArtCategory) => `${clipart.id}` === value);
        if (!!findItem) {
            setDataState((prev) => ({ ...prev, defaultOptionParent: findItem, defaultOption: null }))
        }
    }
    const handleSetDefaultValue = (item) => {
        setDataState(prev => ({ ...prev, defaultOption: item }))
    }
    const handleReposition = (key: any, data: any) => {

        const newReposition = cloneDeep(dataState.rePosition) || [];
        const index = newReposition.findIndex((item: RePosition) => item.clipartItem?.id === data.id)
        let newItem: RePosition = {
            rotation: 0,
            x: 0,
            y: 0,
            width: 0,
            height: 0,
            clipartItem: data
        }
        if (index !== -1) {
            newItem = newReposition[index]
        }

        if (key === "reposition") {
            return dispatch(setReposition({ isReposition: true, data: newItem }))
        }
        if (key === "done" && !!daRepositionSlice) {
            const cloneData = cloneDeep(daRepositionSlice)
            newItem = {
                ...newItem,
                clipartItem: data,
                x: cloneData.x,
                y: cloneData.y,
                width: cloneData.width,
                height: cloneData.height,
                rotation: cloneData.rotation
            }
            if (index !== -1) {
                newReposition[index] = newItem
            } else {
                newReposition.push(newItem)
            }
        }

        if (isReposition) {
            dispatch(closeReposition())
            setDataState(prev => ({
                ...prev, rePosition: [...newReposition]
            }))
        }

    }

    return (
        <Grid container rowGap={2} columnSpacing={2} sx={{ px: 2 }}>
            <Grid item xs={12} >
                <InputCustom
                    onChange={handleChangeInput}
                    label={"Option Title:"}
                    name="title"
                    id="option-title"
                    value={dataState.title}
                    colLabel={12}
                />
            </Grid>
            <Grid item xs={12}>
                <div className='d-flex mb-2 justify-content-between align-items-center'>
                    <label className='text-white fw-bolder'> Select one clipart category:</label>
                    <div role={'button'} className='text-primary text-end' onClick={() => refetch()}>
                        {isRefetching && (
                            <div
                                style={{ width: '14px', height: '14px' }}
                                className='spinner-border  mr-1 text-primary'
                                role='status'
                            ></div>
                        )}
                        <span> Refetch{isRefetching && '...'}</span>
                    </div>
                </div>
                {isFetched && !isLoading && (
                    <TreeSelectComponent
                        size='medium'
                        treeIcon={false}
                        loading={isLoading || isRefetching}
                        id='parent_category_id'
                        value={dataState.clipArtCategory?.id}
                        onSelect={handleChangeSelectCategory}
                        className={'form-select-tree form-select-tree-solid '}
                        showSearch
                        treeData={dataTreeCategories}
                        fieldNames={{
                            label: 'name',
                            children: 'children',
                            value: 'id',
                        }}
                        dropdownClassName='form-select-tree-dropdown'
                        placeholder='Please select category'
                        allowClear
                        onClear={handleClearCategory}
                    />
                )}
            </Grid>

            {dataState?.clipArtCategory?.id && (
                <GeneralItemClipArt
                // idCategroy={dataState?.clipArtCategory?.id}
                    data={dataState?.clipArtCategory}
                    isFetched={isFetchedDetailClipart}
                    isLoading={isLoadingDetailClipart}
                    type={OptionPersonalizationImage.groupClipart}
                    onClickAction={(key, data) => {
                        if (key === "reposition" && isReposition) {
                            dispatch(closeReposition())
                            const timeOut = setTimeout(() => {
                                handleReposition(key, data)
                                clearTimeout(timeOut)
                            }, 100)
                            return;
                        }
                        return handleReposition(key, data);
                    }}
                />
            )}

            <Grid item xs={12}>
                <label className='fw-bolder text-white mb-2'>Clipart category’s default value:</label>
                {
                    dataState?.defaultOption?.id &&
                    <Grid container alignItems={"center"} xs="auto" justifyContent={"space-between"}  >
                        <div style={{ color: "white" }}>{dataState?.defaultOption?.name}</div>
                        <Paper sx={{ width: 50 }}>
                            <CardMedia
                                sx={{ objectFit: "contain" }}
                                component="img"
                                height="50"
                                image={getUrl(dataState?.defaultOption?.thumbnail_url || dataState?.defaultOption?.url)}
                            />
                        </Paper>
                    </Grid>
                }
                <Card sx={{ background: "#343a40", position: "relative", minHeight: "40px", marginTop: "16px" }}>
                    <Grid item xs={12} sx={{ py: 2, px: 1 }}>
                        {
                            !isShowDefaultOption &&
                            <div className="d-flex  justify-content-center">
                                <label className={clsx('', styles.btnShowDefaultBgGray)} onClick={() => setIsShowDefaultOption(true)}>Show option to select default value</label>
                            </div>
                        }
                        {
                            isShowDefaultOption &&
                            <Grid container spacing={0.5}>
                                 <Select
                                    showSearch
                                    value={dataState.defaultOption?.id}
                                    size='large'
                                    placeholder="Select max width"
                                    id='max_width'
                                    onChange={handleChangeSelectDefaultOption}
                                    dropdownClassName="form-custom-select-dropdown "
                                    className={clsx('form-custom-select form-custom-select-solid mb-2')}
                                >
                                {dataState?.clipArtCategory?.children?.map((item: ClipArtCategory) => {
                                        return (
                                            <Select.Option key={item.id} value={`${item.id}`}>
                                                {item?.name}
                                            </Select.Option>
                                        );
                                    })}
                                </Select>
                                
                                {
                                    dataState.defaultOptionParent?.items?.length > 0 &&
                                    <ItemChecked
                                        data={dataState.defaultOptionParent?.items}
                                        id={`selected_default_option_clipart_${dataState.clipArtCategory?.id}`}
                                        key_url={'url'}
                                        isShowHover={true}
                                        value={dataState?.defaultOption?.id}
                                        onChange={(idChecked, item) => handleSetDefaultValue(item)}
                                    />
                                }{
                                    dataState.defaultOptionParent?.items?.length === 0 &&
                                    <Typography color={"gray"} component={"span"} >
                                        Empty items
                                    </Typography>
                                }
                                {
                                    dataState.defaultOption?.id &&
                                    <Grid container justifyContent={"center"} sx={{ mt: 2 }}>
                                        <Grid item className={styles.btnShowDefaultBgGray} onClick={() => setDataState((prev) => ({ ...prev, defaultOption: null }))}>Clear default value</Grid>
                                    </Grid>
                                }
                                {
                                    !dataState.clipArtCategory &&
                                    <Typography color={"gray"} component={"span"} pt={1}>
                                        Set value to grouped clipart to set default value
                                    </Typography>
                                }
                            </Grid>
                        }

                    </Grid>
                </Card>
            </Grid >
            <Grid item xs={12} >
                <InputCustom
                    onChange={handleChangeInput}
                    label={"Placeholder text:"}
                    name="placeholder"
                    placeholder={"Choose Something"}
                    id="placeholder-text"
                    value={dataState.placeholder}
                    colLabel={12}
                />
            </Grid>
            <Grid item xs={12} sx={{ py: 2 }}> <Divider variant="middle" color="white" /></Grid>
            <Grid item xs={12}>
                <FormControlLabel
                    sx={{ color: "white" }}
                    value={"require"}
                    checked={dataState.require}
                    name={"require"}
                    key={"require"}
                    onChange={handleChangeSwitch}
                    control={
                        <Switch
                            sx={{
                                color: "white",
                            }}
                        />
                    }
                    label={"Mark this option as required"}
                />
            </Grid>
            <Grid item xs={12}>
                <FormControlLabel
                    sx={{ color: "white" }}
                    value={"showCustomization"}
                    checked={dataState.showCustomization}
                    name={"showCustomization"}
                    key={"showCustomization"}
                    onChange={handleChangeSwitch}
                    control={
                        <Switch
                            sx={{
                                color: "white",
                            }}
                        />
                    }
                    label={"Show customization data on cart/checkout"}
                />
            </Grid>
        </Grid >
    )
}
const areEqual = (prevProps: Props, nextProps: Props) => {
    // render when return false => any prevProps different nextProps => return true => !return true => false => pass condition
    return !(!isEqual(prevProps?.data, nextProps.data))
}
export default React.memo(GroupClipartCategory, areEqual)
const TypographyCustom = styled(Typography)({
    "&.MuiTypography-root": {
        color: "#fff",
        fontWeight: "bold",
    },
});

const CustomInput = styled(InputBase)(({ theme }) => ({
    "label + &": {
        marginTop: "1.5rem",
        color: "#fff",
    },
    "& .MuiInputBase-input": {
        borderRadius: 4,
        position: "relative",
        backgroundColor: "#fff",
        border: "1px solid #ced4da",
        fontSize: 16,
        padding: "8px 26px 8px 12px",
        // Use the system font instead of the default Roboto font.
        "&:focus": {
            borderRadius: 4,
            borderColor: "#80bdff",
            boxShadow: "0 0 0 0.2rem rgba(0,123,255,.25)",
        },
    },
}));