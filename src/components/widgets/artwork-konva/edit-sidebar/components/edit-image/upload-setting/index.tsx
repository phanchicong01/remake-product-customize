import React, { useCallback, useEffect, useState } from 'react'
import { Divider, FormControlLabel, Grid, Switch } from '@mui/material'
import InputCustom from 'components/common/InputCustom'
import isEqual from "lodash/isEqual"
import { debounce } from 'utils'

import { useDispatch } from 'react-redux'

type Props = {
    data: any,
    onChangeAttr: (data) => void
}
const initClipartSetting = {
    title: "",
    cropper: null,
    liveReview: null,
    instruction: null,
    require: false,
    showCustomization: false

}

const UploadPhotoSetting = (props: Props) => {
    const { data, onChangeAttr } = props;
    const [dataState, setDataState] = useState(initClipartSetting);

    const dispatch = useDispatch()
    const handleChangeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        const name = event.target.name;
        const value = event.target.value;
        if (name === "limitCharacter") {
            debounceCallChange({ ...dataState, [name]: value })
            return setDataState((prev) => ({ ...prev, [name]: isNaN(parseInt(value)) ? null : parseInt(value) }))
        }
        debounceCallChange({ ...dataState, [name]: value })
        return setDataState((prev) => ({ ...prev, [name]: value }))
    };



    const handleChangeSwitch = (event: React.ChangeEvent<HTMLInputElement>) => {
        const name = event.target.name;
        const checked = event.target.checked;
        debounceCallChange({ ...dataState, [name]: checked })
        return setDataState((prev) => ({ ...prev, [name]: checked }))
    };

    const debounceCallChange = useCallback(debounce((dataChange) => onChangeAttr(dataChange), 500), []);

    useEffect(() => {

        if (!!data) {
            setDataState({ ...data })
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <Grid container rowGap={2} columnSpacing={2} sx={{ px: 2 }}>
            <Grid item xs={12} >
                <InputCustom
                    onChange={handleChangeInput}
                    label={"Option Title:"}
                    name="title"
                    id="option-title"
                    value={dataState.title}
                    colLabel={12}
                />
            </Grid>


            <Grid item xs={12} sx={{ py: 2 }}> <Divider variant="middle" color="white" /></Grid>
            <Grid item xs={12}>
                <FormControlLabel
                    sx={{ color: "white" }}
                    value={"require"}
                    checked={dataState.require}
                    name={"require"}
                    key={"require"}
                    onChange={handleChangeSwitch}
                    control={
                        <Switch
                            sx={{
                                color: "white",
                            }}
                        />
                    }
                    label={"Mark this option as required"}
                />
            </Grid>
            <Grid item xs={12}>
                <FormControlLabel
                    sx={{ color: "white" }}
                    value={"showCustomization"}
                    checked={dataState.showCustomization}
                    name={"showCustomization"}
                    key={"showCustomization"}
                    onChange={handleChangeSwitch}
                    control={
                        <Switch
                            sx={{
                                color: "white",
                            }}
                        />
                    }
                    label={"Show customization data on cart/checkout"}
                />
            </Grid>
        </Grid >
    )
}
const areEqual = (prevProps: Props, nextProps: Props) => {
    // render when return false => any prevProps different nextProps => return true => !return true => false => pass condition
    return !(!isEqual(prevProps?.data, nextProps.data))
}
export default React.memo(UploadPhotoSetting, areEqual)

