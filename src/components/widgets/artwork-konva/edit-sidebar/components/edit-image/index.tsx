import React, { useEffect, useState } from "react";

import Grid from "@mui/material/Grid";
import ColorLensIcon from '@mui/icons-material/ColorLens';
import ImageIcon from '@mui/icons-material/Image';
import CollectionsIcon from '@mui/icons-material/Collections';
import VisibilityIcon from '@mui/icons-material/Visibility';
import ElectricalServicesIcon from '@mui/icons-material/ElectricalServices';
// import UploadFileIcon from '@mui/icons-material/UploadFile';

import CollapseComponent from "../common/collapse-component";
import PersonalizationSetting from "./personalization-setting";
import { ClipartSetting, ConditionSetting, GroupClipArtSetting, LayerCommon, LayerImage } from "store/template-reducer/interface";
import { onUpdateAttrConfig } from "store/template-reducer/templateSlice";
import ClipartCategory from "./clipart-category";
import ConditionSettingComponent from "../common/condition-setting";
import InputCustom from "components/common/InputCustom";
import GroupClipartCategory from "./group-clipart-category";
// import UploadPhotoSetting from "./upload-setting"
import { useAppSelector, useAppDispatch } from "hooks";

type IData = LayerCommon & LayerImage
interface IProps {
    data: IData;
}

const EditImage = ({ data }: IProps) => {
    const layerSelected = useAppSelector((state) => state.template.templateActive.layerSelected);

    const [state, setstate] = useState<IData>(null)
    useEffect(() => {
        if (layerSelected[0]) {
            setstate((prev) => ({ ...prev, ...layerSelected[0] }))
        }
    }, [layerSelected])

    const dispatch = useAppDispatch()
    const handleChangePersonalization = (newAttrs: any) => {

        dispatch(onUpdateAttrConfig({ newAttrs, nameAttrs: 'personalization' }));

        // console.log(newData)
    }
    const handleChangeConditionSetting = (newAttrs: ConditionSetting) => {
        dispatch(onUpdateAttrConfig({ newAttrs, nameAttrs: 'conditionalSetting' }));
    }
    const handleChangeClipart = (newAttrs: ClipartSetting) => {

        dispatch(onUpdateAttrConfig({ newAttrs, nameAttrs: 'clipArtSetting' }));


    }
    const handleChangeGroupClipArt = (newAttrs: GroupClipArtSetting) => {
        dispatch(onUpdateAttrConfig({ newAttrs, nameAttrs: 'groupClipArtSetting' }))
    }
    const handleChangeExtra = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = event.target.value;
        const newAttrs = {
            ...state?.configuration?.extra,
            customClass: value
        }
        dispatch(onUpdateAttrConfig({ newAttrs, nameAttrs: 'extra' }));
    }

    // const handleChangeUploadPhoto = (newAttrs: any) => {
    //     console.log(newAttrs, "handleChangeUploadPhoto")
    // }
    if (!state) return <div >
        {" "}
        loading data ...{" "}
    </div>
    return (
        <Grid container >
            <Grid item xs={12}>
                <CollapseComponent title={"PERSONALIZATION SETTING"} icon={<ColorLensIcon />} isOpen={true}>
                    <PersonalizationSetting data={state?.configuration?.personalization} onChangeAttr={handleChangePersonalization} />
                </CollapseComponent>
                {
                    state?.configuration?.personalization?.option === "clipart" &&
                    <CollapseComponent title={"CLIPART SETTING"} icon={<ImageIcon />}
                        isOpen={true}>
                        <ClipartCategory data={state?.configuration?.clipArtSetting} onChangeAttr={handleChangeClipart} />
                    </CollapseComponent>
                }
                {
                    state?.configuration?.personalization?.option === "group-clipart" &&
                    <CollapseComponent title={"GROUP CLIPART SETTING"} icon={<CollectionsIcon />} isOpen={true}>
                        <GroupClipartCategory data={state?.configuration?.groupClipArtSetting} onChangeAttr={handleChangeGroupClipArt} />
                    </CollapseComponent>
                }
                {/* dang phat trien */}
                {/* {
                    state?.configuration?.personalization?.option === "upload-photo" &&
                    <CollapseComponent title={"UPLOAD PHOTO SETTING"} icon={<UploadFileIcon />} isOpen={true}>
                        <UploadPhotoSetting data={state?.configuration?.uploadPhotoSetting} onChangeAttr={handleChangeUploadPhoto} />
                    </CollapseComponent>
                } */}
                <CollapseComponent title={"CONDITIONAL SETTING"} icon={<VisibilityIcon />}>
                    <ConditionSettingComponent data={state?.configuration?.conditionalSetting} onChangeAttr={handleChangeConditionSetting} />
                </CollapseComponent>
                <CollapseComponent title={"EXTRA"} icon={<ElectricalServicesIcon />}>
                    <div style={{ padding: " 0 15px" }}>
                        <InputCustom
                            colLabel={12}
                            label="Custom class:"
                            name="customClass"
                            onChange={handleChangeExtra}
                            value={state?.configuration?.extra?.customClass}
                        />
                    </div>
                </CollapseComponent>
            </Grid>
        </Grid>
    );
};

export default EditImage;
