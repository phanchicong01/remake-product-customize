import React, {useCallback, useEffect, useRef, useState} from 'react'
import {
  Card,
  CardMedia,
  Divider,
  FormControlLabel,
  Grid,
  Paper,
  Switch,
  Typography,
} from '@mui/material'
import {useDispatch} from 'react-redux'

import _cloneDeep from 'lodash-es/cloneDeep'
import _isEqual from 'lodash-es/isEqual'

import {OptionPersonalizationImage} from 'store/template-reducer/enum'
import GeneralItemClipArt from 'components/common/GeneralItemClipArt'
import ItemChecked from 'components/common/item-checked'
import InputCustom from 'components/common/InputCustom'

import {ClipartSetting} from 'store/template-reducer/interface'
import {closeReposition, setReposition} from 'store/re-position-reducer/RePositionSlice'
import {RePosition} from 'store/re-position-reducer/interface'
import {useAppSelector} from 'hooks'

import useClipartCategories from 'data/hooks/use-clipart-categories'
import useDetailClipartCategories from 'data/hooks/use-detail-clipart-categories'
import TreeSelectComponent from 'components/common/tree-select-component'
import { getUrl } from 'helpers'

import styles from './styles.module.scss'

type Props = {
  data: ClipartSetting
  onChangeAttr: (data) => void
}
const initClipartSetting: ClipartSetting = {
  title: '',
  defaultOption: null,
  clipArtCategory: null,
  rePosition: null,
  require: false,
  showCustomization: false,
}

const ClipartCategory = (props: Props) => {
  const {data, onChangeAttr} = props
  const [dataState, setDataState] = useState<ClipartSetting>(initClipartSetting)
  // const [dataTreeCategories, setDataTreeCategories] = useState<any>(null);
  const [isShowDefaultOption, setIsShowDefaultOption] = useState<boolean>(false)
  const {data: daRepositionSlice, isReposition} = useAppSelector((state) => state.rePosition)

  const dispatch = useDispatch()

  const {
    data: dataTreeCategories,
    isFetched,
    isLoading,
    isRefetching,
    refetch,
  } = useClipartCategories({search: ''})

  const {
    data:dataDetailClipart,
    isFetched:isFetchedDetailClipart,
    isLoading:isLoadingDetailClipart,
  } = useDetailClipartCategories(dataState?.clipArtCategory?.id);
 
  const handleChangeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    const name = event.target.name
    const value = event.target.value
    if (name === 'limitCharacter') {
      // debounceCallChange({ ...dataState, [name]: value })
      return setDataState((prev) => ({
        ...prev,
        [name]: isNaN(parseInt(value)) ? null : parseInt(value),
      }))
    }
    // debounceCallChange({ ...dataState, [name]: value })
    return setDataState((prev) => ({...prev, [name]: value}))
  }

  const handleChangeSelectCategory = (value: number, option: any) => {
    if (isReposition) {
      dispatch(closeReposition())
    }
    setDataState((prev) => ({
      ...prev,
      rePosition: null,
      clipArtCategory: option,
      defaultOption: null,
    }))
  }

  const handleClearCategory = () => {
    if (isReposition) {
      dispatch(closeReposition())
    }
    // debounceCallChange({ ...dataState, rePosition: null, clipArtCategory: null })
    setDataState((prev) => ({
      ...prev,
      rePosition: null,
      clipArtCategory: null,
      defaultOption: null,
    }))
  }

  const handleChangeSwitch = (event: React.ChangeEvent<HTMLInputElement>) => {
    const name = event.target.name
    const checked = event.target.checked
    // debounceCallChange({ ...dataState, [name]: checked })
    return setDataState((prev) => ({...prev, [name]: checked}))
  }

  const handleSetDefaultValue = (item) => {
    // debounceCallChange({ ...dataState, defaultOption: item })
    setDataState((prev) => ({...prev, defaultOption: item}))
  }

  const handleReposition = (key: any, data: any) => {
    let newItem: RePosition = {
      rotation: 0,
      x: 0,
      y: 0,
      width: 0,
      height: 0,
      clipartItem: data,
    }

    if (dataState.rePosition) {
      Object.keys(newItem).forEach((keys) => {
        if (dataState?.rePosition && dataState?.rePosition[0] && dataState?.rePosition[0][keys]) {
          newItem[keys] = dataState.rePosition[0][keys]
        }
      })
    }
    if (key === 'reposition') {
      dispatch(setReposition({isReposition: true, data: newItem}))
    } else if (key === 'done') {
      const cloneData = _cloneDeep(daRepositionSlice)
      newItem = {
        ...newItem,
        clipartItem: data,
        x: cloneData.x,
        y: cloneData.y,
        width: cloneData.width,
        height: cloneData.height,
        rotation: cloneData.rotation,
      }

      dispatch(closeReposition())
      setDataState((prev) => ({
        ...prev,
        rePosition: [newItem],
      }))
    }
    // debounceCallChange({ ...dataState, rePosition: [newItem] })
  }

  const debounceCallChange = useCallback(
    (dataChange: any) => onChangeAttr(dataChange),
    [onChangeAttr]
  )

  const isFirstSetState = useRef<boolean>(false)

  useEffect(() => {
    if (!!data) {
      setDataState({...data})
    }
    isFirstSetState.current = true
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    if (isFirstSetState.current) {
      debounceCallChange({...dataState})
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dataState])

  useEffect(() => {
    if (isFetched && dataDetailClipart) {
      setDataState((prev) => ({...prev, clipArtCategory:dataDetailClipart}))
    }
  }, [dataDetailClipart, isFetched])

  return (
    <Grid container rowGap={2} columnSpacing={2} sx={{px: 2}}>
      <Grid item xs={12}>
        <label className='text-white fw-bolder mb-2'> Option title:</label>
        <InputCustom
          onChange={handleChangeInput}
          hiddenLabel={true}
          name='title'
          id='option-title'
          value={dataState.title}
        />
      </Grid>
      <Grid item xs={12}>
        <div className='d-flex mb-2 justify-content-between align-items-center'>
          <label className='text-white fw-bolder'> Select one clipart category:</label>
          <div role={'button'} className='text-primary text-end' onClick={() => refetch()}>
            {isRefetching && (
              <div
                style={{width: '14px', height: '14px'}}
                className='spinner-border  mr-1 text-primary'
                role='status'
              ></div>
            )}
            <span> Refetch{isRefetching && '...'}</span>
          </div>
        </div>
        {isFetched && !isLoading && (
          <TreeSelectComponent
            size='medium'
            treeIcon={false}
            loading={isLoading || isRefetching}
            id='parent_category_id'
            value={dataState.clipArtCategory?.id}
            onSelect={handleChangeSelectCategory}
            className={'form-select-tree form-select-tree-solid '}
            showSearch
            treeData={dataTreeCategories}
            fieldNames={{
              label: 'name',
              children: 'children',
              value: 'id',
            }}
            dropdownClassName='form-select-tree-dropdown'
            placeholder='Please select category'
            allowClear
            onClear={handleClearCategory}
          />
        )}
      </Grid>
      {dataState?.clipArtCategory?.id && (
        <GeneralItemClipArt
          // idCategroy={dataState?.clipArtCategory?.id}
          data={dataDetailClipart}
          isFetched={isFetchedDetailClipart}
          isLoading={isLoadingDetailClipart}
          type={OptionPersonalizationImage.clipart}
          onClickAction={(key, data) => {
            return handleReposition(key, data)
          }}
        />
      )}

      <Grid item xs={12}>
        <label className='fw-bolder text-white mb-2'>Clipart category’s default value:</label>
        {dataState?.defaultOption?.id && (
          <Grid container alignItems={'center'} xs='auto' justifyContent={'space-between'}>
            <div style={{color: 'white'}}>{dataState?.defaultOption?.name}</div>
            <Paper sx={{width: 50}}>
              <CardMedia
                sx={{objectFit: 'contain'}}
                component='img'
                height='50'
                image={getUrl(dataState?.defaultOption?.thumbnail_url || dataState?.defaultOption?.url)}
              />
            </Paper>
          </Grid>
        )}
        <Card
          sx={{background: '#343a40', position: 'relative', minHeight: '40px', marginTop: '8px'}}
        >
          <Grid item xs={12} sx={{py: 2, px: 1}}>
            {!isShowDefaultOption && (
              <Grid container justifyContent={'center'}>
                <Grid
                  item
                  className={styles.btnShowDefaultBgGray}
                  onClick={() => setIsShowDefaultOption(true)}
                >
                  Show option to select default value
                </Grid>
              </Grid>
            )}
            {isShowDefaultOption && (
              <Grid container spacing={0.5}>
                {dataDetailClipart?.items?.length > 0 ? (
                  <ItemChecked
                    data={dataDetailClipart?.items}
                    id={`selected_default_option_clipart_${dataDetailClipart?.id}`}
                    key_url={'url'}
                    isShowHover={true}
                    value={dataState?.defaultOption?.id}
                    onChange={(idChecked, item) => handleSetDefaultValue(item)}
                  />
                ) : (
                  <Typography color={'gray'} component={'span'}>
                    &nbsp;Select item before set default value
                  </Typography>
                )}
              </Grid>
            )}
            {dataState.defaultOption?.id && (
              <Grid container justifyContent={'center'} sx={{mt: 2}}>
                <Grid
                  item
                  className={styles.btnShowDefaultBgGray}
                  onClick={() => setDataState((prev) => ({...prev, defaultOption: null}))}
                >
                  Clear default value
                </Grid>
              </Grid>
            )}
          </Grid>
        </Card>
      </Grid>
      <Grid item xs={12} sx={{py: 2}}>
        {' '}
        <Divider variant='middle' color='white' />
      </Grid>
      <Grid item xs={12}>
        <FormControlLabel
          sx={{color: 'white'}}
          value={'require'}
          checked={dataState.require}
          name={'require'}
          key={'require'}
          onChange={handleChangeSwitch}
          control={
            <Switch
              sx={{
                color: 'white',
              }}
            />
          }
          label={'Mark this option as required'}
        />
      </Grid>
      <Grid item xs={12}>
        <FormControlLabel
          sx={{color: 'white'}}
          value={'showCustomization'}
          checked={dataState.showCustomization}
          name={'showCustomization'}
          key={'showCustomization'}
          onChange={handleChangeSwitch}
          control={
            <Switch
              sx={{
                color: 'white',
              }}
            />
          }
          label={'Show customization data on cart/checkout'}
        />
      </Grid>
    </Grid>
  )
}
const areEqual = (prevProps: Props, nextProps: Props) => {
  // render when return false => any prevProps different nextProps => return true => !return true => false => pass condition
  return !!_isEqual(prevProps?.data, nextProps.data)
}
export default React.memo(ClipartCategory, areEqual)
