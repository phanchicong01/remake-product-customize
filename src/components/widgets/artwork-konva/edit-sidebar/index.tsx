import { useMemo } from "react";

import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";

import { useAppSelector, useAppDispatch } from "hooks";

import EditImage from "./components/edit-image";
import EditText from "./components/edit-text";

import { Layer } from "store/template-reducer/interface";
import { updateDrawer } from "store/drawer-menu/drawerSlice";
import { onUpdateAttrLayer } from "store/template-reducer/templateSlice";
import { closeReposition } from "store/re-position-reducer/RePositionSlice";

import styles from "./styles.module.scss";

const EditSidebar = () => {
    const { open } = useAppSelector((state) => state.drawerMenu);
    const layerSelected = useAppSelector((state) => state.template.templateActive.layerSelected);

    // const handleDrawerClose = () => {};

    const dispatch = useAppDispatch();


    const dataLayerMemo = useMemo(() => {
        if (open && !!layerSelected && layerSelected.length === 1) {
            const currentLayerSelected = layerSelected[0];
            if (!currentLayerSelected.isActiveConfig) {
                const newAttrs: Layer = { ...currentLayerSelected, isActiveConfig: true }
                dispatch(
                    onUpdateAttrLayer({
                        newAttrs: newAttrs,
                    })
                )
            }
            return currentLayerSelected;
        }
        return null
    }, [open, layerSelected, dispatch])

    if (!dataLayerMemo) {
        return (
            <div >
                {" "}
                loading data ...{" "}
            </div>
        );
    }
    return (
        <div className={styles.editSidebarArea}>
            <Grid container style={{ background: "#343a40", color: "#ffff" }} sx={{ py: 2, px: 1 }}>
                <Grid item xs sx={{ position: "relative" }}>
                    <ChevronLeftIcon
                        onClick={() => {
                            dispatch(
                                updateDrawer({
                                    open: false,
                                    data: null,
                                })
                            );
                            dispatch(closeReposition())
                        }}
                        fontSize="small"
                        className={styles.back}
                    />
                    <Typography align={"center"} sx={{ pl: 1, fontWeight: 600, fontSize: 18 }}>
                        {dataLayerMemo?.name}
                    </Typography>
                </Grid>
            </Grid>
            {dataLayerMemo?.type === "text" && (
                <EditText data={dataLayerMemo} />
            )}

            {dataLayerMemo?.type === "image" && (
                <EditImage data={dataLayerMemo} />
            )}

        </div>
    );
};

export default EditSidebar;
