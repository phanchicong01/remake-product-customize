import React, { useState, useEffect } from "react";

import { useAppSelector, useAppDispatch } from "hooks";
import ModalCategory from "../modal-category";

import { IArtwork } from "store/artwork-reducer/interface";
import { updateArtwork } from "store/artwork-reducer/artworkSlice";
import isEqual from "lodash/isEqual"
import { debounce } from "utils";
import useArtworkCategories from "data/hooks/use-artwork-categories";
import TreeSelectComponent from "components/common/tree-select-component";
import _isEmpty from "lodash-es/isEmpty"
import { KTSVG } from "helpers";
import { setModal } from "store/modal-reducer/modalSlice";

const InfoSidebar = () => {
    const artworkSlice = useAppSelector((state) => state.artwork);
    // const [dataCategories, setDataCategories] = useState([]);
    const [stateData, setStateData] = useState<IArtwork>(artworkSlice);
    const [openModal, setOpenModal] = useState<any>({ visible: false, data: null, isReload: false });
    // const [treeExpandedKeys, setTreeExpandedKeys] = useState<any>(undefined);
    const { data: dataCategories  , refetch} = useArtworkCategories('');

    const dispatch = useAppDispatch();

    useEffect(() => {
        if (!isEqual(stateData, artworkSlice)) {
            setStateData(artworkSlice)
        }
    }, [artworkSlice, stateData]);

    const handleChangeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        const {value} = event.target;
        debounce(dispatch(updateArtwork({ ...stateData, nameArtwork: value })), 500)
        setStateData((prev) => ({ ...prev, nameArtwork: value }));
    }

    const handleCreateCategory = () => {
        setOpenModal((prev) => ({
            ...prev,
            visible: true,
        }));
    };
    const handleClose = (isReload) => {
        if(isReload){
            refetch()
        }
        setOpenModal({
            visible: false,
            data: null,
            isReload: isReload,
        });
    };

    const onSelect = (keys: any, info: any) => {
        dispatch(updateArtwork({ ...stateData, categoryArtwork: info }))
        setStateData((prev) => ({ ...prev, categoryArtwork: info }));
        // dispatch(updateStage({ ...artworkSlice, categoryArtwork: info }));
    };

    return (
        <div className="row g-4 px-2">
            <div className="col-lg-12 text-white">
                <div className="row align-items-center" >
                    <h3 className="col text-white">Name</h3>
                    <div className="col text-end">
                        <input className="form form-control"
                        onChange={handleChangeInput}
                        value={stateData.nameArtwork}  />
                    </div>
                </div>
            </div>
            <div className="col-lg-12 text-white">
                <div className="row align-items-center">
                    <h3 className="col text-white"> Category </h3>
                    <div className="col">
                        {
                            !_isEmpty(dataCategories) &&
                                <TreeSelectComponent
                                    size="large"
                                    treeIcon={false}
                                    id='parent_category_id'
                                    value={artworkSlice?.categoryArtwork?.id || null}
                                    onSelect={onSelect}
                                    className={'form-select-tree form-select-tree-solid '}
                                    showSearch
                                    treeData={dataCategories}
                                    fieldNames={{
                                        label: 'name',
                                        children: 'children',
                                        value: 'id'
                                    }}
                                    dropdownClassName="form-select-tree-dropdown"
                                    placeholder="Please parent"
                                />
                        }

                    </div>
                </div>
            </div>
            <div className="col-lg-12 text-white text-end">
                <button className="btn btn-primary text-white" onClick={handleCreateCategory} >
                <KTSVG path='/media/icons/duotune/arrows/arr075.svg' className='svg-icon-2 mr-2' />
                 Create Category
                </button>
            </div>
            <div className="col-lg-12">
                <div className="row align-items-center text-white px-2">
                    <h3 className="col-lg-3 text-white" > Size (px): </h3>
                    <div className="col-lg-8 ">
                        <span>{artworkSlice.size.width}</span> x <span>{artworkSlice.size.height}</span>
                    </div>
                    <div role="button" className="w-auto" onClick={() =>
                            dispatch(
                                setModal({
                                    key: "modal-setting-artwork",
                                    data: null,
                                })
                            )
                        }>
                    <KTSVG
                        path='/media/icons/duotune/art/art005.svg'
                        className='svg-icon-3 w-24 h-24'
                    />
                    </div>
                </div>
            </div>
            <div>
                {
                    <ModalCategory
                        visible={!!openModal.visible}
                        onClose={(isReload) => handleClose(isReload)}
                        dataCategories={dataCategories}
                    />
                }
            </div>
        </div>
    );
};

export default InfoSidebar;
