import { useEffect, useState } from "react";
import { useAppDispatch } from "hooks";
import { onUpdateAttrLayer } from "store/template-reducer/templateSlice";
import { useSelector } from "react-redux";
import { RootState } from "store";
import { closeModal } from "store/modal-reducer/modalSlice";
import { Modal } from "antd";
import ImageIcon from "@mui/icons-material/Image";
import { LoadingButton } from "@mui/lab";

const ModalReplaceImage = () => {
    const { key, data: dataModal } = useSelector((state: RootState) => state.modal)
    const [errorMessage, setErrorMessage] = useState('')
    const dispatch = useAppDispatch();
    const [loading, setLoading] = useState(false)
    const [openModal, setOpenModalMedia] = useState(null)
    let fileManager;

    useEffect(() => {
        if (openModal) openFileManager();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [openModal]);

    function createPop(url, name) {
        fileManager = window.open(url, name, "width=900,height=500,toolbar=0,menubar=0,location=0");
        if (window?.focus && fileManager) {
            fileManager.focus();
        }
        const interval = setInterval(() => {
            if (fileManager?.closed) {
                setOpenModalMedia(false)
                clearInterval(interval);
            }
        }, 1000);
    }

    const openFileManager = () => {
        createPop(`https://customproduct-dev.vellamerch.com/admin/laravel-filemanager`, "Choose Image Replace");
        window.SetUrl = function (items) {

            handleFileManager(items);
        };
        return false;
    };

    const handleFileManager = async (items = []) => {
        if (items.length === 0) return false;
        setLoading(true)
        for (let item of items) {
            const image = new Image();
            image.src = item?.url;
            const tempImage = {
                src: item?.url,
                width: 0,
                height: 0
            };

            image.onload = await function () {
                tempImage.width = image.width;
                tempImage.height = image.height;
                if (tempImage.width / tempImage.height === dataModal.width / dataModal.height) {
                    handleInsertImages({ src: image.src });
                } else {
                    setErrorMessage("Please choose image same ratio")
                }
                setLoading(false)
                return true;
            };



        }
    };

    const handleInsertImages = (item: { src: string }) => {
        const newAttrs = { ...dataModal, src: item.src };
        dispatch(onUpdateAttrLayer({ newAttrs }));
        handleClose()
    };

    const handleClose = (key?: any) => {
        setLoading(false)
        setErrorMessage('')
        return dispatch(
            closeModal()
        );
    };

    const handleOpenModalChooseImage = () => {
        if (!!errorMessage) {
            setErrorMessage('')
        }
        setOpenModalMedia(true)
    }

    return <Modal
        visible={key === "modal-replace-image"}
        onCancel={handleClose}
        destroyOnClose
        footer={false}
        confirmLoading={loading}
        title="Choose new image for this layer"

    >
        <div>
            <p>Please use an image with the same ratio as the original image</p>
            <b>Current image size (width x height): {dataModal?.width | 0} x {dataModal?.height | 0}</b>
            <p style={{ color: "crimson", fontStyle: "italic" }}>{errorMessage}</p>
            <LoadingButton
                loading={loading}
                style={{ margin: "15px 0", width: "100%" }}
                variant="outlined"
                onClick={handleOpenModalChooseImage}
                startIcon={<ImageIcon />}
            >
                Browse image...
            </LoadingButton>
        </div>
    </Modal>
};

export default ModalReplaceImage;
