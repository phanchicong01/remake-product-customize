import React, { forwardRef } from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { useAppDispatch } from "hooks";
import { onDragEndLayer } from "store/template-reducer/templateSlice";

import { Layer, Template } from "store/template-reducer/interface";
import ItemLayer from "../item-layer";

interface TabPanelProps {
    children?: React.ReactNode;
    id?: string;
    index: number;
    value: number;
}
interface IProps {
    data: Template,
    index: number,
    valueActive: number
}
const ItemTemplate = ({ data, valueActive, index, ...orderProps }: IProps) => {
    const dispatch = useAppDispatch();

    const onDragEnd = (result, idLayer) => {
        if (!result.destination) {
            return;
        }
        dispatch(
            onDragEndLayer({
                indexStart: result.source.index,
                indexEnd: result.destination.index,
            })
        );
    };

    return (
        <DragDropContext onDragEnd={(event) => onDragEnd(event, data.id)} key={data.id}>
            <Droppable droppableId="droppable-left">
                {(provided, snapshot) => (
                    <TabPanel
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                        value={valueActive}
                        index={index}
                        key={data.id}
                    >
                        {[...data.layers].reverse().map((layer: Layer, index: number) => {
                            const selected = data?.layerSelected?.some((item) => layer.id === item.id);
                            return (
                                <Draggable key={layer.id} draggableId={layer.id + ""} index={index}>
                                    {(provided, snapshot) => (
                                        <ItemLayer
                                            ref={provided.innerRef}
                                            {...provided.draggableProps}
                                            {...provided.dragHandleProps}
                                            key={index}
                                            data={layer}
                                            selected={selected}
                                            idLayer={data.id}
                                        >
                                            {layer.name}_{layer.id}
                                        </ItemLayer>
                                    )}
                                </Draggable>
                            );
                        })}
                        {provided.placeholder}
                    </TabPanel>
                )}
            </Droppable>
        </DragDropContext>
    );
};

export default ItemTemplate;
const TabPanel = forwardRef((props: TabPanelProps, dropRef: any) => {
    const { children, value, index, ...other } = props;
    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`layer-tabpanel-${index}`}
            aria-labelledby={`layer-tabpanel-${index}`}
            tabIndex={index}
            className="tab-panel"
            style={{ background: "#535353", color: "#fff", overflow: "auto", flex: 1 }}
            {...other}
            ref={dropRef}
        >
            <div>{value === index && children}</div>
        </div>
    );
});
