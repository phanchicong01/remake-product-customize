import React from "react";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Box from "@mui/material/Box";

import SettingsIcon from "@mui/icons-material/Settings";
import AddToPhotosIcon from "@mui/icons-material/AddToPhotos";

import { styled } from "@mui/material/styles";

import ActionTemplate from "../action-template";
import ActionBar from "../action-bar";

import { useAppDispatch, useAppSelector } from "hooks";
import { onAddNewTemplate, onActiveTemplate } from "store/template-reducer/templateSlice";

// import ItemLayer from "../item-layer";
import ModalImage from "../modal-image";

import styles from "./styles.module.scss";
import { Template } from "store/template-reducer/interface";
import ItemTemplate from "../item-template";
import { setModal } from "store/modal-reducer/modalSlice";
import ModalReplaceImage from "../modal-replace-image";

const ListTemplate = () => {
    const templates = useAppSelector((state) => state.template.templates);
    const templatesActive = useAppSelector((state) => state.template.templateActive);
    const dispatch = useAppDispatch();
    // const [valueActive , setActive] = useState<number>(0)


    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        // setActive(newValue);

        dispatch(onActiveTemplate(templates[newValue]));
    };

    if (templates.length <= 0) {
        return <Box sx={{ width: "100%" }}>...loading</Box>;
    }

    const handleAddNewLayer = () => {
        dispatch(onAddNewTemplate());
    };

    // useEffect(() => {
    //     const index = layers.findIndex(layer => layer.id === layerActive.id);
    //     if(index !== valueActive){
    //         setActive(index)
    //     }

    // }, [layerActive])
    const valueActive = templates.findIndex(item => item.id === templatesActive.id)
    return (
        <Box sx={{ width: "100%", background: "#464646" }}>
            <Box
                sx={{
                    borderBottom: 1,
                    borderColor: "divider",
                    color: "#fff",
                    display: "flex",
                    justifyContent: "space-between",
                }}
            >
                <Tabs
                    value={valueActive}
                    onChange={handleChange}
                    aria-label={templatesActive.name}
                    variant="scrollable"
                    scrollButtons={templates.length > 1 ? "auto" : undefined}
                >
                    {templates.map((template: Template, index: number) => {
                        if (template.id === templatesActive.id) {
                            return (
                                <StyledTab key={templatesActive.id} label={templatesActive.name} {...a11yProps(index)} />
                            )
                        }
                        return (
                            <StyledTab key={template.id} label={template.name} {...a11yProps(index)} />
                        )
                    })}
                </Tabs>
                <div className={styles.action}>
                    <AddToPhotosIcon fontSize="small" sx={{ pr: 1 }} onClick={handleAddNewLayer} />
                    <SettingsIcon
                        fontSize="small"
                        onClick={() => dispatch(setModal({ key: "modal-setting-template", data: null }))}
                    />
                </div>
            </Box>
            <ActionTemplate nameTemplate={templatesActive.name} isRemove={templates.length > 1} />
            {templates.map((template: Template, indexTemplate: number) => {
                let tempTemplate = template;
                if (tempTemplate.id === templatesActive.id) {
                    tempTemplate = templatesActive;
                }
                return <ItemTemplate key={tempTemplate.id} data={tempTemplate} index={indexTemplate} valueActive={valueActive} />;
            })}

            <ActionBar />
            <ModalImage />
            <ModalReplaceImage />
        </Box>
    );
};
export default ListTemplate;

function a11yProps(index: number) {
    return {
        id: index,
        "aria-controls": index,
    };
}
const StyledTab = styled((props: any) => <Tab disableRipple {...props} />)(({ theme }) => ({
    color: "rgba(255, 255, 255, 0.7)",

    "&.Mui-selected": {
        color: "#fff",
        backgroundColor: "#535353",
        border: "1px solid #232323",
    },
}));
