import React from "react";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import ImageIcon from "@mui/icons-material/Image";
import FormatTextdirectionRToLIcon from "@mui/icons-material/FormatTextdirectionRToL";
import { useAppDispatch, useAppSelector } from "hooks";
import { addLayer } from "store/template-reducer/templateSlice";
import { roughScale } from "utils";
import { setModal } from "store/modal-reducer/modalSlice";

import "./styles.scss";

const ActionBar = (props: any) => {
    const dispatch = useAppDispatch();
    const artwork = useAppSelector((state) => state.artwork)

    const handleAddText = () => {
        const itemText = {
            x: roughScale(artwork.size.width / 2 - 141),
            y: roughScale(artwork.size.height / 2 - 25),
        }
    

        dispatch(addLayer({ item: itemText, type: "text" }));
    };
    return (
        <>
            <Grid container justifyContent="center" sx={{ px: 2 }}>
                <Grid item sx={{ p: 1 }}>
                    <Button
                        variant="outlined"
                        onClick={() => dispatch(setModal({ key: "modal-image", data: null }))}
                        startIcon={<ImageIcon />}
                    >
                        Image
                    </Button>
                </Grid>
                <Grid item sx={{ p: 1 }}>
                    <Button variant="outlined" onClick={handleAddText} startIcon={<FormatTextdirectionRToLIcon />}>
                        Text
                    </Button>
                </Grid>
            </Grid>
        </>
    );
};

export default ActionBar;
