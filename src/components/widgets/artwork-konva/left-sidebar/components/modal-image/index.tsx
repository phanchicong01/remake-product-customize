import { useEffect } from "react";
import { useAppDispatch } from "hooks";
import { roughScale } from "utils";
import { addLayer, onUpdateAttrLayer } from "store/template-reducer/templateSlice";
import { useSelector } from "react-redux";
import { RootState } from "store";
import { closeModal } from "store/modal-reducer/modalSlice";

const ModalImage = () => {
    const { key, data: dataModal } = useSelector((state: RootState) => state.modal)
    const artwork = useSelector((state: RootState) => state.artwork)
    const dispatch = useAppDispatch();
    let fileManager;

    useEffect(() => {
        if (key === "modal-image") openFileManager();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [key]);

    function createPop(url, name) {
        fileManager = window.open(url, name, "width=900,height=500,toolbar=0,menubar=0,location=0");
        if (window?.focus && fileManager) {
            fileManager.focus();
        }
        const interval = setInterval(() => {
            if (fileManager?.closed) {
                handleClose();
                clearInterval(interval);
            }
        }, 1000);
    }

    const openFileManager = () => {
        createPop(`https://customproduct-dev.vellamerch.com/admin/laravel-filemanager`, "Choose Image");
        window.SetUrl = function (items) {

            handleFileManager(items);
        };
        return false;
    };

    const handleFileManager = (items = []) => {
        if (items.length === 0) return false;

        for (let item of items) {
            const image = new Image();
            image.src = item?.url;
            const tempImage = {
                src: item?.url,
                name: item?.url.split("/").pop(),
                id: `layer-${Date.now()}`,
                width: 350,
                height: 350,
            };
            handleInsertImages(tempImage, true);
            image.onload = function () {
                tempImage.width = image.width;
                tempImage.height = image.height;
                handleInsertImages(tempImage, false);
                return true;
            };
        }
    };

    const handleInsertImages = (item: { name: string; width: number; height: number; src: string }, loading) => {
        const newItem = { ...item };
        const ratio = item.width / item.height;
        let width = item.width < 500 ? item.width : 500;
        let height = width / ratio;

        if (height > artwork.size.height) {
            width = artwork.size.height * ratio;
            height = artwork.size.height;
        }

        // newItem["widthImage"] = roughScale(item.width);
        // newItem["heightImage"] = roughScale(item.height);
        newItem.width = roughScale(width);
        newItem.height = roughScale(height);
        newItem["x"] = roughScale(artwork.size.width / 2 - width / 2);
        newItem["y"] = roughScale(artwork.size.height / 2 - height / 2);
        if (dataModal?.id) {
            const newAttrs = { ...dataModal.data, src: item.src };
            dispatch(onUpdateAttrLayer({ newAttrs, }));
            // !loading && handleClose();
        } else {
            dispatch(addLayer({ item: newItem, type: "image" }));
            // !loading && handleClose();
        }
    };

    const handleClose = (key?: any) => {
        return dispatch(
            closeModal()
        );
    };

    return <div></div>;
};

export default ModalImage;
