import React from "react";

import ModalSettingArtwork from "./components/modal-setting-artwork";
import ModalSettingTemplate from "./components/modal-setting-template";
import InfoSidebar from "./components/Info-sidebar";
import ManageLayer from "./components/list-template";

const LeftSideBar = () => {

    return (
        <div className="row flex-column g-4" style={{ minHeight: "100vh", background: "#535353" }}>
            <h1 className="text-center text-nowrap py-2 text-white" style={{ background: "#343a40", color: "#ffff" }}>
                Customize your Artwork
            </h1>
            <div className="col-lg-12">
                <InfoSidebar />
            </div>
            <div className="col-lg-12" style={{ overflow: "auto", maxHeight:"50vh" }}>
                <ManageLayer />
            </div>
            <>
                <ModalSettingArtwork />
                <ModalSettingTemplate />
            </>
        </div>
    );
};

export default LeftSideBar;
