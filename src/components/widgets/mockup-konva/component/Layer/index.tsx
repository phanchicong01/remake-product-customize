import { useRef} from "react";
import { Layer } from "react-konva";

import ImageURL from "components/common/ImageURL";

import { TypePrintAreas } from "store/template-reducer/enum";
import PrintAreaKonva from "components/widgets/mockup-konva/component/printarea-konva";
import { PrintArea } from "store/mockup-reducer/interface";

interface IProps {
    layerSelected: PrintArea[];
    layers: PrintArea[];
    children: any;
    onChange: (data: PrintArea) => void
    onSelect: (event:any , data: PrintArea) => void
    stageScale: {
        x: number;
        y: number;
    };
}

const LayerComponent = ({
    layers,
    layerSelected,
    children,
    stageScale = { x: 1, y: 1 },
    onChange,
    onSelect
}: IProps) => {
    const refLayer = useRef<any>(null);

    if (!layers) return null;
    return (
        <Layer ref={refLayer}>
            {[...layers].map((layer: PrintArea, index: number) => {
                const selected = layerSelected?.findIndex((item) => layer.id === item.id) !== -1;
                if (layer.type === TypePrintAreas.Image && !!layer.visible) {
                    return (
                        <ImageURL
                            key={index}
                            stageScale={stageScale}
                            shapeProps={{ ...layer }}
                            isSelected={selected}
                            onSelect={(event, data)=> onSelect(event ,data.layerSelected)}
                            onChange={onChange}
                        />
                    )
                }

                if (layer.type === TypePrintAreas.PrintArea) {
                 
                    return (
                        <PrintAreaKonva
                            key={index}
                            shapeProps={{...layer}}
                            isSelected={selected}
                            onSelect={onSelect}
                            onChange={(data)=>onChange(data)}
                        />
                    );
                }
                return null;
            })}
            {children}
        </Layer>
    );
};

export default LayerComponent

