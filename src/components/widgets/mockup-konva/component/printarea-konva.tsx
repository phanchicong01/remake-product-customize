import React from "react";
import { Text ,  Tag } from "react-konva";
import { roughScale } from "utils";
import { PrintArea } from "store/mockup-reducer/interface";

type Props = {
    shapeProps:PrintArea,
    isSelected:boolean,
    onSelect:(event, data:PrintArea) => void,
    onChange:(dataChange:PrintArea) => void
}

const PrintAreaKonva = ({ shapeProps, isSelected, onSelect, onChange }: Props) => {
    const shapeRef = React.useRef<any>();
    
    if (!shapeProps) {
        return null;
    }
    return (
        <React.Fragment>
            {
               
                }
                 <Tag   
                    x={shapeProps.x}
                    y={shapeProps.y}
                    width={shapeProps.width}
                    height={shapeProps.height}
                    fill="#7a86bb"
                    opacity={0.5}
                    visible={shapeProps.visible}
                    />
                <Text
                    onClick={(event) => onSelect( event ,shapeProps)}
                    text={shapeProps.name + ` (${shapeProps.width} x ${shapeProps.height})`}
                    {...shapeProps}
                    align={"center"}
                    verticalAlign="middle"
                    fill="#fff"
                    fontSize={50}
                    name="shape"
                    ref={shapeRef}
                    draggable={isSelected}
                    onDragEnd={(e) => {
                        onChange({
                            ...shapeProps,
                            x: e.target.x(),
                            y: e.target.y(),
                        });
                    }}
                    onTransform={(e) => {
                        // transformer is changing scale of the node
                        // and NOT its width or height
                        // but in the store we have only width and height
                        // to match the data better we will reset scale on transform end
                        const node: any = shapeRef.current;
                        const scaleX = node.scaleX();
                        const scaleY = node.scaleY();

                        // we will reset it back
                        node.scaleX(1);
                        node.scaleY(1);
                        onChange({
                            ...shapeProps,
                            x: roughScale(node.x()),
                            y: roughScale(node.y()),
                            rotation: node.rotation(),
                            // set minimal value
                            width: roughScale(Math.max(5, node.width() * scaleX)),
                            height: roughScale(Math.max(node.height() * scaleY)),
                        });
                    }}
                />
               
        </React.Fragment>
    );
};

export default PrintAreaKonva;
