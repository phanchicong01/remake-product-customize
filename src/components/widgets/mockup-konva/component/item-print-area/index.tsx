/* eslint-disable react-hooks/exhaustive-deps */
import React, { forwardRef } from "react";

import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import CloseIcon from "@mui/icons-material/Close";

import Divider from "@mui/material/Divider";
import Typography from "@mui/material/Typography";
import { useAppDispatch } from "hooks";

import styles from "./styles.module.scss";
import { onRemovePrintArea, onSelectLayer, onUpdateAttrPrintArea } from "store/mockup-reducer/mockupSlice";
import { PrintArea } from "store/mockup-reducer/interface";

type Props =  {
    data:PrintArea,
    selected:boolean,
    [x:string]:unknown
}
const ItemPrintArea = forwardRef(({ data,  selected, ...orderProps }: Props, ref) => {
    const dispatch = useAppDispatch();
    
    const handleClickSelected = (event: React.MouseEvent<HTMLDivElement>) => {
        if (data.visible) {
            dispatch(onSelectLayer(data));
        }

    };
    const handleRemove = () => {
        dispatch(onRemovePrintArea(data.id));
    };
    
    return (
        <>
            <div className={`${styles.object} ${selected ? styles.selected : ""}`} {...orderProps} >
                <div
                    className={styles.iconObj}
                    onClick={() =>
                        dispatch(
                            onUpdateAttrPrintArea({
                                 ...data, visible: !data.visible
                            })
                        )
                    }
                >
                    {data.visible ? <VisibilityIcon fontSize="small" /> : <VisibilityOffIcon fontSize="small" />}
                </div>
                <Divider orientation="vertical" flexItem />
                <div className={styles.label}  onClick={handleClickSelected}>
                    <div className={styles.name}>
                        <Typography sx={{ fontFamily: data.fontFamily }} noWrap>
                            {data.name}
                        </Typography>{" "}
                    </div>
                </div>
                <div className={styles.action}>
                    <CloseIcon sx={{ fontSize: 14 }} onClick={handleRemove} />
                </div>
            </div>
        </>
    );
});

export default ItemPrintArea;

