import React from "react";
import Menu from "@mui/material/Menu";

import SettingsIcon from "@mui/icons-material/Settings";
import Switch from "@mui/material/Switch";
import Button from "@mui/material/Button";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import { useAppDispatch, useAppSelector } from "hooks";
import { onUpdateAttrMockup } from "store/mockup-reducer/mockupSlice";

const toggleData = ["show gird"];

const MenuSetting = () => {
    const {setting} = useAppSelector((state) => state.mockup);
    const dispatch = useAppDispatch();
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

    const open = Boolean(anchorEl);
    const handleClick = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleChangeSwitch = (event: React.ChangeEvent<HTMLInputElement>) => {
        const checked = event.target.checked;
        dispatch(onUpdateAttrMockup({
            name:"setting",
            value: { ...setting, showGrid: checked }
        }));
    };

    return (
        <div className="me-2">
            <Button
                aria-label="more"
                id="menu-zoom-button"
                aria-controls="long-menu"
                aria-expanded={open ? "true" : undefined}
                aria-haspopup="true"
                variant="contained"
                color="dimgray"
                onClick={handleClick}
                startIcon={<SettingsIcon sx={{ color: "white" }} fontSize="small" />}
            >
                    Setting
            </Button>
            <Menu
                id="menu-setting"
                MenuListProps={{
                    "aria-labelledby": "long-button",
                }}
                anchorOrigin={{
                    vertical: "top",
                    horizontal: "center",
                }}
                transformOrigin={{
                    vertical: "bottom",
                    horizontal: "center",
                }}
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                PaperProps={{
                    style: {
                        // maxHeight: ITEM_HEIGHT * 4.5,
                        width: "auto",
                        //   display:"flex",
                    },
                }}
            >
                {toggleData.map((toggleItem: string, index: number) => (
                    <FormGroup key={index}>
                        <FormControlLabel
                            control={<Switch checked={!!setting?.showGrid} onChange={handleChangeSwitch} />}
                            label={toggleItem}
                        />
                    </FormGroup>
                    //    <Switch  onChange={handleChangeSwitch} label={toggleItem} />
                ))}
            </Menu>
        </div>
    );
};

export default MenuSetting;
