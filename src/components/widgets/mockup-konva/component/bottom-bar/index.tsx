import React, { useState, useEffect, useCallback } from "react";
import { useAppDispatch, useAppSelector } from "hooks";
import { useNavigate } from "react-router-dom";

import ButtonGroup from "@mui/material/ButtonGroup";
import LoadingButton from "@mui/lab/LoadingButton";
import MenuZoom from "./menu-zoom";
import MenuSetting from "./menu-setting-gird";

import {  PRODUCT_BASE_DETAIL } from "constants/path";

import { onResetMockup } from "store/mockup-reducer/mockupSlice";
import { ModalKey } from "constants/enum";
import { setModal } from "store/modal-reducer/modalSlice";

import styles from "./styles.module.scss";

const BottomBar = () => {
    const { key } = useAppSelector((state) => state.modal);
    const mockupSlice = useAppSelector((state) => state.mockup);
    const [loadingSaveData, setLoadingSave] = useState<any>(false);
    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    const handleCancel = useCallback(() => {
        dispatch(onResetMockup())
        navigate(PRODUCT_BASE_DETAIL + "/" + mockupSlice.productBaseId);
    }, [dispatch, mockupSlice.productBaseId, navigate])

  
    const handleSetThumbnail = () => {
            setLoadingSave(true);
            dispatch(
                setModal({
                    data: true,
                    key: ModalKey.SaveData,
                })
            );
    }

    useEffect(() => {
        if (!key && loadingSaveData) {
            setLoadingSave(false)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [key]);
 
    return (
        <div className={`${styles.bottomBar}`}>
            <div className="row justify-content-around g-0">
                <div className="d-flex col">
                    <MenuZoom />
                    <MenuSetting />
                </div>
                <div className="align-items-center text-center col-lg-9" >
                    <ButtonGroup variant="outlined" aria-label="outlined button group">
                        <LoadingButton
                            loading={loadingSaveData}
                            onClick={handleSetThumbnail}
                            variant="contained"
                            size="small"
                        >
                            Save
                        </LoadingButton>
                        <LoadingButton
                            loading={loadingSaveData}
                            onClick={handleCancel} size="small">
                            Cancel
                        </LoadingButton>
                    </ButtonGroup>
                </div>
                <div className="col">

                </div>
                {/* )} */}
            </div>
        </div>
    );
};

export default BottomBar;
