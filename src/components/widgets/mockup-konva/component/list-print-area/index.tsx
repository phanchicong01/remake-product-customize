import React, { useCallback, useState } from 'react'
import { Popover } from 'antd'

import { useAppSelector } from 'hooks'
import { useDispatch } from 'react-redux'
import { addPrintAreas } from 'store/mockup-reducer/mockupSlice'
import { PrintArea } from 'store/mockup-reducer/interface'
import { PrintArea as IPrintAreaObject } from 'interfaces/product-base'
import { setModal } from 'store/modal-reducer/modalSlice'
import { TypePrintAreas } from 'store/template-reducer/enum'
import ItemPrintArea from '../item-print-area'
import ModalImageCampaign from '../modal-image-mockup'
import styles from "./styles.module.scss"
import usePrintAreas from 'data/hooks/use-print-areas'
type Props = {}


const ListPrintArea = (props: Props) => {
  const { printAreas, printAreaActive,  size} = useAppSelector(state => state.mockup)
  const [visible, handleVisibleChange] = useState<boolean>(false);

  const dispatch = useDispatch();
  const { data } = usePrintAreas()

  const handleAddPrintarea = useCallback((event: any , itemPrintArea:IPrintAreaObject) => {
    const type = event.target.getAttribute("data-type")
    const width = size.width / 2
    const height =  (size.height) / 2
   
    let newArea: Partial<PrintArea> = {
      name:itemPrintArea.name,
      width:width ,
      height:height, 
      x:(size.width - width) / 2 ,
      y:(size.height - height) / 2  ,
    }
    dispatch(addPrintAreas({ item: newArea, type: type }));
    handleVisibleChange(false)

  }, [dispatch, size.height, size.width])

  return (
    <div className={styles.listPrintArea}>
      <div className={styles.printAreaHeader}>Your Print areas</div>
      <div className={styles.listItem}>
        {
          printAreas.map((print: PrintArea) => {
            const selected = printAreaActive.some((item) => item.id === print.id)
            return <ItemPrintArea data={print} idLayer={print.id} selected={selected} />
          })
        }
      </div>
      <div className={styles.action}>
        <Popover
          content={<div className='bg-white d-flex flex-column'>
            {
              data?.map((item: IPrintAreaObject) =>
                <div onClick={(event)=> handleAddPrintarea(event, item)} 
                data-name={item.name} 
                data-type={TypePrintAreas.PrintArea} 
                className={styles.popoverContent} 
                role="button">{item.name}</div>
              )
            }
          </div>
          }
          className='me-2'
          trigger="click"
          placement='bottomRight'
          visible={visible}
          onVisibleChange={handleVisibleChange}
        >
          <button className="btn  btn-primary">Add Print Areas</button>
        </Popover>

        <div className='btn btn-primary '>

          <div
            onClick={() => dispatch(setModal({ key: "modal-image", data: null }))}
            className="upload-image-print-area">Add Image</div>
        </div>
      </div>
      <ModalImageCampaign />
    </div>
  )
}

export default ListPrintArea