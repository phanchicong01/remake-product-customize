import { Switch } from "@mui/material";
import PickColor from "components/common/pick-color";
import { useAppDispatch, useAppSelector } from "hooks";
import { onUpdateAttrMockup } from "store/mockup-reducer/mockupSlice";

const InfoSidebar = () => {
    const {backgroundColor , backgroundVariantColor} = useAppSelector((state:any)=> state.mockup)
const dispatch = useAppDispatch();
  
    const handlePickColor = (color:string ) => {
        dispatch(onUpdateAttrMockup({
            name:'backgroundColor',
            value:color
        }))
    }
    const handleChangeSwitch = (event: React.ChangeEvent<HTMLInputElement>) => {
        const checked = event.target.checked;
        dispatch(onUpdateAttrMockup({
            name:'backgroundVariantColor',
            value:checked
        }))
    }
    return (
        <div className="row px-4 g-4" >
        <div className="col-lg-12">
            <div className="row text-white ">
                <label className="col" > Background by variant color </label>
                <div className="col">
                <Switch checked={backgroundVariantColor} onChange={handleChangeSwitch} />
                </div>
            </div>
        </div>
        <div className="col-lg-12">
            <div className="row text-white ">
               <div className="col">Background color for PNG (*)</div>
               <div className="col">
               <PickColor id={"pick-color-print-area"+1} color={backgroundColor} onChange={handlePickColor} />
               </div>
            </div>
        </div>
     
    </div>
    );
};

export default InfoSidebar;
