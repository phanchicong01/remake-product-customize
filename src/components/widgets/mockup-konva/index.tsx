import { useAppDispatch, useAppSelector } from "hooks";
import { useCallback, useEffect, useMemo, useRef, useState } from "react";
import { Stage, Transformer , Tag, Layer, Image} from "react-konva";

import LayerComponent from "./component/Layer";

import GridView from "components/common/gird-view";
import ModalPreviewBeforeSave from "./component/modal-preview-before-save";
import { ModalKey } from "constants/enum";

import styles from "./styles.module.scss";
import { PrintArea } from "store/mockup-reducer/interface";
import { onSelectLayer, onUpdateAttrPrintArea, setThumbnail } from "store/mockup-reducer/mockupSlice";
import { getUrl } from "helpers";
import useImage from "use-image";

interface IProps {
    children?: any;
}

const ID_STAGE_AREA = "stage-campaign-area";
const ID_STAGE_CONTAINER = "stage-campaign-container";

const MockupKonvaComponent = (props: IProps) => {
    const mockupSlice = useAppSelector((state) => state.mockup);
    const {  key } = useAppSelector((state) => state.modal);
    const {printAreaActive , setting , size , printAreas , backgroundImage , backgroundColor } = mockupSlice

    const dispatch = useAppDispatch();
    const [positionStage, setPositionStage] = useState<{ top: number; left: number }>({ top: 0, left: 0 });
    
    const trRef = useRef<any>(null);
    const refStage = useRef<any>(null);

    useMemo(() => {
        if (key === ModalKey.SaveData && refStage.current) {
            trRef?.current.setAttrs({
                visible: false,
            });
            dispatch(setThumbnail(refStage.current.toDataURL()));

            const timeOut = setTimeout(() => {
                trRef?.current.setAttrs({
                    visible: true,
                });
                clearTimeout(timeOut)
            }, 100)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [refStage.current, key]);

    const widthStage = size.width * setting.scale.x;
    const heightStage = size.height * setting.scale.y;

    useEffect(() => {
        var container: any = document.getElementById(ID_STAGE_CONTAINER);
        var containerWidth = container?.offsetWidth;
        var containerHeight = container.offsetHeight;

        const topObject = containerHeight / 2 - heightStage / 2;
        const leftObject = containerWidth / 2 - widthStage / 2;

        setPositionStage({
            top: topObject < 0 ? 0 : topObject,
            left: leftObject < 0 ? 0 : leftObject,
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [setting.scale]);



    const handleOnSelect =  useCallback((event:any,data:PrintArea) => {
        // trRef.current.nodes([event.target]);
        // trRef.current.getLayer().batchDraw();
        dispatch(onSelectLayer(data));
    }, [dispatch])

    const handleOnChange = (newAttr:PrintArea) => {
             dispatch(onUpdateAttrPrintArea(newAttr))
    }

    useEffect(() => {
        if(printAreaActive?.length > 0) {
            const shapes = refStage.current?.find(".shape");
            const shapeSelect = shapes.find((shape) => shape.attrs.id === printAreaActive[0].id)
            trRef.current.nodes([shapeSelect]);
            trRef.current.getLayer().batchDraw();
        }

    }, [printAreaActive])
    
    const [img] = useImage(getUrl(backgroundImage));
    const refContainer = useRef(null);
    return (
        <div id={ID_STAGE_AREA} className={styles.stageArea} >
            <div className={styles.containerStage} id={ID_STAGE_CONTAINER} ref={refContainer}>
                {setting.showGrid && (
                    <div
                        className={styles.gridView}
                        style={{
                            width: widthStage,
                            height: heightStage,
                            top: `${positionStage.top}px`,
                        }}
                    >
                        <GridView
                            width={size.width * setting.scale.x}
                            height={size.height * setting.scale.y}
                        />
                    </div> 
                )}

                <Stage
                    width={size.width * setting.scale.x}
                    height={size.height * setting.scale.y}
                    scale={setting.scale}
                    ref={refStage}
                    className={styles.stage}
                    style={{
                        width: size.width * setting.scale.x,
                        height: size.height * setting.scale.y,
                        // top: `${positionStage.top}px`,
                        position: "relative",
                        // backgroundImage:`url(${getUrl(backgroundImage)})`,
                        // backgroundColor:backgroundColor
                       
                    }}
                
                >
                    <Layer>
                        <Tag
                            width={size.width }
                            height={size.height }
                            x={0}
                            y={0}
                            fill={backgroundColor}
                        />
                        <Image
                            width={size.width }
                            height={size.height }
                            name=""
                            x={0}
                            y={0}
                            image={img}
                        />
                    </Layer>
                    
                    {printAreas && Array.isArray(printAreas) && (
                        <LayerComponent
                            layers={printAreas}
                            layerSelected={printAreaActive}
                            stageScale={setting.scale}
                            onChange={handleOnChange}
                            onSelect={handleOnSelect}
                        >
                           
                            
                           
                            <Transformer
                            
                            key={'Transformer'}
                            ref={trRef}
                            rotateEnabled={true}
                            name="transformer"
                            // keepRatio={true}
                            shouldOverdrawWholeArea
                            // enabledAnchors={["top-left", "top-right", "bottom-left", "bottom-right"]}
                            boundBoxFunc={(oldBox, newBox) => {
                                // limit resize
                                if (newBox.width < 5 || newBox.height < 5) {
                                    return oldBox;
                                }
                                return newBox;
                            }}
                        />
                        
                        </LayerComponent>
                    )}
                </Stage>
            </div>
            <ModalPreviewBeforeSave />

        </div>
    );
};

export default MockupKonvaComponent;
