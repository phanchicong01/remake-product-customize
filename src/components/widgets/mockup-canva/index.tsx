import {  useEffect, useRef, useState } from "react";
import { Stage,  Tag, Layer, Image as ImageKonva} from "react-konva";



import styles from "./styles.module.scss";
import useImage from "use-image";

interface IProps {
   
    imgSrc?:any // image mockup of category
   color:string
   dataMockup?:{
        x:number,
        y:number,
        width:number,
        height:number,
        dpi:number,
        src:any //image design seleceted
   }
}

const ID_STAGE_AREA = "stage-campaign-area";

const MockupCanvaComponent = ({dataMockup,color = "rgb(22, 37, 71)" , imgSrc}:IProps) => {
    
    const refStage = useRef<any>(null);

    const widthStage = 1024 / 3;
    const heightStage = 1102 / 3;




    const [imgDesign, setImgDesign] = useState(null)
    useEffect(() => {
        if (!!dataMockup?.src) {
            if(typeof dataMockup?.src === "string"){
                setImgDesign(dataMockup?.src)
            }else{
                let reader = new FileReader();
                reader.onloadend = () => {
                    setImgDesign(reader.result)
                };
                reader.readAsDataURL(dataMockup?.src);
            }
        }
    }, [dataMockup?.src])
    const [img] = useImage(imgSrc);
    
   
    
    const [imgAreaDesign] = useImage(imgDesign);
    if(!img){
        return <div>Loading....</div>
    }
    return (
        <div id={ID_STAGE_AREA} className={styles.stageArea} >
                <Stage
                    width={widthStage}
                    height={heightStage}
                    ref={refStage}
                 
                    className={styles.stage}
                    style={{
                        width: widthStage,
                        height: heightStage,
                        // top: `${positionStage.top}px`,
                        position: "relative",
                        // backgroundImage:`url(${getUrl(backgroundImage)})`,
                        // backgroundColor:backgroundColor
                    }}
                
                >
                    <Layer>
                        <Tag
                             width={widthStage}
                             height={widthStage / (1024 / 1102) }
                            x={0}
                            y={0}
                            fill={color}
                        />
                    </Layer>
                    <Layer>
                        <ImageKonva
                            width={widthStage }
                            height={widthStage / (1024 / 1102) }
                            name=""
                            x={0}
                            y={0}
                            image={img}
                        />
                    </Layer>
                    {/* area design */}
                    {
                        <Layer>
                            <ImageKonva 
                                width={dataMockup.width }
                                height={dataMockup.height}
                                name=""
                                // calculate center area-design = ratio original image mockup - 1/2 ratio image design
                                x={dataMockup.x}
                                y={dataMockup.y}
                                image={imgAreaDesign}
                                stroke={"#fff"}
                            />
                        </Layer>
                    }
                    
                </Stage>
                
            {/* </div> */}

        </div>
    );
};

export default MockupCanvaComponent;
