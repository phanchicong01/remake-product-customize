import { useEffect } from "react";
import { useAppDispatch } from "hooks";
import { roughScale } from "utils";
import { useSelector } from "react-redux";
import { RootState } from "store";
import { closeModal } from "store/modal-reducer/modalSlice";
import { TypePrintAreas } from "store/template-reducer/enum";
import { addPrintAreas } from "store/mockup-reducer/mockupSlice";

const ModalImageCampaign = () => {
    const { key } = useSelector((state: RootState) => state.modal)
    const {size } = useSelector((state: RootState) => state.mockup)
    const dispatch = useAppDispatch();
    let fileManager;

    useEffect(() => {
        if (key === "modal-image") openFileManager();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [key]);

    function createPop(url, name) {
        fileManager = window.open(url, name, "width=900,height=500,toolbar=0,menubar=0,location=0");
        if (window?.focus && fileManager) {
            fileManager.focus();
        }
        const interval = setInterval(() => {
            if (fileManager?.closed) {
                handleClose();
                clearInterval(interval);
            }
        }, 1000);
    }

    const openFileManager = () => {
        createPop(`https://customproduct-dev.vellamerch.com/admin/laravel-filemanager`, "Choose Image");
        window.SetUrl = function (items) {

            handleFileManager(items);
        };
        return false;
    };

    const handleFileManager = (items = []) => {
        if (items.length === 0) return false;

        for (let item of items) {
            const image = new Image();
            image.src = item?.url;
            const width = size.width / 2
            const height =  (size.height) / 2
            const tempImage = {
                src: item?.url,
                name: item?.url.split("/").pop(),
                id: `layer-${Date.now()}`,
                width:width ,
                height:height, 
                x:(size.width - width) / 2 ,
                y:(size.height - height) / 2  ,
            };
            image.onload = function () {
                tempImage.width = image.width;
                tempImage.height = image.height;
                handleInsertImages(tempImage);
                return true;
            };
        }
    };

    const handleInsertImages = (item: { name: string; width: number; height: number; src: string }) => {
        const newItem = { ...item };
        const ratio = item.width / item.height;
        let width = item.width < 500 ? item.width : 500;
        let height = width / ratio;

        if (height > size.height) {
            width = size.height * ratio;
            height = size.height;
        }

        // newItem["widthImage"] = roughScale(item.width);
        // newItem["heightImage"] = roughScale(item.height);
        newItem.width = roughScale(width);
        newItem.height = roughScale(height);
        newItem["x"] = roughScale(size.width / 2 - width / 2);
        newItem["y"] = roughScale(size.height / 2 - height / 2);
        // if (dataModal?.id) {
        //     const newAttrs = { ...dataModal.data, src: item.src };
        //     dispatch(onUpdateAttrLayer({ newAttrs, }));
        //     // !loading && handleClose();
        // } else {
        //     dispatch(addLayer({ item: newItem, type: "image" }));
        //     // !loading && handleClose();
        // }
      
        dispatch(addPrintAreas({item:newItem , type:TypePrintAreas.Image}));
        handleClose();
    };

    const handleClose = (key?: any) => {
        return dispatch(
            closeModal()
        );
    };

    return <div></div>;
};

export default ModalImageCampaign;
