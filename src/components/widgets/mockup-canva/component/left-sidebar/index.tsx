import React from "react";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";

import { useAppSelector } from "hooks";
import InfoSidebar from "../Info-sidebar";
import ListPrintArea from "../list-print-area";

const LeftSideBar = () => {
    const mockupSlice = useAppSelector((state) => state.mockup);
    const { size } = mockupSlice
   
    return (
        <Grid container style={{ height: "100vh", background: "#535353" }} direction="column" rowGap={2}>
            <Grid item>
                <Grid
                    container
                    rowSpacing={2}
                    // spacing={2}
                    style={{ minHeight: 100 }}
                    alignItems="center"
                    rowGap={4}
                >
                    <Grid item xs={12} sx={{ background: "#343a40", color: "#ffff" }}>
                        <Typography align={"center"} noWrap sx={{ py: 1 }}>
                            Customize your Campaign
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <InfoSidebar />
                    </Grid>
                    <Grid item xs={12} alignItems={"center"} rowSpacing={2} sx={{ color: "#fff" }}>
                        <Grid container alignItems="center" sx={{ p: 2 }}>
                            <Grid item xs={3}>
                                <Typography sx={{ fontWeight: "bold" }}> Size (px): </Typography>
                            </Grid>
                            <Grid item xs={8}>
                                <span>{size.width}</span> x <span>{size.height}</span>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item container style={{ flex: 1, overflow: "auto" }}>
                <ListPrintArea />
            </Grid>
        </Grid>
    );
};

export default LeftSideBar;
