import React, { useEffect } from "react";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import ZoomInIcon from "@mui/icons-material/ZoomIn";
import { useAppDispatch, useAppSelector } from "hooks";
import { onUpdateAttrMockup } from "store/mockup-reducer/mockupSlice";

const toggleData = [120, 100, 75, 50, 33, 25, 10];

const MenuZoom = () => {
    const {setting} = useAppSelector((state) => state.mockup);

    const dispatch = useAppDispatch();
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const [valueActive, setActive] = React.useState<any>(100);

    const open = Boolean(anchorEl);
    const handleClick = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleChangeOption = (event: React.MouseEvent<HTMLElement>, item: number) => {

        getItemActive(item, (item) => item && setActive(item));
        dispatch(onUpdateAttrMockup({
            name:"setting",
            value: {
                ...setting,
                scale:{
                    x: item / 100,
                    y: item / 100
                }
            }
        } ))
        handleClose();
    };

    const getItemActive = (value, callback) => {
        const index = toggleData.findIndex((item) => item === value);

        if (index !== -1) {
            callback(toggleData[index]);
        }
    };

    useEffect(() => {
        const value = setting.scale.x * 100;
        getItemActive(value, (item) => item && setActive(item));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div className="me-2">
            <Button
                aria-label="more"
                id="menu-zoom-button"
                aria-controls="long-menu"
                aria-expanded={open ? "true" : undefined}
                aria-haspopup="true"
                variant="contained"
                onClick={handleClick}
                color="dimgray"
                startIcon={<ZoomInIcon fontSize="small" />}
            >
                {valueActive}%
            </Button>
            <Menu
                id="menu-zoom"
                MenuListProps={{
                    "aria-labelledby": "long-button",
                }}
                anchorOrigin={{
                    vertical: "top",
                    horizontal: "center",
                }}
                transformOrigin={{
                    vertical: "bottom",
                    horizontal: "center",
                }}
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                PaperProps={{
                    style: {
                        // maxHeight: ITEM_HEIGHT * 4.5,
                        width: "10ch",
                        //   display:"flex",
                    },
                }}
            >
                {toggleData.map((toggleItem: number, index: number) => (
                    <MenuItem
                        key={toggleItem}
                        selected={toggleItem === valueActive}
                        onClick={(event) => handleChangeOption(event, toggleItem)}
                    >
                        {toggleItem}%
                    </MenuItem>
                ))}
            </Menu>
        </div>
    );
};

export default MenuZoom;
