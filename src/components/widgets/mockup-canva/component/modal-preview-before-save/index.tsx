import { useCallback } from "react";
import { useAppDispatch, useAppSelector } from "hooks";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import { useMutation } from "react-query";
import { useParams, useNavigate } from "react-router-dom";


import { RootState } from "store";
import { closeModal } from "store/modal-reducer/modalSlice";

import { Box, Button, Modal, Stack } from "@mui/material";
import { ModalKey } from "constants/enum";
import LoadingButton from "@mui/lab/LoadingButton"
import { onResetMockup } from "store/mockup-reducer/mockupSlice";
import { PRODUCT_BASE_DETAIL } from "constants/path";
import { UpdateMockupRequest } from "apis/update-mockup/interface";
import updateMockup from "apis/update-mockup";

import styles from "./styles.module.scss"
import Loading from "components/common/loading-metronic-style";


const ModalPreviewBeforeSave = () => {
    const { key } = useSelector((state: RootState) => state.modal)
    const mockupSlice = useAppSelector((state) => state.mockup);
    const { size, setting, thumbnail } = mockupSlice

    const dispatch = useAppDispatch()
    const { idMockup } = useParams()

    const navigate = useNavigate();

    const useMutationUpdateMockup = useMutation((payload: UpdateMockupRequest) => updateMockup(payload))




    const handleCancel = useCallback(() => {
        dispatch(onResetMockup())
        navigate(PRODUCT_BASE_DETAIL + "/" + mockupSlice.productBaseId);
    }, [dispatch, mockupSlice.productBaseId, navigate])

    const handleSaveData = useCallback(async () => {
        const { printAreaActive, ...rest } = mockupSlice
        try {
            const newData: UpdateMockupRequest = {
                id: parseInt(idMockup),
                data: {
                    content: JSON.stringify(rest),
                    product_base_id: mockupSlice.productBaseId,
                    is_background_by_variable_color: rest.backgroundVariantColor ? 1 : 0
                }
            }
            const result = await useMutationUpdateMockup.mutateAsync(newData);
            if (result.code === 200) {
                toast.success(result.message);
                handleCancel()
            } else {
                toast.error(result.message)
            }
        } catch (error) {
            console.error(error);
            toast.error("Something went wrong. Please try again!")
        }

    }, [handleCancel, idMockup, mockupSlice, useMutationUpdateMockup])

    const handleClose = (event: any, reason?: any) => {
        if (reason === "backdropClick") {
            return;
        }
        return dispatch(
            closeModal()
        );
    };

    return <Modal
        open={key === ModalKey.SaveData}
        onClose={handleClose}
        sx={{
            zIndex: 1009,
            maxHeight: "100vh",
            overflow: "auto"
        }}

    >
        <Box sx={style}  >
            <h2 id="parent-modal-title">Preview artwork before save </h2>
            <div>

                {
                    !!mockupSlice &&
                    <div className={styles.waitingSaveContent} style={{ width: size.width * setting.scale.x, height: size.height * setting.scale.y }}>
                        {
                            useMutationUpdateMockup.isLoading &&
                            <div className={styles.backdrop}>
                                <Loading />
                            </div>
                        }
                        <img src={thumbnail} alt="thumbnail" style={{ width: "100%", height: "100%" }} />

                    </div>
                }
            </div>
            <Stack spacing={2} sx={{ pt: 4, textAlign: "right" }} justifyContent="right" direction="row">
                <LoadingButton loading={useMutationUpdateMockup.isLoading} variant="contained" onClick={handleSaveData}>
                    Confirm
                </LoadingButton>
                <Button variant="outlined" onClick={handleClose}>
                    cancel
                </Button>
            </Stack>
        </Box>
    </Modal >
};

export default ModalPreviewBeforeSave;
const style = {
    position: "absolute" as "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
    // height: 500,
    overflow: "auto",
};
