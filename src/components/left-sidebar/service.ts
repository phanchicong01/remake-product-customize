import { callApiFuc } from "server/fetcher";

export const getAllCategories = async (): Promise<any> => {
    const response = await callApiFuc("/api/artwork-categories")
        .then((rs) => {
            return rs.data;
        })
        .catch((err) => console.error(err));
    return response;
};
interface dataCategory {
    name: string;
    parent_id?: number;
    sort?: number;
}
export const createCategories = async (data: dataCategory): Promise<any> => {
    const response = await callApiFuc("/api/artwork-categories", "POST", data)
        .then((rs) => {
            return rs;
        })
        .catch((err) => console.error(err));
    return response;
};
