/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, forwardRef, useRef, useEffect } from "react";

import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import EditIcon from "@mui/icons-material/Edit";
import CloseIcon from "@mui/icons-material/Close";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import SwapHorizIcon from "@mui/icons-material/SwapHoriz";
import ImageIcon from "@mui/icons-material/Image";
import FormatTextdirectionRToLIcon from "@mui/icons-material/FormatTextdirectionRToL";

import Divider from "@mui/material/Divider";
import Typography from "@mui/material/Typography";
import { useAppDispatch } from "hooks";

import { onSelectMultipleLayer, onDuplicateLayer, onUpdateAttrLayer, onSelectLayer, onRemoveLayerByID } from "store/template-reducer/templateSlice";
import InputBase from "@mui/material/InputBase";

import { setModal } from "store/modal-reducer/modalSlice";
import { updateDrawer } from "store/drawer-menu/drawerSlice";

import { alpha, styled } from "@mui/material/styles";
import styles from "./styles.module.scss";

const ItemLayer = forwardRef(({ data, idLayer, selected, ...orderProps }: any, ref) => {
    const refInputName = useRef(null);
    const dispatch = useAppDispatch();
    const [editName, setEditName] = useState({
        visible: false,
        value: data?.name || "",
    });

    const handleDoubleClick = () => {
        setEditName((prev) => ({ ...prev, value: data.name, visible: true }));
        refInputName.current?.focus();
    };
    const handleBlur = () => {
        const newAttrs = { ...data, name: editName.value };
        dispatch(
            onUpdateAttrLayer({
                newAttrs,
            })
        );
        setEditName({
            visible: false,
            value: "",
        });
    };
    const handleChangeName = (event: any) => {
        const target = event.target;
        const value = target.value;
        setEditName((prev) => ({ ...prev, value: value }));
    };
    const handleClickSelected = (event: React.MouseEvent<HTMLDivElement>) => {
        if (event.ctrlKey) {
            dispatch(onSelectMultipleLayer({ status: !selected, layerSelected: data }));
        } else {
            dispatch(onSelectLayer({ status: !selected, layerSelected: data }));
        }

    };
    const handleDuplicateLayer = () => {
        const newIdLayer = `layer-${Date.now()}`;
        dispatch(
            onDuplicateLayer({
                idLayer: data.id,
                newIdLayer,
            })
        );
        // dispatch(
        //     onDuplicateObjectOptions({
        //         idTemplate: idLayer,
        //         idObject: data.id,
        //         newIdObject:newIdLayer,
        //     })
        // );
    };
    const handleRemove = () => {
        dispatch(onRemoveLayerByID({ idLayer: data.id }));
        // dispatch(onRemoveTemplate({ indexTemplate: indexLayer, idObject: data.id }));
    };
    useEffect(() => {
        if (editName.visible && !selected) {
            handleBlur();
        }
    }, [selected]);

    const switchIconName = (type) => {
        switch (type) {
            case "image":
                return <ImageIcon fontSize="small" sx={{ pr: 1 }} />;
            default:
                return <FormatTextdirectionRToLIcon fontSize="small" sx={{ pr: 1 }} />;
        }
    };
    const handleEditLayer = (data) => {
        dispatch(updateDrawer({ open: true, data: data }))
        dispatch(onSelectLayer({ status: true, layerSelected: data }))
    }

    return (
        <>
            <div className={`${styles.object} ${selected ? styles.selected : ""}`} {...orderProps} ref={ref}>
                <div
                    className={styles.iconObj}
                    onClick={() =>
                        dispatch(
                            onUpdateAttrLayer({
                                newAttrs: { ...data, visible: !data.visible },
                            })
                        )
                    }
                >
                    {data.visible ? <VisibilityIcon fontSize="small" /> : <VisibilityOffIcon fontSize="small" />}
                </div>
                <Divider orientation="vertical" flexItem />
                <div className={styles.label} onDoubleClick={handleDoubleClick} onClick={handleClickSelected}>
                    {editName.visible ? (
                        <InputEditName
                            ref={refInputName}
                            value={editName.value}
                            onChange={handleChangeName}
                            onBlur={handleBlur}
                        />
                    ) : (
                        <div className={styles.name}>
                            {switchIconName(data.type)}{" "}
                            <Typography sx={{ fontFamily: data.fontFamily }} noWrap>
                                {data.name}
                            </Typography>{" "}
                        </div>
                    )}
                </div>
                <div className={styles.action}>
                    {/* {!editName.visible ? (
                        <EditIcon onClick={handleDoubleClick} sx={{ fontSize: 14 }} />
                    ) : (
                        <SaveAsIcon sx={{ fontSize: 14 }} onClick={handleBlur} />
                    )} */}
                    <EditIcon
                        onClick={() => handleEditLayer(data)}
                        sx={{ fontSize: 14 }}
                    />
                    {data.type === "image" && (
                        <SwapHorizIcon
                            sx={{ fontSize: 14 }}
                            onClick={() => dispatch(setModal({ key: "modal-replace-image", data }))}
                        />
                    )}
                    <ContentCopyIcon sx={{ fontSize: 14 }} onClick={handleDuplicateLayer} />
                    <CloseIcon sx={{ fontSize: 14 }} onClick={handleRemove} />
                </div>
            </div>
        </>
    );
});

export default ItemLayer;

const InputEditName = styled(InputBase)(({ theme }: any) => ({
    "&.MuiInputBase-root": {
        borderRadius: "5px",

        transition: theme.transitions.create(["border-color", "background-color", "box-shadow"]),
    },
    "& .MuiInputBase-input": {
        background: "white",
        borderRadius: "5px",
        padding: "5px 15px",
    },
    "&:focus": {
        boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
        borderColor: theme.palette.primary.main,
    },
}));
