import React, { useState, useEffect } from "react";
import InputCustom from "components/common/InputCustom";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import AddIcon from "@mui/icons-material/Add";
import { getAllCategories } from "components/left-sidebar/service";
import { useAppSelector, useAppDispatch } from "hooks";
import ModalCategory from "../modal-category";
import { TreeSelect } from "antd";
import { IArtwork } from "store/artwork-reducer/interface";
import { updateArtwork } from "store/artwork-reducer/artworkSlice";
import isEqual from "lodash/isEqual"
import { debounce } from "utils";

const InfoSidebar = () => {
    const artworkSlice = useAppSelector((state) => state.artwork);
    const [dataCategories, setDataCategories] = useState([]);

    const [stateData, setStateData] = useState<IArtwork>(artworkSlice);
    const [openModal, setOpenModal] = useState<any>({ visible: false, data: null, isReload: false });
    // const [treeExpandedKeys, setTreeExpandedKeys] = useState<any>(undefined);

    const dispatch = useAppDispatch();

    useEffect(() => {
        if (openModal.isReload) {
            getAllCategories().then((res) => {
                if (openModal.isReload) {
                    setOpenModal((prev) => ({ ...prev, isReload: false }));
                }

                if (res) {
                    setDataCategories(res);
                }
            });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [openModal]);


    const generateList = (data, preKey = null) => {
        return data.map((i) => {
            const key = preKey ? `${preKey}-${i.id}` : `${i.id}`;
            i.key = key;
            if (i.children) {
                generateList(i.children, key);
            }
            return i;
        });
    };

    useEffect(() => {
        if (!isEqual(stateData, artworkSlice)) {
            setStateData(artworkSlice)
        }
    }, [artworkSlice, stateData]);

    useEffect(() => {
        getAllCategories().then((res) => {
            if (res) {
                // const data = res.find((item) => item.id == stageSlice.categoryArtworkId);
                const data = generateList(res);
                setDataCategories(data);
            }
        });
    }, []);


    const handleChangeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = event.target.value;
        debounce(dispatch(updateArtwork({ ...stateData, nameArtwork: value })), 500)
        setStateData((prev) => ({ ...prev, nameArtwork: value }));
    }

    const handleCreateCategory = () => {
        setOpenModal((prev) => ({
            ...prev,
            visible: true,
        }));
    };
    const handleClose = (isReload) => {
        setOpenModal({
            visible: false,
            data: null,
            isReload: isReload,
        });
    };

    const handleChangeTree = (value, label, extra) => {
        console.log({ value, label, extra }, "value");
    };
    const onSelect = (keys: any, info: any) => {
        dispatch(updateArtwork({ ...stateData, categoryArtwork: info }))
        setStateData((prev) => ({ ...prev, categoryArtwork: info }));
        // dispatch(updateStage({ ...artworkSlice, categoryArtwork: info }));
    };
    if (!dataCategories || !stateData) {
        return <div style={{ fontFamily: "corinthia-bold" }}>loading ....</div>;
    }

    return (
        <Grid container sx={{ p: 2 }}>
            <Grid item xs={12} sx={{ color: "#ffff" }}>
                <InputCustom label={"Name"} value={stateData.nameArtwork} onChange={handleChangeInput} />
            </Grid>
            <Grid item xs={12} sx={{ color: "#ffff", py: 2 }}>
                <Grid container>
                    <Grid item xs={4}>
                        <Typography sx={{ fontWeight: "bold" }}> Category </Typography>
                    </Grid>
                    <Grid item xs={8}>
                        {dataCategories && (
                            <TreeSelect
                                // treeDataSimpleMode
                                showSearch
                                style={{ width: "100%", borderRadius: 5 }}
                                value={stateData.categoryArtwork?.id}
                                dropdownStyle={{ maxHeight: 400, overflow: "auto" }}
                                placeholder="Please select"
                                allowClear
                                size={"large"}
                                treeDefaultExpandAll
                                onChange={handleChangeTree}
                                onSelect={onSelect}
                                showCheckedStrategy="SHOW_PARENT"
                                treeData={dataCategories}
                                fieldNames={{ label: "name", value: "id", children: "children" }}
                            />
                        )}
                    </Grid>
                </Grid>
            </Grid>
            <Grid item xs={12} sx={{ color: "#ffff", textAlign: "right" }}>
                <Button startIcon={<AddIcon />} onClick={handleCreateCategory} sx={{ color: "#fff" }}>
                    Create Category
                </Button>
            </Grid>
            <Grid>
                {
                    <ModalCategory
                        visible={!!openModal.visible}
                        onClose={(isReload) => handleClose(isReload)}
                        dataCategories={dataCategories}
                    />
                }
            </Grid>
        </Grid>
    );
};

export default InfoSidebar;
