import React from "react";

import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";
import Divider from "@mui/material/Divider";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";

import { useAppSelector, useAppDispatch } from "hooks";

import { onUpdateSettingTemplate } from "store/artwork-reducer/artworkSlice";
import { closeModal } from "store/modal-reducer/modalSlice";
import { typeDisplayOption } from "store/artwork-reducer/interface";

const ModalSettingTemplate = () => {
    const settingTemplate = useAppSelector((state) => state.artwork.setting.template);
    const {key} = useAppSelector((state) => state.modal);
    const dispatch = useAppDispatch();

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newAttrs = { ...settingTemplate, displayOption: (event.target as HTMLInputElement).value as typeDisplayOption };
        dispatch(onUpdateSettingTemplate(newAttrs));
    };

    const handleClose = () => {
        dispatch(closeModal());
    };

    return (
        <Modal open={key === "modal-setting-template"} onClose={handleClose} >
            <Box sx={style}>
                <h2 id="parent-modal-title">Template Setting</h2>
                <Divider />

                <FormControl component="fieldset">
                    <FormLabel component="h3" sx={{ fontWeight: 700, color: "#000" }}>
                        Display Template options as
                    </FormLabel>
                    <RadioGroup
                        aria-label="gender"
                        name="controlled-radio-buttons-group"
                        value={settingTemplate.displayOption}
                        onChange={handleChange}
                    >
                        <FormControlLabel value="Radio" control={<Radio />} label="Radio Button" />
                        <FormControlLabel value="Dropdown" control={<Radio />} label="Dropdown List" />
                    </RadioGroup>
                </FormControl>
                <div>
                    <i style={{ color: "#6c757d", fontSize: 12 }}>
                        Note: if a template includes a thumbnail image, the Template options will show as Image Radio
                        instead
                    </i>
                </div>
                <Divider sx={{ my: 2 }} />
                <TextField
                    label="Label of template options:"
                    name="name"
                    id="outlined-font-size-small"
                    onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                        dispatch(onUpdateSettingTemplate({ ...settingTemplate, label: event.target.value }))
                    }
                    fullWidth
                    type="text"
                    value={settingTemplate.label}
                    size="small"
                />
                <div style={{ textAlign: "right", marginTop: "1rem" }}>
                    <Button onClick={handleClose} variant="contained">
                        Done
                    </Button>
                </div>
            </Box>
        </Modal>
    );
};

export default ModalSettingTemplate;
const style = {
    position: "absolute" as "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
    // height: 500,
    width: 400,
    overflow: "auto",

    // maxHeight:"calc(100vh - 64px)";
};
