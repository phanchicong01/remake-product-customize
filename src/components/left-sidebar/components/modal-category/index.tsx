import React, { useState } from "react";

import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import { createCategories } from "components/left-sidebar/service";
import { TreeSelect, Modal, Input } from "antd";

const ModalCategory = ({ dataCategories, visible, onClose }) => {
    const [stateData, setStateData] = useState<any>({
        name: "",
        parent_id: undefined,
    });
    const [loading, setLoading] = useState(false);

    const handleChangeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = event.target.value;
        setStateData((prev) => ({ ...prev, name: value }));
    };

    const handleClose = () => {
        onClose(false);
        setStateData({
            name: "",
            parent_id: undefined,
        });
    };
    const handleCreateCategory = () => {
        const tempData = {
            name: stateData.name,
            parent_id: stateData.parent_id || null,
        };
        Object.keys(tempData).forEach((key) => {
            if (tempData[key] === undefined || tempData[key] === null) {
                delete tempData[key];
            }
        });
        setLoading(true);
        createCategories(tempData)
            .then((res) => {
                onClose(true);
                setLoading(false);
            })
            .catch((err) => {
                setLoading(false);
                console.error(err);
            });
    };
    const handleChangeTree = (value) => {
        setStateData((prev) => ({ ...prev, parent_id: value }));
    };
    return (
        <Modal
            visible={visible}
            onCancel={handleClose}
            destroyOnClose
            onOk={handleCreateCategory}
            okText={"Create"}
            confirmLoading={loading}
        >
            <div>
                <Grid container rowSpacing={2}>
                    <Grid item xs={12} textAlign="left">
                        <Typography sx={{ fontWeight: "bold" }}> Parent Category: </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <TreeSelect
                            treeDataSimpleMode
                            showSearch
                            style={{ width: "100%", borderRadius: 5 }}
                            value={stateData.parent_id}
                            dropdownStyle={{ maxHeight: 400, overflow: "auto" }}
                            placeholder="Please select"
                            allowClear
                            treeDefaultExpandAll
                            size={"large"}
                            onChange={handleChangeTree}
                            treeData={dataCategories}
                            fieldNames={{ label: "name", value: "id", children: "children" }}
                        />
                    </Grid>
                    <Grid item xs={12} textAlign="left">
                        <Typography sx={{ fontWeight: "bold" }}> Name: </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Input
                            placeholder="Enter artwork category name"
                            onChange={handleChangeInput}
                            value={stateData.name}
                            size={"large"}
                        />
                    </Grid>
                </Grid>
            </div>
        </Modal>
    );
};

export default ModalCategory;
