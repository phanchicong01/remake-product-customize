import React, { useState, useEffect } from "react";

import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";

import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";
import Stack from "@mui/material/Stack";
import { useAppSelector, useAppDispatch } from "hooks";
import { roughScale } from "utils";
import { closeModal } from "store/modal-reducer/modalSlice";
import { updateArtwork } from "store/artwork-reducer/artworkSlice";
import { IArtwork } from "store/artwork-reducer/interface";
import cloneDeep from "lodash/cloneDeep";

const ModalSettingArtwork = () => {
    const artwork = useAppSelector((state) => state.artwork);
    const {key } = useAppSelector((state) => state.modal);
    const dispatch = useAppDispatch();
    const [state, setState] = useState<any>(null);

    useEffect(() => {
        
        setState({
            width:artwork.size.width,
            height:artwork.size.height,
            nameArtwork:artwork.nameArtwork,
            ...artwork
        });
        
    }, []);

    const handleClose = () => {
        dispatch(closeModal());
    };

    const onUpdateData = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = event.target.value;
        const name = event.target.name;

        let newValue: any = value;
        if (name === "width" || name === "height") {
            newValue = roughScale(value);
        }
        const newAttrs = { ...state, [name]: newValue };

        setState(newAttrs);
    };
    const handleSubmit = () => {
        const newArtwork = { 
            nameArtwork:state.nameArtwork,
            size:{
                width:state.width,
                height:state.height
            }
        }
        dispatch(updateArtwork(newArtwork));
        handleClose();
    };
    return (
        <Modal
            open={key === "modal-setting-artwork"}
            onClose={handleClose}
            // aria-labelledby="modal-modal-title"
            // aria-describedby="modal-modal-description"
            // style={{ overflow: "auto", minWidth: 500 }}
            // title={""}
        >
            {
                !state ?   <Box sx={style}>Loading ....</Box> :
                <Box sx={style}>
                <h2 id="parent-modal-title">Create Artwork</h2>
                <TextField
                    label="Name Artwork"
                    name="nameArtwork"
                    sx={{ my: 1 }}
                    id="outlined-font-size-small"
                    onChange={onUpdateData}
                    // style={{ maxWidth: 80 }}
                    type="text"
                    value={state?.nameArtwork}
                    size="small"
                    fullWidth
                />
                <TextField
                    label="width"
                    name="width"
                    sx={{ my: 1 }}
                    id="outlined-font-size-small"
                    onChange={onUpdateData}
                    // style={{ maxWidth: 80 }}
                    type="number"
                    value={state?.width || 0}
                    size="small"
                    fullWidth
                />
                <TextField
                    label="height"
                    name="height"
                    sx={{ my: 1 }}
                    id="outlined-font-size-small"
                    onChange={onUpdateData}
                    // style={{ maxWidth: 80 }}
                    type="number"
                    value={state?.height || 0}
                    size="small"
                    fullWidth
                />
                <Stack spacing={2} sx={{ pt: 4, textAlign: "right" }} justifyContent="right" direction="row">
                    <Button variant="contained" onClick={handleSubmit}>
                        Save
                    </Button>
                    <Button variant="outlined" onClick={handleClose}>
                        Cancel
                    </Button>
                </Stack>
            </Box>
            }
        </Modal>
    );
};

export default ModalSettingArtwork;
const style = {
    position: "absolute" as "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
    // height: 500,
    width: 400,
    overflow: "auto",

    // maxHeight:"calc(100vh - 64px)";
};
