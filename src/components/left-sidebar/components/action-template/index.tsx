import React, { useState, useRef } from "react";
import ClearIcon from "@mui/icons-material/Clear";
import EditIcon from "@mui/icons-material/Edit";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import InputBase from "@mui/material/InputBase";
import Grid from "@mui/material/Grid";
import { alpha, styled } from "@mui/material/styles";
import { onRemoveTemplateByID, onDuplicateTemplateByID, onUpdateTemplateActive } from "store/template-reducer/templateSlice";
import { useAppDispatch, useAppSelector } from "hooks";

import styles from "./styles.module.scss";

const ActionTemplate = ({ nameTemplate, isRemove }: any) => {
    const refInputName = useRef<any>(null);
    const dispatch = useAppDispatch();
    const { templateActive } = useAppSelector((state) => state.template);
    const [editName, setEditName] = useState({
        visible: false,
        value: nameTemplate || "",
    });
    const handleChangeName = (event: any) => {
        const target = event.target;
        const value = target.value;
        setEditName((prev) => ({ ...prev, value: value }));
    };

    const handleBlur = () => {
        // const index = layers.findIndex((layer) => layer.id === layerActive.id);

        const newLayer = { ...templateActive, name: editName.value };
        dispatch(onUpdateTemplateActive({ newTemplateAttr: newLayer }));
        setEditName({
            visible: false,
            value: "",
        });
    };
    const handleRemoveLayer = () => {
        dispatch(onRemoveTemplateByID(templateActive.id));
    };
    const handleCopyTemplate = () => {
        //     const index = layers.findIndex((layer) => layer.id === layerActive.id);
        const newIdLayer = `layer-${Date.now()}`;
        dispatch(onDuplicateTemplateByID({ newId: newIdLayer }));
    };

    return (
        <>
            <div className={styles.editorLayer}>
                {!editName.visible ? (
                    <Grid container alignItems={"center"} justifyContent={"center"} className={styles.wrapEdit}>
                        {" "}
                        Editing {nameTemplate} &nbsp;{" "}
                        <Grid item sx={{ px: 1 }}>
                            <EditIcon
                                onClick={() => setEditName((prev) => ({ ...prev, visible: true, value: nameTemplate }))}
                                fontSize="small"
                            />
                        </Grid>
                        <Grid item sx={{ px: 1 }}>
                            <ContentCopyIcon fontSize="small" onClick={handleCopyTemplate} />
                        </Grid>
                        {isRemove && (
                            <Grid item sx={{ px: 1 }}>
                                {" "}
                                <ClearIcon onClick={handleRemoveLayer} fontSize="small" />
                            </Grid>
                        )}
                    </Grid>
                ) : (
                    <InputEditName
                        ref={refInputName}
                        value={editName.value}
                        onChange={handleChangeName}
                        onBlur={handleBlur}
                    />
                )}
            </div>
        </>
    );
};

export default ActionTemplate;
const InputEditName = styled(InputBase)(({ theme }: any) => ({
    "&.MuiInputBase-root": {
        borderRadius: "5px",

        transition: theme.transitions.create(["border-color", "background-color", "box-shadow"]),
    },
    "& .MuiInputBase-input": {
        background: "white",
        borderRadius: "5px",
        padding: "5px 15px",
    },
    "&:focus": {
        boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
        borderColor: theme.palette.primary.main,
    },
}));
