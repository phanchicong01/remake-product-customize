import { useEffect, useState } from "react";
import { useAppDispatch } from "hooks";
import { roughScale } from "utils";
import { onUpdateAttrLayer } from "store/template-reducer/templateSlice";
import { useSelector } from "react-redux";
import { RootState } from "store";
import { closeModal } from "store/modal-reducer/modalSlice";
import { Modal } from "antd";
import ImageIcon from "@mui/icons-material/Image";
import { LoadingButton } from "@mui/lab";

const ModalReplaceImage = () => {
    const { key, data: dataModal } = useSelector((state: RootState) => state.modal)
    const artwork = useSelector((state: RootState) => state.artwork)

    const dispatch = useAppDispatch();
    const [loading, setLoading] = useState(false)
    const [openModal, setOpenModalMedia] = useState(null)
    let fileManager;

    useEffect(() => {
        if (openModal) openFileManager();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [openModal]);

    function createPop(url, name) {
        fileManager = window.open(url, name, "width=900,height=500,toolbar=0,menubar=0,location=0");
        if (window?.focus && fileManager) {
            fileManager.focus();
        }
        const interval = setInterval(() => {
            if (fileManager?.closed) {
                setOpenModalMedia(false)
                clearInterval(interval);
            }
        }, 1000);
    }

    const openFileManager = () => {
        createPop(`https://customproduct-dev.vellamerch.com/admin/laravel-filemanager`, "Choose Image Replace");
        window.SetUrl = function (items) {

            handleFileManager(items);
        };
        return false;
    };

    const handleFileManager = (items = []) => {
        if (items.length === 0) return false;
        setLoading(true)
        for (let item of items) {
            const image = new Image();
            image.src = item?.url;
            const tempImage = {
                src: item?.url,
                name: item?.url.split("/").pop(),
                id: `layer-${Date.now()}`,
                width: 350,
                height: 350,
            };
            image.onload = function () {
                tempImage.width = image.width;
                tempImage.height = image.height;
                handleInsertImages(tempImage);
                return true;
            };
        }
    };

    const handleInsertImages = (item: { name: string; width: number; height: number; src: string }) => {

        const newItem = { ...item };
        const ratio = item.width / item.height;
        let width = item.width < 500 ? item.width : 500;
        let height = width / ratio;

        if (height > artwork.size.height) {
            width = artwork.size.height * ratio;
            height = artwork.size.height;
        }

        // newItem["widthImage"] = roughScale(item.width);
        // newItem["heightImage"] = roughScale(item.height);
        newItem.width = roughScale(width);
        newItem.height = roughScale(height);
        newItem["x"] = roughScale(artwork.size.width / 2 - width / 2);
        newItem["y"] = roughScale(artwork.size.height / 2 - height / 2);
        console.log(dataModal)

        if (dataModal?.id) {
            const newAttrs = { ...dataModal.data, src: item.src };
            dispatch(onUpdateAttrLayer({ newAttrs }));
            handleClose()
        }
    };

    const handleClose = (key?: any) => {
        setLoading(false)
        return dispatch(
            closeModal()
        );
    };

    return <Modal
        visible={key === "modal-replace-image"}
        onCancel={handleClose}
        destroyOnClose
        footer={false}
        confirmLoading={loading}
        title="Choose new image for this layer"

    >
        <div>
            <p>Please use an image with the same ratio as the original image</p>
            <b>Current image size (width x height): {dataModal?.width | 0} x {dataModal?.height | 0}</b>
            <LoadingButton
                loading={loading}
                style={{ margin: "15px 0", width: "100%" }}
                variant="outlined"
                onClick={() => setOpenModalMedia(true)}
                startIcon={<ImageIcon />}
            >
                Browse image...
            </LoadingButton>
        </div>
    </Modal>
};

export default ModalReplaceImage;
