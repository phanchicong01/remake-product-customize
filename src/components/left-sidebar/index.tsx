import React from "react";
import Grid from "@mui/material/Grid";
import EditIcon from "@mui/icons-material/Edit";
import Typography from "@mui/material/Typography";

import { useAppSelector, useAppDispatch } from "hooks";
import { setModal } from "store/modal-reducer/modalSlice";

import ModalSettingArtwork from "./components/modal-setting-artwork";
import ModalSettingTemplate from "./components/modal-setting-template";
import InfoSidebar from "./components/Info-sidebar";
import ManageLayer from "./components/list-template";

const LeftSideBar = () => {
    const artwork = useAppSelector((state) => state.artwork);
    const dispatch = useAppDispatch();

    return (
        <Grid container style={{ height: "100vh", background: "#535353" }} direction="column" rowGap={2}>
            <Grid item>
                <Grid
                    container
                    rowSpacing={2}
                    // spacing={2}
                    style={{ minHeight: 100 }}
                    alignItems="center"
                    rowGap={4}
                >
                    <Grid item xs={12} sx={{ background: "#343a40", color: "#ffff" }}>
                        <Typography align={"center"} noWrap sx={{ py: 1 }}>
                            Customize your Artwork
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <InfoSidebar />
                    </Grid>
                    <Grid item xs={12} alignItems={"center"} rowSpacing={2} sx={{ color: "#fff" }}>
                        <Grid container alignItems="center" sx={{ p: 2 }}>
                            <Grid item xs={3}>
                                <Typography sx={{ fontWeight: "bold" }}> Size (px): </Typography>
                            </Grid>
                            <Grid item xs={8}>
                                <span>{artwork.size.width}</span> x <span>{artwork.size.height}</span>
                            </Grid>
                            <Grid item xs={1}>
                                <EditIcon
                                    fontSize="small"
                                    onClick={() =>
                                        dispatch(
                                            setModal({
                                                key: "modal-setting-artwork",
                                                data: null,
                                            })
                                        )
                                    }
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item container style={{ flex: 1, overflow: "auto" }}>
                <ManageLayer />
            </Grid>
            <Grid item>
                <ModalSettingArtwork />
                <ModalSettingTemplate />
            </Grid>
        </Grid>
    );
};

export default LeftSideBar;
