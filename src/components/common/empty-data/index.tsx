import React from 'react'

type Props = {
  title?: string
  className?: string
}

const EmptyData = ({ title = 'Empty Data', className }: Props) => {
  return (
    <div className="d-flex align-items-center justify-content-center h-300px ">
      <span className="">{title}</span>
    </div>
  )
}

export default EmptyData
