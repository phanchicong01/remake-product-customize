import { useState, useEffect } from "react";
import Grid from "@mui/material/Grid";
import Select from "antd/lib/select";

const { Option } = Select;
interface IProps {
    data: any[];
    placeHolderText?: any;
    onChange: (valueSelected, data) => void;
    filedNames?: {
        name: string;
        value: string;
    };
    value: any;
    id: any;
}
const ItemDropDown = ({
    data,
    placeHolderText,
    onChange,
    id,
    filedNames = {
        name: "name",
        value: "id",
    },
    value,
}: IProps) => {
    const [clipArtSelect, setSelect] = useState<any>(null);
    const [dataState, setData] = useState<any>([]);

    const handleChange = (value) => {
        const findItem = dataState.find((item) => item[filedNames.value] === value);
        setSelect(value);
        onChange(value, findItem);
    };
    useEffect(() => {
        setData(data);
        setSelect(value);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Grid item xs={12}>
            <Select
                value={clipArtSelect}
                onChange={handleChange}
                size={"large"}
                placeholder={placeHolderText}
                style={{ width: "100%" }}
            >
                {dataState?.map((item, index: number) => {
                    const name = item[filedNames.name] || item?.url?.split("/")?.pop();
                    return (
                        <Option key={index} value={item[filedNames.value]}>
                            {" "}
                            {name}
                        </Option>
                    );
                })}
            </Select>
        </Grid>
    );
};

export default ItemDropDown;
