import React from 'react'

type Props = {
    title?:string,
    onChange?:(e:React.ChangeEvent<HTMLInputElement>)=>void
    [x:string]:unknown
}

const Switch = ({title , onChange, ...otherProps}: Props) => {
  return (
    <div className='form-check form-switch form-switch-sm form-check-custom form-check-solid'>
      <input
        className='form-check-input'
        type='checkbox'
        onChange={onChange}
        name='notifications'
        {...otherProps}
      />
      <label className='form-check-label'>{title}</label>
    </div>
  )
}

export default Switch
