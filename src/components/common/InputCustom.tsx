import React from "react";
import OutlinedInput from "@mui/material/OutlinedInput";

import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";

import styled from "@mui/styles/styled";

const InputCustom = ({ label = null, colLabel = 4, hiddenLabel = false, ...otherProps }) => {
    return (
        <Grid container alignItems="center"  rowSpacing={1}>
            {label && (
                <Grid item xs={colLabel} style={{opacity:hiddenLabel? 0 : 1}}>
                    <TypographyCustom>{label}</TypographyCustom>
                </Grid>
            )}
            <Grid item xs>
                {<OutlinedInputCustom fullWidth size="small" {...otherProps} />}
            </Grid>
        </Grid>
    );
};

export default InputCustom;
const OutlinedInputCustom = styled(OutlinedInput)({
    "&.MuiOutlinedInput-root": {
        color: "#000",
        background: "white",
    },
});
const TypographyCustom = styled(Typography)({
    "&.MuiTypography-root": {
        color: "#fff",
        fontWeight: "bold",
    },
});
