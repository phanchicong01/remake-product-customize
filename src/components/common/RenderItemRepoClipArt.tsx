import React from "react";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import Paper from "@mui/material/Paper";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";

interface IProps {
    name: string;
    items: any[];
    onChange: () => void;
    isActive: boolean;
}

const RenderItemRepoClipArt = ({ name, items, onChange, isActive }: IProps) => {
    return (
        <Grid container>
            <Grid item xs={12} sx={{ p: 1 }}>
                <Grid sx={{ display: "flex", justifyContent: "space-between" }}>
                    <Typography color={"white"} component="h4">
                        {name}
                    </Typography>

                    <Button size="small" sx={{ fontSize: 12 }} onClick={onChange}>
                        {isActive ? "Done" : "Re-position"}
                    </Button>
                </Grid>
                <Grid item xs={12}>
                    <Grid container spacing={0.5}>
                        {items.length === 0 &&
                            <Typography
                                px={0.5}
                                color={"gray"}
                                component={"span"}>Empty Items
                            </Typography>
                        }
                        {items?.map((item) => {
                            return (
                                <Grid item key={item.id} xs="auto">
                                    <Paper sx={{ width: 50 }}>
                                        <CardMedia
                                            sx={{ objectFit: "contain" }}
                                            component="img"
                                            height="50"
                                            image={item?.thumbnail_url || item?.url}
                                            alt={item.id}
                                        />
                                    </Paper>
                                </Grid>
                            );
                        })}
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default RenderItemRepoClipArt;
