import clsx from 'clsx';
import React, { CSSProperties, useCallback, useMemo, useState } from 'react'
import _debounce from "lodash-es/debounce"
import _assign from "lodash-es/assign"
type Props = {
  onChange: (value: string, name?: string) => void;
  color: string,
  id: string,
  width?: number | string,
  name?: string,
  label?: string,
  styleLabel?: CSSProperties | undefined
  hiddenLabel?: boolean,
  [key: string]: unknown
}

const PickColor = ({ onChange, color, id, width = "auto", name, hiddenLabel, styleLabel, label }: Props) => {

  const [stateValue, setStateValue] = useState(null)

  const handleOnChange = _debounce((value) => {
    onChange(value, name)
  }, 300)

  const handleChangeColor = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value } = event.target
      setStateValue(value)
      handleOnChange(value)
    },
    [handleOnChange],
  )

  const memoizedColor = useMemo(() => stateValue === null ? color : stateValue , [color, stateValue])

  const styleLabelDefault = _assign({
    width: "100%",
    color: "#fff",
    fontWeight: "bold"
  }, styleLabel)

  return (
    <div className='position-relative row g-1'>
      {!hiddenLabel && !!label &&
        <div style={styleLabelDefault}>
          {label}
        </div>

      }
      <label className='d-flex' htmlFor={'pick-color' + id} role="button">
        <div className={'input-group w-' + width + ' input-group-sm'} >
          <input name={name} onChange={handleChangeColor} value={memoizedColor} type="text" className='form-control ' aria-describedby="inputGroup-sizing-sm" />
          <div className='input-group-text p-1' style={{
            backgroundImage: !memoizedColor ? "url('/bg-transparent.png')" : undefined
          }} >
            <span className={clsx("w-25px h-25px rouded-sm", {})}
              style={{
                backgroundColor: memoizedColor
              }}></span>
          </div>
        </div>
      </label>
      <input
        name={name}
        onChange={handleChangeColor}
        value={memoizedColor} type={"color"} id={'pick-color' + id} className="position-absolute top-0 start-0 opacity-0 " />
    </div>
  )
}

export default PickColor