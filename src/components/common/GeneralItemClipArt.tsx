import Card from "@mui/material/Card";
import Grid from "@mui/material/Grid";
import Collapse from "@mui/material/Collapse";
import Button from "@mui/material/Button";
import React, { useState } from "react";
import RenderItemRepoClipArt from "components/common/RenderItemRepoClipArt";
import { OptionPersonalizationImage } from "store/template-reducer/enum";
import { ClipArtCategory } from "store/template-reducer/interface";
import styled from "@emotion/styled";
import { Typography } from "@mui/material";

type Props = {
    data: ClipArtCategory;
    onClickAction: (key: "reposition" | "done", data: ClipArtCategory) => void;
    type: OptionPersonalizationImage.groupClipart | OptionPersonalizationImage.clipart
}
const GeneralItemClipArt = ({ data, onClickAction, type }: Props) => {
    const [activeAction, setActiveAction] = useState<any>(null);
    const [showGroupClipArt, setShowGroupClipArt] = useState(false);
    const handelChangeRepo = (dataItem: ClipArtCategory) => {
        if (dataItem?.id !== activeAction) {
            onClickAction("reposition", dataItem);
            setActiveAction(dataItem.id);
        } else {
            onClickAction("done", dataItem);
            setActiveAction(null);
        }
    };

    return (
        <Grid item xs={12}>
            <Card sx={{ background: "#343a40", position: "relative", minHeight: "40px" }}>
                <Grid container alignItems={"center"}>
                    {type === OptionPersonalizationImage.groupClipart ? (
                        <Grid item xs={12} sx={{ py: 2, px: 1 }}>
                            {!showGroupClipArt && (
                                <Grid container justifyContent={"center"} alignItems="center">
                                    <LabelCustom onClick={() => setShowGroupClipArt(true)}>Show cliparts to re-position</LabelCustom>
                                </Grid>

                            )}
                            <Collapse in={showGroupClipArt}>
                                {data?.children?.map((child) => (
                                    <RenderItemRepoClipArt
                                        name={child.name}
                                        items={child.items}
                                        key={child.id}
                                        onChange={() => handelChangeRepo(child)}
                                        isActive={activeAction === child.id}
                                    />
                                ))}
                            </Collapse>
                        </Grid>
                    ) : (
                        <RenderItemRepoClipArt
                            name={data.name}
                            items={data?.items || []}
                            onChange={() => handelChangeRepo(data)}
                            isActive={activeAction === data?.id}
                        />
                    )}
                </Grid>
            </Card >
        </Grid >
    );
};

export default GeneralItemClipArt;
const LabelCustom = styled(Typography)({
    "&.MuiTypography-root": {
        color: '#fff',
        backgroundColor: '#6c757d',
        borderColor: '#6c757d',
        padding: '5px 7px',
        borderRadius: '5px',
        fontSize: "14px"
    },
    "&.MuiTypography-root:hover": {
        cursor: "pointer"
    }
});