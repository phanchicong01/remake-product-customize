import React, { useEffect, useRef } from "react";
import { Text } from "react-konva";
import { roughScale } from "utils";

const TextDrag = ({ textProps, isSelected, onSelect, onChange, stageScale }: any) => {
    const textRef = useRef<any>();

    // React.useLayoutEffect(() => {
    //     if (!!isSelected && !!textRef.current && !!textProps) {
    //         // we need to attach transformer manually
    //         onSelect({ target: textRef.current }, { status: true, layerSelected: textProps });
    //     }

    //     // eslint-disable-next-line react-hooks/exhaustive-deps
    // }, [isSelected]);

    useEffect(() => {
        if (
            textRef.current &&
            textProps.text &&
            !!textProps &&
            !firstRender.current &&
            !textProps.width &&
            !textProps.height
        ) {
            const width = roughScale(textRef.current.getWidth());
            const height = roughScale(textRef.current.getHeight());
            onChange({
                ...textProps,
                width,
                height,
            });
            firstRender.current = true;
        }
        if (textRef.current) {
            textRef.current.fontFamily(textProps.fontFamily);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [textProps, textRef.current]);

    const firstRender = useRef(false);

    if (!textProps) {
        return null;
    }
    return (
        <React.Fragment>
            <Text
                onClick={(event) => onSelect(event, { status: true, layerSelected: textProps })}
                onTap={(event) => onSelect(event, { status: true, layerSelected: textProps })}
                ref={textRef}
                {...textProps}
                text={textProps.text}
                name="shape"
                ellipsis
                fillAfterStrokeEnabled
                draggable={isSelected}
                onDragEnd={(e) => {
                    onChange({
                        ...textProps,
                        x: e.target.x(),
                        y: e.target.y(),
                    });
                }}
                onTransformEnd={(e) => {
                    // transformer is changing scale of the node
                    // and NOT its width or height
                    // but in the store we have only width and height
                    // to match the data better we will reset scale on transform end
                    const node = textRef.current;
                    const scaleX = node.scaleX();
                    const scaleY = node.scaleY();

                    // we will reset it back
                    node.scaleX(1);
                    node.scaleY(1);

                    onChange({
                        ...textProps,
                        x: roughScale(node.x()),
                        y: roughScale(node.y()),
                        rotation: roughScale(node.rotation()),
                        // set minimal value
                        width: roughScale(Math.max(5, node.width() * scaleX)),
                        height: roughScale(Math.max(node.height() * scaleY)),
                    });
                }}
            />
        </React.Fragment>
    );
};

export default TextDrag;
