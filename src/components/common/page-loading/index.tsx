import React from 'react'

type Props = {}

const PageLoading = (props: Props) => {
  return (
    <div className="vw-100 vh-100 position-fixed d-block top-0 bg-dark" style={{left:0 , opacity:.5 , zIndex:9999999999999}}>
      <div className="top-50 my-0 mx-auto position-relative d-flex justify-content-center align-items-center flex-columns">
      <div className="spinner-border w-100px h-100px  text-secondary" role="status">
        <span className="sr-only"> Processing....</span>
        </div>
     
      </div>
    </div>
  )
}

export default PageLoading
