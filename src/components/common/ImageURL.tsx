import React, { useEffect } from "react";
import useImage from "use-image";
import { Image, Text } from "react-konva";
import { roughScale } from "utils";

const ImageURL = ({ shapeProps, isSelected, onSelect, onChange, stageScale }: any) => {
    const shapeRef = React.useRef<any>();

    const [img] = useImage(shapeProps.src);

    // useEffect(() => {
    //     if (!!isSelected && !!shapeRef.current && !!shapeProps && !!img) {
    //         // we need to attach transformer manually
    //         onSelect({ target: shapeRef.current }, { status: true, layerSelected: shapeProps });
    //     }

    //     // eslint-disable-next-line react-hooks/exhaustive-deps
    // }, [isSelected, shapeRef.current ,img]);


    if (!shapeProps) {
        return null;
    }
    return (
        <React.Fragment>

            {
                <Image
                    onClick={(event) => onSelect(event, { status: true, layerSelected: shapeProps })}
                    onTap={(event) => onSelect(event, { status: true, layerSelected: shapeProps })}
                    ref={shapeRef}
                    {...shapeProps}

                    image={img}
                    draggable={isSelected}
                    name="shape"
                    onDragEnd={(e) => {
                        onChange({
                            ...shapeProps,
                            x: e.target.x(),
                            y: e.target.y(),
                        });
                    }}
                    onTransform={(e) => {
                        // transformer is changing scale of the node
                        // and NOT its width or height
                        // but in the store we have only width and height
                        // to match the data better we will reset scale on transform end
                        const node: any = shapeRef.current;
                        const scaleX = node.scaleX();
                        const scaleY = node.scaleY();

                        // we will reset it back
                        node.scaleX(1);
                        node.scaleY(1);
                        onChange({
                            ...shapeProps,
                            x: roughScale(node.x()),
                            y: roughScale(node.y()),
                            rotation: roughScale(node.rotation()),
                            // set minimal value
                            width: roughScale(Math.max(5, node.width() * scaleX)),
                            height: roughScale(Math.max(node.height() * scaleY)),
                        });
                    }}
                />}
            {!img &&
                <Text
                    {...shapeProps}
                    id={"loading"}
                    zIndex={shapeProps.zIndex + 1}
                    text="Image loading..."
                    align={"center"}
                    verticalAlign="middle"
                    fill="#0e83cd"
                    draggable={false}
                    // ref={shapeRef}
                    fontSize={50}
                />
            }
        </React.Fragment>
    );
};

export default ImageURL;
