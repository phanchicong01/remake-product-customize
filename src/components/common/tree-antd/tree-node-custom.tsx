import React from 'react'
import Tree, { DataNode, TreeProps } from 'antd/lib/tree';
import { KTSVG } from 'helpers'

type Props = {}
const { TreeNode } = Tree;
// titleRender={(nodeData) => {
//     console.log(nodeData)
//     return <div className='d-flex justify-content-between align-items-center'>
//         <span>{nodeData['name']}</span>
//         <div className='action-tree'>
//             <a
//                 onClick={() => handleOnRemove(nodeData)}
//                 // style={{ height: 16, width: 16, zIndex: 10 }}
//                 className='btn btn-icon btn-bg-light btn-active-color-primary btn-sm'
//             >
//                 <KTSVG
//                     path='/media/icons/duotune/general/gen027.svg'
//                     className='svg-icon-3'
//                 />
//             </a>
//         </div>
//     </div>
// }
// }
const TreeNodeCustom = (props: TreeProps) => {
    const renderTitle = (nodeData: DataNode) => {
        return <div className='d-flex justify-content-between align-items-center'>
            <span>{nodeData['name']}</span>
            <div className='action-tree'>
                <a
                    // onClick={() => handleOnRemove(nodeData)}
                    // style={{ height: 16, width: 16, zIndex: 10 }}
                    className='btn btn-icon btn-bg-light btn-active-color-primary btn-sm'
                >
                    <KTSVG
                        path='/media/icons/duotune/general/gen027.svg'
                        className='svg-icon-3'
                    />
                </a>
            </div>
        </div>

    }

    return (
        <TreeNode {...props} title={renderTitle}  >

        </TreeNode >
    )
}

export default TreeNodeCustom