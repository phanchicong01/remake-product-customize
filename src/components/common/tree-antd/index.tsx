import Tree, {  TreeProps } from 'antd/lib/tree';
import React from 'react'


const TreeAntd = React.forwardRef((props: TreeProps, ref: any) => {
 

    const { treeData, ...restProps } = props
    return (
        <Tree
            ref={ref}
            showLine={{
                showLeafIcon: false
            }}
            showIcon={false}
            rootClassName={"tree-custom"}
            autoExpandParent={true}
            {...restProps}
            treeData={props.treeData}
        >

        </Tree>
    )
})

export default TreeAntd