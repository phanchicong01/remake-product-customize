import React, { forwardRef } from "react";
import { Rect } from "react-konva";
import { roughScale } from "utils";

const SelectionRectangle = forwardRef(({ onSelect, onChange, ...shapeProps }: any, ref) => {
    const shapeRef = React.useRef();
    const trRef = React.useRef<any>();

    React.useEffect(() => {
        if (!!trRef.current && Array.isArray(shapeProps.selectedNodes) && shapeProps.selectedNodes.length > 0) {
            const transformRef = trRef.current;
            // we need to attach transformer manually
            trRef.current.nodes(shapeProps.selectedNodes);
            trRef.current.getLayer().batchDraw();
            const newProps = shapeProps;
            newProps.width = transformRef.getWidth();
            newProps.height = transformRef.getHeight();
            newProps.x = transformRef.x();
            newProps.y = transformRef.y();
            // onChange(null, newProps);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [shapeProps?.selectedNodes]);
    // const stageScaleX = shapeProps?.x / stageScale?.x || 1;
    // const stageScaleY = shapeProps?.y / stageScale?.y || 1;
    return (
        <React.Fragment>
            <Rect
                {...shapeProps}
                ref={ref}
                // fillPatternImage={img}
                draggable={true}
                onDragMove={(e) => {
                    onChange &&
                        onChange(e, {
                            ...shapeProps,
                            x: e.target.x(),
                            y: e.target.y(),
                        });
                }}
                onDragEnd={(e) => {
                    onChange &&
                        onChange(e, {
                            ...shapeProps,
                            x: e.target.x(),
                            y: e.target.y(),
                        });
                }}
                onTransform={(e) => {
                    // transformer is changing scale of the node
                    // and NOT its width or height
                    // but in the store we have only width and height
                    // to match the data better we will reset scale on transform end
                    const node: any = shapeRef.current;
                    const scaleX = node.scaleX();
                    const scaleY = node.scaleY();

                    // we will reset it back
                    node.scaleX(1);
                    node.scaleY(1);
                    onChange &&
                        onChange(e, {
                            ...shapeProps,
                            x: roughScale(node.x()),
                            y: roughScale(node.y()),
                            rotation: roughScale(node.rotation()),
                            // set minimal value
                            width: roughScale(Math.max(5, node.width() * scaleX)),
                            height: roughScale(Math.max(node.height() * scaleY)),
                            // scaleX,
                            // scaleY
                            // fillPatternScaleX: Math.max(3, node.width() * scaleX) / shapeProps.widthImage,
                            // fillPatternScaleY:Math.max(node.height() * scaleY) / shapeProps.heightImage
                        });
                }}
            />
        </React.Fragment>
    );
});

export default SelectionRectangle;
