import React from 'react'
import styles from "./styles.module.scss"
type Props = {}

const Loading = (props: Props) => {
    return (
        <div className={styles.ldsRing}><div></div><div></div><div></div><div></div></div>
    )
}

export default Loading