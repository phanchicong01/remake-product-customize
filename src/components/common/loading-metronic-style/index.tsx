const Loading = ({title = "Processing..."}) => {
  const styles = {
    borderRadius: '0.475rem',
    boxShadow: '0 0 50px 0 rgb(82 63 105 / 15%)',
    backgroundColor: '#fff',
    color: '#7e8299',
    fontWeight: '500',
    margin: '0',
    width: 'auto',
    padding: '1rem 2rem',
    top: '50%',
    left: '50%',
    display:"flex",
    justifyContent:"center",
    alignItems:"center",
    zIndex:999,
    transform: 'translate(-50%, -50%)'
  }

  return <div style={{ ...styles, position: 'absolute', textAlign: 'center' }}>
    <span>{title}</span>
  </div>
}

export default Loading
