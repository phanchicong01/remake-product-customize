import { KTSVG } from 'helpers';
import { BaseParamRequest } from 'interfaces/common.interface';
import React, { useCallback, useMemo } from 'react'
import ReactPaginate from 'react-paginate';
import { createSearchParams, useNavigate, useSearchParams } from 'react-router-dom';
import styles from './styles.module.scss';
import _pickBy from 'lodash-es/pickBy';
import _identity from 'lodash-es/identity';

type Props = {
  forcePage?: number
  pageCount: number
  pathname: string
}
const PaginationCommon = ({ forcePage, pageCount, pathname }: Props) => {
  const [searchParams] = useSearchParams();
  const navigate = useNavigate();
  const query: BaseParamRequest = useMemo(() => {
    return {
      search: searchParams.get('search'),
      page: searchParams.get('page') ? parseInt(searchParams.get('page')) : 1,
      per_page: searchParams.get('per_page') ? parseInt(searchParams.get('per_page')) : 10
    }
  }, [searchParams])

  const handlePageChange = useCallback(
    (event) => {
      const newArtworkQuery = _pickBy(
        {
          ...query,
          page: event.selected + 1
        },
        _identity()
      )
      navigate({
        pathname: pathname,
        search: `?${createSearchParams(newArtworkQuery)}`,
      });
    },
    [navigate, pathname, query],
  )

  return (
    <ReactPaginate
      breakLabel="..."
      onPageChange={handlePageChange}
      pageRangeDisplayed={3}
      marginPagesDisplayed={1}
      nextLabel={<div className={styles.next}>
        Next
        <KTSVG path='media/icons/duotune/arrows/arr024.svg' className='svg-icon-2' />
      </div>}
      previousLabel={<div className={styles.previous}>
        <KTSVG path='media/icons/duotune/arrows/arr021.svg' className='svg-icon-2' />
        Previous
      </div>}
      pageCount={pageCount}
      forcePage={searchParams.get('page') ? Number(searchParams.get('page')) - 1 : 0}
      containerClassName={styles.container}
      pageLinkClassName={styles.pageLink}
      activeLinkClassName={styles.activeLink}
      disabledClassName={styles.disabled}
      breakClassName={styles.break}

    />

  )
}

export default PaginationCommon
