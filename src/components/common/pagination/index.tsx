import { KTSVG } from 'helpers';
import React, { useCallback } from 'react'
import ReactPaginate from 'react-paginate';
import styles from './styles.module.scss';

type Props = {
  forcePage?: number
  pageCount: number
  onPageChange: (page: number) => void
}
const Pagination = ({ forcePage, pageCount, onPageChange }: Props) => {
  const handlePageChange = useCallback(
    (event) => {
      onPageChange(event.selected)
    },
    [onPageChange],
  )

  return (
      <ReactPaginate
        breakLabel="..."
        onPageChange={handlePageChange}
        pageRangeDisplayed={3}
        marginPagesDisplayed={1}
        nextLabel={<div className={styles.next}>
          Next
          <KTSVG path='media/icons/duotune/arrows/arr024.svg' className='svg-icon-2' />
        </div>}
        previousLabel={<div className={styles.previous}>
          <KTSVG path='media/icons/duotune/arrows/arr021.svg' className='svg-icon-2' />
          Previous
        </div>}
        pageCount={pageCount}
        forcePage={forcePage}
        containerClassName={styles.container}
        pageLinkClassName={styles.pageLink}
        activeLinkClassName={styles.activeLink}
        disabledClassName={styles.disabled}
        breakClassName={styles.break}
      
      />

  )
}

export default Pagination
