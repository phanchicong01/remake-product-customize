import React from "react";

import styles from "./styles.module.scss";

const GridView = ({ width = 495, height = 495, index = 0 }: any) => {
    const funcRenderLineVertical = ({ width, index }) => {
        if (width <= index * 25) {
            return (
                <div
                    className={styles.itemVertical}
                    style={{ left: `${width}px`, borderRightStyle: "solid", opacity: 0.8 }}
                ></div>
            );
        }
        if (index % 4 === 0) {
            return (
                <>
                    <div
                        className={styles.itemVertical}
                        style={{ left: `${25 * index}px`, borderRightStyle: "solid", opacity: 0.8 }}
                    ></div>
                    {funcRenderLineVertical({ width: width, index: index + 1 })}
                </>
            );
        }
        return (
            <>
                <div className={styles.itemVertical} style={{ left: `${25 * index}px` }}></div>
                {funcRenderLineVertical({ width: width, index: index + 1 })}
            </>
        );
    };
    const funcRenderLineHorizontal = ({ height, index }) => {
        if (height <= index * 25) {
            return (
                <div
                    className={styles.itemHorizontal}
                    style={{ top: `${height}}px`, borderBottomStyle: "solid", opacity: 1 }}
                ></div>
            );
        }
        if ((index + 1) % 4 === 0) {
            return (
                <>
                    <div
                        className={styles.itemHorizontal}
                        style={{ top: `${25 * index}px`, borderBottomStyle: "solid", opacity: 1 }}
                    ></div>
                    {funcRenderLineHorizontal({ height: height, index: index + 1 })}
                </>
            );
        }
        return (
            <>
                <div className={styles.itemHorizontal} style={{ width: "100%", top: `${25 * index}px` }}></div>
                {funcRenderLineHorizontal({ height: height, index: index + 1 })}
            </>
        );
    };
    return (
        // <Grid container sx={{ width: width / idParent }}>
        <div style={{ position: "relative", width: width, height: height }}>
            {funcRenderLineVertical({ width: width, index: index })}
            {funcRenderLineHorizontal({ height: height, index: index })}
        </div>
        // </Grid>
    );
};

export default GridView;
