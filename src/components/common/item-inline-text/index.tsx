import React from "react";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import Typography from "@mui/material/Typography";

import Tooltip from "@mui/material/Tooltip";

import styles from "./styles.module.scss";
interface IProps {
    value: any;
    onChecked: (checked, data) => void;
    data: any[];
    id?: any;
    isShowHover: boolean;
    filedNames?: {
        name: string;
        value: string;
        id: any;
    };
    minWidth?: number
}

const ItemInlineText = (props: IProps) => {
    const {
        data,
        value,
        onChecked,
        filedNames = {
            name: "name",
            value: "id",
            id: null
        },
        isShowHover,
        minWidth = 60
    } = props;
    const [active, setActive] = React.useState<any>("");
    const handleChange = (e, item) => {
        const checked = e.target.checked;

        onChecked(checked, item);
        setActive(item[filedNames.value]);
    };

    React.useEffect(() => {
        if (value !== active) {
            setActive(value);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [value]);

    return (
        <Grid container spacing={1}>
            {data?.map((item) => {
                const name = item[filedNames.name] || item?.url?.split("/")?.pop();
                return (
                    <Tooltip title={name} disableHoverListener={!isShowHover} key={item[filedNames.value]}>
                        <Grid item xs={"auto"} sx={{ maxWidth: "100%" }}>
                            <Card
                                className={`${styles.tab} ${active === item[filedNames.value] ? styles.active : ""}`}
                                sx={{ minWidth: minWidth, height: 40 }}
                            >
                                <input
                                    type="checkbox"
                                    id={`${item[filedNames.value]}_${props.id}`}
                                    className={styles.checkbox}
                                    checked={active === item[filedNames.value]}
                                    onChange={(e) => handleChange(e, item)}
                                />
                                <label className={styles.label} htmlFor={`${item[filedNames.value]}_${props.id}`}>
                                    {" "}
                                    <Typography noWrap textOverflow="ellipsis" component={"span"}>
                                        {name}
                                    </Typography>
                                </label>
                                <span style={{ opacity: 0, pointerEvents: "none" }}>{name}</span>
                            </Card>
                        </Grid>
                    </Tooltip>
                );
            })}
        </Grid>
    );
};

export default ItemInlineText;
