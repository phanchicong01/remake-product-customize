import React, { useState, useEffect } from "react";
import { Card, Grid, InputBase } from "@mui/material";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import Button from "@mui/material/Button";
import Collapse from "@mui/material/Collapse";
import Typography from "@mui/material/Typography";
import RenderItemClipArtAdvanced from "components/common/GeneralItemAdvanced/RenderItemClipArtAdvanced";
import { styled } from "@mui/styles";

const GeneralItemAdvanced = ({ data, onClickAction, type, noneBackground = false, valueChecked }) => {
    const [dataSelected, setDataSelected] = useState<any>(null);
    const [showGroupClipArt, setShowGroupClipArt] = useState<any>(false);

    useEffect(() => {
        setDataSelected(valueChecked);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    const handelChange = (itemChecked) => {
        onClickAction("itemAdvanced", itemChecked);
        setDataSelected(itemChecked);
    };
    const [clipArtSelect, setClipArtSelect] = useState("");

    const handleChangeSelect = (event: SelectChangeEvent) => {
        const dataSelected = data?.children?.find((child) => child.id + "" === (event.target.value as string));
        setClipArtSelect(event.target.value as string);
        setDataSelected(dataSelected);
    };
    return (
        <Grid item xs={12}>
            <Card
                sx={{
                    background: !noneBackground ? "#343a40" : "transparent",
                    position: "relative",
                    minHeight: "40px",
                    color: "#f2f2f2",
                    p: 1,
                }}
            >
                <Grid container>
                    {type === "group-clip-art" ? (
                        <>
                            {type === "group-clip-art" && (
                                <Grid item xs={12}>
                                    {!showGroupClipArt && !noneBackground && (
                                        <Button onClick={() => setShowGroupClipArt(true)}>
                                            Show option to select default value
                                        </Button>
                                    )}
                                    <Collapse in={showGroupClipArt}>
                                        {!data && !noneBackground && (
                                            <Typography
                                                sx={{ color: "#ffff", opacity: 0.5, fontSize: 14 }}
                                                component={"span"}
                                            >
                                                Set value to grouped clipart to set default value
                                            </Typography>
                                        )}

                                        {Array.isArray(data?.children) && (
                                            <Select
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={clipArtSelect}
                                                sx={{ p: 1.5 }}
                                                fullWidth
                                                onChange={handleChangeSelect}
                                                input={<CustomInput size={"small"} />}
                                            >
                                                {data?.children?.length === 0 && (
                                                    <MenuItem key={0} value="">
                                                        List empty
                                                    </MenuItem>
                                                )}
                                                {data?.children?.map((item, index: number) => {
                                                    return (
                                                        <MenuItem key={index} value={`${item.id}`}>
                                                            {item.name}
                                                        </MenuItem>
                                                    );
                                                })}
                                            </Select>
                                        )}

                                        {dataSelected && (
                                            <RenderItemClipArtAdvanced
                                                items={dataSelected?.items || []}
                                                onChange={handelChange}
                                                id={`dependency_value_group_clipart`}
                                                value={dataSelected}
                                            />
                                        )}
                                    </Collapse>
                                </Grid>
                            )}
                        </>
                    ) : (
                        <>
                            {!data && !noneBackground && (
                                <Typography sx={{ color: "#ffff", opacity: 0.5, fontSize: 14 }} component={"span"}>
                                    Set value to clipart to set default value
                                </Typography>
                            )}
                            <RenderItemClipArtAdvanced
                                items={data?.items || []}
                                onChange={handelChange}
                                id={`dependency_value_clipart`}
                                value={dataSelected}
                            />
                        </>
                    )}
                </Grid>
            </Card>
        </Grid>
    );
};

export default GeneralItemAdvanced;
const CustomInput = styled(InputBase)(({ theme }) => ({
    "label + &": {
        marginTop: "1.5rem",
        color: "#fff",
    },
    "& .MuiInputBase-input": {
        borderRadius: 4,
        position: "relative",
        backgroundColor: "#fff",
        border: "1px solid #ced4da",
        fontSize: 16,
        padding: "8px 26px 8px 12px",

        // Use the system font instead of the default Roboto font.
        "&:focus": {
            borderRadius: 4,
            borderColor: "#80bdff",
            boxShadow: "0 0 0 0.2rem rgba(0,123,255,.25)",
        },
    },
}));
