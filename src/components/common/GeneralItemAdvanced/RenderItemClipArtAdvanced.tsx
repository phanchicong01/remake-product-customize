import Grid from "@mui/material/Grid";
import ItemChecked from "../item-checked";

interface IProps {
    items: any[];
    onChange: (item) => void;
    value?: any;
    id: any;
}

const RenderItemClipArtAdvanced = ({ items, onChange, value, id }: IProps) => {
    const handleChange = (id: any, item: any) => {
        onChange(item);
    };
    return (
        <Grid container>
            <Grid item xs={12}>
                <ItemChecked data={items} onChange={handleChange} id={`${id}_advanced`} value={value?.id} />
            </Grid>
        </Grid>
    );
};

export default RenderItemClipArtAdvanced;
