import React, { ChangeEvent, memo, useCallback, useEffect, useRef, useState } from "react";
import OutlinedInput from "@mui/material/OutlinedInput";

import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";

import styled from "@mui/styles/styled";
import isEqual from "lodash/isEqual";
import _debounce from "lodash/debounce"

function usePrevious(value) {
    // The ref object is a generic container whose current property is mutable ...
    // ... and can hold any value, similar to an instance property on a class
    const ref = useRef();
    // Store current value in ref
    useEffect(() => {
        ref.current = value;
    }, [value]); // Only re-run if value changes
    // Return previous value (happens before update in useEffect above)
    return ref.current;
}

type Props = {
    label: string, colLabel?: number, hiddenLabel?: boolean, value: any,
    onChangeValue: (name: string, value: string) => void;
    [otherProps: string]: unknown

}
const InputCustom = ({ label = null, colLabel = 4, hiddenLabel = false, value, onChangeValue, ...otherProps }: Props) => {
    const [localState, setLocalState] = useState('');
    const previousValue = usePrevious(value)
    useEffect(() => {
       
        if (!isEqual(previousValue, value)) {
            setLocalState(value)

        }
    }, [localState, value])

    const handleChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
        const value = event.target.value;
        const name = event.target.name;
        setLocalState(value)
        _debounce(() => onChangeValue(name, value), 3000)
    }, [onChangeValue])


    return (
        <Grid container alignItems="center" rowSpacing={1}>
            {label && (
                <Grid item xs={colLabel} style={{ opacity: hiddenLabel ? 0 : 1 }}>
                    <TypographyCustom>{label}</TypographyCustom>
                </Grid>
            )}
            <Grid item xs>
                {<OutlinedInputCustom value={localState} onChange={handleChange} fullWidth size="small" {...otherProps} />}
            </Grid>
        </Grid>
    );
};

export default memo(InputCustom);
const OutlinedInputCustom = styled(OutlinedInput)({
    "&.MuiOutlinedInput-root": {
        color: "#000",
        background: "white",
    },
});
const TypographyCustom = styled(Typography)({
    "&.MuiTypography-root": {
        color: "#fff",
        fontWeight: "bold",
    },
});
