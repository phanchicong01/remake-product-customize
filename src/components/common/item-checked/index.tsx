import React, { useEffect, useState } from "react";
import Card from "@mui/material/Card";

import CardMedia from "@mui/material/CardMedia";
import Grid from "@mui/material/Grid";

import Tooltip from "@mui/material/Tooltip";

import styles from "./styles.module.scss";
interface IProps {
    data: {
        id: any;
        name: string;
    }[];
    onChange: (idChecked: any, item?: any) => void;
    key_url?: any;
    id: any;
    value?: any;
    isShowHover?: any;
    max_width?: number;
}
const ItemChecked = ({ data, onChange, key_url = "url", id, value, isShowHover, max_width = 60 }: IProps) => {
    // const valueActive = layers?.findIndex((layer) => layer.id === layerActive?.id);
    const [valueActive, setActive] = useState<any>(value);
    useEffect(() => {
        setActive(value);
    }, [value]);

    const handleClickTab = (e: any, item: any) => {
        onChange(item?.id, item);
    };
    const handleChange = () => {
        console.log('handleChange');
    };
    return (
        <Grid container spacing={1}>
            {data?.map((item: any, index: number) => {
                const name = item.title || item.url?.split("/")?.pop() || item[key_url]?.split("/")?.pop();
                return (
                    <Tooltip title={name} disableHoverListener={!isShowHover} key={index}>
                        <Grid item>
                            <Card
                                className={`${styles.tab} ${valueActive === item.id ? styles.active : ""}`}
                                sx={{ maxWidth: max_width, maxHeight: max_width, width: 60, height: 60 }}
                            >
                                <input
                                    type="checkbox"
                                    id={`${item.id}_${id}`}
                                    className={styles.checkbox}
                                    checked={valueActive === item.id}
                                    onChange={handleChange}
                                    onClick={(e) => handleClickTab(e, item)}
                                />
                                <label htmlFor={`${item.id}_${id}`} className={styles.label}>
                                    {item.id}
                                </label>
                                {key_url === "color" ? (
                                    <div
                                        style={{
                                            width: 60,
                                            height: 60,
                                            padding: "0.25rem",
                                            backgroundColor: item[key_url],
                                        }}
                                    ></div>
                                ) : (
                                    <CardMedia
                                        sx={{ objectFit: "contain", height: "100%" }}
                                        component="img"
                                        image={item[key_url]}
                                        alt={item.name}
                                    />
                                )}
                            </Card>
                        </Grid>
                    </Tooltip>
                );
            })}
        </Grid>
    );
};

export default ItemChecked;
