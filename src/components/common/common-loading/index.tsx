import { toAbsoluteUrl } from 'helpers'
import React from 'react'
import styles from './styles.module.scss'
type Props = {
    show: boolean
}

const CommonLoading = ({ show }: Props) => {
    return (<div className={show ? styles.container : styles.hide}>

        <div className={styles.loader}>
            {/* <img src={toAbsoluteUrl('/media/logos/logo-thumb.jpg')} alt='loading...' /> */}
        </div>
    </div>
    )
}

export default CommonLoading
