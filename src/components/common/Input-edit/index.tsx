import React, { useCallback, useRef, useState } from 'react'
import _debounce from "lodash-es/debounce"
import _isEmpty from "lodash-es/isEmpty"
import clsx from 'clsx'

type Props = {
  value: any,
  classNameLabel?: string,
  classNameInput?: string,
  classNames?: string,
  onChange: (value: any, name?: string) => void
  loading?: boolean,
  name?: string,
  isShowConfirm?: boolean;
  size?:"sm" | "lg"
}

const InputEdit = ({size, value, name, classNameLabel, classNameInput, classNames, onChange, loading, isShowConfirm }: Props) => {
  const [inputValue, setInputValue] = useState<string>(value);
  const [isEdit, setIsEdit] = useState<boolean>(false);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const handleOnChangeDebounce = _debounce((value) => {
    return onChange(value, name)
  }, 500)

  const handleOnChangeInput = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value } = event.target;
      setInputValue(value)
      if(isShowConfirm || _isEmpty(value) ) return ;
      handleOnChangeDebounce(value)
    },
    [handleOnChangeDebounce, isShowConfirm],
  )

  const handleOnBlur = () => {
    if(isShowConfirm || _isEmpty(inputValue)) return ;
    if (value !== inputValue) {
      onChange(inputValue, name)
    }
    setIsEdit(false)
  }

  const handleOnclickEdit = useCallback(() => {
    setIsEdit(true);
    ref.current?.focus();
  }, [])

  const handleConfirm = useCallback(( )=> {
    if(!_isEmpty(inputValue)){
      onChange(inputValue, name)
      setIsEdit(false)
    }
  }, [inputValue, onChange, name])

const handleCancel = () =>{
  setInputValue(value)
  setIsEdit(false)
}
  const ref = useRef(null)
  return (
    <div className={clsx('me-2 text-truncate', classNames)}>
      {
        !isEdit ? <span
          className={classNameLabel}
          onClick={handleOnclickEdit}
        >{inputValue}</span> :
          <div className="position-relative d-flex align-items-center">
            <input
              disabled={loading}
              ref={ref}
              onBlur={handleOnBlur}
              name={name}
              onChange={handleOnChangeInput}
              type="text" className={clsx('form-control form-sm', classNameInput , {
                    'me-2 col':isShowConfirm,
                    'form-control-sm':size === "sm",
                    'form-control-lg':size === "lg",
              })}
              value={inputValue} />
             {
              isShowConfirm &&
               <div className='w-auto d-flex'>
                <div onClick={handleConfirm} className={clsx('btn btn-bg-primary me-2 btn-icon btn-active-color-white', {
                    'btn-sm':size === "sm",
                    'btn-lg':size === "lg",
                })}>
                  <i className="fas fa-check text-white"></i>
                </div>
                <div onClick={handleCancel} className={clsx('btn btn-bg-secondary btn-icon btn-active-color-primary', {
                    'btn-sm':size === "sm",
                    'btn-lg':size === "lg",
                })}>
                  <i className="fas fa-times text-dark" ></i>
                </div>
             </div>
             }
          </div>
      }
    </div>
  )
}

export default InputEdit