import React, { useState, useEffect } from "react";
import useImage from "use-image";
import { Image, Transformer, Text } from "react-konva";
import { roughScale } from "utils";

const Reposition = ({ shapeProps, visible, onChange }: any) => {
    const shapeRef = React.useRef<any>();
    const trRef = React.useRef<any>();
    const [img] = useImage(shapeProps.src);

    const handleTransform = (e) => {
        // transformer is changing scale of the node
        // and NOT its width or height
        // but in the store we have only width and height
        // to match the data better we will reset scale on transform end
        const node: any = shapeRef.current;
        const scaleX = node.scaleX();
        const scaleY = node.scaleY();

        // we will reset it back
        node.scaleX(1);
        node.scaleY(1);
        onChange({
            ...shapeProps,
            x: roughScale(node.x()),
            y: roughScale(node.y()),
            rotation: roughScale(node.rotation()),
            // set minimal value
            width: roughScale(Math.max(5, node.width() * scaleX)),
            height: roughScale(Math.max(node.height() * scaleY)),
            // scaleX,
            // scaleY
            // fillPatternScaleX: Math.max(3, node.width() * scaleX) / shapeProps.widthImage,
            // fillPatternScaleY:Math.max(node.height() * scaleY) / shapeProps.heightImage
        });
    };

    const handleDragEnd = (e) => {
        onChange({
            ...shapeProps,
            x: e.target.x(),
            y: e.target.y(),
        });
    };

    React.useLayoutEffect(() => {
        if (!!trRef.current && !!shapeRef.current) {
            // we need to attach transformer manually
            trRef.current.nodes([shapeRef.current]);
            trRef.current.getLayer().batchDraw();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [trRef.current, shapeRef.current]);

    if (!shapeProps) {
        return null;
    }

    return (
        <React.Fragment>
            {!!img ? (
                <Image
                    ref={shapeRef}
                    {...shapeProps}
                    // zIndex={shapeProps.zIndex}
                    image={img}
                    name="shape"
                    visible={visible}
                    draggable={true}
                    onDragEnd={handleDragEnd}
                    onTransform={handleTransform}
                />
            ) : (
                <Text
                    ref={shapeRef}
                    {...shapeProps}
                    // zIndex={shapeProps.zIndex}
                    text="Repositioning..."
                    align={"center"}
                    verticalAlign="middle"
                    name="shape"
                    fill="#0e83cd"
                    fontSize={50}
                    visible={visible}
                    draggable={true}
                    onDragEnd={handleDragEnd}
                    onTransform={handleTransform}
                />
            )}

            {(
                <Transformer
                    ref={trRef}
                    rotateEnabled={true}
                    shouldOverdrawWholeArea
                    boundBoxFunc={(oldBox, newBox) => {
                        // limit resize
                        if (newBox.width < 5 || newBox.height < 5) {
                            return oldBox;
                        }
                        return newBox;
                    }}
                />
            )}
        </React.Fragment>
    );
};

export default Reposition;
