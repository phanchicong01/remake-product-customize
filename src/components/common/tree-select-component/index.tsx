import {  TreeSelect, TreeSelectProps } from 'antd';
import clsx from 'clsx';
import React from 'react'

interface Props extends TreeSelectProps  {
    dataTree:any,
    className?:string,
    [unknown:string]:any
}

const TreeSelectComponent = React.forwardRef(({dataTree,className,...otherProps}: Props , ref) => {
  
  return (
    <TreeSelect
        size="large"
        treeIcon={false}
        className={clsx(
            'form-select-tree form-select-tree-solid ',
            className
        )}
        showSearch
        treeData={dataTree}
        fieldNames={{
            label: 'name',
            children: 'children',
            value: 'id'
        }}
       treeDefaultExpandAll
        {...otherProps}
    />
  )
})

export default TreeSelectComponent