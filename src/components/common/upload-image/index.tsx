import uploadFileWithThumbnail from 'apis/upload-file-with-thumbnail';

import { UploadFileResponse } from 'apis/upload-files/interfaces';
import clsx from 'clsx';
import { getUrl } from 'helpers';
import React, {  useState } from 'react'
import { toast } from 'react-toastify';
import Thumb from '../thumb'
import styles from "./styles.module.scss"

type Props = {
    isPreview?: boolean;
    onChange?: (image:UploadFileResponse) => void;
    readOnly?: boolean;
    src?: string;
    id: any
    title?:string;
    classNameWrapper?:string,
    classPreview?:string,
    classTitle?:string,
    loading?:boolean,
    size?:{
      width:number,
      height:number
    }

}

const UploadImage = ({size={width: 75 , height:75}, loading ,  isPreview = true, onChange, src, readOnly , title="Change thumbnail"  , classNameWrapper , classPreview , classTitle , id}: Props) => {
    const [lastFile, setLastFile] = useState<File>(null)
    const [isLoading, setIsLoading] = useState(loading)

    const handleChangeUpload = async (
        event: React.ChangeEvent<HTMLInputElement>,
      ) => {
        const files = event.target.files
    
        if (files) {
          setIsLoading(true)
          try {
            const newFiles = Array.from(files)
            setLastFile(newFiles[0])
            const newFromData = new FormData();
            newFromData.append("file", newFiles[0])
            newFromData.append("folder", "media")
            const response = await uploadFileWithThumbnail(newFromData)
            if(response.code === 200){
              setIsLoading(false);
              onChange(response.data)
            }
          } catch (error) {
            toast.error("Something went wrong. Please try again!")
            setIsLoading(false)
          }
          
        }
       
      }
    return (
        <div className={clsx(styles.classNameWrapper,classNameWrapper)}>
            <div className={styles.thumbnail} 
            style={{width:size.width , height:size.height, backgroundImage: "url('/bg-transparent.png')" }} 
            role="button">
                <label role="button" className={clsx(styles.labelThumbnail , classTitle , {
                    "d-none":readOnly
                })} htmlFor={'upload-input-' + id} >
                    {title}
                </label>
                {
                  isLoading &&  
                  <label style={{borderRadius:"5px",background:"rgba(0,0,0,0.3)" }} className={clsx('position-absolute top-0 start-0 w-100 h-100 d-flex justify-content-center align-items-center')}>
                       <div className="spinner-border text-white" role="status">
                        </div>
                  </label>
                }
                {
                    !readOnly && <input onChange={handleChangeUpload} className='d-none' id={'upload-input-' + id} type="file" accept='.png,.jpg,.jpeg' />
                }
                {
                    !isLoading && isPreview && (lastFile) && <label className={clsx('position-absolute top-0 start-0 w-100 h-100', classPreview)}>
                        <Thumb width={"100%"} height="100%" file={lastFile}  />
                    </label>
                }
                {
                    (!isLoading && src && !lastFile) && <label className={clsx('round-sm position-absolute top-0 start-0 w-100 h-100', classPreview)}>
                                <img src={getUrl(src)}
                                    alt={'Thumb'}
                                    className={styles.imgThumbnail}
                                    style={{background:readOnly? "transparent" :  '#fff'}}
                         />
                        </label>
                }
            </div>
        </div>
    )
}

export default UploadImage