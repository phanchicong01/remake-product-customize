import styled from '@emotion/styled';
import InputBase from "@mui/material/InputBase"
export const CustomInput = styled(InputBase)(({ theme }) => ({
    "label + &": {
        marginTop: "1.5rem",
        color: "#fff",
    },
    "&.MuiInputBase-root": {
        position: "relative",
        "& .btn-clear-icon": {
            position: "absolute",
            right: "1.25em",
            padding: "4px",
            color: "rgba(0, 0, 0, 0.54)",
            borderRadius: '50%',
            fontSize: "1.25rem",

            '&.MuiSvgIcon-root': {

            },
            "&:hover": {
                background: 'rgba(0, 0, 0, 0.04)',
                cursor: "pointer"
            }
        }
    },
    "& .MuiInputBase-input": {
        borderRadius: 4,
        position: "relative",
        backgroundColor: "#fff",
        border: "1px solid #ced4da",
        fontSize: 16,
        padding: "8px 26px 8px 12px",
        // Use the system font instead of the default Roboto font.
        "&:focus": {
            borderRadius: 4,
            borderColor: "#80bdff",
            boxShadow: "0 0 0 0.2rem rgba(0,123,255,.25)",
        },

    },
}));