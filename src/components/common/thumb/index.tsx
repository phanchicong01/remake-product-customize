import { useEffect, useState } from "react";

const Thumb = ({ file=null , width  = "100px" , height = "100px" }) => {
    const [thumb, setThumb] = useState<any>(null)
    const [loading, setLoading] = useState<boolean>(false)
 

    useEffect(() => {
        if (!!file) {
            setLoading(true);
            let reader = new FileReader();
            reader.onloadend = () => {
                setLoading(false)
                setThumb(reader.result)
            };
            reader.readAsDataURL(file);
        }
    }, [file])


    if (!thumb ) { return null; }

    if (loading) { return <p>loading...</p>; }
    return (<img src={thumb}
        alt={'Thumb'}
        className="img-thumbnail "
        style={{ objectFit: "contain", height: height, width: width }} />);
}

export default Thumb