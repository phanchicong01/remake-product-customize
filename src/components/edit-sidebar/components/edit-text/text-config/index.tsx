import React, { useState, useEffect, useCallback } from "react";
import axios from "axios";
import Select from "antd/lib/select";

import Grid from "@mui/material/Grid";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import FormControlLabel from "@mui/material/FormControlLabel";
import RadioGroup from "@mui/material/RadioGroup";
import Radio from "@mui/material/Radio";
import Switch from "@mui/material/Switch";
import Typography from "@mui/material/Typography";

import { LayerText } from "store/template-reducer/interface";
import { valueFontKind, fontKind } from "store/fonts-family/interface";

import { onUpdateAttrLayer } from "store/template-reducer/templateSlice";
import { createGoogleFonts } from "store/fonts-family/fontsFamilySlice";

import { useAppDispatch, useAppSelector } from "hooks";
import { debounce, roughScale } from "utils";
import { API_KEY_GOOGLE_FONTS } from "constants/index";
import VirtualizedSelect from "./component/virtualized-select";
import InputCustom from "components/common/InputCustom";
import MenuFontStyle from "./component/menu-font-style";
import { Helmet } from "react-helmet";
import WebFont from 'webfontloader';
import isEqual from "lodash/isEqual"
import styles from "./styles.module.scss"
import LoadingSelect from "./component/loading";
interface IProps {
    data: LayerText
    onChangeAttr: (data) => void;
}

const TextConfig = ({ data, onChangeAttr }: IProps) => {
    const { templateActive } = useAppSelector((state) => state.template);
    const { customFonts, googleFonts } = useAppSelector((state) => state.fontsFamily);

    const [stateData, setStateData] = useState<LayerText>({
        fontKind: "webfonts#webfontList"
    });
    const [customFontSelected, setCustomFontSelected] = useState(null)
    const [loadingFont, setLoadingFont] = useState(false)

    const dispatch = useAppDispatch();

    const handleChangeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        const name = event.target.name;
        const value = event.target.value;
        if (name === "strokeWidth" || name === "fontSize") {
            const newValue = roughScale(value);
            return handleChangeAttrs({ [name]: newValue });
        }
        return handleChangeAttrs({ [name]: value });
    };
    const handleChangeSelectFont = (value, options) => {
        let fontItem = null
        setLoadingFont(true)
        if (stateData.fontKind === "webfonts#webfontList") {
            fontItem = googleFonts?.items?.find(item => value === item.family)
            WebFont.load({
                google: {
                    families: [value]
                },
                fontactive: function () {
                    setLoadingFont(false)
                    handleChangeAttrs({ fontFamily: value, fontKind: stateData.fontKind })
                    setStateData((prevState) => ({ ...prevState, fontFamily: value }));
                }
            })

        }
        if (stateData.fontKind === "customfonts#customfontList") {
            fontItem = customFonts?.items?.find(item => value === item.family)
            let markup = [
                '@font-face {\n',
                '\tfont-family: \'', fontItem.name, '\';\n',
                '\tfont-style: \'normal\';\n',
                '\tfont-weight: \'normal\';\n',
                '\tsrc: url(\'', fontItem.url, '\');\n',
                '}\n'
            ].join('');


            setCustomFontSelected(markup)

            WebFont.load({
                custom: {
                    families: [value],
                },
                fontactive: function () {
                    setLoadingFont(false)
                    handleChangeAttrs({ fontFamily: value, fontKind: stateData.fontKind })
                    setStateData((prevState) => ({ ...prevState, fontFamily: value }));
                }
            })
        }

    };
    const handleChangeRadio = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = (event.target as HTMLInputElement).value as fontKind;

        return setStateData((prev) => ({ ...prev, fontKind: value }));
    };
    useEffect(() => {
        if (googleFonts.items.length <= 0) {
            axios({
                method: "get",
                url: `https://www.googleapis.com/webfonts/v1/webfonts?key=${API_KEY_GOOGLE_FONTS}`,
                // responseType: "stream",
            }).then(function (response) {
                dispatch(createGoogleFonts(response.data.items));
            });
        }

    }, []);
    useEffect(() => {
        if (!!data && !isEqual(data, stateData)) {
            setStateData(data)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [data])

    const handleChangeAttrs = (attrsUpdate) => {
        const newAttrs = { ...attrsUpdate };
        debounceDispatch({ ...stateData, ...newAttrs });
        setStateData((prev) => ({ ...prev, ...newAttrs }));
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
    const debounceDispatch = useCallback(
        debounce(
            (stateData) =>
                dispatch(
                    onUpdateAttrLayer({
                        newAttrs: stateData,
                    })
                ),
            300
        ),
        []
    );
    const handleChangeSwitch = (event: React.ChangeEvent<HTMLInputElement>) => {
        const name = event.target.name;
        const checked = event.target.checked;
        const newAttrs = { ...stateData, [name]: checked };
        setStateData((prevState) => ({ ...prevState, [name]: checked }));
        debounceDispatch(newAttrs);
    };



    if (!stateData) {
        return null;
    }
    return (
        <Grid container rowSpacing={2} >
            <Helmet >
                <style type="text/css" >
                    {customFontSelected}
                </style>

            </Helmet>

            <Grid item xs={12} px={2}>
                <InputCustom
                    onChange={handleChangeInput}
                    value={stateData.text}
                    name="text"
                    id="text"
                    label={"Preview text:"}
                    colLabel={12}
                />
            </Grid>
            <Grid item xs={12} px={2}>
                <InputCustom
                    onChange={handleChangeInput}
                    value={stateData?.fontSize}
                    name="fontSize"
                    id="fontSize"
                    type="number"
                    label={"Font Size:"}
                    colLabel={12}
                />
            </Grid>
            <Grid item xs={6} px={2}>
                <Typography
                    sx={{
                        color: "#fff",
                        fontWeight: "bold",
                        margin: "auto",
                        display: "flex",
                        height: "100%",
                        alignItems: "center",
                    }}
                >
                    Font Style
                </Typography>
            </Grid>
            <Grid item xs={6}>
                <MenuFontStyle data={stateData} onChangeAttr={handleChangeAttrs} />
            </Grid>
            <Grid item xs={10} pl={2}>
                <InputCustom
                    onChange={handleChangeInput}
                    value={stateData.fill}
                    name="fill"
                    id="color"
                    colLabel={12}
                    label={"Text color:"}
                />
            </Grid>
            <Grid item xs={2} pr={2}>
                <InputCustom
                    onChange={handleChangeInput}
                    value={stateData.fill}
                    inputProps={{
                        sx: { p: 0, height: 40 },
                    }}
                    hiddenLabel={true}
                    name="fill"
                    id="color"
                    type={"color"}
                    label={"color"}
                    colLabel={12}
                />
            </Grid>
            <Grid item xs={12} px={2}>
                <FormControlLabel
                    sx={{ color: "white" }}
                    value="show-option"
                    checked={!!stateData?.strokeEnabled}
                    onChange={handleChangeSwitch}
                    name={"strokeEnabled"}
                    control={
                        <Switch
                            sx={{
                                color: "white",
                            }}
                        />
                    }
                    label="Enable Stroke"
                />
            </Grid>
            {
                stateData?.strokeEnabled && (
                    <>
                        <Grid item xs={10} pl={2}>
                            <InputCustom
                                onChange={handleChangeInput}
                                value={stateData.stroke}
                                name="stroke"
                                id="color"
                                colLabel={12}
                                label={"Stroke color:"}
                            />
                        </Grid>
                        <Grid item xs={2} pr={2}>
                            <InputCustom
                                onChange={handleChangeInput}
                                value={stateData.stroke}
                                inputProps={{
                                    sx: { p: 0, height: 40 },
                                }}
                                name="stroke"
                                id="stroke"
                                label="color"
                                hiddenLabel={true}
                                colLabel={12}
                                type={"color"}
                            />
                        </Grid>
                        <Grid item xs={12} px={2}>
                            <InputCustom
                                onChange={handleChangeInput}
                                value={stateData?.strokeWidth || 0}
                                name="strokeWidth"
                                id="strokeWidth"
                                type="number"
                                colLabel={12}
                                label={"Stroke Width:"}
                            />
                        </Grid>
                    </>
                )
            }
            <Grid item xs={12} px={2}>
                <FormControl component="fieldset">
                    <FormLabel component="b" sx={{ fontWeight: 900, color: "white", textAlign: "left" }}>
                        Which kind of fonts do you want to use?:
                    </FormLabel>
                    <RadioGroup
                        aria-label="Personalize Options"
                        value={stateData.fontKind}
                        name={"personalizeOption"}
                        onChange={handleChangeRadio}
                    >
                        {[
                            { name: "Google fonts", value: valueFontKind.googleFonts },
                            { name: "Custom fonts", value: valueFontKind.customFonts },
                        ].map((item: any, index: number) => (
                            <FormControlLabel
                                key={index}
                                sx={{ color: "white", fontWeight: 600 }}
                                value={item.value}
                                control={
                                    <Radio
                                        size="small"
                                        sx={{
                                            pt: 0,
                                            pb: 0,
                                            color: "white",
                                        }}
                                    />
                                }
                                label={item.name}
                            />
                        ))}
                    </RadioGroup>
                </FormControl>
            </Grid>
            <Grid item xs={12} px={2} sx={{ position: "relative" }}>
                {stateData.fontKind === "webfonts#webfontList" && (
                    <div >
                        <VirtualizedSelect
                            label="Font"
                            value={stateData.fontFamily}
                            onChange={handleChangeSelectFont}
                            name="fontFamily"
                            data={googleFonts.items}
                        />
                        {loadingFont &&
                            <div className={styles.loadingFont}><LoadingSelect width={40} height={40} /></div>
                        }

                    </div>
                )}

            </Grid>
            <Grid item xs={12} px={2} >
                {stateData.fontKind === "customfonts#customfontList" && (
                    <div style={{ position: "relative" }}>
                        <Select
                            showSearch
                            placeholder="Select a font"
                            optionFilterProp="children"
                            fieldNames={{ label: "family", value: "family" }}
                            onChange={handleChangeSelectFont}
                            // onSearch={onSearch}
                            size={"large"}
                            style={{ width: "100%" }}
                            value={stateData.fontFamily}
                            options={customFonts?.items}

                        />
                        {loadingFont &&
                            <div className={styles.loadingFont}><LoadingSelect width={40} height={40} /></div>
                        }
                    </div>
                )}

            </Grid>
        </Grid >
    );
};

export default TextConfig;
