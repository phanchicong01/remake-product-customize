import React from "react";
import Grid from "@mui/material/Grid";
import FormatTextdirectionRToLIcon from '@mui/icons-material/FormatTextdirectionRToL';
import ColorLensIcon from '@mui/icons-material/ColorLens';
import VisibilityIcon from '@mui/icons-material/Visibility';
import ElectricalServicesIcon from '@mui/icons-material/ElectricalServices';

import { useDispatch } from "react-redux";
import { onUpdateAttrConfig, onUpdateAttrLayer } from "store/template-reducer/templateSlice";
import { LayerCommon, LayerText } from "store/template-reducer/interface";

import CollapseComponent from "../common/collapse-component"
import ConditionSetting from "../common/condition-setting";
import PersonalizationSetting from "./personalization-setting";
import TextConfig from "./text-config";
import InputCustom from "components/common/InputCustom";

type IData = LayerCommon & LayerText
interface IProps {
  data: IData;
  onChange: (data: any) => void;
}


const EditText = ({ onChange, data }: IProps) => {
  const dispatch = useDispatch()

  const handleChangeTextConfig = (newAttrs, nameAttrs: string) => {
    console.log(newAttrs, "handleChangeTextConfig");
    dispatch(onUpdateAttrConfig({ newAttrs, nameAttrs }));
    // const newData = {
    //   ...data,
    //     }
    //   }
    // }
    // dispatch(onUpdateAttrLayer({ newAttrs: newData }))
  };

  const handleChangePersonalization = (newAttrs: any) => {
    const newData = {
      ...data,
      configuration: {
        ...data?.configuration,
        personalization: {
          ...data?.configuration?.personalization,
          ...newAttrs
        }
      }
    }
    dispatch(onUpdateAttrLayer({ newAttrs: newData }))
  }

  const handleChangeExtra = (event: React.ChangeEvent<HTMLInputElement>) => {
    const name = event.target.name;
    const value = event.target.value;
    dispatch(onUpdateAttrLayer({
      newAttrs: {
        ...data,
        configuration: {
          ...data?.configuration,
          extra: {
            ...data?.configuration?.extra,
            [name]: value
          }
        }
      }
    }))
  }
  const handleChangeGeneral = (newAttrs: any) => {
    dispatch(onUpdateAttrLayer(newAttrs));

  }
  return (
    <Grid container >
      <Grid item xs={12}>
        <CollapseComponent title={"General"} icon={<FormatTextdirectionRToLIcon />}>
          <TextConfig data={data} onChangeAttr={handleChangeGeneral} />
        </CollapseComponent>
        <CollapseComponent title={"PERSONALIZATION SETTING"} icon={<ColorLensIcon />}>
          <PersonalizationSetting data={data?.configuration?.personalization} onChangeAttr={handleChangePersonalization} />
        </CollapseComponent>
        <CollapseComponent title={"CONDITIONAL SETTING"} icon={<VisibilityIcon />}>
          <ConditionSetting data={data?.configuration?.conditionalSetting} onChangeAttr={(newAttrs) => handleChangeTextConfig(newAttrs, "conditionalSetting")} />
        </CollapseComponent>
        <CollapseComponent title={"EXTRA"} icon={<ElectricalServicesIcon />}>
          <div style={{ padding: " 0 15px" }}>
            <InputCustom
              colLabel={12}
              label="Custom class:"
              onChange={handleChangeExtra}
              value={data?.configuration?.extra}
            />
          </div>
        </CollapseComponent>
      </Grid>
    </Grid>
  );
};

export default EditText;
