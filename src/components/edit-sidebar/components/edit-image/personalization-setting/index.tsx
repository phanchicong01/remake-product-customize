import React, { useCallback, useEffect, useMemo, useState } from 'react'
import Grid from "@mui/material/Grid";
import Switch from "@mui/material/Switch";
import FormControlLabel from "@mui/material/FormControlLabel";
import InputCustom from "components/common/InputCustom";
import { IPersonalizationImage, LayerImage } from 'store/template-reducer/interface';
import { FormControl, FormLabel, Radio, RadioGroup } from '@mui/material';
import isEqual from "lodash/isEqual"
import { debounce } from 'utils';
import { OptionPersonalizationImage } from 'store/template-reducer/enum';
import { useAppDispatch, useAppSelector } from 'hooks';
import { closeReposition } from 'store/re-position-reducer/RePositionSlice';

type Props = {
    onChangeAttr: (data) => void;
    data: IPersonalizationImage
}
const dataSwitch = [
    {
        name: "toggleShowLayer",
        label: "Add toggle to show/hide this layer",
        value: "toggleShowLayer",
    },
    {
        name: "defaultValueToggleShow",
        label: "Pre-enable toggle (show layer by default)",
        value: "defaultValueToggleShow",
    },
];
const options = [
    {
        name: "option",
        label: "No personalization",
        value: OptionPersonalizationImage.none,
    },
    {
        name: "option",
        label: "1 Clipart category",
        value: OptionPersonalizationImage.clipart,
    },
    {
        name: "option",
        label: "1 Group of Clipart categories",
        value: OptionPersonalizationImage.groupClipart,
    },
    {
        name: "option",
        label: "Upload photo",
        value: OptionPersonalizationImage.uploadPhoto,
    },
]
const initPersonalization: IPersonalizationImage = {
    toggleShowLayer: false,
    defaultValueToggleShow: true,
    option: "none"

}
const PersonalizationSetting = (props: Props) => {
    const { data, onChangeAttr } = props
    const [dataState, setDataState] = useState<IPersonalizationImage>(initPersonalization)
    const { isReposition } = useAppSelector((state) => state.rePosition)
    const dispatch = useAppDispatch()
    const handleChangeSwitch = (event: React.ChangeEvent<HTMLInputElement>) => {
        const name = event.target.name;
        const checked = event.target.checked;
        onChangeAttr({ ...dataState, [name]: checked })

    };


    const handleChangeRadio = (event) => {
        const value = (event.target as HTMLInputElement).value;
        const name = (event.target as HTMLInputElement).name;
        if (isReposition) {
            dispatch(closeReposition())
        }
        onChangeAttr({ ...dataState, [name]: value })
        // return setDataState((prev) => ({ ...prev, [name]: value }));
    }
    useEffect(() => {
        if (!!data) {
            setDataState({ ...data })
        }
    }, [data])


    return (
        <Grid container rowGap={2} columnSpacing={2} sx={{ px: 2 }}>
            <Grid item xs={12}>
                {dataSwitch.map((switchItem, index) => {
                    if (switchItem.name === "defaultValueToggleShow" && !dataState.toggleShowLayer) {
                        return null
                    }
                    return (
                        <FormControlLabel
                            sx={{ color: "white" }}
                            value={switchItem.value}
                            checked={dataState[switchItem.name]}
                            name={switchItem.name}
                            key={index}
                            onChange={handleChangeSwitch}
                            control={
                                <Switch
                                    sx={{
                                        color: "white",
                                    }}
                                />
                            }
                            label={switchItem.label}
                        />
                    );
                })}
            </Grid>
            <Grid item xs={12}>
                <FormControl component="fieldset" >
                    <FormLabel component="legend" sx={{ fontWeight: 900, color: "white", width: "100%" }}>
                        Personalize Options:
                    </FormLabel>
                    <RadioGroup
                        aria-label="Personalize Options"
                        value={dataState?.option}
                        name={"option"}
                        onChange={handleChangeRadio}
                    >
                        {options.map((item: any, index: number) => (
                            <FormControlLabel
                                key={index}
                                sx={{ color: "white", fontWeight: 600 }}
                                value={item.value}
                                control={
                                    <Radio
                                        size="small"
                                        sx={{
                                            pt: 0,
                                            pb: 0,
                                            color: "white",
                                        }}
                                    />
                                }
                                label={item.label}
                            />
                        ))}
                    </RadioGroup>
                </FormControl>
            </Grid>
        </Grid>
    )
}
// const areEqual = (prevProps: Props, nextProps: Props) => {
//     // render when return false => any prevProps different nextProps => return true => !return true => false => pass condition
//     return !(!isEqual(prevProps?.data, nextProps.data))
// }

export default PersonalizationSetting