import React, { useCallback, useEffect, useRef, useState } from 'react'
import { Card, CardMedia, Divider, FormControlLabel, Grid, Paper, Switch, Typography } from '@mui/material'
import { useDispatch } from 'react-redux'
import { TreeSelect } from 'antd'
import isEqual from "lodash/isEqual"
import cloneDeep from "lodash/cloneDeep"

import { OptionPersonalizationImage } from 'store/template-reducer/enum'
import { getTreeClipArtCategories } from 'components/edit-sidebar/service'
import GeneralItemClipArt from 'components/common/GeneralItemClipArt'
import ItemChecked from "components/common/item-checked";
import InputCustom from 'components/common/InputCustom'
import { debounce } from 'utils'

import { ClipartSetting } from 'store/template-reducer/interface'
import { closeReposition, setReposition } from 'store/re-position-reducer/RePositionSlice'
import { RePosition } from 'store/re-position-reducer/interface'
import { useAppSelector } from 'hooks'
import { styled } from '@mui/system'

import styles from "./styles.module.scss"

type Props = {
    data: ClipartSetting,
    onChangeAttr: (data) => void
}
const initClipartSetting: ClipartSetting = {
    title: "",
    defaultOption: null,
    clipArtCategory: null,
    rePosition: null,
    require: false,
    showCustomization: false
}

const ClipartCategory = (props: Props) => {
    const { data, onChangeAttr } = props;
    const [dataState, setDataState] = useState<ClipartSetting>(initClipartSetting);
    const [dataTreeCategories, setDataTreeCategories] = useState<any>(null);
    const [isShowDefaultOption, setIsShowDefaultOption] = useState<boolean>(false)
    const { data: daRepositionSlice, isReposition } = useAppSelector((state) => state.rePosition)

    const dispatch = useDispatch()
    const handleChangeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        const name = event.target.name;
        const value = event.target.value;
        if (name === "limitCharacter") {
            // debounceCallChange({ ...dataState, [name]: value })
            return setDataState((prev) => ({ ...prev, [name]: isNaN(parseInt(value)) ? null : parseInt(value) }))
        }
        // debounceCallChange({ ...dataState, [name]: value })
        return setDataState((prev) => ({ ...prev, [name]: value }))
    };


    const handleChangeSelectCategory = (value: number, option: any) => {
        if (isReposition) {
            dispatch(closeReposition());
        }
        setDataState((prev) => ({ ...prev, rePosition: null, clipArtCategory: option, defaultOption: null }))
    };
    const handleClearCategory = () => {
        if (isReposition) {
            dispatch(closeReposition());
        }
        // debounceCallChange({ ...dataState, rePosition: null, clipArtCategory: null })
        setDataState((prev) => ({ ...prev, rePosition: null, clipArtCategory: null, defaultOption: null }))
    };
    const handleChangeSwitch = (event: React.ChangeEvent<HTMLInputElement>) => {
        const name = event.target.name;
        const checked = event.target.checked;
        // debounceCallChange({ ...dataState, [name]: checked })
        return setDataState((prev) => ({ ...prev, [name]: checked }))
    };
    const handleSetDefaultValue = (item) => {
        // debounceCallChange({ ...dataState, defaultOption: item })
        setDataState(prev => ({ ...prev, defaultOption: item }))
    }
    const handleReposition = (key: any, data: any) => {
        let newItem: RePosition = {
            rotation: 0,
            x: 0,
            y: 0,
            width: 0,
            height: 0,
            clipartItem: data
        }

        if (dataState.rePosition) {
            Object.keys(newItem).forEach(keys => {
                if (dataState.rePosition[0][keys]) {
                    newItem[keys] = dataState.rePosition[0][keys]
                }
            });
        }
        if (key === "reposition") {
            dispatch(setReposition({ isReposition: true, data: newItem }))
        } else if (key === "done") {
            console.log(daRepositionSlice)
            const cloneData = cloneDeep(daRepositionSlice)
            console.log(cloneData)
            newItem = {
                ...newItem,
                clipartItem: data,
                x: cloneData.x,
                y: cloneData.y,
                width: cloneData.width,
                height: cloneData.height,
                rotation: cloneData.rotation
            }

            dispatch(closeReposition())
            setDataState(prev => ({
                ...prev, rePosition: [newItem]
            }))
        }
        // debounceCallChange({ ...dataState, rePosition: [newItem] })

    }
    const debounceCallChange = useCallback(debounce((dataChange) => onChangeAttr(dataChange), 500), []);

    const isFirstSetState = useRef<boolean>(false)
    useEffect(() => {
        getTreeClipArtCategories().then((res) => {
            if (!!res) {
                setDataTreeCategories(res);
            }
        });
        if (!!data) {
            setDataState({ ...data })
        }
        isFirstSetState.current = true
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        if (isFirstSetState.current) {
            debounceCallChange({ ...dataState })
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [dataState])

    return (
        <Grid container rowGap={2} columnSpacing={2} sx={{ px: 2 }}>
            <Grid item xs={12} >
                <InputCustom
                    onChange={handleChangeInput}
                    label={"Option Title:"}
                    name="title"
                    id="option-title"
                    value={dataState.title}
                    colLabel={12}
                />
            </Grid>
            <Grid item xs={12}>
                <TypographyCustom> Select one clipart category:</TypographyCustom>
                {(
                    <TreeSelect
                        // treeDataSimpleMode
                        showSearch
                        style={{ width: "100%", borderRadius: 5 }}
                        value={dataState.clipArtCategory?.id}
                        dropdownStyle={{ maxHeight: 400, overflow: "auto" }}
                        placeholder="Please select"
                        allowClear
                        onClear={handleClearCategory}
                        size={"large"}
                        // onChange={handleChangeTree}
                        onSelect={handleChangeSelectCategory}
                        treeData={dataTreeCategories || []}
                        fieldNames={{ label: "name", value: "id", children: "children" }}
                    />
                )}
            </Grid>

            {!!dataState.clipArtCategory && (
                <GeneralItemClipArt
                    data={dataState.clipArtCategory}
                    type={OptionPersonalizationImage.clipart}
                    onClickAction={(key, data) => {
                        return handleReposition(key, data);
                    }}
                />
            )}

            <Grid item xs={12}>
                <TypographyCustom>Clipart category’s default value:
                </TypographyCustom>
                {

                    dataState?.defaultOption?.id &&
                    <Grid container alignItems={"center"} xs="auto" justifyContent={"space-between"}  >
                        <div style={{ color: "white" }}>{dataState?.defaultOption?.name}</div>
                        <Paper sx={{ width: 50 }}>
                            <CardMedia
                                sx={{ objectFit: "contain" }}
                                component="img"
                                height="50"
                                image={dataState?.defaultOption?.thumbnail_url || dataState?.defaultOption?.url}
                            />
                        </Paper>
                    </Grid>
                }
                <Card sx={{ background: "#343a40", position: "relative", minHeight: "40px", marginTop: "8px" }}>
                    <Grid item xs={12} sx={{ py: 2, px: 1 }}>
                        {
                            !isShowDefaultOption &&
                            <Grid container justifyContent={"center"}>
                                <Grid item className={styles.btnShowDefaultBgGray} onClick={() => setIsShowDefaultOption(true)}>Show option to select default value</Grid>
                            </Grid>
                        }
                        {
                            isShowDefaultOption && <Grid container spacing={0.5}>
                                {dataState.clipArtCategory?.items.length > 0 ?
                                    <ItemChecked
                                        data={dataState.clipArtCategory?.items}
                                        id={`selected_default_option_clipart_${dataState.clipArtCategory?.id}`}
                                        key_url={'url'}
                                        isShowHover={true}
                                        value={dataState?.defaultOption?.id}
                                        onChange={(idChecked, item) => handleSetDefaultValue(item)}
                                    />
                                    : <Typography color={"gray"} component={"span"}>&nbsp;Select item before set default value
                                    </Typography>}
                            </Grid>
                        }
                        {
                            dataState.defaultOption?.id &&
                            <Grid container justifyContent={"center"} sx={{ mt: 2 }}>
                                <Grid item className={styles.btnShowDefaultBgGray} onClick={() => setDataState((prev) => ({ ...prev, defaultOption: null }))}>Clear default value</Grid>
                            </Grid>
                        }

                    </Grid>
                </Card>
            </Grid >
            <Grid item xs={12} sx={{ py: 2 }}> <Divider variant="middle" color="white" /></Grid>
            <Grid item xs={12}>
                <FormControlLabel
                    sx={{ color: "white" }}
                    value={"require"}
                    checked={dataState.require}
                    name={"require"}
                    key={"require"}
                    onChange={handleChangeSwitch}
                    control={
                        <Switch
                            sx={{
                                color: "white",
                            }}
                        />
                    }
                    label={"Mark this option as required"}
                />
            </Grid>
            <Grid item xs={12}>
                <FormControlLabel
                    sx={{ color: "white" }}
                    value={"showCustomization"}
                    checked={dataState.showCustomization}
                    name={"showCustomization"}
                    key={"showCustomization"}
                    onChange={handleChangeSwitch}
                    control={
                        <Switch
                            sx={{
                                color: "white",
                            }}
                        />
                    }
                    label={"Show customization data on cart/checkout"}
                />
            </Grid>
        </Grid >
    )
}
const areEqual = (prevProps: Props, nextProps: Props) => {
    // render when return false => any prevProps different nextProps => return true => !return true => false => pass condition
    return !(!isEqual(prevProps?.data, nextProps.data))
}
export default React.memo(ClipartCategory, areEqual)
const TypographyCustom = styled(Typography)({
    "&.MuiTypography-root": {
        color: "#fff",
        fontWeight: "bold",
    },
});
