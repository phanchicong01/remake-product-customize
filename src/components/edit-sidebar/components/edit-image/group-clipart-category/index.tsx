import { Card, CardMedia, Divider, FormControlLabel, Grid, InputBase, MenuItem, Paper, Select, SelectChangeEvent, Switch, Typography } from '@mui/material'
import InputCustom from 'components/common/InputCustom'
import React, { useCallback, useEffect, useRef, useState } from 'react'
import { ClipArtCategory, GroupClipArtSetting } from 'store/template-reducer/interface'
import isEqual from "lodash/isEqual"
import { debounce } from 'utils'
import { styled } from '@mui/system'
import { TreeSelect } from 'antd'
import { getTreeClipArtCategories } from 'components/edit-sidebar/service'
import GeneralItemClipArt from 'components/common/GeneralItemClipArt'
import { OptionPersonalizationImage } from 'store/template-reducer/enum'
import { RePosition } from 'store/re-position-reducer/interface'

import ItemChecked from 'components/common/item-checked'
import { useAppDispatch, useAppSelector } from 'hooks'
import { closeReposition, setReposition } from 'store/re-position-reducer/RePositionSlice'
import cloneDeep from "lodash/cloneDeep"

import styles from "./styles.module.scss"

type Props = {
    data: GroupClipArtSetting,
    onChangeAttr: (data) => void
}
const initClipartSetting: GroupClipArtSetting = {
    title: "",
    defaultOption: null,
    defaultOptionParent: null,
    clipArtCategory: null,
    rePosition: [],
    require: false,
    showCustomization: false,
    placeholder: ""
}

const GroupClipartCategory = (props: Props) => {
    const { data, onChangeAttr } = props;
    const [dataState, setDataState] = useState<GroupClipArtSetting>(initClipartSetting);
    const [dataTreeCategories, setDataTreeCategories] = useState<any>(null);

    const [isShowDefaultOption, setIsShowDefaultOption] = useState<boolean>(false)
    const { data: daRepositionSlice, isReposition } = useAppSelector((state) => state.rePosition)

    const dispatch = useAppDispatch()
    const handleChangeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        const name = event.target.name;
        const value = event.target.value;
        if (name === "limitCharacter") {
            return setDataState((prev) => ({ ...prev, [name]: isNaN(parseInt(value)) ? null : parseInt(value) }))
        }
        return setDataState((prev) => ({ ...prev, [name]: value }))
    };


    const handleChangeSelectCategory = (value: number, option: any) => {
        if (isReposition) {
            dispatch(closeReposition());
        }
        setDataState((prev) => ({ ...prev, rePosition: null, clipArtCategory: option, defaultOption: null, defaultOptionParent: null }))
    };
    const handleClearCategory = () => {
        if (isReposition) {
            dispatch(closeReposition());
        }
        setDataState((prev) => ({ ...prev, rePosition: null, clipArtCategory: null, defaultOption: null, defaultOptionParent: null }))
    };
    const handleChangeSwitch = (event: React.ChangeEvent<HTMLInputElement>) => {
        const name = event.target.name;
        const checked = event.target.checked;
        return setDataState((prev) => ({ ...prev, [name]: checked }))
    };


    const isFirstSetState = useRef<boolean>(false)

    useEffect(() => {
        getTreeClipArtCategories().then((res) => {
            if (!!res) {
                setDataTreeCategories(res);
            }
        });

        if (!!data) {
            setDataState({ ...data })

        }
        isFirstSetState.current = true

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    const debounceCallChange = debounce(useCallback((dataChange) => onChangeAttr(dataChange), [onChangeAttr]), 500)

    useEffect(() => {
        if (isFirstSetState.current) {
            debounceCallChange({ ...dataState })
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [dataState])
    const handleChangeSelectDefaultOption = (event: SelectChangeEvent<typeof dataState.defaultOptionParent.id>) => {
        const {
            target: { value },
        } = event;
        const dataClipartSelected = dataState.clipArtCategory
        const findItem = dataClipartSelected?.children.find((clipart: ClipArtCategory) => `${clipart.id}` === value);
        if (!!findItem) {
            setDataState((prev) => ({ ...prev, defaultOptionParent: findItem, defaultOption: null }))
        }
    }
    const handleSetDefaultValue = (item) => {
        setDataState(prev => ({ ...prev, defaultOption: item }))
    }
    const handleReposition = (key: any, data: any) => {

        const newReposition = cloneDeep(dataState.rePosition) || [];
        const index = newReposition.findIndex((item: RePosition) => item.clipartItem?.id === data.id)
        let newItem: RePosition = {
            rotation: 0,
            x: 0,
            y: 0,
            width: 0,
            height: 0,
            clipartItem: data
        }
        if (index !== -1) {
            newItem = newReposition[index]
        }

        if (key === "reposition") {
            return dispatch(setReposition({ isReposition: true, data: newItem }))
        }
        if (key === "done" && !!daRepositionSlice) {
            const cloneData = cloneDeep(daRepositionSlice)
            newItem = {
                ...newItem,
                clipartItem: data,
                x: cloneData.x,
                y: cloneData.y,
                width: cloneData.width,
                height: cloneData.height,
                rotation: cloneData.rotation
            }
            if (index !== -1) {
                newReposition[index] = newItem
            } else {
                newReposition.push(newItem)
            }
        }

        if (isReposition) {
            dispatch(closeReposition())
            setDataState(prev => ({
                ...prev, rePosition: [...newReposition]
            }))
        }

    }

    return (
        <Grid container rowGap={2} columnSpacing={2} sx={{ px: 2 }}>
            <Grid item xs={12} >
                <InputCustom
                    onChange={handleChangeInput}
                    label={"Option Title:"}
                    name="title"
                    id="option-title"
                    value={dataState.title}
                    colLabel={12}
                />
            </Grid>
            <Grid item xs={12}>
                <TypographyCustom> Select one clipart category:</TypographyCustom>
                {(
                    dataTreeCategories ?
                        <TreeSelect
                            // treeDataSimpleMode
                            showSearch
                            style={{ width: "100%", borderRadius: 5 }}
                            value={dataState.clipArtCategory?.id}
                            dropdownStyle={{ maxHeight: 400, overflow: "auto" }}
                            placeholder="Please select"
                            allowClear
                            onClear={handleClearCategory}
                            size={"large"}
                            // onChange={handleChangeTree}

                            onSelect={handleChangeSelectCategory}
                            treeData={dataTreeCategories || []}
                            fieldNames={{ label: "name", value: "id", children: "children" }}
                        />
                        :
                        <div className={styles.loadingTreeCategories}>Loading ...</div>
                )}
            </Grid>

            {!!dataState.clipArtCategory && (
                <GeneralItemClipArt
                    data={dataState.clipArtCategory}
                    type={OptionPersonalizationImage.groupClipart}
                    onClickAction={(key, data) => {
                        console.log('reposition')
                        if (key === "reposition" && isReposition) {
                            dispatch(closeReposition())
                            const timeOut = setTimeout(() => {
                                handleReposition(key, data)
                                clearTimeout(timeOut)
                            }, 100)
                            return;
                        }
                        return handleReposition(key, data);
                    }}
                />
            )}

            <Grid item xs={12}>
                <TypographyCustom>Clipart category’s default value:
                </TypographyCustom>
                {
                    dataState?.defaultOption?.id &&
                    <Grid container alignItems={"center"} xs="auto" justifyContent={"space-between"}  >
                        <div style={{ color: "white" }}>{dataState?.defaultOption?.name}</div>
                        <Paper sx={{ width: 50 }}>
                            <CardMedia
                                sx={{ objectFit: "contain" }}
                                component="img"
                                height="50"
                                image={dataState?.defaultOption?.thumbnail_url || dataState?.defaultOption?.url}
                            />
                        </Paper>
                    </Grid>
                }
                <Card sx={{ background: "#343a40", position: "relative", minHeight: "40px", marginTop: "16px" }}>
                    <Grid item xs={12} sx={{ py: 2, px: 1 }}>
                        {
                            !isShowDefaultOption &&
                            <Grid container justifyContent={"center"}>
                                <Grid item className={styles.btnShowDefaultBgGray} onClick={() => setIsShowDefaultOption(true)}>Show option to select default value</Grid>
                            </Grid>
                        }
                        {
                            isShowDefaultOption &&
                            <Grid container spacing={0.5}>
                                <Select
                                    labelId="select-default-option-group-clipart-label"
                                    id="select-default-option-group-clipart"
                                    value={dataState.defaultOptionParent?.id}
                                    placeholder="Select an option"
                                    fullWidth
                                    sx={{ paddingBottom: 1 }}
                                    onChange={handleChangeSelectDefaultOption}
                                    input={<CustomInput size={"small"} />}
                                >
                                    {
                                        (!dataState.clipArtCategory || dataState.clipArtCategory?.children?.length === 0) && <MenuItem disabled value="empty">
                                            List empty
                                        </MenuItem>
                                    }
                                    {dataState.clipArtCategory?.children.map((item: ClipArtCategory) => {
                                        return (
                                            <MenuItem key={item.id} value={`${item.id}`}>
                                                {item?.name}
                                            </MenuItem>
                                        );
                                    })}
                                </Select>
                                {
                                    dataState.defaultOptionParent?.items.length > 0 &&
                                    <ItemChecked
                                        data={dataState.defaultOptionParent?.items}
                                        id={`selected_default_option_clipart_${dataState.clipArtCategory?.id}`}
                                        key_url={'url'}
                                        isShowHover={true}
                                        value={dataState?.defaultOption?.id}
                                        onChange={(idChecked, item) => handleSetDefaultValue(item)}
                                    />
                                }{
                                    dataState.defaultOptionParent?.items.length === 0 &&
                                    <Typography color={"gray"} component={"span"} >
                                        Empty items
                                    </Typography>
                                }
                                {
                                    dataState.defaultOption?.id &&
                                    <Grid container justifyContent={"center"} sx={{ mt: 2 }}>
                                        <Grid item className={styles.btnShowDefaultBgGray} onClick={() => setDataState((prev) => ({ ...prev, defaultOption: null }))}>Clear default value</Grid>
                                    </Grid>
                                }
                                {
                                    !dataState.clipArtCategory &&
                                    <Typography color={"gray"} component={"span"} pt={1}>
                                        Set value to grouped clipart to set default value
                                    </Typography>
                                }
                            </Grid>
                        }

                    </Grid>
                </Card>
            </Grid >
            <Grid item xs={12} >
                <InputCustom
                    onChange={handleChangeInput}
                    label={"placeholder text:"}
                    name="placeholder"
                    placeholder={"Choose Something"}
                    id="placeholder-text"
                    value={dataState.placeholder}
                    colLabel={12}
                />
            </Grid>
            <Grid item xs={12} sx={{ py: 2 }}> <Divider variant="middle" color="white" /></Grid>
            <Grid item xs={12}>
                <FormControlLabel
                    sx={{ color: "white" }}
                    value={"require"}
                    checked={dataState.require}
                    name={"require"}
                    key={"require"}
                    onChange={handleChangeSwitch}
                    control={
                        <Switch
                            sx={{
                                color: "white",
                            }}
                        />
                    }
                    label={"Mark this option as required"}
                />
            </Grid>
            <Grid item xs={12}>
                <FormControlLabel
                    sx={{ color: "white" }}
                    value={"showCustomization"}
                    checked={dataState.showCustomization}
                    name={"showCustomization"}
                    key={"showCustomization"}
                    onChange={handleChangeSwitch}
                    control={
                        <Switch
                            sx={{
                                color: "white",
                            }}
                        />
                    }
                    label={"Show customization data on cart/checkout"}
                />
            </Grid>
        </Grid >
    )
}
const areEqual = (prevProps: Props, nextProps: Props) => {
    // render when return false => any prevProps different nextProps => return true => !return true => false => pass condition
    return !(!isEqual(prevProps?.data, nextProps.data))
}
export default React.memo(GroupClipartCategory, areEqual)
const TypographyCustom = styled(Typography)({
    "&.MuiTypography-root": {
        color: "#fff",
        fontWeight: "bold",
    },
});

const CustomInput = styled(InputBase)(({ theme }) => ({
    "label + &": {
        marginTop: "1.5rem",
        color: "#fff",
    },
    "& .MuiInputBase-input": {
        borderRadius: 4,
        position: "relative",
        backgroundColor: "#fff",
        border: "1px solid #ced4da",
        fontSize: 16,
        padding: "8px 26px 8px 12px",
        // Use the system font instead of the default Roboto font.
        "&:focus": {
            borderRadius: 4,
            borderColor: "#80bdff",
            boxShadow: "0 0 0 0.2rem rgba(0,123,255,.25)",
        },
    },
}));