
import React, { useEffect, useState } from "react";

import ArrowLeftIcon from '@mui/icons-material/ArrowLeft';

import Collapse from '@mui/material/Collapse';

import styles from "./styles.module.scss"

type Props = {
  title: string | React.ReactNode;
  icon?: any;
  children?: React.ReactNode,
  isOpen?: boolean
}

const CollapseComponent = (props: Props) => {
  const [open, setOpen] = useState<boolean>(false);
  const [firstOpen, setFirstOpen] = useState(false);
  useEffect(() => {
    if ((open && !firstOpen)) {
      setFirstOpen(true)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [open, props.isOpen])
  useEffect(() => {
    if (props.isOpen && !firstOpen) {
      setOpen(!open);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.isOpen])

  const handleCollapse = () => {
    setOpen(!open)
  }
  return (
    <>
      <div className={styles.titleCollapse} onClick={handleCollapse}>
        <div className={styles.text}><span className={styles.iconText}>{props.icon}</span>{props.title}</div>
        <ArrowLeftIcon className={`${styles.icon} ${open ? styles.open : styles.collapse}`} />
      </div>
      <Collapse className={`${styles.contentCollapse} ${open ? styles.open : styles.collapse}`} in={open}>
        {firstOpen && <div className={styles.children}>{props.children}</div>}
      </Collapse>
    </>
  )
}

export default CollapseComponent;
