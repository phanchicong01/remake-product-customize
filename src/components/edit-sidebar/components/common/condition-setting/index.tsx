
import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { InputAdornment, ButtonBase, FormControlLabel, Grid, InputBase, InputLabel, Select, SelectChangeEvent, Switch, Typography } from '@mui/material';
import { useSelector } from 'react-redux';
import { RootState } from 'store';
import MenuItem from "@mui/material/MenuItem";
import { ClipArtCategory, ConditionSetting as IConditionSetting, Layer } from 'store/template-reducer/interface';
import { OptionPersonalizationImage } from 'store/template-reducer/enum';
import ItemChecked from 'components/common/item-checked';
import ClearIcon from "@mui/icons-material/Clear"
import styled from '@emotion/styled';

type Props = {
    data: IConditionSetting,
    onChangeAttr: (data: IConditionSetting) => void;
}
const initDataConditionSetting: IConditionSetting = {
    clipartMatchShow: [],
    itemMatchShow: null,
    layerShowOptionWhen: null,
    showLayerFirstLoad: null
}

const ConditionSetting = ({ data, onChangeAttr }: Props) => {
    const { layers, layerSelected } = useSelector((state: RootState) => state.template.templateActive);
    const [clipartsSelected, setClipartSelected] = useState<any[]>([])
    // const [dataLayerSelected, setDataSelected] = useState<Layer>(null)
    const [show, setShow] = useState(false);
    const [open, setOpen] = useState(false);

    useEffect(() => {
        if (!data) {
            onChangeAttr(initDataConditionSetting)
        }
    }, [data, onChangeAttr])

    const layersAllowCondition = useMemo(() => {
        return layers.filter((layer: Layer) => layer.type !== "text" &&
            layer.id !== layerSelected[0].id
            && layer.isActiveConfig
            && !!layer?.configuration?.personalization?.option
            && (layer?.configuration?.personalization?.option === OptionPersonalizationImage.clipart
                || layer?.configuration?.personalization?.option === OptionPersonalizationImage.groupClipart)
        )
    }, [layerSelected, layers])

    const handleGetLayerSelected = useCallback((layerId: any) => {
        const result = layersAllowCondition.find(layer => layer.id === layerId)
        return result;
    }, [layersAllowCondition])

    const handleSelectLayerShow = useCallback((event: SelectChangeEvent<typeof data.layerShowOptionWhen>) => {
        const {
            target: { value },
        } = event;

        const layerSelected = handleGetLayerSelected(value)
        onChangeAttr({ ...data, layerShowOptionWhen: layerSelected, clipartMatchShow: [], itemMatchShow: null });

    }, [data, handleGetLayerSelected, onChangeAttr])



    const handleChangeSwitch = (event: React.ChangeEvent<HTMLInputElement>) => {
        const name = event.target.name;
        const checked = event.target.checked;
        // debounceCallChange({ ...dataState, [name]: checked })
        // return setDataState((prev) => ({ ...prev, [name]: checked }))
        return onChangeAttr({ ...data, [name]: checked })
    };
    const handleCheckedItem = (idChecked: any, item: any) => {
        return onChangeAttr({ ...data, itemMatchShow: item })
    }
    const dataLayerSelected: Layer = useMemo(() => {
        return handleGetLayerSelected(data?.layerShowOptionWhen?.id)
    }, [data?.layerShowOptionWhen?.id, handleGetLayerSelected])

    const handleClearOptionLayer = useCallback(() => {
        if (clipartsSelected) {
            setClipartSelected([])
        }
        onChangeAttr({ ...data, layerShowOptionWhen: null, itemMatchShow: null, clipartMatchShow: null })
    }, [clipartsSelected, data, onChangeAttr])

    const handleSelectMultipleClipart = useCallback((event: SelectChangeEvent<typeof clipartsSelected>) => {
        const {
            target: { value },
        } = event;
        const clipartOfLayerSelect = dataLayerSelected.configuration.groupClipArtSetting.clipArtCategory.children;
        const filterClipart = clipartOfLayerSelect.filter((clipart: any) => {
            return Object.keys(value).some(key => {
                console.log(value[key], clipart.id)
                return value[key] === clipart.id
            })
        }
        )
        onChangeAttr({ ...data, clipartMatchShow: filterClipart })
        setClipartSelected(typeof value === "string" ? value.split(",") : value)
        // const filterClipartsSelected = data.layerShowOptionWhen.configuration.groupClipArtSetting.clipArtCategory.filter(clipArt => value.some(clipArt.id))
    }, [data, dataLayerSelected?.configuration?.groupClipArtSetting?.clipArtCategory.children, onChangeAttr])

    if (!data) {
        return <div className='text-center'>Loading...</div>
    }
    console.log(dataLayerSelected, "dataLayerSelected")
    return (
        <Grid container rowSpacing={2}>
            <Grid item xs={12} px={2}>
                <TypographyCustom sx={{ color: "#ffff", fontWeight: "bold" }} >
                    Show this layer and its option when:
                </TypographyCustom>
                <Select
                    labelId="layer-show-its-option-when"
                    id="layer-show-its-option-when"
                    placeholder="Select an option"
                    fullWidth
                    value={dataLayerSelected?.id || 'placeholder'}
                    renderValue={(selected) => {
                        if (selected === "placeholder") {
                            return <em> Select an option</em>;
                        }

                        return dataLayerSelected?.name
                    }}
                    // onChange={handleSelectLayerShow}
                    label="Select an option"
                    displayEmpty
                    input={<CustomInput size={"small"} />}
                >
                    {
                        !dataLayerSelected && <MenuItem hidden value="placeholder">
                            Select an option
                        </MenuItem>
                    }
                    {
                        (layersAllowCondition?.length === 0) && <MenuItem disabled value="empty">
                            List empty
                        </MenuItem>
                    }
                    {layersAllowCondition?.length > 0 && layersAllowCondition.map((item: Layer) => {
                        console.log(item, "item")
                        return (
                            <MenuItem key={item.id} value={`${item.id}`}>
                                {item.configuration.personalization.option === OptionPersonalizationImage.clipart &&
                                    (item.configuration.clipArtSetting?.title || item.name)
                                }
                                {item.configuration?.personalization.option === OptionPersonalizationImage.groupClipart &&
                                    (item?.configuration?.groupClipArtSetting?.title || item.name)
                                }
                            </MenuItem>
                        );
                    })}

                </Select>
            </Grid>
            {!!dataLayerSelected &&
                <>
                    <Grid item xs={12} sx={{ px: 2, pt: 2 }}>
                        <TypographyCustom>Match:
                        </TypographyCustom>
                        {
                            dataLayerSelected?.configuration?.personalization?.option === OptionPersonalizationImage.clipart &&
                            <Grid container >
                                <ItemChecked
                                    data={dataLayerSelected?.configuration?.clipArtSetting?.clipArtCategory?.items}
                                    id={`selected_default_option_clipart_${dataLayerSelected?.configuration?.clipArtSetting?.clipArtCategory?.id}`}
                                    key_url={'url'}
                                    isShowHover={true}
                                    value={data?.itemMatchShow?.id}
                                    onChange={handleCheckedItem}
                                />
                            </Grid>
                        }
                        {
                            dataLayerSelected?.configuration?.personalization?.option === OptionPersonalizationImage.groupClipart &&
                            <Grid container >
                                <Select
                                    labelId="item-match"
                                    id="item-match"
                                    value={data?.clipartMatchShow.map(item => item.id)}
                                    fullWidth
                                    displayEmpty
                                    multiple
                                    onChange={handleSelectMultipleClipart}
                                    input={<CustomInput size={"small"} />}
                                >
                                    {
                                        dataLayerSelected?.configuration?.groupClipArtSetting?.clipArtCategory?.children?.map((clipart: ClipArtCategory) => {
                                            return <MenuItem value={clipart.id}>{clipart.name}</MenuItem>
                                        })
                                    }
                                </Select>

                            </Grid>
                        }
                        <Grid container >
                            <Grid item xs={12}>
                                <FormControlLabel
                                    sx={{ color: "white" }}
                                    value={"showLayerFirstLoad"}
                                    checked={data?.showLayerFirstLoad}
                                    name={"showLayerFirstLoad"}
                                    key={"showLayerFirstLoad"}
                                    onChange={handleChangeSwitch}
                                    control={
                                        <Switch
                                            sx={{
                                                color: "white",
                                            }}
                                        />
                                    }
                                    label={"Show this layer on first load"}
                                />
                            </Grid>
                        </Grid>
                    </Grid>

                </>
            }


        </Grid >
    )
}

export default ConditionSetting
const CustomInput = styled(InputBase)(({ theme }) => ({
    "label + &": {
        marginTop: "1.5rem",
        color: "#fff",
    },
    "&.MuiInputBase-root": {
        position: "relative",
        "& .btn-clear-icon": {
            position: "absolute",
            right: "1.25em",
            padding: "4px",
            color: "rgba(0, 0, 0, 0.54)",
            borderRadius: '50%',
            fontSize: "1.25rem",

            '&.MuiSvgIcon-root': {

            },
            "&:hover": {
                background: 'rgba(0, 0, 0, 0.04)'
            }
        }
    },
    "& .MuiInputBase-input": {
        borderRadius: 4,
        position: "relative",
        backgroundColor: "#fff",
        border: "1px solid #ced4da",
        fontSize: 16,
        padding: "8px 26px 8px 12px",
        // Use the system font instead of the default Roboto font.
        "&:focus": {
            borderRadius: 4,
            borderColor: "#80bdff",
            boxShadow: "0 0 0 0.2rem rgba(0,123,255,.25)",
        },

    },
})); const TypographyCustom = styled(Typography)({
    "&.MuiTypography-root": {
        color: "#fff",
        fontWeight: "bold",
        margin: "5px 0"
    },

});
