import React, { useEffect, useState } from "react";

import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import isEqual from "lodash/isEqual"

import { useAppSelector, useAppDispatch } from "hooks";

import EditImage from "./components/edit-image";
import EditText from "./components/edit-text";

import { Layer } from "store/template-reducer/interface";
import { updateDrawer } from "store/drawer-menu/drawerSlice";
import { onUpdateAttrLayer } from "store/template-reducer/templateSlice";

import styles from "./styles.module.scss";
import { closeReposition } from "store/re-position-reducer/RePositionSlice";

const EditSidebar = () => {
    const { open, data } = useAppSelector((state) => state.drawerMenu);
    const layerSelected = useAppSelector((state) => state.template.templateActive.layerSelected);

    // const handleDrawerClose = () => {};
    const [dataLayer, setDataLayer] = useState<Layer>(null);

    const dispatch = useAppDispatch();

    useEffect(() => {
        if (open && !!data && !!layerSelected && layerSelected.length > 0) {

            const currentLayerSelected = layerSelected[0];
            if (!!currentLayerSelected && !isEqual(currentLayerSelected, dataLayer)) {
                setDataLayer(currentLayerSelected);
                if (!currentLayerSelected.isActiveConfig) {
                    const newAttrs: Layer = { ...currentLayerSelected, isActiveConfig: true }
                    dispatch(
                        onUpdateAttrLayer({
                            newAttrs: newAttrs,
                        })
                    )
                }

            }
        }
        /* eslint-disable react-hooks/exhaustive-deps */
    }, [open, data, layerSelected]);


    const handleChangeText = ({ name, value }) => {
        console.log({ name, value })

    };


    if (!dataLayer) {
        return (
            <div >
                {" "}
                loading data ...{" "}
            </div>
        );
    }
    return (
        <div className={styles.editSidebarArea}>
            <Grid container style={{ background: "#343a40", color: "#ffff" }} sx={{ py: 2, px: 1 }}>
                <Grid item xs sx={{ position: "relative" }}>
                    <ChevronLeftIcon
                        onClick={() => {
                            dispatch(
                                updateDrawer({
                                    open: false,
                                    data: null,
                                })
                            );
                            dispatch(closeReposition())
                        }}
                        fontSize="small"
                        className={styles.back}
                    />
                    <Typography align={"center"} sx={{ pl: 1, fontWeight: 600, fontSize: 18 }}>
                        {dataLayer?.name}
                    </Typography>
                </Grid>
            </Grid>
            {dataLayer?.type === "text" && (
                <EditText data={dataLayer} onChange={handleChangeText} />
            )}

            {dataLayer?.type === "image" && (
                <EditImage data={dataLayer} onChange={handleChangeText} />
            )}
            { /*
            {stateData?.typeObject === "image" && (
                <EditImage data={stateData} onChange={handleChangeEditImage} dataObj={data} />
            )}
            {stateData?.typeObject === "text" && (
                <EditText data={stateData} dataObj={currentObjectEdit} onChange={handleChangeText} />
            )}
            <Divider sx={{ my: 2, background: "#fff" }} />
            {stateData && (
                <AdvancedOption
                    dataSelect={previewsOptionsOfLayerActive}
                    currentObject={stateData}
                    onChange={handleChangeAdvanced}
                />
            )}
            */}
        </div>
    );
};

export default EditSidebar;
