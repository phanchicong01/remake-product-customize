import React, { useRef, useState, useMemo, useEffect } from "react";
import { Layer } from "react-konva";

import ImageURL from "components/common/ImageURL";
import TextDrag from "components/common/TextDrag";

import Reposition from "components/common/Reposition";
import { Layer as LayerRedux } from "store/template-reducer/interface";
import cloneDeep from "lodash/cloneDeep"
import isEqual from "lodash/isEqual"
import { RePosition } from "store/re-position-reducer/interface";
interface IProps {
    layers: LayerRedux[];
    layerSelected: LayerRedux[];
    onUpdateAttribute: (data: any) => void;
    onSelect: (event: any, data: any) => void;
    onChangeRepo?: (data: any) => void;
    children: any;
    stageScale: {
        x: number;
        y: number;
    };
    reposition?: RePosition;
    isReposition?: boolean
}

const LayerComponent = ({
    layers: state,
    layerSelected,
    onUpdateAttribute,
    onSelect,
    reposition,
    isReposition,
    onChangeRepo,
    children,
    stageScale = { x: 1, y: 1 },

}: IProps) => {
    const refLayer = useRef<any>(null);
    // const [state, setState] = useState<LayerRedux[]>(null);
    const [repositionState, setReposition] = useState<any>(null);

    useEffect(() => {
        // console.log(Array.isArray(layers) && !isEqual(state, layers), layers, "Array.isArray(layers) && !isEqual(state, layers)")
        // if (Array.isArray(layers) && !isEqual(state, layers)) {
        //     const cloneDeepLayers = cloneDeep(layers);
        //     setState([...cloneDeepLayers]);
        // }
        // eslint-disable-next-line react-hooks/exhaustive-deps
        // refLayer.current?.clearBeforeDraw()
    }, [state]);

    useEffect(() => {
        console.log(isReposition, repositionState, reposition, "defaultImageRepo")
        if (isReposition && reposition) {
            const shapeActive = cloneDeep(layerSelected[0]);

            const defaultImageRepo = reposition.clipartItem.items[0] ? reposition.clipartItem.items[0].url : null

            const newShapeReposition = {
                ...shapeActive,
                id: `reposition_${reposition.clipartItem.id}`,
                src: defaultImageRepo,
                x: reposition.x ? reposition.x : shapeActive.x,
                y: reposition.y ? reposition.y : shapeActive.y,
                width: reposition.width ? reposition.width : shapeActive.width,
                height: reposition.height ? reposition.height : shapeActive.height,
                rotation: reposition.rotation ? reposition.rotation : shapeActive.rotation
            }
            setReposition(newShapeReposition);
        }
        if (!isReposition && !reposition) {
            setReposition(null)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isReposition]);
    const handleChangeReposition = (data: any) => {
        if (!!data) {
            setReposition((prev) => ({ ...prev, ...data }))
            onChangeRepo({ ...repositionState, ...data });
        }
    };
    if (!state) return null;
    return (
        <Layer ref={refLayer}>
            {[...state].map((layer: LayerRedux, index: number) => {
                const selected = layerSelected?.some((item) => layer.id === item.id);
                if (layer.type === "image" && !!layer.visible) {
                    return (
                        <React.Fragment key={index}>
                            <ImageURL
                                key={index}
                                stageScale={stageScale}
                                shapeProps={{ ...layer, visible: !(isReposition && selected && !!repositionState) }}
                                isSelected={selected}
                                zIndex={index}
                                onSelect={(event, data) => {
                                    // const newAttrs = { ...object };
                                    return onSelect(event, data);
                                }}
                                onChange={(newAttrs: any) => {
                                    return onUpdateAttribute({ newAttrs });
                                }}
                            />
                            {
                                isReposition && selected && !!repositionState && <Reposition
                                    visible={isReposition}
                                    shapeProps={repositionState}
                                    zIndex={index}
                                    onChange={handleChangeReposition}
                                />
                            }

                        </React.Fragment>
                    )
                }

                if (layer.type === "text" && !!layer.visible) {
                    return (
                        <TextDrag
                            key={index}
                            stageScale={stageScale}
                            textProps={layer}
                            isSelected={selected}
                            zIndex={index}
                            onSelect={(event, data) => {
                                // const newAttrs = { ...layer };
                                return onSelect(event, data);
                            }}
                            onChange={(newAttrs: any) => {
                                return onUpdateAttribute({ newAttrs });
                            }}
                        />
                    );
                }
                return null;
            })}
            {children}
        </Layer>
    );
};
const areEqual = (prevProps: IProps, nextProps: IProps) => {
    // console.log("areEqual-layout-component" , !isEqual(prevProps.layers , nextProps.layers) , {
    //     prevProps:prevProps.layers,
    //     nextProps:nextProps.layers
    // })
    // render when return false => any prevProps different nextProps => return true => !return true => false => pass condition
    return !(!isEqual(prevProps.layers, nextProps.layers)
        || !isEqual(prevProps.layerSelected, nextProps.layerSelected)
        || !isEqual(prevProps.reposition, nextProps.reposition)

    )
}
export default LayerComponent

