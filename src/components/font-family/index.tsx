import { useAppDispatch, useAppSelector } from 'hooks'
import React, { useCallback, useEffect, useMemo } from 'react'
import { Helmet } from "react-helmet";
import { setFontImported } from 'store/fonts-family/fontsFamilySlice';
import WebFont from 'webfontloader';
import _isEmpty from "lodash-es/isEmpty"
type Props = {}

const FontFamilyImport = (props: Props) => {
    const { fontsSelected } = useAppSelector((state) => state.fontsFamily);
    const dispatch = useAppDispatch()
    const listMarkupCustomFont = useMemo(() => {
       if(!_isEmpty(fontsSelected?.customFonts)){
        const listMarkup = fontsSelected.customFonts.map((customFont: any) => {
            let markup = [
                '@font-face {\n',
                '\tfont-family: \'', customFont.name, '\';\n',
                '\tfont-style: \'normal\';\n',
                '\tfont-weight: \'normal\';\n',
                '\tsrc: url(\'', customFont.url, '\');\n',
                '}\n'
            ].join('');
            return markup
        })
        return listMarkup
       }
    }, [fontsSelected.customFonts]);

    const importFontGoogle = useCallback((listGoogleFontSelected) => {
        WebFont.load({
            google: {
                families: listGoogleFontSelected
            },
            fontactive: (familyName: string) => {
                dispatch(setFontImported(familyName))
            },
            timeout: 2000
        })
    }, [dispatch])

    const importFontCustom = useCallback((listCustomFontSelected: any[]) => {

        WebFont.load({
            custom: {
                families: listCustomFontSelected.map(font => font.name)
            },
            fontactive: (familyName: string) => {
                dispatch(setFontImported(familyName))
            },
            timeout: 2000
        })
    }, [dispatch])

    useEffect(() => {
        const { customFonts, googleFonts } = fontsSelected
        if (customFonts.length > 0 && googleFonts.length === 0) {
            importFontCustom(googleFonts);
            return;
        }
        if (customFonts.length === 0 && googleFonts.length > 0) {
            importFontGoogle(googleFonts);
            return;
        }
        if (customFonts.length > 0 && googleFonts.length > 0) {
            // importFontCustom(listCustomFontSelected);

            WebFont.load({
                custom: {
                    families: customFonts.map(font => font.name)
                },
                google: {
                    families: googleFonts
                },
                fontactive: function (familyName: string) {
                    dispatch(setFontImported(familyName))
                },
                timeout: 2000
            })
        }

    }, [dispatch, fontsSelected, importFontCustom, importFontGoogle])


    return (
        <>
            {
                listMarkupCustomFont?.length > 0 && <Helmet>
                    {listMarkupCustomFont.map((font: any) => <style type="text/css">
                        {
                            font
                        }
                    </style>

                    )}
                </Helmet>
            }
            <div></div>
        </>
    )
}

export default FontFamilyImport