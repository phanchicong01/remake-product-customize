import React, { useState, useEffect } from "react";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";


import { roughScale } from "utils";

import styles from "./styles.module.scss";
import MenuComponent from "./menu.component";


interface IProps {
    data: any;
    onChangeAttr: (data) => void;
}
const MenuImage = ({ data, onChangeAttr }: IProps) => {

    const arrayFiledNumber = ['x', 'y', 'fontSize', 'width', 'height']
    const onUpdateData = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = event.target.value;
        const name = event.target.name;
        const type = event.target.type;

        let newValue: any = value;
        if (arrayFiledNumber.indexOf(name) !== -1) {
            newValue = roughScale(value);
        }
        if (name === "scale") {
            newValue = { x: value, y: value };
        }
        const newAttrs = { ...data, [name]: newValue };

        onChangeAttr(newAttrs);
    };

    if (!data) {
        return <div> ...loading </div>;
    }
    return (
        <Grid item xs={12} className={styles.menuText} sx={{ py: 1, px: 2 }}>
            <Grid container rowGap={2}>
                <TextField
                    label="X"
                    id="outlined-x-small"
                    value={data?.x || 0}
                    name="x"
                    style={{ maxWidth: 100 }}
                    sx={{ mr: 1 }}
                    type="number"
                    onChange={onUpdateData}
                    size="small"
                />
                <TextField
                    label="Y"
                    name="y"
                    sx={{ mr: 1 }}
                    id="outlined-y-small"
                    onChange={onUpdateData}
                    style={{ maxWidth: 100 }}
                    type="number"
                    value={data?.y || 0}
                    size="small"
                />
                <TextField
                    label="width"
                    id="outlined-x-small"
                    value={data?.width || 0}
                    name="width"
                    style={{ maxWidth: 100 }}
                    sx={{ mr: 1 }}
                    type="number"
                    onChange={onUpdateData}
                    size="small"
                />
                <TextField
                    label="height"
                    name="height"
                    sx={{ mr: 1 }}
                    id="outlined-y-small"
                    onChange={onUpdateData}
                    style={{ maxWidth: 100 }}
                    type="number"
                    value={data?.height || 0}
                    size="small"
                />
                {/* <TextField
                    label="Scale X"
                    name="scaleX"
                    sx={{ mr: 1 }}
                    id="outlined-y-small"
                    onChange={onUpdateData}
                    style={{ maxWidth: 100 }}
                    type="number"
                    value={data?.fillPatternScaleX || 1}
                    size="small"
                />
                  <TextField
                    label="Scale Y"
                    name="scaleY"
                    sx={{ mr: 1 }}
                    id="outlined-y-small"
                    onChange={onUpdateData}
                    style={{ maxWidth: 100 }}
                    type="number"
                    value={data?.fillPatternScaleY || 1}
                    size="small"
                /> */}
                {/* <MenuComponent data={data} onChangeAttr={onChangeAttr} /> */}
            </Grid>
        </Grid>
    );
};

export default MenuImage;
