import { callApiFuc } from "server/fetcher";

export const getCustomFonts = async (): Promise<any> => {
    const response = await callApiFuc(`/api/fonts`)
        .then((rs) => {
            return rs.data.data;
        })
        .catch((err) => console.error(err));
    return response;
};
