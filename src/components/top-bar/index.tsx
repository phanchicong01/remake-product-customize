import { useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "hooks";


import MenuText from "./menu-text";
import { API_KEY_GOOGLE_FONTS } from "constants/index";
import { listFonts } from "utils";

import axios from "axios";
import { createGoogleFonts, createLocalFonts, createCustomFonts } from "store/fonts-family/fontsFamilySlice";
import { getCustomFonts } from "./service";
import styles from "./styles.module.scss";

const TopBar = () => {
    const layers = useAppSelector((state) => state.template.templateActive.layers);
    const layerSelected = useAppSelector((state) => state.template.templateActive.layerSelected);
    const [currentSelected, setCurrentSelect] = useState<any>();
    const dispatch = useAppDispatch();

    useEffect(() => {
        const indexLayer = layers.findIndex((layer) => layer.id === layerSelected[0].id);

        if (indexLayer !== -1) {
            const objectSelected: string[] = layers[indexLayer].selected;
            if (objectSelected.length === 1) {
                // const obj = layerSelected.objects.filter(i => i.id === )

                const indexObj = layers[indexLayer].objects.findIndex((obj) =>
                    objectSelected.some((objSelect) => objSelect === obj.id)
                );
                if (indexObj !== -1) {
                    return setCurrentSelect(layers[indexLayer].objects[indexObj]);
                }
            }
            return setCurrentSelect(null);
        }
    }, [layers, layerSelected]);

    useEffect(() => {
        axios({
            method: "get",
            url: `https://www.googleapis.com/webfonts/v1/webfonts?key=${API_KEY_GOOGLE_FONTS}`,
            // responseType: "stream",
        }).then(function (response) {
            dispatch(createGoogleFonts(response.data.items));
        });
        getCustomFonts().then((res) => {
            const newRes = res.map((item) => ({
                ...item,
                family: item.name,
                kind: "customfonts#customfontList",
            }));

            dispatch(createCustomFonts(newRes));
        });
        dispatch(createLocalFonts(listFonts()));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleChangeAttrs = (data) => {
        // dispatch(
        //     onUpdateAttrLayer({
        //         newAttrs: data,
        //         idLayer: currentSelected.id,
        //     })
        // );
    };

    return (
        <div className={styles.topBar}>
            {currentSelected?.type === "text" && currentSelected.visible && (
                <MenuText data={currentSelected} onChangeAttr={handleChangeAttrs} />
            )}
            {/* {
                currentSelected?.type === "image" && currentSelected.visible &&
                <MenuImage data={currentSelected} onChangeAttr={handleChangeAttrs}/>
            } */}
        </div>
    );
};

export default TopBar;
