import React, { useState, useEffect } from "react";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";

import Divider from "@mui/material/Divider";

import { roughScale } from "utils";

import styles from "./styles.module.scss";
import MenuFontStyle from "./menu-font-style";
import MenuAlign from "./menu-align";
import { useAppSelector } from "hooks";
import VirtualizedSelect from "./virtualized-select";

interface IProps {
    data: any;
    onChangeAttr: (data) => void;
}
const MenuText = ({ data, onChangeAttr }: IProps) => {
    const [listFontFamily, setFontListFamily] = useState<any[]>([]);
    const [textPreview, setTextPreview] = useState("Preview Text");
    const fontsFamily = useAppSelector((state) => state.fontsFamily);
    useEffect(() => {
        if (fontsFamily.googleFonts || fontsFamily.localFonts) {
            const concatFonts = fontsFamily?.localFonts?.items
                .concat(fontsFamily?.googleFonts?.items)
                .concat(fontsFamily?.customFonts?.items);
            setFontListFamily(concatFonts);
        }
    }, [fontsFamily]);
    useEffect(() => {
        setTextPreview(data?.text);
        // if (data?.text !== textPreview) {
        // }
    }, []);
    const onUpdateData = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = event.target.value;
        const name = event.target.name;

        if (name === "text") {
            const newAttrs = { ...data, [name]: value, width: undefined, height: undefined };
            setTextPreview(value);
            return onChangeAttr(newAttrs);
        }

        let newValue: any = value;
        if (name === "x" || name === "y" || name === "fontSize") {
            newValue = roughScale(value);
        }

        const newAttrs = { ...data, [name]: newValue, width: undefined, height: undefined };
        return onChangeAttr(newAttrs);
    };

    if (!data) {
        return <div> ...loading </div>;
    }

    return (
        <Grid item xs={12} className={styles.menuText} sx={{ py: 1, px: 2 }}>
            <Grid container rowGap={2}>
                <MenuFontStyle data={data} onChangeAttr={onChangeAttr} />
                <Divider orientation="vertical" flexItem />
                {/* <TextField
                    id="outlined-select-font-family"
                    // label="color"
                    value={data.fill}
                    sx={{ mr: 1, p: 0, width: 40 }}
                    InputProps={{
                        classes: {
                            input: styles.inputColor,
                        },
                    }}
                    size="small"
                    onChange={onUpdateData}
                    name="fill"
                    type={"color"}
                /> */}
                {/* <VirtualizedSelect
                    label="Font"
                    value={data.fontFamily}
                    onChange={(value) => onChangeAttr({ ...data, fontFamily: value })}
                    name="fontFamily"
                    data={listFontFamily}
                /> */}

                <TextField
                    label="size"
                    name="fontSize"
                    sx={{ mr: 1 }}
                    id="outlined-font-size-small"
                    onChange={onUpdateData}
                    style={{ maxWidth: 80 }}
                    type="number"
                    value={data?.fontSize || 0}
                    size="small"
                />

                {/* <TextField
                    label="Preview text"
                    name="text"
                    sx={{ mr: 1, width: 120 }}
                    id="outlined-font-text-small"
                    onChange={onUpdateData}
                    value={textPreview || ""}
                    size="small"
                /> */}
                <MenuAlign data={data} onChangeAttr={onChangeAttr} />

                {/* <MenuComponent data={data} onChangeAttr={onChangeAttr} /> */}
            </Grid>
        </Grid>
    );
};

export default MenuText;
