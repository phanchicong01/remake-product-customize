import React, { useEffect, useState } from "react";

import WebFont from "webfontloader";
import { Select } from "antd";

import styles from "./styles.module.scss";

const VirtualizedSelect = ({ data, value, name, label, onChange, ...orderProps }: any) => {
    const [stateData, setStateData] = useState([]);
    const [valueActive, setValueActive] = useState(value);

    const handleChangeFont = (value, option) => {
        // const index = stateData.findIndex((item) => item.family === value);

        // if (index !== -1) {
        //     // const newFontSelected = [...fontsSelected];
        //     // console.log(newFontSelected, "[...Array.from(new Set(newFontSelected))]");
        //     // newFontSelected.push(stateData[index].url);
        //     // dispatch(updateFontsSelected([...Array.from(new Set(newFontSelected))]));

        // }

        if (option.kind === "webfonts#webfontList") {
            WebFont.load({
                google: {
                    families: [value],
                },
            });
        }

        if (option.kind === "customfonts#customfontList") {
            WebFont.load({
                custom: {
                    families: [value],
                    urls: [option.url],
                },
            });
        }

        onChange(value);
        setValueActive(value);
    };
    useEffect(() => {
        if (data) {
            setStateData(
                data.map((item: any) => ({
                    ...item,
                    family: item.family || item.name,
                    url: "https://fonts.googleapis.com/css?family=" + item.family.replace(/ /g, "+"),
                }))
            );
        }
    }, [data]);
    useEffect(() => {
        if (value !== valueActive) {
            setValueActive(value);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [value]);
    return (
        <>
            <Select
                showSearch
                placeholder="Select a person"
                optionFilterProp="children"
                fieldNames={{ label: "family", value: "family" }}
                onChange={handleChangeFont}
                // onSearch={onSearch}
                size={"large"}
                style={{ width: 150, paddingRight: 15 }}
                value={valueActive}
                options={stateData}
            />
            {/* <input
                value={valueAcctive}
                name={name}
                list="fonts"
                className={styles.virtualSelect}
                onChange={handleChangeFont}
            />
            <datalist id="fonts">
                {stateData.map((item) => {
                    return (
                        <div>
                            <option key={item.family} value={item.family}>
                                {item.family}
                            </option>
                        </div>
                    );
                })}
            </datalist> */}
        </>
        // <TextField
        //     id="outlined-select-font-family"
        //     select
        //     label={label}
        //     value={value}
        //     sx={{ mr: 1 }}
        //     size="small"
        //     onChange={onUpdateData}
        //     name={name}
        //     style={{ minWidth: 120 }}
        //     {...orderProps}
        //     onScroll={handleScroll}
        // >
        //     {data.map((option, index: number) => (
        //         <MenuItem key={index} value={option.family} style={{ fontFamily: option.family }}>
        //             {option.family}
        //         </MenuItem>
        //     ))}
        // </TextField>
    );
};

export default VirtualizedSelect;
