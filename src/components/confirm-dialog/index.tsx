import React from 'react'
import styles from './style.module.scss'
interface Props {
    show: boolean,
    onClose: () => any,
    next: () => any,
    message?: string
}

const ConfirmDialog = ({ show, onClose, next, message }: Props) => {


    return <div className={show ? styles.container : styles.hide}>
        <div className={styles.content}>
            <div className={styles.messageText}>
                {message ? message : 'Confirm  Delete?'}
            </div>
            <div className={styles.buttonContainer}>
                <button
                    className='btn btn-danger'
                    onClick={onClose}>
                    Cancel
                </button>
                <button
                    className='btn btn-primary'
                    onClick={next}>
                    Confirm
                </button>
            </div>
        </div>

    </div>

}

export default ConfirmDialog