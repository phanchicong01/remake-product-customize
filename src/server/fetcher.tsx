import axios, { Method } from "axios";

export const callApiFuc = (endpoint: any, method?: Method, body?: any, ...otherProps) => {
    const BASE_URL = process.env.REACT_APP_PUBLIC_URL;

    return axios({
        method: method,
        url: `${BASE_URL}${endpoint}`,
        data: body,
        // validateStatus: function (status) {
        //     return status < 500; // Resolve only if the status code is less than 500
        // },
        ...otherProps,
    });
};


