import axios from "axios";

let flagRun404 = true;

export function setAuthorizationTokenObservable() {
    // const { enqueueSnackbar } = useSnackbar();
    const token = localStorage.getItem("jwtToken") || `zpmtwzbdjboi4bqkd3otr7yz2pq0gb8a`;
    // Axios.defaults.headers.common['Access-Control-Allow-Origin '] = "*";
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
    axios.interceptors.response.use(
        function (response) {
            // Do something with response json1
            return response;
        },
        (error) => {
            // xử lý lỗi 401
            if (error.config && error.response) {
                var status = error.response.status;
                switch (status) {
                    case 401: {
                        window.location.href = `/#/login`;
                        if (flagRun404) {
                            localStorage.clear();

                            flagRun404 = false;
                        }
                        break;
                    }
                    case 400: {
                        // notification['error']({
                        //     message: 'Error!',
                        //     description:
                        //     error.response.data.message
                        // });
                        break;
                    }
                    case 502: {
                        // notification['error']({
                        //     message: 'Error!',
                        //     description:
                        //     error.response.data.message
                        // });
                        break;
                    }
                    case 403: {
                        break;
                    }
                    default: {
                    }
                }
            } else {
                // toastWarning('Server 500 interval')
            }
            if (token) {
                if (status !== 403) {
                    return console.error("error");
                }
            }

            return Promise.reject(error);
        }
    );
}

export function setLanguageAxios(_language) {
    localStorage.setItem("language", _language);
    axios.defaults.headers.common["Accept-Language"] = _language;
}

export function setTokenAxios(jwtToken) {
    flagRun404 = true;
    localStorage.setItem("jwtToken", jwtToken);
    axios.defaults.headers.common["Authorization"] = `Bearer ${jwtToken}`;
}

export function expiredToken() {
    localStorage.setItem("jwtToken", "");
    axios.defaults.headers.common["Authorization"] = `Bearer ${""}`;
}
