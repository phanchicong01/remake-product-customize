import {useQuery} from 'react-query'

import {QUERY_KEYS} from 'constants/fetch-query'

import getPrintAreas from 'apis/get-print-areas';

const usePrintAreas = ( ) => {
  return useQuery(QUERY_KEYS.PRINT_AREAS, ()=> getPrintAreas(), {
    refetchOnWindowFocus: false,
  })
}

export default usePrintAreas
