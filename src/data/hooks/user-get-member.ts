import getAuthMember from 'apis/get-auth-member';
import {useQuery} from 'react-query'

import {QUERY_KEYS} from 'constants/fetch-query'

const useGetMember = () => {
  return useQuery(QUERY_KEYS.MEMBER, getAuthMember, {
    refetchOnWindowFocus: false,
  })
}

export default useGetMember
