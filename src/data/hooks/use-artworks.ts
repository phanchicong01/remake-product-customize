import {useQuery} from 'react-query'

import {QUERY_KEYS} from 'constants/fetch-query'
import { ArtworkListRequest } from 'apis/get-artworks/interface';
import getArtworks from 'apis/get-artworks';

const useArtworks = ( params:ArtworkListRequest , isShow?:boolean) => {
  return useQuery([QUERY_KEYS.ARTWORK, params], ()=> getArtworks(params), {
    refetchOnWindowFocus: false,
    enabled:!!params.category || isShow
  })
}

export default useArtworks
