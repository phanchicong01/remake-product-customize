import { ID } from 'helpers';
import {useQuery} from 'react-query'

import {QUERY_KEYS} from 'constants/fetch-query'

import getCampaignDetail from 'apis/get-campaign-detail';

const useCampaignDetail = ( idCampaign:ID) => {
  return useQuery([QUERY_KEYS.CAMPAIGN_DETAIL, idCampaign], ()=> getCampaignDetail(idCampaign), {
    refetchOnWindowFocus: false,
    enabled:!isNaN(idCampaign)
  })
}

export default useCampaignDetail
