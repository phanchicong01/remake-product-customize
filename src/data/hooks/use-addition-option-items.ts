import { ID } from 'helpers';
import {useQuery} from 'react-query'

import {QUERY_KEYS} from 'constants/fetch-query'

import getAdditionalOptionItems from 'apis/get-additional-option-items'

const useAdditionOptionItems = (additionalId:ID) => {
  return useQuery(QUERY_KEYS.ADDITION_OPTION_ITEMS, ()=> getAdditionalOptionItems(additionalId), {
    refetchOnWindowFocus: false,
  })
}

export default useAdditionOptionItems
