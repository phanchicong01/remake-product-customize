import {useQuery} from 'react-query'

import {QUERY_KEYS} from 'constants/fetch-query'
import getClipartCategories from 'apis/get-clipart-categories'
import { ClipArtCategoryRequest } from 'apis/get-clipart-categories/interface'

const useClipartCategories = (params:ClipArtCategoryRequest) => {
  const {search} = params
  return useQuery([QUERY_KEYS.CLIPART_CATEGORIES , search], ()=>getClipartCategories({search:search}), {
    refetchOnWindowFocus: false,
  })
}

export default useClipartCategories
