import {useQuery} from 'react-query'

import {QUERY_KEYS} from 'constants/fetch-query'

import getMockupDetail from 'apis/get-mockup-detail';

const useMockupDetail = (Id:any) => {
  return useQuery([QUERY_KEYS.MOCKUP_LIST_DETAIL , Id], ()=> getMockupDetail(Id), {
    refetchOnWindowFocus: false,
    enabled:!isNaN(Id)
  })
}

export default useMockupDetail
