import {useQuery} from 'react-query'

import {QUERY_KEYS} from 'constants/fetch-query'
import getArtworkList from 'apis/get-artwork-list'
import { ArtworkListRequest } from 'apis/get-artworks/interface';

const useArtworkList = ( params:ArtworkListRequest) => {
  return useQuery([QUERY_KEYS.ARTWORK_LIST, params], ()=> getArtworkList(params), {
    refetchOnWindowFocus: false,
    enabled:!isNaN(params.category)
  })
}

export default useArtworkList
