import {useQuery} from 'react-query'

import {QUERY_KEYS} from 'constants/fetch-query'

import getProductBases from 'apis/get-product-bases';
import { ProductBaseRequest } from 'apis/get-product-bases/interface';

const useProductBases = ( params:ProductBaseRequest) => {
  return useQuery([QUERY_KEYS.PRODUCT_BASE, params], () => getProductBases(params), {
    refetchOnWindowFocus: false,
  })
}

export default useProductBases
