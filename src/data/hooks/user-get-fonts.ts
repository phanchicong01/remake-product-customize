import {useQuery} from 'react-query'

import {QUERY_KEYS} from 'constants/fetch-query'
import getFonts from 'apis/get-fonts';
import { BaseParamRequest } from 'interfaces/common.interface';



const useFonts = (params?: BaseParamRequest , isUnEnable?:boolean) => useQuery([QUERY_KEYS.FONTS, params], () => getFonts(params), {
  refetchOnWindowFocus: false,
  enabled:!isUnEnable
})
export default useFonts
