import {useQuery} from 'react-query'

import {QUERY_KEYS} from 'constants/fetch-query'

import getDetailArtwork from 'apis/get-detail-artwork';
import _isNumber from 'lodash-es/isNumber';
import _isNaN from "lodash-es/isNaN"
const useDetailArtwork = (artworkId:any) => {
  return useQuery([QUERY_KEYS.ARTWORK_DETAIL , artworkId], ()=> getDetailArtwork(artworkId), {
    refetchOnWindowFocus: false,
    enabled:_isNumber(artworkId) && !_isNaN(artworkId)
  })
}

export default useDetailArtwork
