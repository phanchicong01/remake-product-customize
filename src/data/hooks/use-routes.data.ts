import {useQuery} from 'react-query'

import {QUERY_KEYS} from 'constants/fetch-query'
import getRoutes from 'apis/get-routes/getRoutes'

const useRoutes = () => {
  return useQuery(QUERY_KEYS.MENU, getRoutes, {
    refetchOnWindowFocus: false,
  })
}

export default useRoutes
