import {useQuery} from 'react-query'

import {QUERY_KEYS} from 'constants/fetch-query'

import getMockupList from 'apis/get-mockup-list';
import { MockupListRequest } from 'apis/get-mockup-list/interface';

const useMockupList = ( params:MockupListRequest) => {
  return useQuery([QUERY_KEYS.MOCKUP_LIST, params], () => getMockupList(params), {
    refetchOnWindowFocus: false,
  })
}

export default useMockupList
