import {useQuery} from 'react-query'

import {QUERY_KEYS} from 'constants/fetch-query'
import { GetCampaignRequest } from 'apis/get-campaigns/interface';
import getCampaigns from 'apis/get-campaigns';

const useCampaigns = ( params:GetCampaignRequest) => {
  return useQuery([QUERY_KEYS.CAMPAIGNS, params], ()=> getCampaigns(params), {
    refetchOnWindowFocus: false,
  })
}

export default useCampaigns
