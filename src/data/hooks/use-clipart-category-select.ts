import {useQuery} from 'react-query'

import {QUERY_KEYS} from 'constants/fetch-query'
import getClipartCategories from 'apis/get-clipart-categories'

const useClipartCategorySelect = () => {
  return useQuery([QUERY_KEYS.CLIPART_CATEGORIES_SELECT], ()=>getClipartCategories({search:''}), {
    refetchOnWindowFocus: false,
  })
}

export default useClipartCategorySelect
