import { useQuery } from 'react-query';
import { QUERY_KEYS } from 'constants/fetch-query';
import getListUsers from './../../apis/users/index';
import { BaseParamRequest } from 'interfaces/common.interface';


const useUsersList = (params: BaseParamRequest) => useQuery([QUERY_KEYS.USER, params], () => getListUsers(params), {
    refetchOnWindowFocus: false,
})
export default useUsersList
