import { ID } from 'helpers';
import {useQuery} from 'react-query'

import {QUERY_KEYS} from 'constants/fetch-query'

import getProductBaseDetail from 'apis/get-product-base-detail';

const useProductBaseDetail = ( ID:ID) => {
  return useQuery([QUERY_KEYS.PRODUCT_BASE_DETAIL , ID], () => getProductBaseDetail(ID), {
    refetchOnWindowFocus: false,
  })
}

export default useProductBaseDetail
