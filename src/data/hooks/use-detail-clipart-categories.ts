import { ID } from 'helpers';
import {useQuery} from 'react-query'

import {QUERY_KEYS} from 'constants/fetch-query'
import getDetailClipartCategory from 'apis/get-detail-clipart-categories'

const useDetailClipartCategories = (payload:ID) => {
  return useQuery([QUERY_KEYS.DETAIL_CLIPART_CATEGORIES , payload], ()=>getDetailClipartCategory(payload), {
    refetchOnWindowFocus: false,
    enabled:!isNaN(payload)
  }) 
}

export default useDetailClipartCategories
