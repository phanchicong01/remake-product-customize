import {useQuery} from 'react-query'

import {QUERY_KEYS} from 'constants/fetch-query'

import getAdditionalOptions from 'apis/get-additional-options'
import { AdditionalOptionsRequest } from 'apis/get-additional-options/interface'

const useAdditionOptions = (params:AdditionalOptionsRequest) => {
  const {search} = params
  return useQuery([QUERY_KEYS.ADDITION_OPTION , search], ()=> getAdditionalOptions({
    search
  }), {
    refetchOnWindowFocus: false,
  })
}

export default useAdditionOptions
