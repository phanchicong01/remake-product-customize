import { ID } from 'helpers';
import {useQuery} from 'react-query'

import {QUERY_KEYS} from 'constants/fetch-query'

import getDetailAdditionalOptions from 'apis/get-detail-additional-option'

const useDetailAdditionOptions = (additionalId:ID) => {
  return useQuery([QUERY_KEYS.DETAIL_ADDITION_OPTION , additionalId], ()=> getDetailAdditionalOptions(additionalId), {
    refetchOnWindowFocus: false,
    enabled:!isNaN(additionalId)
  })
}

export default useDetailAdditionOptions
