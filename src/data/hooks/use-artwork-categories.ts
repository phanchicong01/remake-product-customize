import {useQuery} from 'react-query'

import {QUERY_KEYS} from 'constants/fetch-query'
import getArtworkCategories from 'apis/get-artwork-categories'

const useArtworkCategories = (searchValue?:string) => {
  return useQuery([QUERY_KEYS.ARTWORK_CATEGORIES, searchValue], ()=>getArtworkCategories({search:searchValue}), {
    refetchOnWindowFocus: false,
  })
}

export default useArtworkCategories
