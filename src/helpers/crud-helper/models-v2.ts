import { createContext, SetStateAction, Dispatch } from 'react';

import { ID } from 'helpers';
import { FetcherResponse } from 'interfaces/fetcher';

export const initialQueryState ={
  page:1,
  per_page:10,
  search:"",
  sort:""
}

export const initialQueryResponse = {
  refetch: () => {}, 
  isLoading: false,
  isFetched: false,
  totalPage:0,
  query:initialQueryState,
  isRefetching:false,
};

export type QueryResponseContextProps<T> = {
  response?: FetcherResponse<T> | undefined
  refetch: () => void,
  isRefetching?:boolean,
  isLoading: boolean,
  isFetched: boolean,
  totalPage:number
}

export function createResponseContext<T>(initialState: QueryResponseContextProps<T>) {
  return createContext(initialState)
}

export type ListViewContextProps = {
  modalData?: {
    visible:boolean,
    data:ID
  }
  setModalData: Dispatch<SetStateAction<{
    visible:boolean,
    data:ID
  }>>
  disabled?: boolean
}

export const initialListView: ListViewContextProps = {
  setModalData: () => {},
  disabled: false,
}
