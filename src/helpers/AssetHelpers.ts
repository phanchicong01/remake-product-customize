export const toAbsoluteUrl = (pathname: string) => process.env.PUBLIC_URL + pathname;
export const getUrl = (pathName:string) => {
    if(!pathName){
        return null
    }
    if(pathName?.includes(process.env.REACT_APP_PUBLIC_URL) || pathName?.includes("data:image")){
        return  pathName 
    }else{
        return process.env.REACT_APP_PUBLIC_URL+"/storage/"+pathName
    }
}