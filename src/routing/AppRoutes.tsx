/**
 * High level router.
 *
 * Note: It's recommended to compose related routes in internal router
 * components (e.g: `src/app/modules/Auth/pages/AuthPage`, `src/app/BasePage`).
 */

import {FC, lazy} from 'react'
import {Routes, Route, BrowserRouter, Navigate} from 'react-router-dom'
import {PrivateRoutes} from './PrivateRoutes'
import {ErrorsPage} from 'modules/errors/ErrorsPage'
import {Logout, AuthPage, useAuth} from 'modules/auth'
import App from 'App'
import _isEmpty from 'lodash-es/isEmpty'
import MockupCreateCampagin from 'modules/mockup-canva/create-campaign'
import MasterLayout from 'layout/master-layout'
import {MockupCanvaProvider} from 'modules/mockup-canva/core/MockupCanvaProvider'

/**
 * Base URL of the website.
 *
 * @see https://facebook.github.io/create-react-app/docs/using-the-public-folder
 */
const {PUBLIC_URL} = process.env

const AppRoutes: FC = () => {
  const {currentUser} = useAuth()

  const EditMockupPage = lazy(() => import('modules/product-base/edit-mockup'))

  return (
    <BrowserRouter basename={PUBLIC_URL}>
      <Routes>
        <Route element={<App />}>
          <Route path='error/*' element={<ErrorsPage />} />
          <Route path='logout' element={<Logout />} />
          {/* <Route path='/*' element={<PrivateRoutes />} /> */}
          <Route element={<MasterLayout />}>
            <Route
              path='mockup-cava/fe-mockups-canva/create'
              element={
                <MockupCanvaProvider>
                  <MockupCreateCampagin />
                </MockupCanvaProvider>
              }
            />
          </Route>
          <Route path='admin/fe-mockups/edit/:idMockup' element={<EditMockupPage />} />

          {!_isEmpty(currentUser) ? (
            <>
              <Route path='/*' element={<PrivateRoutes />} />
            </>
          ) : (
            <>
              <Route path='auth/*' element={<AuthPage />} />
              <Route path='*' element={<Navigate to='/auth' />} />
            </>
          )}
        </Route>
      </Routes>
    </BrowserRouter>
  )
}

export {AppRoutes}
