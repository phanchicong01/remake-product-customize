import { lazy} from 'react'
import { Route, Routes, Navigate } from 'react-router-dom'
import MasterLayout from 'layout/master-layout'
import MockupCreateCampagin from 'modules/mockup-canva/create-campaign'

const PrivateRoutes = () => {

  const ProfilePage = lazy(() => import('modules/profile/ProfilePage'))
  const AdminPage = lazy(() => import('modules/admin/adminPage'))
  const AssetsPage = lazy(() => import('modules/assets/assetsPage'))
  const CreateArtworkPage = lazy(()=>  import('modules/artworks/create-artwork'))
  const EditMockupPage = lazy(()=>  import('modules/product-base/edit-mockup'))
  const ListArtworkPage = lazy(()=>  import('modules/artworks/list-artwork'))
  const ProductBasePage = lazy(()=>  import('modules/product-base'))
  const CampaignPage = lazy(()=>  import('modules/campaigns'))
  const MockupCanva = lazy(() => import('modules/mockup-canva'))
  return (
    <Routes>
      <Route path='admin/fe-artworks/create' element={<CreateArtworkPage />} />
      <Route path='admin/fe-artworks/edit/:idArtwork' element={<CreateArtworkPage />} />
      <Route path='admin/fe-campaigns/create' element={<div>Campains</div>} />
      <Route path='admin/fe-mockups/edit/:idMockup' element={<EditMockupPage />} />
      
      {/* Path = "/"  => navigate - header {outle} fotter*/ }
      {/* Path = "/artworks"  => navigate - header {outle} fotter*/ }
      {/*  */}
      <Route element={<MasterLayout />}>
        {/* Redirect to Dashboard after success login/registartion */}
        <Route path='auth/*' element={<Navigate to='/' />} />
        {/* Pages */}
        <Route path='/' element={<Navigate to="/artworks?id=all" replace={true} />} />
    
        <Route path='/artworks' element={<ListArtworkPage />} />
        <Route path='/product-base/*' element={<ProductBasePage />} />
        <Route path='/campaigns/*' element={<CampaignPage />} />
        <Route path='/campaigns/*' element={<CampaignPage />} />
        {/* Lazy Modules */}
        {/* <Route
          path='crafted/pages/artworks'
          element={
            <SuspensedView>
              <ArtworksPage />
            </SuspensedView>
          }
        /> */}
        <Route
          path='/assets/*'
          element={
            <AssetsPage />
          }
        />
        <Route
          path='/profile/*'
          element={
            <ProfilePage />
          }
        />
        <Route
          path='/admin/*'
          element={
            <AdminPage />
          }
        />
        <Route
          path='/mockup-canva/*'
          element={
            <MockupCanva />
          }
        />

        {/* Page Not Found */}
        <Route path='*' element={<Navigate to='/error/404' />}  />
      </Route>
    </Routes >
  )
}

// const SuspensedView = (props: any) => {
//   const baseColor = getCSSVariableValue('--bs-primary')
//   TopBarProgress.config({
//     barColors: {
//       '0': baseColor,
//     },
//     barThickness: 1,
//     shadowBlur: 5,
//   })
//   //@ts-ignore
//   // return <Suspense fallback={<TopBarProgress />}>{props.children}</Suspense>
// }

export { PrivateRoutes }
