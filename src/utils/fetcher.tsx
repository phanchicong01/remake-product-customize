import axios, {Method} from 'axios'
import * as authHelper from 'modules/auth/core/AuthHelpers'

const API_URL = process.env.REACT_APP_API_URL

export const instance = axios.create({baseURL: API_URL})
instance.defaults.headers.common['Content-Type'] = 'application/json; charset=utf-8'

// const CancelToken = axios.CancelToken

const axiosInstance = axios.create({
  baseURL: `${API_URL}`,
  timeout: 20000,
  // withCredentials: true,
  headers: {
    'Content-Type': 'application/json; charset=utf-8',
  },
  // cancelToken: new CancelToken(function executor(c) {
  //     // An executor function receives a cancel function as a parameter
  //     cancel = c
  // })
})

axiosInstance.interceptors.response.use(
  (response) => {
    return response
  },
  (error) => {
    if (error.response && error.response.status === 401) {
      // authHelper.removeUser()
      // return authHelper.removeAuth();
    } else if (error.response) {
      return Promise.resolve(error.response)
    }

    return Promise.reject(error)
  }
)

export function setupAxios(token?: any) {
  // if (auth && auth.access_token) {
  //   axiosInstance.defaults.headers.common["Authorization"] = `Bearer ${token || auth.access_token}`
  // }
  axiosInstance.interceptors.request.use(
    (config: {headers: {Authorization: string}}) => {
      const auth = authHelper.getAuth()
      if (auth && auth.access_token) {
        config.headers.Authorization = `Bearer ${token || auth.access_token}`
      }

      return config
    },
    (err: any) => Promise.reject(err)
  )
}

const callApiFuc = (endpoint: any, method?: Method, body?: any, ...otherProps) => {
  return axios({
    method: method,
    url: `${endpoint}`,
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
    },
    data: body,
    ...otherProps,
  })
}

export {callApiFuc, axiosInstance}
