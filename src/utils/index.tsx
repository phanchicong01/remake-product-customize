import imageCompression from "browser-image-compression";

export const listFonts = () => {
    let { fonts }: any = document;
    const it = fonts.entries();

    let arr = [];
    let done = false;

    while (!done) {
        const font = it.next();
        if (!font.done) {
            arr.push(font.value[0].family);
        } else {
            done = font.done;
        }
    }

    // converted to set then arr to filter repetitive values
    return [...Array.from(new Set(arr))].map((font) => ({ family: font, kind: "localfonts#localfontList" }));
};

export function roughScale(x) {
    const parsed = Math.round(x);

    if (isNaN(parsed)) {
        return 0;
    }
    return parsed;
}
export const debounce = (func, timeout = 300) => {
    let timer;
    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => {
            func.apply(this, args);
        }, timeout);
    };
};
const blobToBase64 = blob => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(blob);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
});
const compressedFile = async (file) => {
    let isAllow = true;
    let compressedFile = file
    let compressedFileBase64 = null
    if (file) {
        const options = {
            maxSizeMB: 1,
            maxWidthOrHeight: 1920,
            useWebWorker: true
        }
        try {
            compressedFile = await imageCompression(file, options);
            compressedFileBase64 = await blobToBase64(compressedFile)

            // if (compressedFile.size / 1024 / 1024 > 1) {
            //     isAllow = false;
            // } else {
            //     isAllow = true
            // }
        } catch (error) {
            console.log(error);
        }
    }
    return {
        isAllow,
        compressedFile,
        url: compressedFileBase64
    }
}
