import { useContext } from 'react'
import { ConfirmDialogContext } from 'context/confirm-dialog.context'


const useConfirmDialog = () => {
  const confirmDialogData = useContext(ConfirmDialogContext)

  return confirmDialogData
}

export default useConfirmDialog
