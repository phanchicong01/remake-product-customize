import { useContext } from 'react'

import { ContentPreviewContext } from 'context/content-preview.context'

const useContentPreview = () => {
    const useContentPreviewData = useContext(ContentPreviewContext)

    return useContentPreviewData
}

export default useContentPreview
