FROM node:16.16.0-alpine as ui-builder
LABEL Maintainer="Hieu Tran" \
      Description="docker node"
ARG ENV_PRODUCTION
ENV WORKDIR=/canawan
RUN mkdir -p $WORKDIR
WORKDIR  $WORKDIR
ADD . $WORKDIR
RUN echo "${ENV_PRODUCTION}" > $WORKDIR/.env.staging
RUN  cd $WORKDIR &&  yarn install
EXPOSE 3000
CMD ["yarn","staging"]
